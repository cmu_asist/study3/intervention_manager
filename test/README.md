# Testing Interventions

This module provides resources for testing interventions.


## Requirements

This module requires the following repository:
- [`BaseAgent`](https://gitlab.com/cmu_asist/BaseAgent) (develop branch)

Optionally, make use of the following repositories:
- [`asist_nlp`](https://gitlab.com/cmu_asist/nlp) (version >= 0.2.0)
- [`tom`](https://gitlab.com/cmu_asist/study3/tom) (version >= 3.4.2)


## Running

Test an intervention (e.g. "time_elapsed"):

	python test.py <intervention>

Test with a particular input metadata file:

	python test.py <intervention> -i <input_path>

Test with the ToM and/or NLPManager:

	python test.py <intervention> [--tom] [--nlp]

Test multiple interventions:
	
	python test.py <intervention1> <intervention2> <intervention3> ...

Test all interventions in the InterventionManager library:

	python test.py all

For example:

	python test.py time_elapsed player_idle -i ./trials/trial_420.metadata --tom

For more information, run `python test.py --help`.
