# -*- coding: utf-8 -*-
"""
.. module:: interventions.intervention
    :platform: Linux, Windows, OSX
    :synopsis: Definition of a base Intervention class
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Defines a base Intervention class to encapsulate functionality common to all 
interventions.  Primarily, this is used to maintain the state of the
intervention and automate the interaction between the intervention and the
intervention manager.
"""

import enum
import re
from ..base import BaseComponent

###from collections import namedtuple

class InvalidStateChangeAttemptException(Exception):
    """
    Definition of an exception that relates to an external client attempting
    to change the state of an intervention.  Intervention objects are in
    control of their state, and should not be attempted by external objects.

    Attributes
    ----------
    intervention : Intervention
        The intervention whose state was attempted to be changed
    state : Intervention.State
        The state that was attempted to be set
    """

    def __init__(self, intervention, state):
        """
        Arguments
        ---------
        intervention : Intervention
            The intervention whose state has attempted to been changed
        state : Intervention.State
            The state that the intervention was to be set to
        """

        self.intervention = intervention
        self.state = state

### TODO: Need to be able to elicit machine-readable information from the 
###       intervention.  Probably just use the `getInterventionInformation`,
###       and return a dictionary custom for the intervention.  This should be
###       used to get 1) necessary fields for presentation templates, and 2)
###       allow the _presented_ intervention to be machine readable.

class Intervention(BaseComponent):
    """
    Definition of an abstract intervention encapsulating functionality common
    to all interventions.  In order to work with the Intervention Manager, all
    concrete interventions should subclass this intervention.

    Nested Classes
    --------------
    Time : namedtuple
        A named tuple consisting of `elapsed_milliseconds` (int) and 
        `mission_timer` (tuple of ints)
    State : enum
        Enumeration of states that an Intervention can be in


    Attributes
    ----------
    state : Intervention.State
        Indication of which state the intervention is in
    state_entry_times : dictionary
        Dictionary mapping intervention states (of class Intervention.State) to
        Intervetion.Time values indicating when the intervention entered the
        state. 
    presented : boolean
        Indication of whether the intervention has been presented to the
        participant(s).
    evidence : list
        List of Evidence instances containing information relevant to the 
        decision making process of the intervention.
    relevant_participants : set
        List of IDs of participants that are relevant for this intervention.

    Methods
    -------
    addRelevantParticipant
        Adds a participant to the set of relevant participants
    getInterventionInformation
        Returns a dictionary containing relevant intervention information
    register_callback
        Indicate method to be called when a specific message class if received
    receive
        Callback from MinecraftBridge instance when a message is received
    queueForResolution
        Indicate that this intervention is ready for resolution
    resolve
        Indicate that this intervention has been resolved
    discard
        Indicate that this intervention is to be discarded
    """


    @enum.unique
    class State(enum.Enum):
        """
        An enumeration of possible states that an Intervention can be in.  The
        semantics of each state are defined as

        INITIALIZED:  The intervention has been created, but has not been
                      connected to the internal & external buses.
        ACTIVE:  The intervention is actively receiving information and 
                 updating its internal attributes
        DISCARDED:  The intervention has flagged itself for deletion, and will 
                    no longer update itself based on received information
        QUEUED_FOR_RESOLUTION:  The intervention is queued to be presented or 
                                resolved
        RESOLVED:  The intervention has been presented or resolved, and is 
                   being maintained for follow-up
        """

        INITIALIZED = enum.auto()
        ACTIVE = enum.auto()
        DISCARDED = enum.auto()
        QUEUED_FOR_RESOLUTION = enum.auto()
        VALIDATING = enum.auto()
        RESOLVED = enum.auto()


    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the intervention manager that should control this
            intervention

        Keyword Arguments
        -----------------
        parent : Followup or InterventionTrigger, default=None
            Component which generated this intervention
        relevant_participants : set, default = {}
            Set of participant ids that are relevant to this intervention
        """
        
        BaseComponent.__init__(self)
        
        self._manager = manager

        self._state = Intervention.State.INITIALIZED

        self._parent = kwargs.get("parent", None)

        self._state_entry_times = { s: Intervention.Time(-1, (-1,-1)) 
                                    for s in Intervention.State}

        self.presented = False

        # What was the ultimate intervention message sent, and who was it sent
        # to?
        self._presented_message = None
        self._message_recipients = None

        # Evidence stores what information is relevant to the decision of the
        # intervention
        self._evidence = []
        self._resolution_notes = []

        # Which participants are relevant for this intervention?
        self._relevant_participants = set()


    def __str__(self):
        """
        Provide a string representation of the object.

        Returns
        -------
        string
            The name of the class and current state
        """

        return "%s:%s" % (self.__class__.__name__, self.state.name)


    @property
    def relevant_participants(self):
        return self._relevant_participants
    


    @property
    def manager(self):
        """
        The top-level InterventionManager for this component.

        Returns
        -------
        InterventionManager
            Instance of the intervention manager
        """
        return self._manager


    @property
    def parent(self):
        """
        The component that was responsible for creating this Intervention
        """

        return self._parent



    @property
    def state(self):
        """
        Return the state of the intervention.  Setting the `state` of the 
        intervention externally is not possible, attempting to do so will raise
        an `InvalidStateChangeAttemptException`.

        Returns
        -------
        Intervention.State
            The current enumerated state of the intervention
        """

        return self._state


    @state.setter
    def state(self, _state):
        raise InvalidStateChangeAttemptException(self, _state)


    @property
    def presented_message(self):
        """
        Return the actual message that was presented to the participant(s).  If
        no message was presented, returns None.

        `presented_message` can only be set once, and should be set by the 
        resolution class that presents the message
        """

        return self._presented_message


    @presented_message.setter
    def presented_message(self, message):

        if self._presented_message is not None:
            self.logger.warning("%s:  Attempting to set already existing presented_message for Intervenion", self)
            self.logger.warning("%s:    Intervention message:  %s", self, self._presented_message)
            self.logger.warning("%s:    Attempted new message: %s", self, message)
            return

        self._presented_message = message


    @property
    def message_recipients(self):
        """
        Return the list of recipients that the intervention message was
        presented to.  If the message was not presented, returns None.

        `message_recipients` can only be set one, and should be set by the
        resolution class that presented the message.
        """

        return self._message_recipients


    @message_recipients.setter
    def message_recipients(self, recipients):

        if self._message_recipients is not None:
            self.logger.warning("%s:  Attempting to set already existing message_recipients for Intervention", self)
            self.logger.warning("%s:    Recipient list: %s", self, self._message_recipients)
            self.logger.warning("%s:    Attemted new recipients: %s", self, self._message_recipients)
            return

        self._message_recipients = recipients


    @property
    def state_entry_times(self):
        """
        Get a dictionary of times that this intervention entered into the each
        state.  If the intervention never entered the state, the dictionary 
        contains a defaule value of  -1 for the `elapsed_milliseconds` field
        and (-1,-1) for the `mission_timer` field of the return value.

        Returns
        -------
        Dictionary mapping Intervention.State to Intervention.Time
        """

        return self._state_entry_times


    @property
    def evidence(self):
        return self._evidence
    

    def addEvidence(self, evidence):
        """
        Add a piece of evidence (e.g., a message, ToM state, etc.) relevant to 
        this intervention.  The evidence is stamped with the current elapsed
        milliseconds.

        Arguments
        ---------
        evidence : Evidence
            Evidence to add to this intervention
        """

        # Set the time, if not currently provided, to the closest time from the
        # mission clock
        if evidence.time is None:
            evidence.time = self.manager.mission_clock.elapsed_milliseconds

        self._evidence.append(evidence)


    def addRelevantParticipant(self, participant_id):
        """
        Add a relevant participant to the intervention.  Nominally, the
        participant ID should be the actual participant_id.  This method checks
        for the participant in the manager's participant collection, and will
        issue a warning if the participant isn't found

        Arguments
        ---------
        participant_id : string
            ID of the participant
        """

        participant = self.manager.participants[participant_id]

        if participant is None:
            self.logger.warning("%s:  Attempting to add an unknown participant: %s", self, participant_id)
            return

        self._relevant_participants.add(participant.participant_id)



    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.

        The generated dictionary *must* have the properties listed in the 
        `Required Fields` below.

        TODO:  Identify a set of universal intervention properties, so that the
               returned information is sufficient for whichever type of 
               intervention.

        Return
        -------
        intervention_info : dictionary
            Mapping of relevant intervention properties to needed information

        Required Fields
        ---------------
        default_recipients : list of strings
            List of participant_ids that the intervention should be sent to
        default_message : string
            A (default) text representation of the intervention
        """

        raise NotImplementedError       


    def register_callback(self, message_class, callback):
        """
        Register a callback, indicating the function that should be called when
        a message of the given class is received.  Note that only one callback
        function can be associated with a message class.

        Arguments
        ---------
        message_class : MinecraftBridge.messages
            Message class to register
        callback : Intervention method
            Method to call when a message of message_class is received
        """

        self.messageProcessors[message_class] = callback


    def receive(self, message):
        """
        Receive a message from a MinecraftBridge instance

        Arguments
        ---------
        message : MinecraftBridge.message
            Message received from the bridge
        """

        self.logger.debug("%s:  Received %s message", self, message)

        # Get a processor for the message, if it exists
        processor = self.messageProcessors.get(message.__class__, None)

        # Process the message
        if processor is not None:
            processor(message)
        else:
            self.logger.warning("%s:  No message processor found for %s message.  Ignoring message.", self, message)


    def queueForResolution(self):
        """
        Queue the intervention for a response
        """

        self._state = Intervention.State.QUEUED_FOR_RESOLUTION
        self._manager._queueForResolution(self)


    def resolve(self):
        """
        Indicate that the intervention has been resolved
        """

        self._state = Intervention.State.RESOLVED
        self._manager._resolve(self)


    def discard(self):
        """
        Mark the intervention for discard
        """

        self._state = Intervention.State.DISCARDED
        self._manager._discard(self)


###    @staticmethod
###    def get_player_display_name(playername):
###        """
###        Get a display name for interventions from a full playername.
###        """
###        match = re.search(r'\w+_ASIST1', playername)
###        if match:
###            return playername.split('_')[0].title()
###        else:
###            return playername


    def _setStateEntryTime(self, elapsed_milliseconds, mission_timer):
        """
        Set the time (in terms of mission time) that this intervention has 
        entered its current state.  This value is set by the intervention 
        manager, and should not be called directly by the intervention.

        Arguments
        ---------
        elapsed_milliseconds : int
            Number of milliseconds since mission start
        mission_timer : tuple
            Number of minutes and seconds on the mission timer
        """

        self._state_entry_times[self.state] = Intervention.Time(elapsed_milliseconds, mission_timer)


    def _onSpawn(self):
        """
        Callback from the manager when the Intervention is spawned
        """

        self._state = Intervention.State.ACTIVE
        self._manager._activate(self)


    def _onActivated(self):
        """
        Callback from the manager when the Intervention is activated
        """

        pass


    def _onQueuedForResolution(self):
        """
        Callback from the manager when the Intervention is queued for resolution
        """

        pass


    def _onResolved(self):
        """
        Callback from the manager when the Intervention has been resolved
        """

        pass


    def _onValidating(self):
        """
        Callback from the manager when the Intervention is being validated
        """

        pass

    def _onValidated(self):
        """
        Callback when the Intervention has been validated
        """


    def _onDiscard(self):
        """
        Callback from the manager when the Intervention is discarded
        """

        pass
