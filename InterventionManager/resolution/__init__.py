# -*- coding: utf-8 -*-
"""
.. module:: resolution
   :platform: Linux, Windows, OSX
   :synopsis: Collection of intervention-agnostic resolutions

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>
"""

from .adaptive_chat_resolution import AdaptiveChatPresentation
from .chat_presentation import ChatPresentation
from .mtot_presentation import MToT_Presentation
from .compliance_initiator import ComplianceFollowupInitiator
from .composite_resolution import CompositeResolution
from .followup_initiator import FollowupInitiator

__all__ = ["AdatpiveChatPresentation",
           "ChatPresentation",
           "ComplianceFollowupInitiator",
           "CompositeResolution",
           "FollowupInitiator",
           "MToT_Presentation"
           ]