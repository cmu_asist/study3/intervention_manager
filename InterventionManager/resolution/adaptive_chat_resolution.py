# -*- coding: utf-8 -*-
"""
.. module:: adaptive_chat_resolution
   :platform: Linux, Windows, OSX
   :synopsis: Component for producing chat messages adaptively based on the
              history of messages previously presented to a participant.

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

The AdaptiveChatPresentation class is a resolution designed to generate
different messages based on the number of times an intervention has been 
produced for a participant.
"""


from string import Formatter

from MinecraftBridge.utils import Loggable

from BaseAgent.utils import DependencyInjection

from MinecraftBridge.messages import AgentChatIntervention
from MinecraftBridge.messages import BusHeader, MessageHeader
from MinecraftBridge.mqtt.parsers import MessageType, MessageSubtype

from collections import defaultdict

class AdaptiveChatPresentation(Loggable):
    """
    A resolution which adaptively presents different Chat Interventions based 
    on the history of interventions resolved for a participant.  This resolution
    (currently) requires / assumes the following:

    1.  A list of templated messages as a dictionary, with the fields below:

        * `message_class` - Fully qualified class name the template is for
        * `templates` - list of dictionaries with the following:
            * `message` - Templated message to present to the participant
            * `short_explanation` - Templated explanation of why the 
                                    intervention is presented
            * `long_explanation` - A dictionary of reasons why the explanation 
                                   was produced
            * `recipients` - A list of participants to present the intervention
                             to.

    2.  Intervention instances either provide a `participant` or `participant_id`
        attribute.

    Message templates will be provided thorugh the keyword argument 
    `message_templates`, allowing the intervention manager to load this as part
    of the config dictionary from external files.

    Since the participant_ids for recipients are not know, the strings below
    are used to indicate to whom to send the message.  String labels are case
    insensitive:

        * "PARTICPANT": The target participant identified in the intervention
        * "OTHERS": A team members except the participant identified in the
                    intervention
        * "MEDIC": The participant in the Medical Specialist role
        * "TRANSPORTER": The participant in the Transport Specialist role
        * "ENGINEER": The participant in the Hazardous Material Specialist role
        * "RED": The participant with the callsign "RED"
        * "GREEN": The participant with the callsign "GREEN"
        * "BLUE": The participant with the callsign "BLUE"


    Methods
    -------
    resolve(intervention)
        Generate an AgentChatIntervention message and publish to the bus
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the Intervention Manager that the presenter is
            associated with

        Keyword Arguments
        -----------------
        message_templates : list of dictionaries
            Message template dictionaries consisting of a templated message and
            explanations
        """

        self.manager = manager

        # Formatter for replacing fields in tempaltes with concrete values.  We
        # explicitly use a Formatter instance 
        self._formatter = Formatter()

        self._message_templates = {}

        # Load the message templates into a mapping of 
        for template_info in kwargs.get('message_templates',{}):
            try:
                # Get the message class to use as a key, then populate the
                # message templates dictionary
                _, MessageClass = DependencyInjection.get_module_and_class(template_info['message_class'])
                self._message_templates[MessageClass] = template_info.get("templates", [])
            except Exception as e:
                self.logger.exception("%s:  Exception raised when attempting to add message template: ", self)
                self.logger.exception("%s:    %s", self, e)

        self._message_levels = defaultdict(lambda: 0)



    def str(self):
        """
        String representation of the AdaptiveChatPresentation
        """

        return self.__class__.__name__



    def _get_recipients(self, recipients_key, intervention):
        """
        Converts a recipient key to a list of participant_ids.  recipients_key
        should be one of the following labels:

        * "PARTICPANT": The target participant identified in the intervention
        * "OTHERS": A team members except the participant identified in the
                    intervention
        * "TEAM": All participants in the team
        * "MEDIC": The participant in the Medical Specialist role
        * "TRANSPORTER": The participant in the Transport Specialist role
        * "ENGINEER": The participant in the Hazardous Material Specialist role
        * "RED": The participant with the callsign "RED"
        * "GREEN": The participant with the callsign "GREEN"
        * "BLUE": The participant with the callsign "BLUE"
        * "NONE": Indicate that the message should not be presented

        Arguments
        ---------
        recipients_key : string
            label indicating which participants should receive a message
        intervention : Intervention
            intervention instance

        Returns
        -------
        recipients : list of strings or None
            List of participant ids.  Returns `None` if the recipients_key is
            not valid, or "NONE".  Returning `None` implies that the message
            should not be presented.
        """

        # Convert the provided recipients_key to uppercase
        recipients_key = recipients_key.upper()

        # Create a map of the recipients
        participant_id = self._get_participant_id(intervention)

        # TODO: Double check the string representation of roles and callsigns
        participant_map = { "PARTICIPANT": [],
                            "OTHERS": [],
                            "TEAM": [p.participant_id for p in self.manager.participants],
                            "MEDIC": [self.manager.participants["Medical_Specialist"]],
                            "TRANSPORTER": [self.manager.participants["Transport_Specialist"]],
                            "ENGINEER": [self.manager.participants["Engineering_Specialist"]],
                            "RED": [self.manager.participants["Red"]],
                            "BLUE": [self.manager.participants["Blue"]],
                            "GREEN": [self.manager.participants["Green"]],
                            "NONE": None
        }

        if participant_id is not None:
            participant_map["PARTICIPANT"] = [participant_id]
            for participant in self.manager.participants:
                if participant.participant_id != participant_id:
                    participant_map["OTHERS"].append(participant.participant_id)



        # Make sure that the recipeints_key is a valid label
        if not recipients_key in participant_map:
            self.logger.warning("%s:  Invalid recipients key provided: %s", self, recipients_key)

        return participant_map.get(recipients_key, None)



    def _populate_template(self, template, intervention):
        """
        Fill in the message template with properties provided in the
        intervention.

        Arguments
        ---------
        template : string

        intervention : Intervention
            Intervention being used to populate the template

        Returns
        -------
        message : string or None
            Returns the populated message, or None if the provided template is None
        """

        if template is None:
            return None


        # TODO:  Talk with Ying to identify what property names to use with 
        #        the intervention
        # NOTE:  String Formatter in Python can handle field names such as
        #        "arg_name.attribute_name" or similar.  This implies that we
        #        can simply format the arguments in the templates in terms of
        #        the intervention (e.g., 'intervention.participant_id'), but
        #        we'll need to 


        # Get the field names used in the template
        field_names = [f[1] for f in Formatter().parse(template) if f[1] is not None]

        return template


    def _get_participant_id(self, intervention):
        """
        Helper function to extract the participant_id from an intervention, if
        available.

        Arguments
        ---------
        intervention : Intervention
            Intervention instance to extract participant_id from

        Returns
        -------
        participant_id : string or None
            Returns the id of the participant the intervention is targeting, or
            None if not found
        """

        # Extract the target participant from the intervention, if available.
        # If not, then simply assign None
        participant_id = None

        if hasattr(intervention, 'participant_id'):
            participant_id = intervention.participant_id

        elif hasattr(intervention, 'participant'):
            try:
                participant_id = intervention.participant.participant_id
            except Exception as e:
                self.logger.exception("%s:  Intervention has `participant` attribute mapping to non-Partcipant object %s", self, intervention.participant)

        return participant_id        



    def resolve(self, intervention):
        """
        Determine the message and recipients for the given intervention and
        publish.  Update the count of the intervention being received by the
        participant identified in the intervention by `participant` or 
        `participant_id`

        Arguments
        ---------
        intervention : Intervention
            Intervention instance
        """

        # Get the template from the template library for this intervention.  If
        # no templates are available, issue a warning and do nothing
        templates = self._message_templates.get(intervention.__class__, [])
        if len(templates) == 0:
            self.logger.warning("%s: No templates provided for intervention of class %s", self, intervention.__class__)
            return

        participant_id = self._get_participant_id(intervention)

        # If no participant_id is available, then simply publish the base /
        # "level-0" intervention.  Otherwise, determine the message level to
        # use, using the intervention class and participant ID as a key
        if participant_id is None:
            message_level = 0
        else:
            message_level = self._message_levels[(intervention.__class__, participant_id)]

        template = templates[message_level]

        # Populate the message template, and publish
        message = self._populate_template(template.get("message", None), intervention)
        recipients = self._get_recipients(template.get("recipients", "PARTICIPANT"), intervention)

        # Try to get the dictionary of intervention info from the intervention;
        # raise an exception if it doesn't exist
        try:
            info = intervention.getInterventionInformation()
        except NotImplementedError:
            info = {}
            self.logger.warning("%s: getInterventionInformation not implemented by intervention %s", self, intervention)

        info["intervention_class"] = intervention.__class__.__name__            


        # Store the presented message in the intervention itself
        intervention.presented_message = message
        intervention.message_recipients = recipients

        # If there's something to present, indicate that the intervention was
        # presented and present the message
        if recipients is not None and message is not None:
            intervention.presented = True
            self.manager.publish_chat_intervention(message, recipients,
                                                   intervention_info=info)


        # Increate the message level for this intervention class and
        # participant_id, so that then next time this intervention is received
        # targeting the participant, it uses the next level up
        if participant_id is not None:
            next_message_level = min(message_level + 1, len(templates)-1)
            self._message_levels[(intervention.__class__, participant_id)] = next_message_level
