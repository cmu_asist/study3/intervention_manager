# -*- coding: utf-8 -*-
"""
.. module:: resolution.resolution_with_followup
    :platform: Linux, Windows, OSX
    :synopsis: Abstrafct base-class for all resolutions that trigger a followup
.. moduleauthor:: Simon Stepputtis <sstepput@andrew.cmu.edu>

Definition of a base-class for all resolutions that trigger a followup

Derived classes, instead of reimplementing resolve(), should reimplement resolveWithFollowup() instead
"""

from .resolution import Resolution

class ResolutionWithFollowup(Resolution):
    def __init__(self, manager):
        super(ResolutionWithFollowup, self).__init__(manager)

    def _get_module_and_class(self, path):
        """
        Given a path, returns the module and class.
        This is needed to resolutions to initiate followups
        """
        # Split the package and class from the qualified path
        module_name = '.'.join(path.split('.')[:-1])
        class_name = path.split('.')[-1]

        # Load the module and get the class
        _module = __import__(module_name, fromlist=[class_name])
        _class = getattr(_module, class_name)

        return _module, _class
    
    def resolve(self, intervention):
        """
        General interface for all resolutions, however, when a followup is requested,
        the resolveWithFollowup of the child class is called and a followup is initiated

        Arguments
        ---------
        intervention : Intervention
            Intervention to publish
        """
        self.resolveWithFollowup(intervention)
        self._followup()

    def resolveWithFollowup(self, intervention):
        """
        Method that needs to be overwritten in the child classes in order generate
        the apropriate response for the given intervention

        Arguments
        ---------
        intervention : Intervention
            Intervention to publish
        """
        raise NotImplementedError

    def _followup(self):
        """
        Initiates the configured followup for this particular resolve
        """
        pass