# -*- coding: utf-8 -*-
"""
.. module:: resolution.chat_presentation
    :platform: Linux, Windows, OSX
    :synopsis: Definition of a class to present interventions through chat
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class to present interventions via chat, through a Minecraft
Bridge connection.
"""

from MinecraftBridge.messages import AgentChatIntervention
from MinecraftBridge.messages import BusHeader, MessageHeader
from MinecraftBridge.mqtt.parsers import MessageType, MessageSubtype

from MinecraftBridge.utils import Loggable


class ChatPresentation(Loggable):
    """
    This class is used to present interventions as chat messages in Minecraft.
    Interventions are constructed from intervention information collected from 
    Intervention instances, and pushed to Minecraft through a MinecraftBridge
    as a ChatIntervention message.

    Methods
    -------
    resolve
        Generate an AgentChatIntervention message and publish to the bus
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the Intervention Manager that the presenter is
            associated with
        """

        self.manager = manager

        # Default values for interventions
        self._agent_name = kwargs.get("agent_name", "asi_cmu_ta1_atlas")
        self._duration = kwargs.get("intervention_duration", 10000)
        

    def str(self):
        """
        String representation of the ChatPresentation
        """

        return self.__class__.__name__


    def resolve(self, intervention):
        """
        Create an AgentChatIntervention message and publish to the message bus

        Arguments
        ---------
        intervention : Intervention
            Intervention to publish
        """

        # Try to get the dictionary of intervention info from the intervention;
        # raise an exception if it doesn't exist
        try:
            info = intervention.getInterventionInformation()
        except NotImplementedError:
            info = {}
            self.logger.warning("%s: getInterventionInformation not implemented by intervention %s", self, intervention)

        info["intervention_class"] = intervention.__class__.__name__

        # Get the text message to send from the intervention info.  Try 
        # `default_message` as a key first, then `text_message`, to handle
        # the fact that we're transitioning to `default_message
        text_message = info.get("default_message", 
                                info.get("text_message", None))
        if text_message is None:
            self.logger.warning("%s: No intervention message provided by intervention %s", self, intervention)
            text_message = "<NO MESSAGE>"

        # Create the message and send to the bridge
        start = info.get("start", -1)
        duration = info.get("duration", self._duration)
        explanation = info.get("explanation", {})

        # Get the recipieints -- try `default_recipients` first, then the
        # deprecated `participant_ids`
        receiver = info.get("default_recipients", 
                            info.get("participant_ids", []))

        receiver = [self.manager.participants[name].participant_id for name in receiver]

        # Send a warning if no recipients are provided, otherwise, indicate
        # that the intervention has been presented
        if len(receiver) == 0:
            self.logger.warning("%s: No receiver provided to present intervention %s to", self, intervention)
        else:
            intervention.presented = True

        # Store the presented message in the intervention itself
        intervention.presented_message = text_message
        intervention.message_recipients = receiver

        # Aaand, present the intervention
        self.manager.publish_chat_intervention(text_message, receiver, 
                                               start=start, duration=duration,
                                               explanation=explanation,
                                               intervention_info=info)
