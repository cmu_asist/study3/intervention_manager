# -*- coding: utf-8 -*-
"""
.. module:: resolution.followup_initiator
    :platform: Linux, Windows, OSX
    :synopsis: This resolution triggers a followup
.. moduleauthor:: Simon Stepputtis <sstepput@andrew.cmu.edu>

Resolution to initiate a followup, based on received Intervention class.
"""

from MinecraftBridge.utils import Loggable

from BaseAgent.utils import DependencyInjection


class FollowupInitiator(Loggable):
    """
    The FollowupInitiator class creates a followup for the received intervention
    based on the intervention's class.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the Intervention Manager controlling this resolution

        Keyword Arguments
        -----------------
        followups : list
            List of dictionary entries containing the following fields:

            intervention_class : string
                Fully qualified class name of an Intervention of interest
            followup_class : string
                Fully qualified class name of a Followup for the intervention
            parameters : dictionary
                Parameters to pass to the followup upon creation, passed as
                kwargs
        """

        self.manager = manager

        # Load in the followups parameters
        self._followup_map = {}
        self.__load_followup_map(kwargs.get("followups", []))

        
    
    def resolve(self, intervention):
        """
        Creates the Followup for the given intervention, if available

        Arguments
        ---------
        intervention : Intervention
            Intervention to produce a followup for
        """

        # Can this intervention be handled correctly?
        if not intervention.__class__ in self._followup_map:
            self.logger.warning("%s:  Cannot find Followup class for Intervention class %s", self, intervention.__class__)
            return

        # Was this intervention presented?
        if not intervention.presented:
            self.logger.debug("%s:  Intervention %s was not presented, Followup will not be initiated", self, intervention)
            return

        # Get the followup class and parameters for this intervention's class
        followup_class, parameters = self._followup_map[intervention.__class__]

        # Create the followup, and initiate
        followup = DependencyInjection.create_instance(followup_class, 
                                                       intervention,
                                                       self.manager, 
                                                       **parameters)
        self.manager.initiateFollowup(followup)



    def __load_followup_map(self, followup_configs):
        """
        Helper function to load followup classes
        """

        for config in followup_configs:

            # Extract the class from the config
            intervention_class = config.get("intervention_class", None)

            if intervention_class is not None:
                _, InterventionClass = DependencyInjection.get_module_and_class(intervention_class)
                self._followup_map[InterventionClass] = (config.get("followup_class", None),
                                                         config.get("parameters", {}))




