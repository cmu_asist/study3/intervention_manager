# -*- coding: utf-8 -*-
"""
.. module:: player_idle
   :platform: Linux, Windows, OSX
   :synopsis: Resolution to compose atomic resolutions into a single resolution.

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a simple class to compose atomic resolutions into a single
resolution instance.
"""

from BaseAgent.utils import DependencyInjection
from MinecraftBridge.utils import Loggable

class CompositeResolution(Loggable):
    """
    A composite resolution is used to perform multiple resolutions on an
    intervention in sequence.  
    NOTE:  Assume a single threaded execution, so resolutions that spawn
           a followup (or other intervention) should be performed last.

    Methods
    -------
    resolve(intervention)
        Perform the resolutions in this composite
    """

    def __init__(self, manager, *args, **kwargs):
        """
        Resolutions will be added in the following order:

        1.  Resolutions listed in *args
        2.  Resolutions created through DI, listed in the keyword
            arguments under "resolutions"

        Arguments
        ---------
        args: list of resolutions

        Keyword Arguments
        -----------------
        resolutions : list of dictionaries
            List of resolution class names and parameter lists.
        """

        self.resolutions = list(args)
        self.manager = manager

        for resolution in kwargs.get("resolutions", []):
            self.resolutions.append(DependencyInjection.create_instance(resolution["resolution_class"],
                                                                        self.manager,
                                                                        **resolution.get("parameters", {})))

        self.logger.info("%s:  List of Resoultions:", self)
        for resolution in self.resolutions:
            self.logger.info("%s:    %s", self, resolution)


    def __str__(self):
        """
        String representation of this class
        """

        return self.__class__.__name__


    def resolve(self, intervention):
        """
        Present the intervention to each resolution
        """

        for resolution in self.resolutions:
            resolution.resolve(intervention)
