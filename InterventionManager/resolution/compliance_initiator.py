# -*- coding: utf-8 -*-
"""
.. module:: resolution.compliance_initiator
    :platform: Linux, Windows, OSX
    :synopsis: This resolution triggers a compliance followup for interventions
.. moduleauthor:: Simon Stepputtis <sstepput@andrew.cmu.edu>

Resolution to initiate a compliance followup for each relevant participant
listed in the intervention, and 
"""

from MinecraftBridge.utils import Loggable

from BaseAgent.utils import DependencyInjection


class ComplianceFollowupInitiator(Loggable):
    """
    The FollowupInitiator class creates a followup for the received intervention
    based on the intervention's class.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the Intervention Manager controlling this resolution

        Keyword Arguments
        -----------------
        followups : list
            List of dictionary entries containing the following fields:

            intervention_class : string
                Fully qualified class name of an Intervention of interest
            followup_class : string
                Fully qualified class name of a Followup for tracking compliance
                of each relevant participant in the intervention
            team_followup_class : string or None
                Fully qualified class name of a Followup for calculating team
                compliance
            parameters : dictionary
                Parameters to pass to the followups upon creation, passed as
                kwargs
        """

        self.manager = manager

        # Load in the followups parameters
        self._followup_map = {}
        self.__load_followup_map(kwargs.get("followups", []))

        
    
    def resolve(self, intervention):
        """
        Creates the Followup for the given intervention, if available

        Arguments
        ---------
        intervention : Intervention
            Intervention to produce a followup for
        """

        # Can this intervention be handled correctly?
        if not intervention.__class__ in self._followup_map:
            self.logger.warning("%s:  Cannot find Followup class for Intervention class %s", self, intervention.__class__)
            return

        # Get the followup class and parameters for this intervention's class
        followup_class, team_followup_class, parameters = self._followup_map[intervention.__class__]

        followups = []

        # If the followup class exists, create an instance for each participant
        # and link the three followups    
        if follow_class is not None:
            for participant_id in intervention.relevant_participants:
                # Copy the parameters, so that the target participant can be
                # injected into the parameters
                _parameters = parameters.copy()
                _parameters['target_participant'] = participant_id
                followups.append(DependencyInjection.create_instance(followup_class, 
                                                                     intervention,
                                                                     self.manager, 
                                                                     **_parameters))

            # Link the followups prior to initiating
            for followup, other_followup in [(x,y) for x in followups for y in followups if x is not y]:
                followup.link_followup(other_followup)

        # If a team_followup_class is provided, also create and link that
        if team_followup_class is not None:

            team_followup = DependencyInjection.create_instance(team_followup_class, 
                                                                intervention,
                                                                self.manager, 
                                                                **parameters)

            for followup in followups:
                team_followup.link_followup(followup)
                followup.link_followup(team_followup)

            followups.append(team_followup)

        # Initiate the followups
        for followup in followups:
            self.manager.initiateFollowup(followup)



    def __load_followup_map(self, followup_configs):
        """
        Helper function to load followup classes
        """

        for config in followup_configs:

            # Extract the class from the config
            intervention_class = config.get("intervention_class", None)

            if intervention_class is not None:
                _, InterventionClass = DependencyInjection.get_module_and_class(intervention_class)
                self._followup_map[InterventionClass] = (config.get("followup_class", None),
                                                         config.get("team_followup_class", None),
                                                         config.get("parameters", {}))
