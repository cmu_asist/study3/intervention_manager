# -*- coding: utf-8 -*-
"""
.. module:: resolution.resolution
    :platform: Linux, Windows, OSX
    :synopsis: Abstrafct base-class for all resolutions that do not have a followup
.. moduleauthor:: Simon Stepputtis <sstepput@andrew.cmu.edu>

Definition of a base-class for all resolutions that do not need a followup

Derived classes have to reimplement the resolve() method
"""

from MinecraftBridge.utils import Loggable

class Resolution(Loggable):
    def __init__(self, manager):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the Intervention Manager that the presenter is
            associated with
        """
        super(Resolution, self).__init__()

        # NOTE:  Are these necessary?
        self._bridge = manager.minecraft_interface
        self._manager = manager

    
    def resolve(self, intervention):
        """
        Method that needs to be overwritten in the child classes in order generate
        the apropriate response for the given intervention

        Arguments
        ---------
        intervention : Intervention
            Intervention to publish
        """
        raise NotImplementedError


    def reset(self):
        """
        Reset the resolution to its initial state
        """

        pass