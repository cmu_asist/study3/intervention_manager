# -*- coding: utf-8 -*-
"""
.. module:: resolution.create_followup_resolve
    :platform: Linux, Windows, OSX
    :synopsis: This resolve acts as a dummy that immediately triggers a followup
.. moduleauthor:: Simon Stepputtis <sstepput@andrew.cmu.edu>
"""

from .chat_presentation import ChatPresentation
from .resolution_with_followup import ResolutionWithFollowup

class CreateFollowupResolve(ResolutionWithFollowup):
    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the Intervention Manager that the presenter is
            associated with
        """
        super(CreateFollowupResolve, self).__init__(manager)
        self._target_followups = kwargs.get("followup", "")
        
    
    def resolveWithFollowup(self, intervention):
        """
        Creates a speficied followup 

        Arguments
        ---------
        intervention : Intervention
            Intervention to publish
        """

        # Test is a followup was specified. If not, throw a warning and keep going
        # While this is an issue, it shouldn't be breaking here
        if self._target_followups == "":
            self.logger.warning("%s: Followup not specified by resolve!", self)
            return

        # Instantiate each listed followup...
        for followup in self._target_followups:
            module, followup_class = self._get_module_and_class(followup)
            instance = followup_class(intervention, self._manager)
            self._manager.initiateFollowup(instance)