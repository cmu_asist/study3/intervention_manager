# -*- coding: utf-8 -*-
"""
.. module:: mtot_presentation
   :platform: Linux, Windows, OSX
   :synopsis: Component for producing chat messages based on the current state
              of the agent's theory of teams

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

The MToT_Presentation class uses a Machine Theory of Mind to determine if a
resolved intervention should be presented to the participant(s).  This allows
the agent to determine if advice should be withheld for various reasons.
"""


from string import Formatter

from MinecraftBridge.utils import Loggable

from BaseAgent.utils import DependencyInjection

from MinecraftBridge.messages import AgentChatIntervention, PlanningStageEvent, ScoreboardEvent
from MinecraftBridge.messages import BusHeader, MessageHeader
from MinecraftBridge.mqtt.parsers import MessageType, MessageSubtype

from collections import defaultdict

class MToT_Presentation(Loggable):
    """
    The MToT_Presentation class uses a Machine Theory of Mind to determine if a
    resolved intervention should be presented to the participant(s).  This
    allows the agent to determine if advice should be withheld, despite an
    intervention being resolved.

    Currently, this class considers the following when determining if advice
    should be presented:

    * Mission Time:    Advice will not be presented within the first _n_ seconds
                       of the mission, so that the agent can collect enough
                       information to effectively assess the teams effectiveness.

    * Advice Latency:  An intervention will not be presented to a participant 
                       if that participant was presented the same class of
                       intervention within the previous _n_ seconds.


    Attributes
    ----------
    intervention_filters : list
        List of functions that takes in an intervention, and returns a boolean
        indicating if the intervention should be presented (true) or 
        not (false), as well as an (optional) dictionary for explanation.
    recipient_filters : list
        List of functions that takes in an intervention and a list of
        participants, and returns a list of filtered participants and dictionary
        containing explanations


    Methods
    -------
    resolve(intervention)
        Generate an AgentChatIntervention message and publish to the bus
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the Intervention Manager that the presenter is
            associated with

        Keyword Arguments
        -----------------
        minimum_elapsed_mission_time : float, default=120
            Minimum amount of time (in seconds) that must pass since the end of
            the Planning Stage before any intervention will be presented
        intervention_latency : float, default=30
            Minimum amount of time (in seconds) that must pass between two 
            subsequent presentations of an intervention to the same participant
        """

        self.manager = manager

        # Parse keyword arguments.  Intervention latency needs to be converted
        # to milliseconds, to be easily compared to elapsed_milliseconds
        self._minimum_elapsed_mission_time = kwargs.get("minimum_elapsed_mission_time", 120.0)
        self._intervention_latency = int(1000*kwargs.get("intervention_latency", 40.0))

        # Set up filters
        self.intervention_filters = []
        self.recipient_filters = []

        # Keep track of the last time a participant was provided an intervention
        # Set this to the negative of the intervention latency, so that the
        # first intervention will be presented at a time that is guaranteed to
        # be valid
        self.participant_next_advice_times = defaultdict(lambda: 0)

        # Variables to monitor
        self._intervention_start_time = None

        # Add the actual filters.  Note, we'll probably do some factory or DI
        # thing, once we get all fancy with this
        self.intervention_filters.append(self.filter_by_mission_time)
        self.intervention_filters.append(self.filter_by_player_compliance)
        self.intervention_filters.append(self.filter_by_predicted_team_score)
        self.intervention_filters.append(self.filter_by_total_intervention_occurance) # Needs to be the last one
        self.recipient_filters.append(self.filter_by_participant_latency)

        # Register callbacks with the Minecraft interface to receive messages
        # relevant to the filtering
        self.manager.minecraft_interface.register_callback(PlanningStageEvent, self.__onPlanningStage)
        self.manager.minecraft_interface.register_callback(ScoreboardEvent, self.__onScoreboardUpdate)

        # Initially assume we don't know how the team is performing
        self._team_performing_well = None
        # Track how good much evidence we received
        self._num_score_updates = 0

        # Count globally how often an intervention was triggered
        self._internvention_issue_count = {}


    def str(self):
        """
        String representation of the AdaptiveChatPresentation
        """

        return self.__class__.__name__


    ## Callbacks
    def __onPlanningStage(self, message):
        """
        Callback when a Planning Stage message is received.
        """

        if message.state == PlanningStageEvent.PlanningState.Stop:

            # Get the current time
            current_time = self.manager.mission_clock.elapsed_milliseconds

            # Sanity check:  make sure that the current time is valid
            if current_time is None:
                self.logger.warning("%s:  Misson Clock gave `None` for elapsed_milliseconds at end of Plannign Stage", self)
                return

            # Set the earliest time that interventions can be presented
            self._intervention_start_time = current_time + self._minimum_elapsed_mission_time * 1000
            self.logger.debug("%s:  Intervention Start Time Set: %d", self, self._intervention_start_time)

    
    def __onScoreboardUpdate(self, message):
        """
        Callback for when a scoreboard update is received on the message bus.
        Here, we track the most recent M1 score and update whether or not we 
        anticipate if the team is doing well.
        """
        self._num_score_updates += 1

        # Get score and relevant mission time
        m1_score = message.scoreboard["TeamScore"]
        time = message.elapsed_milliseconds

        # Remove planning phase
        time -= (1000 * 60 * 2)
        # Convert to seconds
        time /= 1000.0

        # Calculated performance
        # Given the HSR data, we want to not advise the top 10% of teams, thus we target a score of 
        # 780 to be "performant" and approximate it with a linear equation
        expected_performance = 0.86666667 * time

        # Now, filter for relevance
        # Check if we have enough evidence:
        if self._num_score_updates < 10:
            self._team_performing_well = None
            return
        
        # Check if we have enough absolute score:
        if m1_score < 500:
            self._team_performing_well = None
            return
        
        # Check if enough time in the game passed:
        if time < 7 * 60:
            self._team_performing_well = None
            return

        # Seems like we are in a position of estimating the team performance
        self._team_performing_well = expected_performance < m1_score
    
    def filter_by_predicted_team_score(self, intervention):
        """
        Filter the interventions by the expected team score given the recent observations 
        """
        if self._team_performing_well:
            return False, {"comment": "Team seems to be performing well. Omitting interventions!"}
        return True, {"comment": "Team seems to need further help from ATLAS"}

    
    def filter_by_total_intervention_occurance(self, intervention):
        """
        Filter the interventions by the total number of occurances
        """
        # This should come from a config file...
        filtered_interventions = [
            "InterventionManager.library.encourage_player_proximity_to_medic_ihmc_dyad.py.EncouragePlayerProximityToMedicIHMCDyad"
        ]
        total_allowed = 4

        module = intervention.__class__.__module__
        name   = intervention.__class__.__qualname__
        intervention_class = module + "." + name

        # Check if this is relevant
        if intervention_class not in filtered_interventions:
            return True, {"comment": "Intervention not globally rate limited!"}
        
        # Check if we already have a count for it
        if intervention_class not in self._internvention_issue_count:
            self._internvention_issue_count[intervention_class] = 0
        
        if self._internvention_issue_count[intervention_class] < total_allowed:
            self._internvention_issue_count[intervention_class] += 1
            return True, {"comment", "Intervention has not reached its rate limit yet!"}
        
        return False, {"comment": "Intervention reached rate limit"}


    def filter_by_participant_latency(self, intervention, recipients):
        """
        Filter out recipients if they received an intervention in the recent
        past (defined by self._intervention_latency).

        Arguments
        ---------
        intervention : Intervention
        recipeints : list
            List of participant_ids to receive the advice

        Returns
        -------
        recipients : list
            Filtered list of participant_ids
        notes : dict or None
            Dictionary of notes regarding the filtering
        """

        # What is the current time?
        current_time = self.manager.mission_clock.elapsed_milliseconds

        # If the current time is `None`, then don't assume that any participant
        # can be filtered out
        if current_time is None or current_time == -1:
            return recipients, {"comment": "Elapsed milliseconds is not valid."}

        filtered_recipients = []
        notes = { "current_time": current_time,
                  "next_intervention_time": {} }

        # For each participant, see if they are allowed to receive advice based
        # on the current time.  If so, add them to the filtered participants,
        # and update their next advice time to the current time plus whatever
        # latency
        for recipient in recipients:
            next_advice_time = self.participant_next_advice_times[(recipient,intervention.__class__)]

            # Record the what the recipient's next intervention time is, so
            # that it's clear why a recipient was filtered out
            notes["next_intervention_time"][recipient] = next_advice_time

            if current_time >= next_advice_time:
                filtered_recipients.append(recipient)
                self.participant_next_advice_times[(recipient,intervention.__class__)] = current_time + self._intervention_latency
            else:
                self.logger.debug("%s:  Recipient %s filtered out of intervention %s", self, recipient, intervention)
                self.logger.debug("%s:    Current Time: %d", self, current_time)
                self.logger.debug("%s:    Recipient's Next Intervention Time: %d", self, next_advice_time)

        return filtered_recipients, notes


    def filter_by_player_compliance(self, intervention):
        """
        Indicates if an intervention shouldbe withheld based on the compliance
        rate it has shown previously. If no compliance data is available, we
        proceed to publish it.
        """
        # This needs to come from a config later
        followup_filtered_interventions = [
            "InterventionManager.library.inform_about_triaged_victims.InformAboutTriagedVictimsIntervention"
        ]
        minimal_expected_compliance_rate = 0.8

        # Get intervention class name:
        module = intervention.__class__.__module__
        name   = intervention.__class__.__qualname__
        intervention_class = module + "." + name

        # If we don't want to filter this intervention, just approve it
        if intervention_class not in followup_filtered_interventions:
            return True, {"comment": ""}

        # Track statistics
        followup_completed = 0
        followup_complied = 0
        # Find the followups we care about
        for followup in self.manager._completed_followup_queue:
            module = followup.intervention.__class__.__module__
            name   = followup.intervention.__class__.__qualname__
            followup_intervention_class = module + "." + name
                       
            # If this is our intervention
            if followup_intervention_class == intervention_class:
                followup_completed += 1
                # Log if it is complied to
                if followup.player_complied:
                    followup_complied += 1
        
        # Check if we have data on that intervention
        if followup_completed == 0:
            return True, {"comment": "No data on compliance; approving intervention."}
        # Calculate compliance ratio
        compliance_ratio = followup_complied / float(followup_completed)

        if compliance_ratio < minimal_expected_compliance_rate:
            return False, {"comment": "Intervention was historically not complied to; discarding intervention."}

        return True, {"comment": "Intervention seems to be complied to; approving intervention."}


    def filter_by_mission_time(self, intervention):
        """
        Indicates if an intervention should be withheld based on the current 
        mission time.  If the mission time hasn't exceeded the minimum elapsed
        mission time after the planning stage end, then the intervention should
        be withheld.

        Arguments
        ---------
        intervention : Intervention
        """

        # Get the current time (elapsed_milliseconds) from the mission clock
        current_time = self.manager.mission_clock.elapsed_milliseconds

        # If the intervention start time is None, then the Trial hasn'e even
        # reached the end of the Planning Stage yet.
        if self._intervention_start_time is None:
            return False, {"comment": "Planning Stage not complete."}

        # Is the mission timer running?  If not, then withhold the intervention
        if current_time is None:
            return False, {"comment": "Mission Clock not running."}

        # Check to see if the current time exceeds the intervention start
        # time.  If so, then it's fine to present, otherwise, don't.
        if current_time > self._intervention_start_time:
            return True, {"comment": ""}
        else:
            return False, {"comment": "Not enough elapsed mission time",
                           "start_time": self._intervention_start_time,
                           "current_time": current_time }


    def resolve(self, intervention):
        """
        Determine the message and recipients for the given intervention and
        publish.  For each recipient, determine if the message should be
        filtered out based on the current state of the MToT model.

        Arguments
        ---------
        intervention : Intervention
            Intervention instance
        """

        self.logger.debug("%s:  Resolving: %s", self, intervention)

        # Check to see if the intervention can be presented
        for intervention_filter in self.intervention_filters:

            can_present, notes = intervention_filter(intervention)

            if not can_present:
                # Store the notes received by the filter, then simply return.
                intervention._resolution_notes.append(notes)

                self.logger.debug("%s:  Intervention %s will not be presented:", self, intervention)
                self.logger.debug("%s:    Reason: %s", self, notes.get("comment", ""))
                return

        self.logger.debug("%s:  Intervention %s will be presented", self, intervention)


        # Try to get the dictionary of intervention info from the intervention;
        # raise an exception if it doesn't exist
        try:
            info = intervention.getInterventionInformation()
        except NotImplementedError:
            info = {}
            self.logger.warning("%s: getInterventionInformation not implemented by intervention %s", self, intervention)

        info["intervention_class"] = intervention.__class__.__name__

        # Get the list of recipients from the intervention info
        recipients = info.get("default_recipients", None)
        message = info.get("default_message", None)

        for recipient_filter in self.recipient_filters:

            recipients, notes = recipient_filter(intervention, recipients)

            if notes is not None:
                intervention._resolution_notes.append(notes)


        # Are there any recipients left?
        if len(recipients) == 0:
            recipients = None


        # Store the presented message in the intervention itself
        intervention.presented_message = message
        intervention.message_recipients = recipients

        # If there's something to present, indicate that the intervention was
        # presented and present the message
        if recipients is not None and message is not None:
            intervention.presented = True
            self.manager.publish_chat_intervention(message, recipients,
                                                   intervention_info=info)
