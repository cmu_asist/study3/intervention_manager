# -*- coding: utf-8 -*-
"""
.. module:: InterventionManager
   :platform: Linux, Windows, OSX
   :synopsis: Package for managing the lifecycle of interventions

.. moduleauthor:: CMU-TA1

The InterventionManager module provides functionality for managing
interventions, including

    * Definition of a Base Intervention class

    * Trigger management for instantiating interventions

    * Management of intervention lifecycle

    * Presentation classes for interventions
"""

__author__ = "CMU-TA1"
__email__ = "danahugh@andrew.cmu.edu"
__url__ = "https://gitlab.com/cmu_asist/study3/intervention_manager"
__version__ = "0.3.7"

from .manager import InterventionManager
from .interventions import Intervention
from .triggers import InterventionTrigger
from .followups import Followup

__all__ = ["InterventionManager", "Intervention", "InterventionTrigger", "Followup"]
