# -*- coding: utf-8 -*-
"""
.. module:: participant
   :platform: Linux, Windows, OSX
   :synopsis: Decoration of BaseAgent Participant class to include Intervention
              Manager components

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

A decoration of BaseAgent participants to incorporate characteristics and 
read access to the participant's ToM model (via Redis).
"""

from MinecraftBridge.utils import Loggable


class Characteristics:
    """
    Lightweight class to capture characteristics of participants that are 
    influenced by observed reactions to Interventions.  Currently, this stores
    the rate of compliance of a participant to interventions.

    NOTE:  Would it be preferable to pass in the manager, and have the
           Characteristics iterate through followups and count this, so that
           it isn't necessary to manually increment this?  At the moment, a 
           Followup instance would need to increment (non)compliance.  It may
           be preferable to have each followup have a `target_participant(s)`
           attribute, that contains a (list of) participant instance(s), and
           simply iterate through the followups and count.  This would let us
           later be able to filter out compliance (e.g., compliance by Followup
           type).

    Methods
    -------
    increment_compliance
        Indicate that the player has complied to presented advice
    increment_noncompliance
        Indicate that the player has not complied to presented advice


    Attributes
    ----------
    compliance_rate : float or None
        Participant's rate of compliance to provided advice.  Returns `None` if
        no compliance or noncompliance has been reported yet.
    """

    def __init__(self, participant_id):
        """
        Arguments
        ---------
        participant_id : string
            ID of the participant
        """

        self._participant_id = participant_id

        self._compliance_count = 0
        self._noncompliance_count = 0


    @property
    def compliance_rate(self):
        """
        Return the rate of compliance of the participant.  If no complance /
        noncompliance instances have been reported, returns `None`
        """

        # Return None if no compliance / noncompliance has been reported yet
        if self._compliance_count + self._noncompliance_count == 0:
            return None

        return self._compliance_count / (self._compliance_count + self._noncompliance_count)


    def increment_compliance(self):
        """
        Indicate that this participant has complied with an intervention
        """

        self._compliance_count += 1


    def increment_noncompliance(self):
        """
        Indicate that this participant has not complied with an intervention
        """

        self._noncompliance_count += 1



class InterventionParticipant:
    """
    Decorated version of BaseAgent Participant class to include participant's
    characteristics relevant to Interventions (e.g., compliance rate)

    Attributes
    ----------
    participant_id : string (from BaseAgent.Participant)
        Unique ID of the participant
    id : string (from BaseAgent.Participant)
        Alias for `participant_id`
    playername : string (from BaseAgent.Participant)
        Minecraft playername of the participant
    callsign : string (from BaseAgent.Participant)
        Callsign of the participant
    role : string (from BaseAgent.Participant)
        The participant's role
    characteristics : Characteristics
        The internal (intervention-related) characteristics of the participant
    tom : ToM Proxy
        Proxy to the ToM model for the participant stored in the TToM submodule
    """

    def __init__(self, participant):

        self._base_participant = participant

        self._characteristics = Characteristics(participant.participant_id)
        self._tom = None         # TBD


    def __str__(self):
        """
        String representation of the participant
        """

        return "%s: \n   participant_id: %s\n   playername: %s\n   callsign: %s" % (self.__class__.__name__, self.participant_id, self.playername, self.callsign)


    @property
    def participant_id(self):
        return self._base_participant.participant_id

    @participant_id.setter
    def participant_id(self, _):
        pass

    @property
    def id(self):
        return self._base_participant.participant_id

    @id.setter
    def id(self, _):
        pass

    @property
    def playername(self):
        return self._base_participant.playername

    @playername.setter
    def playername(self, _):
        pass

    @property
    def callsign(self):
        return self._base_participant.callsign

    @callsign.setter
    def callsign(self, _):
        pass

    @property
    def role(self):
        return self._base_participant.role

    @role.setter
    def role(self, value):
        self._base_participant.role = value

    @property
    def characteristics(self):
        return self._characteristics

    @characteristics.setter
    def characteristics(self, _):
        pass

    @property
    def tom(self):
        return self._tom

    @tom.setter
    def tom(self, _):
        pass
    

