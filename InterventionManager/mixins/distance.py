import numpy as np

from ..components import InterventionScheduler as Scheduler
from ..utils import angle_between, distance, to_2D
from MinecraftBridge.messages import PlayerState



class DistanceMixin:
    """
    Mixin that reports when a player moves away from a given location.

    This mixin can be used by any component that inherits from BaseComponent
    (i.e., Interventions or Followups).  Note that the __init__ method of the
    BaseComponent must be called prior to calling this mixin's __init__ method.

    Callbacks
    ---------
    _onMovingAway()
        Callback for when the player begins moving away from the location
    _onDistanceExceeded()
        Callback for when the player has exceeded the distance threshold
    _onTimeout()
        Callback for when a given time interval has elapsed in the mission
        without the component being disconnected
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        player : string
            Key for the player (e.g. participant ID, playername, callsign, etc)
        location : string or tuple
            Either room name or location coordinates
        angle_threshold : float, default=135.0
            Minimum angle between the location and direction of player
            movement to consider the player "moving away"
        distance_threshold : float, default=5
            Maximum distance from location that a player can go
            before the location is considered to have been "left"
        timeout : float, default=15
            Maximum mission time (in seconds) before calling _onTimeout() callback
        """
        self.__manager = manager
        self.__scheduler = Scheduler(self)

        # Set attributes
        self.__angle_threshold = kwargs.get('angle_threshold', 135.0)
        self.__distance_threshold = kwargs.get('distance_threshold', 5)
        self.__location = kwargs.get('location')
        self.__participant = manager.participants[kwargs.get('player')]

        # Initialize distance function
        if isinstance(self.__location, str):
            # Location is a room name
            self.__dist_func = manager.semantic_map.distance
        else:
            # Location is some (x, z) or (x, y, z) iterable
            self.__dist_func = distance

        # Add Minecraft callback
        self.add_minecraft_callback(PlayerState, self.__onPlayerState)

        # Schedule timeout
        timeout = kwargs.get('timeout', 15)
        if timeout:
            self.__scheduler.wait(self._onTimeout, delay=timeout)


    def _onMovingAway(self):
        """
        Callback for when the player begins moving away from the location.
        """
        pass


    def _onDistanceExceeded(self):
        """
        Callback for when the player has exceeded the distance threshold.
        """
        pass


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without a discard or resolution.
        """
        pass


    def __onPlayerState(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.PlayerState
            Received PlayerState message
        """
        if message.elapsed_milliseconds <= 0:
            return

        # Ignore if message is for another participant
        participant = self.__manager.participants[message.participant_id]
        if participant != self.__participant:
            return

        # Check if player distance from location is above threshold
        dist = self.__dist_func(message.position, self.__location)
        if dist > self.__distance_threshold:
            self._onDistanceExceeded()

        # Check to see if the player is moving away from the location.
        # This is calculated based on the dot product between the displacement
        # vector between the player and location, and the velocity of the
        # player.
        if isinstance(self.__location, (tuple, list)):
            displacement = np.array(to_2D(self.__location)) - to_2D(message.position)
            angle = angle_between(displacement, to_2D(message.velocity))
            if angle is not None and angle >= self.__angle_threshold:
                self._onMovingAway()
