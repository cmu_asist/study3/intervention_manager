from MinecraftBridge.utils import Loggable
from ..components import InterventionScheduler as Scheduler



class ComplianceMixin(Loggable):
    """
    Mixin that checks for compliance after a given time interval.

    This mixin can be used by any component that inherits from Followup.
    Note that the __init__ method of the Followup must be called prior
    to calling this mixin's __init__ method.

    Callbacks
    ---------
    _onCompliance()
        Callback for when team is compliant with the intervention
    _onNonCompliance()
        Callback for when team is non-compliant with the intervention
    _onTimeout()
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance (or the mission ends).
    """

    def __init__(self, timeout=None):
        """
        Arguments
        ---------
        timeout : float
            Number of seconds without recording compliance or non-compliance
            before calling _onTimeout()
        """
        self.__scheduler = Scheduler(self)

        # Schedule timeout
        self.__scheduler.countdown(self._onTimeout, t_minus=0) # mission end
        if timeout:
            self.__scheduler.wait(self._onTimeout, delay=timeout)


    def _onCompliance(self):
        """
        Callback for when team is compliant with the intervention.
        """
        self.logger.info(f"{self}:  Recording compliance.")

        # Check if the intervention has been presented, and if so, set the
        # player_complied to True
        if self.intervention.presented:
            self.player_complied = True

        self.complete()
        self.__scheduler.reset()


    def _onNonCompliance(self):
        """
        Callback for when team is non-compliant with the intervention.
        """
        self.logger.info(f"{self}:  Recording non-compliance.")

        # Check if the intervention has been presented, and if so, set the
        # player_complied to True
        if self.intervention.presented:
            self.player_complied = False

        self.complete()
        self.__scheduler.reset()


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance (or the mission ends).
        """
        self.logger.info(f"{self}:  Timed out.")
        self.complete()
        self.__scheduler.reset()
        
