import numpy as np
from ..components import InterventionScheduler as Scheduler



class MisalignmentMixin:
    """
    Mixin that reports when the mean ToM misalignment value increases quickly
    (i.e. increases by a given value in a given time interval).

    This mixin can be used by any component that inherits from BaseComponent
    (i.e., Interventions or Followups).  Note that the __init__ method of the
    BaseComponent must be called prior to calling this mixin's __init__ method.

    Callbacks
    ---------
    _onMisalignment()
        Callback for when misalignment increases quickly
    """

    def __init__(self, manager, *args, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        min_delta : float, default=0.02
            Minimum increase in misalignment required to trigger an intervention
        interval : float, default=float('inf')
            How far back to look for misalignment increases (in seconds)
        request_interval : float, default=5
            How often to make misalignment requests (in seconds)
        misalignment_query : dict, default={}
            Misalignment query dictionary
            (see https://gitlab.com/cmu_asist/study3/tom/-/blob/develop/tom/query.py)
        """
        self.__manager = manager
        self.__scheduler = Scheduler(self)

        # Extract kwargs
        self.__min_delta = kwargs.get('min_delta', 0.02)
        self.__interval = kwargs.get('interval', float('inf'))
        self.__request_interval = kwargs.get('request_interval', 5)
        self.__misalignment_query = kwargs.get('misalignment_query', {})

        # Trigger-specific attributes
        self.__misalignments = [] # list of (time, misalignment_value)
        self.__misalignment_queries = {} # indexed by request ID

        # Register Redis callback
        self.add_redis_callback(
            self.__onMisalignmentResponse, 'misalignment', message_type='Response')

        # Schedule misalignment requests at regular intervals
        self.__scheduler.repeat(self.__requestMisalignment, interval=self.__request_interval)


    def _onMisalignment(self):
        """
        Callback for when misalignment increases quickly.
        """
        pass


    def __requestMisalignment(self):
        """
        Request pairwise misalignment values from the team ToM.
        """

        # Update query with current time
        query = self.__misalignment_query.copy()
        query['time'] = self.scheduler.time

        # Make misalignment request and store query
        request_id = self.__manager.request_redis(
            self.__misalignment_query, channel='misalignment', blocking=False)
        self.__misalignment_queries[request_id] = query


    def __onMisalignmentResponse(self, response):
        """
        Update misalignment values from team ToM response.
        """
        if response.request_id not in self.__misalignment_queries:
            return

        # Update `self.__misalignments` from response
        # Response data is a list of (_, _, value) tuples
        time = self.__misalignment_queries[response.request_id]['time']
        average_misalignment = np.array([value for _, _, value in response.data]).mean()
        self.__misalignments.append((time, average_misalignment))

        # Filter misalignments going back `self.__interval` seconds
        misalignments = np.array(self.__misalignments)
        times = misalignments[:, 0]
        start_time = times[-1] - self.__interval
        misalignments = misalignments[times > start_time]

        # Check for significant increase
        values = misalignments[:, 1]
        increase = values[-1] - values.min()
        if increase >= self.__min_delta:
            self._onMisalignment()
