# -*- coding: utf-8 -*-
"""
.. module:: inform_trapped_medics_to_be_productive
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for encouraging medics trapped in a trap
              room to be productive

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of an set of Intervention classes and InterventionTrigger class for
an intervention designed to encourage medics to be productive if they have been
trapped in a room (and they detect that they're trapped).

* Trigger:  Medic becomes trapped in a room

* Discard States:  N/A

* Resolve State:  Immediately queued for resolution

"""

from .. import Intervention
from .. import InterventionTrigger

from ..mixins import ComplianceMixin, TrappedMixin
from MinecraftBridge.messages import *



class MedicTrappedIntervention(Intervention):

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """
        Intervention.__init__(self, manager, **kwargs)
        self.participant = manager.participants['Medical_Specialist']


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {
            'default_recipients': [self.participant.id],
            'default_message': (
                "Medic, despite being trapped, you can still be productive "
                "in this room while waiting to be rescued."
            )     
        }



class MedicTrappedTrigger(InterventionTrigger, TrappedMixin):
    """
    This trigger is used to detect when a medic detects they are trapped in a trap room.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        TrappedMixin.__init__(self, manager, **kwargs)


    def _onPlayerTrapped(self, participant, loc):
        """
        Callback for when a player becomes trapped in a room.
        """
        if participant.role == 'Medical_Specialist':
            # TODO: determine if medic "knows" they are trapped
            trap_room = self.manager.semantic_map.rooms[loc]
            intervention = MedicTrappedIntervention(self.manager)
            self.logger.info(f"{self}: Medic trapped at {trap_room}. Spawning {intervention}.")
            self.manager.spawn(intervention)
