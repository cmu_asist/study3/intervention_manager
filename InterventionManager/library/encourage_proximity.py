# -*- coding: utf-8 -*-
"""
.. module:: encourage_proximity
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for team to work in an appropriate proximity

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.edu>

Definition of an Intervention class and InterventionTrigger class for an
intervention designed to suggest smaller or larger distance between members
depending on team performance and team proximity.

* Trigger:  A specified time interval has elapsed without team progress:

    1. Discovering new victims
    2. Triaging victims
    3. Evacuating victims

* Discard States:
    - Players are within "optimal" pairwise distances
    - The standard deviation of pairwise distances is above a certain threshold

* Resolve State:  Players are outside "optimal" pairwise distances
                  and the standard deviations below a certain threshold
"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..components import InterventionScheduler as Scheduler
from ..mixins import ComplianceMixin, SawVictimMixin

import numpy as np

from itertools import combinations
from MinecraftBridge.messages import PlanningStageEvent, PlayerState, TriageEvent, VictimEvacuated



class ProximityMonitor:
    """
    Object that monitors team proximity over time.
    """

    def __init__(self, component, **kwargs):
        """
        Arguments
        ---------
        component : BaseComponent
            The Intervention manager component to connect this monitor to

        Keyword Arguments
        -----------------
        optimal_range : tuple or list of (float, float), default=(10, 50)
            Range for optimal pairwise distance between team members
        std_threshold : float, default=10
            Maximum standard deviation of distances that is considered "consistent"
        """

        # Set attributes
        self._optimal_range = kwargs.get('optimal_range', (10, 50))
        self._std_threshold = kwargs.get('std_threshold', 10)
        self._locations = {} # keys are participant IDs
        self._proximities = [] # list of proximity values over time

        # Add Minecraft callback through component
        component.add_minecraft_callback(PlayerState, self.__onPlayerState)

        # Schedule proximity updates through component
        self._scheduler = Scheduler(component)
        self._scheduler.repeat(self._update_proximity, interval=1)


    def __len__(self):
        return len(self._proximities)


    def clear(self):
        self._proximities.clear()


    def copy(self, component, **kwargs):
        kwargs.setdefault('optimal_range', self._optimal_range)
        kwargs.setdefault('std_threshold', self._std_threshold)
        pm = ProximityMonitor(component, **kwargs)
        pm._locations = self._locations
        pm._proximities = self._proximities
        return pm


    def mean(self):
        return np.array(self._proximities).mean()


    def std(self):
        return np.array(self._proximities).std()


    def is_consistent(self):
        return len(self) > 0 and self.std() <= self._std_threshold


    def is_optimal(self):
        if len(self._proximities) > 0:
            return (
                self._optimal_range[0] <= self.mean()
                and self.mean() <= self._optimal_range[1]
            )

        return False


    def is_too_close(self):
        if len(self._proximities) > 0:
            return self.mean() < self._optimal_range[0]

        return False


    def _update_proximity(self):
        self._proximities.append(self._average_pairwise_distance())


    def _average_pairwise_distance(self):
        pairs = np.array(list(combinations(self._locations.values(), 2)))
        if len(pairs) > 0:
            distances = np.linalg.norm(pairs[:, 0] - pairs[:, 1], axis=-1)
            return distances.mean()

        return 0


    def __onPlayerState(self, message):
        self._locations[message.playername] = [message.x, message.z]



class EncourageProximityIntervention(Intervention):
    """
    An Intervention designed to encourage / discourage team proximity
    if the team's behaviour is deemed suboptimal.
    """

    def __init__(self, manager, proximity_monitor, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        proximity_monitor : ProximityMonitor
            Team proximity history
        """
        Intervention.__init__(self, manager, **kwargs)

        # Set attributes
        self.proximity_monitor = proximity_monitor.copy(self)

        # Indicate that all participants are relevant to this intervention
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.participant_id)


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately determine whether to resolve or discard.
        """
        if self.proximity_monitor.is_consistent() and not self.proximity_monitor.is_optimal():
            self.logger.info(
                "%s: %s %s %s",
                self,
                f"Team proximity ({self.proximity_monitor.mean()})",
                f"is consistently outside optimal_range (std {self.proximity_monitor.std()}).",
                f"Queuing intervention for resolution.",
            )
            self.queueForResolution()
        else:
            self.logger.info(
                f"{self}:  Team proximity is not consistently suboptimal. Discarding intervention.")
            self.discard()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        if self.proximity_monitor.is_too_close():
            text_message = (
                "You haven't made much progress towards rescuing victims lately. Try spreading out to cover more ground."
            )
        else:
            text_message = (
                "You have not made much progress lately. Consider staying closer to the medic to help triage victims."
            )

        return {
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': text_message,            
        }



class EncourageProximityTrigger(InterventionTrigger, SawVictimMixin):
    """
    Class that generates an intervention to encourage player proximity.
    Interventions are triggered when a specified time interval has elapsed 
    without team progress, with progess defined as any of the following:

        1. Discovering new victims
        2. Triaging victims
        3. Evacuating victims
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        optimal_range : tuple or list of (float, float), default=(10, 50)
            Range for optimal pairwise distance between team members
        std_threshold : float, default=10
            Maximum standard deviation of distances that is considered "consistent"
        stall_time_threshold : float, default=60
            Minimum number of seconds of stalled team progress required
            to trigger an intervention
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        SawVictimMixin.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Set attributes
        self._stall_time_threshold = kwargs.get('stall_time_threshold', 60)
        self._proximity_monitor = ProximityMonitor(self, **kwargs)
        self._scheduler = Scheduler(self)

        # Add Minecraft callbacks
        self.add_minecraft_callback(PlanningStageEvent, self._enable_trigger)
        self.add_minecraft_callback(TriageEvent, self._reset_progress)
        self.add_minecraft_callback(VictimEvacuated, self._reset_progress)


    def spawn_intervention(self):
        intervention = EncourageProximityIntervention(
            self.manager, self._proximity_monitor, **self.kwargs)
        self.logger.info(
            "%s:  %s. %s.",
            self,
            f"No progress has been made in {self._stall_time_threshold} seconds",
            f"Spawning {intervention}",
        )
        self.manager.spawn(intervention)


    def _onNewVictimSeen(self, participant_id, victim):
        """
        Callback for when a victim is first seen by a player.
        """
        self._reset_progress()


    def _enable_trigger(self, message):
        """
        Schedule an intervention after the planning stage is done.
        """
        if message.state == PlanningStageEvent.PlanningState.Stop:
            self._scheduler.wait(self.spawn_intervention, delay=self._stall_time_threshold)


    def _reset_progress(self, *args, **kwargs):
        # Clear proximity monitor
        self._proximity_monitor.clear()

        # Reset scheduler and reschedule the intervention
        self._scheduler.reset()
        self._scheduler.wait(self.spawn_intervention, delay=self._stall_time_threshold)



class EncourageProximityFollowup(Followup, ComplianceMixin):
    """
    Followup to an EncourageProximityIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=60
            Number of seconds in the time period of follow up validation
        """

        duration = kwargs.get("duration", 60)


        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)

        # Initialize proximity monitor
        self._proximity_monitor = intervention.proximity_monitor.copy(self)
        self._proximity_monitor.clear()


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """

        # Evaluate the team response to the intervention
        if self._proximity_monitor.is_consistent() and self._proximity_monitor.is_optimal():
            self._onCompliance()
        else:
            self._onNonCompliance()
