# -*- coding: utf-8 -*-
"""
.. module:: remind_triage_critical
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for reminding players when a limited
              amount of time remains

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to remind players to triage critical victims they may
have forgotten about.

* Trigger:  The intervention is triggered when the remaining time for a
            a trial is less than t minutes.

* Discard States:  Players are unaware of any untriaged critical victims
                   (according to ToM)

* Resolve State:  All players have knowledge of untriaged critical victims
                  (according to ToM)

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..components import InterventionScheduler as Scheduler
from ..mixins import ComplianceMixin

from MinecraftBridge.messages import TriageEvent



class RemindTriageCriticalIntervention(Intervention):
    """
    An Intervention which is designed to alert the team
    that there is only a few minutes left in the mission.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        redis_timeout : float, default=2.0
            Timeout for a Redis request on the internal bridge
        belief_threshold : float, default=0.99
            The minimum number of expected critical victims in a room from the belief state
            that is considered being "aware"
        """
        Intervention.__init__(self, manager, **kwargs)
        self._redis_timeout = kwargs.get('redis_timeout', 2.0)
        self._belief_threshold = kwargs.get('belief_threshold', 0.99)
        self._known_untriaged_critical_victim_rooms = set()

        # Indicate that all participants are relevant to this intervention
        self.addRelevantParticipant(self.manager.participants['Medical_Specialist'].participant_id)        


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately determine whether to resolve or discard.
        """
        
        # Create belief query for ToM
        query = {
            'cells': ['victim_C_injured'],
            'rooms': 'all',
        }

        # Get and process response
        try:
            response = self.manager.request_redis(
                query, channel='belief', timeout=self._redis_timeout)

            for room in self.manager.semantic_map.rooms.unique():
                is_critical_victim_known_to_all_teammates = all(
                    response.data[player][room]['victim_C_injured'] > 0.99
                    for player in response.data)
                if is_critical_victim_known_to_all_teammates:
                    self._known_untriaged_critical_victim_rooms.add(room)

        except TimeoutError:
            pass

        # Discard if there are no known untriaged critical victim rooms,
        # otherwise queue for resolution
        if len(self._known_untriaged_critical_victim_rooms) == 0:
            self.logger.info(
                f"{self}:  No untriaged critical victims known to all players. Discarding intervention.")
            self.discard()
        else:
            self.logger.info(
                "%s: %s %s %s",
                self,
                f"All players are aware that there are untriaged critical victims",
                f"in rooms {self._known_untriaged_critical_victim_rooms}.",
                f"Queuing intervention for resolution.",
            )
            self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        room_string = ' and '.join(str(room) for room in self._known_untriaged_critical_victim_rooms)
        message = f"Team, remember to triage the critical victims in room {room_string}."
        return {
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': message,            
        }



class RemindTriageCriticalTrigger(InterventionTrigger):
    """
    Class that generates RemindTriageCriticalIntervention instances.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        minutes_left_threshold : int, default=2
            Minutes left in the trial for the intervention to be triggered
        redis_timeout : float, default=2.0
            Timeout for a Redis request on the internal bridge
        belief_threshold : float, default=0.99
            The minimum number of expected critical victims in a room from the belief state
            that is considered being "aware"
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Trigger-specific attributes
        self._minutes_left_threshold = kwargs.get('minutes_left_threshold', 2)

        # Schedule an intervention
        self._scheduler = Scheduler(self)
        self._scheduler.countdown(
            self.spawn_intervention, t_minus=60*self._minutes_left_threshold)


    def spawn_intervention(self):
        """
        Spawn a RemindTriageCriticalIntervention.
        """
        intervention = RemindTriageCriticalIntervention(self.manager, **self.kwargs)
        self.logger.info(
            f"{self}:  {self._minutes_left_threshold} minutes left.  Spawning {intervention}.")
        self.manager.spawn(intervention)



class RemindTriageCriticalFollowup(Followup, ComplianceMixin):
    """
    Followup to an RemindTriageCriticalIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup
        """
        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self)

        # Add Minecraft callback
        self.add_minecraft_callback(TriageEvent, self.__onTriageEvent)


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance (or the mission ends).
        """
        self._onNonCompliance()


    def __onTriageEvent(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.TriageEvent
            Recieved TriageEvent message
        """
        if message.type in {'victim_c', 'victim_saved_c'}:
            self._onCompliance()
