# -*- coding: utf-8 -*-
"""
.. module:: encourage_transporter_scout
   :platform: Linux, Windows, OSX
   :synopsis: Intervention designed to remind engineer to place markers
                for threat rooms

.. moduleauthor:: Huao Li <hul52@pitt.edu>

* Trigger:  A team member is trapped and no threat room markers has been placed

* Discard States:  N/A

* Resolve State:  Immediately queued for resolution

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

import numpy as np

from ..mixins import ComplianceMixin, TrappedMixin
from ..utils import angle_between, compass_direction

from MinecraftBridge.messages import LocationEvent, PlayerState, MarkerPlacedEvent, VictimSignal,VictimEvacuated



class RemindEngineerMarkThreatRoomIntervention(Intervention):
    """
    Intervention designed to remind engineer to place markers for threat rooms
    """

    def __init__(self, manager, suggestion, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """
        Intervention.__init__(self, manager, **kwargs)
        self.participant_id = self.manager.participants['Engineering_Specialist'].id



    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        self.queueForResolution()


    def getInterventionInformation(self):
        return {
            'default_recipients': [self.manager.participants['Engineering_Specialist'].id],
            'default_message': "Engineering, try to place markers for threat rooms.",
        }



class RemindEngineerMarkThreatRoomTrigger(InterventionTrigger):
    """
    Trigger that spawns EncourageTransporterScoutIntervention instances.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """
        InterventionTrigger.__init__(self, manager, **kwargs)


        # Trigger-specific attributes
        self.kwargs = kwargs
        self.participant_id = self.manager.participants['Engineering_Specialist'].id
        self._threat_markers = 0
        self._num_interventions = 0

        # Add Minecraft callbacks
        self.add_minecraft_callback(TrappedMixin, self.__onPlayerTrappedEvent)
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onMarkerPlacedMessage)

    def _onPlayerTrapped(self, message):
        """
        Callback for when a player is trapped
        """
        if self._threat_markers ==0 and self._num_interventions < 3:
            intervention = RemindEngineerMarkThreatRoomIntervention(self, message, self.kwargs)
            self.logger.info(f"{self}: Participant {message.participant_id} is trapped and not threat markers are placed. Spawning {intervention}")
            self.manager.spawn(intervention)
            self._num_interventions+=1


    def __onMarkerPlacedMessage(self, message):
        # Ignore this Marker Placed message if it's not associated with the participant
        if message.playername != self.participant_id:
            return
        # Ignore this Marker Placed message if the marker placed is not a threat room marker
        threat_markers = [MinecraftElements.MarkerBlock._threat]
        if message.marker_type not in threat_markers:
            return
        else:
            self._threat_markers+=1


class RemindEngineerMarkThreatRoomFollowup(Followup, ComplianceMixin):
    """
    Followup to an EncourageTransporterScoutIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=90
            The duration of the followup validation period (in seconds)
        """

        duration = kwargs.get("duration", 120)
        self.participant_id = intervention.participant_id
        Followup.__init__(self, intervention, manager)
        ComplianceMixin.__init__(self, timeout=duration)

        # Add Minecraft callback
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onMarkerPlacedMessage)


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """
        self._onNonCompliance()

    def __onMarkerPlacedMessage(self, message):
        # Ignore this Marker Placed message if it's not associated with the participant
        if message.playername != self.participant_id:
            return
        # Ignore this Marker Placed message if the marker placed is not a threat room marker
        threat_markers = [MinecraftElements.MarkerBlock._threat]
        if message.marker_type not in threat_markers:
            return
        else:
            self._onCompliance()



