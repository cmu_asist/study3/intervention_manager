# -*- coding: utf-8 -*-
"""
.. module:: leave_room_empty_handed
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for informing players to be efficient
              when leaving a room.

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger, Intervention, and Followup to remind a participant
to grab a victim when leaving a room if the opportunity presents itself.
"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from MinecraftBridge.messages import (
   LocationEvent,
   VictimPickedUp,
   VictimPlaced   
)

from ..components import VictimsMonitor

from collections import defaultdict



################################################################################
################################################################################

class LeaveRoomEmptyHandedTrigger(InterventionTrigger):
   """
   Class that spawns a LeaveRoomEmptyHandedIntervention whenever a player
   enters a room.
   """

   def __init__(self, manager, **kwargs):
      """
      Arguments
      ---------
      manager : InterventionManager
      """

      InterventionTrigger.__init__(self, manager, **kwargs)

      # Start monitoring the location of victims
      self._victims_monitor = VictimsMonitor(self)

      # Dictionary to keep track of whether or not participants are carrying
      # something.
      self._carrying_victim = defaultdict(lambda: False)

      # Add callback to monitor when a player enters a room.  Also, monitors when
      # players pick up and place victims, to see if they are carrying something
      # when they enter the room
      self.add_minecraft_callback(LocationEvent, self.__onLocationEvent)
      self.add_minecraft_callback(VictimPickedUp, self.__onVictimPickedUp)
      self.add_minecraft_callback(VictimPlaced, self.__onVictimPlaced)


   def __onLocationEvent(self, message):
      """
      Callback when a participant changes their location

      Arguments
      ---------
      message : MinecraftBridge.messages.LocationEvent
      """

      # Check to see if the player entered a room
      if len(message.locations) == 0:
         # No locations provided -- player didn't enter anything
         return

      # message.location may contain multiple locations, but 1) the first
      # should contain the `parent` room, and 2) the semantic map should 
      # contain all the sub_room ids
      if message.locations[0]['id'] in self.manager.semantic_map.hallways:
         # Location is a hallway
         return

      # The player entered a room, get the room ID and who entered, and spawn
      # an intervention to see if they leave empty-handed.

      # Figure out what the room ID is (there are "subroom" IDs in the location
      # attribute of the message as well)
      room_id = None
      for location in message.locations:
         if location['id'] in self.manager.semantic_map.rooms.unique():
            room_id = location['id']

      # Was a Room ID found?
      if room_id is None:
         self.logger.debug("%s:  No Room ID found in LocationEvent message", self)
         self.logger.debug("%s:    %s", self, message)
         return


      intervention = LeaveRoomEmptyHandedIntervention(self.manager, 
                                                      message.participant_id,
                                                      room_id,
                                                      self._victims_monitor,
                                                      self._carrying_victim[message.participant_id],
                                                      parent=self)


   def __onVictimPlaced(self, message):
      """
      Callback when a victim is placed.  Used to flag that the participant is 
      not carrying a victim.

      Arguments
      ---------
      message : MinecraftBridge.messages.VictimPlaced
      """

      self._carrying_victim[message.participant_id] = False


   def __onVictimPickedUp(self, message):
      """
      Callback when a victim is picked up.  Used to flag that the participant
      is carrying a vicitm.

      Arguments
      ---------
      message : MinecraftBridge.messages.VictimPickedUp
      """

      self._carrying_victim[message.participant_id] = True



class LeaveRoomEmptyHandedIntervention(Intervention):
   """
   Intervention that monitors a participant while in a room to see if they 
   """

   def __init__(self, manager, participant_id, room_id, victims_monitor, 
                      carrying_victim, **kwargs):
      """
      Arguments
      ---------
      manager : InterventionManager

      participant_id : string
         ID of the participant being monitored
      room_id : string
         ID of the room the participant is in
      victims_monitor : VictimsMonitor
         Monitor used to determine the location and status of victims
      carrying_victim : boolean
         Flag to indicate if the participant is carrying a victim when this
         intervention was spawned
      """

      Intervention.__init__(self, manager, **kwargs)

      self._participant_id = participant_id
      self._room_id = room_id
      self._victims_monitor = victims_monitor
      self._carrying_victim = carrying_victim


      # Register callbacks to determine when a participant leaves a room and if
      # they are carrying a victim
      self.add_minecraft_callback(LocationEvent, self.__onLocationEvent)
      self.add_minecraft_callback(VictimPickedUp, self.__onVictimPickedUp)
      self.add_minecraft_callback(VictimPlaced, self.__onVictimPlaced)


   def __isVictimAvailable(self):
      """
      Helper method to check to see if a vicitm is available to carry out of
      the room.

      Returns
      -------
      True if a victim (SAVED or REGULAR) is known to be in the room
      """

      # Iterate through the victims in the victims monitor and see if any
      # are in the room, and if they can be moved (REGULAR or SAVED)
      for victim in self._victims_monitor.victims.items():

         # Unpack the victim location for convenience
         x,y,z = victim.location

         if self.manager.semantic_map.rooms[x,y,z] == self._room_id and \
            victim.stabilized_state in { 'REGULAR', 'SAVED' }:

            # Found a suitable victim
            return True

      # None found
      return False


   def __onLocationEvent(self, message):
      """
      Callback when a LocationEvent message is received.  Used to check to see
      if the participant leaves the room.

      Arguments
      ---------
      message : MinecraftBridge.messages.LocationEvent
      """

      # Does this event concern the observed participant?
      if message.participant_id != self._participant_id:
         return

      # Did the participant enter a hallway / exit a room?
      if message.locations[0]['id'] in self.manager.semantic_map.hallways:

         # Check to see if the participant is carrying a victim.  If so, then
         # there's no need to provide advice to the participant
         if self._carrying_victim:
            self.discard()

         else:

            # TODO: A ToM version would check the belief state of the
            #       participant.  Implement this after the non-tom version is 
            #       tested.  An easy thing to have would be to have a ToM and
            #       non-ToM version of the `__isVictimAvailable` method, and
            #       set the `__isVictimAvailable` method to whichever version.

            # If there's a victim available, then queue the intervention for 
            # resolution.  Otherwise, discard
            if self.__isVictimAvailable():
               self.logger.debug("%s:  Known victim can be moved out of room", self)
               self.queueForResolution()
            else:
               self.logger.debug("%s:  No known victim in room", self)
               self.dicard()



   def __onVictimPickedUp(self, message):
      """
      Callback when a VictimPickedUp message is received.  Used to check to see
      if the participant picks up a victim.

      Arguments
      ---------
      message : MinecraftBridge.messages.VictimPickedUp
      """

      if message.participant_id == self._participant_id:
         self._carrying_victim = True


   def __onVictimPlaced(self, message):
      """
      Callback when a VictimPlaced message is received.  Used to check to see
      if the participant puts their victim down while in the room.

      Arguments
      ---------
      message : MinecraftBridge.messages.VictimPlaced
      """

      if message.participant_id == self._participant_id:
         self._carrying_victim = False

