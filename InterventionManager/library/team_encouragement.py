# -*- coding: utf-8 -*-
"""
.. module:: team_encouragement_message
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for encouragement message

.. moduleauthor:: Simon Stepputtis <stepputtis@cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to encourageme the team if they are doing a good job,
given the M1 metric
"""

import math

from .. import Intervention
from .. import InterventionTrigger

from MinecraftBridge.messages import PlanningStageEvent, ScoreboardEvent


class TeamEncouragementIntervention(Intervention):
    """
    A TeamWelcomeMessageIntervention is an Intervention which simply presents
    a welcome message to all participants in the team.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """
        Intervention.__init__(self, manager, **kwargs)
        self._message = kwargs.get('message', "No message for <TeamEncouragementIntervention>")

        # Listen to score updates:
        self.add_minecraft_callback(ScoreboardEvent, self._onScoreUpdate)

    def _isTeamDoingWell(self, elapsed_milliseconds, current_m1):
        """
        Given the latest M1 score update, calculate whether or not the team is doing well
        """
        # Correct for planning phase
        elapsed_milliseconds -= 1000*60*2
        current_minute = math.ceil(elapsed_milliseconds/(1000*60))
        value = 49.152 * current_minute - 17.984
        return value < current_m1

    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        self._last_m1_update = 0
        self._last_m1_score = 0


    def _onScoreUpdate(self, message):
        """
        Callback when the intervention gets a score update
        """
        m1_score = message.scoreboard["TeamScore"]
        time = message.elapsed_milliseconds
        performant = self._isTeamDoingWell(time, m1_score)
        if time > 12 * 60 * 1000: # In the last five minutes, uppon victim evacuation
            if performant:
                self.logger.info(f"{self}: Team is performing well. Encouragement triggered!")
                self.queueForResolution()
            else:
                self.logger.info(f"{self}: Team is not doing well. Encouragement discarded!")
                self.discard()

    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self._message,
        }



class TeamEncouragementTrigger(InterventionTrigger):
    """
    Class that generates TeamWelcomeMessageInterventions.  Interventions are
    triggered at the start of a trial.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Register the trigger to receive FoV messages
        self.add_minecraft_callback(PlanningStageEvent, self.__onPlanningStageEvent)


    def __onPlanningStageEvent(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.PlanningStageEvent
            Received PlanningStageEvent message
        """
        self.logger.debug(f"{self}: Received PlanningStage Message")

        # Check to see if this is a Planning Stage end message
        if message.state == PlanningStageEvent.PlanningState.Stop:
            intervention = TeamEncouragementIntervention(self.manager, **self.kwargs)
            self.logger.info(f"{self}:  Spawning {intervention}.")
            self.manager.spawn(intervention)
