# -*- coding: utf-8 -*-
"""
.. module:: marker_place_after_triaging_victim
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for reminding the medic to place the
              correct marker block after triaging a victim

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to remind medic to put the correct marker block.

* Trigger:  Medic triages a victim

* Discard States:
    -  Medic places a marker block in proximity of victim
    -  Victim is picked up

* Resolve States: 
    -  Medic leaves proximity of the victim
    -  Medic triages another victim
    -  A given time interval elapses without discard / resolution
"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..mixins import ComplianceMixin, VictimMarkerMixin
from MinecraftBridge.messages import TriageEvent



class TriagedVictimMarkerIntervention(Intervention, VictimMarkerMixin):
    """
    An Intervention designed to alert the medic when they
    forget to place marker block after triaging a victim.
    """

    def __init__(self, manager, *args, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the intervention manager in charge of this intervention

        Keyword Arguments
        -----------------
        triage_event : TriageEvent
            Message that triggered this intervention
        distance_threshold : float, default=5
            Maximum distance from victim that a player can go
            before the victim is considered to have been "left"
        timeout : float, default=15
            Maximum mission time (in seconds) without a discard / resolution
            before queuing for resolution
        """
        Intervention.__init__(self, manager, **kwargs)
        VictimMarkerMixin.__init__(self, manager, **kwargs)

        self.kwargs = kwargs
        self.participant = manager.participants[self.kwargs.get('triage_event').participant_id]

        self.addRelevantParticipant(self.participant.participant_id)


    def _onVictimMarkerCallback(self, callback):
        """
        Default behavior for all VictimMarkerMixin callbacks unless they are overriden.
        """
        self.logger.info(f"{self}:  Triggered {callback.__name__}. Discarding intervention.")
        self.discard()


    def _onAnotherVictimTriaged(self):
        """
        Callback for when a different victim is triaged.
        """
        self.logger.info(f"{self}:  Player triaged another victim. Queuing intervention for resolution.")
        self.queueForResolution()


    def _onDistanceExceeded(self):
        """
        Callback for when player has left proximity of the victim.
        """
        self.logger.info(
            f"{self}:  Player has left victim proximity. Queuing intervention for resolution.")
        self.queueForResolution()


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without a discard or resolution.
        """
        self.logger.info(f"{self}:  Intervention has timed out. Queuing intervention for resolution.")
        self.queueForResolution()


    def getInterventionInformation(self):
        return {
            'default_recipients': [self.participant.id],
            'default_message': "Medic, remember to put a marker to show victim type after treaing to help your team evacuate correctly."
        }



class TriagedVictimMarkerTrigger(InterventionTrigger):
    """
    Trigger for a TriagedVictimMarkerIntervention.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        distance_threshold : float, default=5
            Maximum distance from victim that a player can go
            before the victim is considered to have been "left"
        timeout : float, default=15
            Maximum mission time (in seconds) without a discard / resolution
            before queuing for resolution
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Add Minecraft callback
        self.add_minecraft_callback(TriageEvent, self.__onTriageEvent)

        # Since this is triggered in IN_PROGRESS, don't spawn interventions for the same victims twice
        # While triaging, there will be multiple IN_PROGRESS messages and we don't want a new intervention for each
        self.active_interventions_per_victim = {}


    def __onTriageEvent(self, message):
        """
        Callback when an TriageEvent message is received.
        """

        # Make sure we don't already have an intervention for this particular victim
        if message.victim_id in self.active_interventions_per_victim.keys():
            # If we have one but it is already dealt with (as in, not active anymore), allow for another
            if not (self.active_interventions_per_victim[message.victim_id].state == Intervention.State.ACTIVE or
                self.active_interventions_per_victim[message.victim_id].state == Intervention.State.INITIALIZED):
                self.active_interventions_per_victim.pop(message.victim_id, None)
            # If another intervention instance is already dealing with victim, don't make a new one
            else:
                return

        if message.triage_state == TriageEvent.TriageState.IN_PROGRESS:
            intervention = TriagedVictimMarkerIntervention(
                self.manager,
                triage_event=message,
                **self.kwargs,
            )
            self.logger.info(f"{self}:  Spawning Intervention {intervention}")
            self.manager.spawn(intervention)
            self.active_interventions_per_victim[message.victim_id] = intervention



class TriagedVictimMarkerFollowup(Followup, ComplianceMixin, VictimMarkerMixin):
    """
    Followup to a TriagedVictimMarkerIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=10
            Number of seconds in the time period of follow up validation
        """

        duration = kwargs.get("duration", 10)

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)

        # Increase the distance threshold by two blocks to delay
        # intervention triggering until the player moves further away
        distance_threshold = intervention.kwargs.get('distance_threshold', 5) + 2
        VictimMarkerMixin.__init__(self, manager, **dict(
            intervention.kwargs, timeout=None, distance_threshold=distance_threshold))


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """
        self._onNonCompliance()


    def _onVictimMarkerCallback(self, callback):
        """
        Default behavior for all VictimMarkerMixin callbacks unless they are overriden.
        """
        self.logger.info(
            f"{self}:  Triggered {callback.__name__}. Recording noncompliance.")
        self._onNonCompliance()


    def _onCorrectMarkerPlaced(self, message):
        """
        Callback for when a correct marker block is placed near the victim.
        """
        self._onCompliance()


    def _onIncorrectMarkerPlaced(self, message):
        """
        Callback for when an incorrect marker block is placed near the victim.
        """
        from .correct_victim_misidentification import VictimMisidentificationIntervention
        new_intervention = VictimMisidentificationIntervention(self.manager, **self.intervention.kwargs)
        self.logger.info(
            f"{self}:  Player placed an incorrect marker. Spawning {new_intervention}.")
        self.manager.spawn(new_intervention)
        new_intervention._onIncorrectMarkerPlaced(message)
