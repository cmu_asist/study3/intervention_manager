# -*- coding: utf-8 -*-
"""
.. module:: ihmc_cognitive_load_suggest_clean_markers
	:platform: Linux, Windows, OSX
	:synopsis: Intervention and trigger for suggest the team clean stale markers
				  to decrease cognitive load

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger, Intervention, and Followup to suggest to the team they
remove stale markers to decrease cognitive load

TODO:  Need to migrate to using the agent's knowledge from the TToM model.
       Right now, it's using a rough data structure, as a matter of expediency.
"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

import MinecraftElements

from MinecraftBridge.messages import (
	IHMC_CognitiveLoad,
	PlanningStageEvent,	
	PlayerState,
	LocationEvent,
	VictimPickedUp,
	VictimPlaced   
)

from ..components import MarkerBlocksMonitor
from ..components import VictimsMonitor

from collections import defaultdict

import math

from ..utils import distance

from ..components import InterventionScheduler


################################################################################
################################################################################

# Some sets to simplify categorization of markers.  May be worth moving to 
# MinecraftElements

MARKER_REGULAR_VICTIM = { MinecraftElements.Block.red_regularvictim,
						  MinecraftElements.Block.blue_regularvictim,
						  MinecraftElements.Block.green_regularvictim
						}

MARKER_CRITICAL_VICTIM = { MinecraftElements.Block.red_criticalvictim,
						   MinecraftElements.Block.blue_criticalvictim,
						   MinecraftElements.Block.green_criticalvictim   
						 }

MARKER_NO_VICTIM = { MinecraftElements.Block.red_novictim,
					 MinecraftElements.Block.blue_novictim,
					 MinecraftElements.Block.green_novictim
				   }

MARKER_ABRASION = { MinecraftElements.Block.red_abrasion,
					MinecraftElements.Block.blue_abrasion,
					MinecraftElements.Block.green_abrasion
				  }

MARKER_BONE_DAMAGE = { MinecraftElements.Block.red_bonedamage,
					   MinecraftElements.Block.blue_bonedamage,
					   MinecraftElements.Block.green_bonedamage
					 }

MARKER_RUBBLE = { MinecraftElements.Block.red_rubble,
				  MinecraftElements.Block.green_rubble,
				  MinecraftElements.Block.blue_rubble
				}

MARKER_THREAT = { MinecraftElements.Block.red_threat,
				  MinecraftElements.Block.blue_threat,
				  MinecraftElements.Block.green_threat
				}

MARKER_SOS = { MinecraftElements.Block.red_sos,
			   MinecraftElements.Block.green_sos,
			   MinecraftElements.Block.blue_sos
			 }





################################################################################
################################################################################

class IHMC_CleanMarkersTrigger(InterventionTrigger):
	"""
	Class that spawns an IHMC_CleanMarkersIntervention.
	"""

	def __init__(self, manager, **kwargs):
		"""
		Arguments
		---------
		manager : InterventionManager
		"""

		InterventionTrigger.__init__(self, manager, **kwargs)

		# Start monitoring the location of victims and marker blocks
		self._victims_monitor = VictimsMonitor(self)
		self._marker_blocks_monitor = MarkerBlocksMonitor(self)

		# Callbacks for when to trigger
		self.add_minecraft_callback(PlanningStageEvent, self.__onPlanningStage)


	def __onPlanningStage(self, message):
		"""
		Arguments
		---------
		message : MinecraftBridge.messages.PlanningStageEvent
		"""

		if message.state == PlanningStageEvent.PlanningState.Stop:

			# Spawn the intervention
			intervention = IHMC_CleanMarkersIntervention(self.manager, 
				                                         self._victims_monitor,
				                                         self._marker_blocks_monitor,
				                                         parent=self)
			self.manager.spawn(intervention)





class IHMC_CleanMarkersIntervention(Intervention):
	"""
	Intervention that monitors the team cognitive load for excessive 
	"""

	def __init__(self, manager, victims_monitor, marker_blocks_monitor, **kwargs):
		"""
		Arguments
		---------
		manager : InterventionManager

		victims_monitor : VictimsMonitor
			Monitor used to determine the location and status of victims
		marker_blocks_monitor : MarkerBlocksMonitor
			Monitor used to determine the locaitons and types of marker blocks
		"""

		Intervention.__init__(self, manager, **kwargs)

		self.cognitive_load = kwargs.get("cognitive_load", None)
		self.cognitive_load_threshold = kwargs.get("cognitive_load_threshold", 1.5)
		self.smoothing_parameter = kwargs.get("smoothing_parameter", 0.8)
		self.minimum_number_blocks = kwargs.get("minimum_number_blocks", 30)
		self.correspondence_threshold = kwargs.get("correspondence_threshold", 0.6)

		self.victims_monitor = victims_monitor
		self.marker_blocks_monitor = marker_blocks_monitor
		self.semantic_map = self.manager.semantic_map

		# Parse the keyword arguments
		self._marker_victim_distance_threshold = kwargs.get("marker_victim_distance_threshold", 2)
		self._marker_door_distance_threshold = kwargs.get("marker_door_distance_threshold", 3)

		# Keep track of the location of the participants
		self._participant_locations = { p.participant_id: (0,0,0) for p in self.manager.participants }

		# Register needed callbacks
		self.add_minecraft_callback(PlayerState, self.__onPlayerState)
		self.add_minecraft_callback(IHMC_CognitiveLoad, self.__onCognitiveLoad)



	def __onPlayerState(self, message):
		"""
		Callback when a PlayerState message is received.  Used to update 
		participant locations

		Arguments
		---------
		message : MinecraftBridge.messages.PlayerState
		"""

		location = (math.floor(message.x), math.floor(message.y), math.floor(message.z))

		self._participant_locations[message.participant_id] = location


	def __onCognitiveLoad(self, message):
		"""
		Callback when a CognitiveLoad message is received.  Used to monitor and
		update the team's cognitive load

		Arguments
		---------
		message : MinecraftBridge.messages.IHMC_CognitiveLoad
		"""

		# If we haven't received a cognitive load yet, initialize the value
		if self.cognitive_load is None:
			self.cognitive_load = message.cognitive_load.value
			return

		self.cognitive_load = self.smoothing_parameter * self.cognitive_load + (1.0-self.smoothing_parameter) * message.cognitive_load.value

		# Did this cognitive load exceed the threshold?
		if message.cognitive_load.value >= self.cognitive_load_threshold * self.cognitive_load:
			self.logger.debug("%s:  Cognitive Load Exceeds Baseline (%f): %f", self, self.cognitive_load, message.cognitive_load.value)
		else:
			return

		# Examine the markers and see if correspondences can be extracted
		victim_correspondences = self.__get_marker_victim_correspondence().values()
		sos_correspondences = self.__get_sos_marker_correspondences().values()
		signal_correspondence = self.__get_signal_marker_correspondences().values()

		# Extract statistics
		total_victim_correspondences = len(victim_correspondences) + 0.0001 
		correct_victim_correspondences = len([c for c in victim_correspondences if c is not None])
		total_sos_correspondences = len(sos_correspondences) + 0.0001
		correct_sos_corresponces = len([c for c in sos_correspondences if c is not None])
		total_signal_correspondences = len(signal_correspondence) + 0.0001
		correct_signal_correspondences = len([c for c in signal_correspondence if c is not None])

		self.logger.debug("%s:  Victim Correspondence Rate: %f (%d / %d)", self, correct_victim_correspondences/total_victim_correspondences, correct_victim_correspondences, total_victim_correspondences)
		self.logger.debug("%s:  SOS Correspondence Rate:    %f (%d / %d)", self, correct_sos_corresponces/total_sos_correspondences, correct_sos_corresponces, total_sos_correspondences)
		self.logger.debug("%s:  Signal Correspondence Rate: %f (%d / %d)", self, correct_signal_correspondences/total_signal_correspondences, correct_signal_correspondences, total_signal_correspondences)
		self.logger.debug("%s:  =================================================", self)

		total_blocks = len(victim_correspondences) + len(sos_correspondences) + len(signal_correspondence)
		correct_blocks = correct_victim_correspondences + correct_sos_corresponces + correct_signal_correspondences

		# No intervention yet if the total blocks is below the threshold
		if total_blocks < self.minimum_number_blocks:
			return

		# Intervene if the ratio of correct to total blocks is below the 
		# threshold
		if correct_blocks / total_blocks < self.correspondence_threshold:
			self.queueForResolution()


	def getInterventionInformation(self):
		"""
		Provide a dictionary containing a information related to the
		intervention
		Return
		-------
		intervention_info : dictionary
		    Mapping of relevant intervention properties to needed information
		Required Fields
		---------------
		default_recipients : list of strings
		    List of participant_ids that the intervention should be sent to
		default_message : string
		    A (default) text representation of the intervention
		"""
		intervention_info = {
			"default_recipients": [ p.participant_id for p in self.manager.participants ],
			"default_message":  "Your team may be confused by the number of stale marker blocks.  You should assign someone to clean these up.",
		}
		return intervention_info	



	def __get_surrounding_area(self, location, _distance):
		"""
		Get all locations within a distance from a provided location.

		Arguments
		---------
		location : tuple
			(x,y,z) location to find surrounding area
		_distance : int
			distance from provided location

		Returns
		-------
		list of (x,y,z) locations within the distance of the location
		"""

		x,y,z = location 

		return [(x+dx, y, z+dz) for dx in range(-_distance, _distance+1)
										for dz in range(-_distance, _distance+1)
										if distance((0,0), (dx,dz)) <= _distance
										and (dx != 0 or dz != 0)]


	def __get_room_contents(self):
		"""
		Helper function to return a mapping of room name to whether the room
		contains (at least) a regular victim, critical victim, or no victim.
		"""

		room_contents = { room: [] for room in self.semantic_map.rooms.unique() }

		for victim_location, victim in self.victims_monitor.victims.items():

			room_id = self.semantic_map.rooms[victim_location]

			if room_id in room_contents:

				room_contents[room_id].append(victim)

		return room_contents


	def __get_marker_victim_correspondence(self, distance_threshold = 2):
		"""
		Helper function to return a mapping of marker block locations to Victim
		instances, based on the contents of the monitors.

		Arguments
		---------
		distance_threshold : int, default=2
			Maximum distance between a victim and marker block to consider that
			marker block as corresponding to the victim
		"""

		correspondences = {}

		for marker_location, marker_block in self.marker_blocks_monitor.marker_blocks.items():

			# Only consider marker blocks about victim types or status
			if marker_block in MARKER_REGULAR_VICTIM or marker_block in MARKER_CRITICAL_VICTIM or marker_block in MARKER_ABRASION or marker_block in MARKER_BONE_DAMAGE:

				# Initially assume no correspondence
				correspondences[marker_location] = None
				surrounding_area = self.__get_surrounding_area(marker_location, distance_threshold)

				for victim_location, victim in self.victims_monitor.victims.items():

					# Is the victim in the surrounding area?
					if victim_location in surrounding_area:

						# Check if the marker makes sense for the victim
						if marker_block in MARKER_REGULAR_VICTIM and victim.stabilized_state == "REGULAR":
							correspondences[marker_location] = victim

						if marker_block in MARKER_CRITICAL_VICTIM and victim.injury_type == "C":
							correspondences[marker_location] = victim

						if marker_block in MARKER_ABRASION and victim.injury_type == "A":
							correspondences[marker_location] = victim

						if marker_block in MARKER_BONE_DAMAGE and victim.injury_type == "B":
							correspondences[marker_location] = victim

		return correspondences


	def __get_sos_marker_correspondences(self):
		"""
		Helper function to return a mapping of SOS marker blocks to participant
		ID if the participant is (supposedly) trapped in a room.
		"""

		correspondences = {}

		for marker_location, marker_block in self.marker_blocks_monitor.marker_blocks.items():

			# Only consider SOS blocks
			if marker_block in MARKER_SOS:

				# Initially assume no correspondence
				correspondences[marker_location] = None

				# Which room is the block in (if any)?
				room_id = self.semantic_map.rooms[marker_location]

				# Is any of the participants in the room?  Then assume that the
				# SOS block corresponds to that participant.
				for participant_id, participant_location in self._participant_locations.items():
					if self.semantic_map.rooms[participant_location] == room_id:
						correspondences[marker_location] == participant_id

		return correspondences


	def __get_signal_marker_correspondences(self):
		"""
		Helper function to return a mapping of signal marker blocks to
		corresponding room names, if the contents of the room correspond to
		the marker signal.
		"""

		correspondences = {}

		# Get a list of room content
		room_content = self.__get_room_contents()

		for marker_location, marker_block in self.marker_blocks_monitor.marker_blocks.items():

			# Only consider markers that could correspond to signals, i.e.,
			# they are the correct type, and are not in a room
			correct_type = marker_block in MARKER_REGULAR_VICTIM or marker_block in MARKER_CRITICAL_VICTIM or marker_block in MARKER_NO_VICTIM
			in_room = self.semantic_map.rooms[marker_location] is not None

			if correct_type and in_room:

				correspondences[marker_location] = None

				# Which room is this marker associated with?
				closest_room = self.semantic_map.closest_room(marker_location)

				# Check to see if the marker type corresponds to the room
				# content.  If so, then put the room in the correspondence
				# dictionary, otherwise, put None.

				if marker_block in MARKER_NO_VICTIM:

					if room_content.get(closest_room, []) == []:
						correspondences[marker_location] = closest_room
					else:
						correspondences[marker_location] = None

				if marker_block in MARKER_CRITICAL_VICTIM:

					correspondences[marker_location] = None 

					for victim in room_content.get(closest_room, []):
						if victim.injury_type == "C":
							correspondences[marker_location] = closest_room

				if marker_block in MARKER_REGULAR_VICTIM:

					victim_injuries = [v.injury_type for v in room_content.get(closest_room, [])]

					if len(victim_injuries) == 0 or "C" in victim_injuries:
						correspondences[marker_location] = None
					else:
						correspondences[marker_location] = closest_room

		return correspondences
