# -*- coding: utf-8 -*-
"""
.. module:: team_welcome_message
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for introductory mmessage for team

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to introduce the ATLAS agent to the team.

* Trigger: The intervention is triggered at the start of the misison

* Discard States:  N/A

* Resolve State:  The intervention is immediately queued for a resolution

"""

from .. import Intervention
from .. import InterventionTrigger

from MinecraftBridge.messages import PlanningStageEvent



class TeamWelcomeMessageIntervention(Intervention):
    """
    A TeamWelcomeMessageIntervention is an Intervention which simply presents
    a welcome message to all participants in the team.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """
        Intervention.__init__(self, manager, **kwargs)
        self._message = kwargs.get('message', "The game is afoot!")

        # Indicate that all participants are relevant to this intervention
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.participant_id)        


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self._message,
        }



class TeamWelcomeMessageTrigger(InterventionTrigger):
    """
    Class that generates TeamWelcomeMessageInterventions.  Interventions are
    triggered at the start of a trial.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Register the trigger to receive FoV messages
        self.add_minecraft_callback(PlanningStageEvent, self.__onPlanningStageEvent)


    def __onPlanningStageEvent(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.PlanningStageEvent
            Received PlanningStageEvent message
        """
        self.logger.debug(f"{self}: Received PlanningStage Message")

        # Check to see if this is a Planning Stage end message
        if message.state == PlanningStageEvent.PlanningState.Start:
            intervention = TeamWelcomeMessageIntervention(self.manager, **self.kwargs)
            self.logger.info(f"{self}:  Spawning {intervention}.")
            self.manager.spawn(intervention)
