# -*- coding: utf-8 -*-
"""
.. module:: encourage_evacuate_critical
   :platform: Linux, Windows, OSX
   :synopsis: This intervention will monitor the ratio of critical and
              regular victims, and suggest the team to focus on critical
              victims if they appear to be ignoring the critical ones

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

* Trigger:  Given a minimum number of total evacuated victims, the ratio
            of regular-to-critical exceeds some threshold.

* Discard States:  N/A

* Resolve State:  Immediately queued for resolution

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..mixins import ComplianceMixin
from ..utils import VictimMonitor



class EvacuateCriticalVictimsIntervention(Intervention):
    """
    Intervention designed to encourage team to focus more on
    evacuating critical victims.
    """

    def __init__(self, manager, victim_monitor, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        victim_monitor : VictimMonitor
            Monitor for triaged / evacuated victims
        """
        Intervention.__init__(self, manager, **kwargs)
        self.victim_monitor = victim_monitor.copy(self)

        # All of the participants are relevant
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.participant_id)


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        self.queueForResolution()


    def getInterventionInformation(self):
        message = (
            "Team, you seem to be neglecting high-value critical victims. "
            "Rescuing more critical victims would likely result in a higher score."
        )
        return {
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': message,
        }



class EvacuateCriticalVictimsTrigger(InterventionTrigger):
    """
    Trigger that spawns EvacuateCriticalVictimsIntervention instances.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        min_ratio : float, default=1.5
            Minimum ratio of regular-to-critical evacuated victims needed
            to trigger an intervention
        min_evacuated : int, default=5
            Minimum number of evacuated victims needed to trigger an intervention
        """
        InterventionTrigger.__init__(self, manager, **kwargs)

        # Trigger-specific attributes
        self._min_ratio = kwargs.get('min_ratio', 1.5)
        self._min_evacuated = kwargs.get('min_evacuated', 5)
        self._victim_monitor = VictimMonitor(self, on_evacuation=self._on_evacuation)
        self._has_triggered = False


    def spawn_intervention(self):
        if not self._has_triggered: # only triggers once
            intervention = EvacuateCriticalVictimsIntervention(self.manager, self._victim_monitor)
            self.logger.info(
                f"{self}: Team not evacuating enough critical victims. Spawning {intervention}")
            self.manager.spawn(intervention)
            self._has_triggered = True


    def _on_evacuation(self, msg):
        total = len(self._victim_monitor.evacuated_victim_ids)
        ratio = self._victim_monitor.evacuated_ratio()
        if total >= self._min_evacuated and ratio >= self._min_ratio:
            self.spawn_intervention()



class EvacuateCriticalVictimsFollowup(Followup, ComplianceMixin):
    """
    Followup to an EvacuateCriticalVictimsIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=90
            The duration of the followup validation period (in seconds)
        """

        duration = kwargs.get("duration", 90)

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)

        # Copy victim monitor from intervention
        self._victim_monitor = intervention.victim_monitor.copy(self)
        self._initial_ratio = self._victim_monitor.evacuated_ratio()


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """
        if self._victim_monitor.evacuated_ratio() < self._initial_ratio:
            self._onCompliance()
        else:
            self._onNonCompliance()



class EvacuateCriticalVictimsIndividualComplianceFollowup(Followup, ComplianceMixin):
    """
    Followup to determine if an individual has complied with the advice from an
    EvacuateCriticalVictimsIntervention.  An individual is considered compliant
    if they place a critical victim in an evacuation region.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=90
            The duration of the followup validation period (in seconds)
        """

        duration = kwargs.get("duration", 90)

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)

        # List of events relevant to this followup
        self._history = []

        # Add callback to handle when a victim is evacuated
        self.add_minecraft_callback(VictimEvacuated, self.__onVictimEvacuated)


    def _onTimeout(self):
        """
        If the no one on the team has evacuated a victim within the alloted
        duration, then consider this participant non-compliant.
        """

        # If the followup monitoring time has exceeded the monitoring duration
        # and no participants have evacuated a victim, then mark this 
        # participant as non-compliant.  Otherwise, simply retain the
        # compliance of the participant as "N/A", as someone on the team has
        # evacuated a critical victim, and the participant may not have taken
        # that role.

        if len(self._history) == 0:
            self._onNonCompliance()
        else:
            self.complete()


    def _victim_evacuated(self, message):
        """
        Callback from linked followups to indicate that a victim has been
        evacuated by another participant.  The followup should record this in
        its history, but still monitor the participant to see if they comply as
        well.

        NOTE:  This reasoning assumes that the player is not evacuating critical
               victims because they were made aware that other team member(s)
               would evacuate victims.  This may need to be revisited.

        Arguments
        ---------
        message : MinecraftBridge.message.VictimEvacuated
            VictimEvacuated message for critical victim
        """

        self._history.append(message)


    def __onVictimEvacuated(self, message):
        """
        Check if the participant being monitored is the one which evacuated the
        victim, and if so, mark the participant as complying with the advice and
        inform any linked followups.

        Arguments
        ---------
        message : MinecraftBridge.messages.VictimEvacuated
            VictimEvacuated message
        """

        # Record that the participant complied with the advice, and inform any
        # other linked followups of the message
        if message.participant_id == self.target_participant and message.type == 'victim_saved_c':
            self._onCompliance()
            for followup in self._linked_followups():
                followup._victim_evacuated(message)

            # Record this message to indicate that it was responsible for the
            # change of state in this followup
            self._history.append(message)



class EvacuateCriticalVictimsTeamComplianceFollowup(Followup, ComplianceMixin):
    """
    Followup to determine if an individual has complied with the advice from an
    EvacuateCriticalVictimsIntervention.  An individual is considered compliant
    if they place a critical victim in an evacuation region.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=90
            The duration of the followup validation period (in seconds)
        """

        duration = kwargs.get("duration", 90)

        self._history = []

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)


    def _onTimeout(self):
        """
        If no participants have evacuated a victim, then the team should be
        considered non-compliant
        """

        if len(self._history) == 0:
            self._onNonCompliance()
        else:
            self._onCompliance()

            # TODO:  1) should compliance be recorded at _victim_evacuated, and
            #        2) should a second intervention be issued if the ratio 
            #        of critical to regular victims still exceeds a threshold?


    def _victim_evacuated(self, message):
        """
        Callback when one of the team members has evacuated a critical victim

        Arguments
        ---------
        message : MinecraftBridge.messages.VictimEvacuated
        """

        self._history.append(message)
