# -*- coding: utf-8 -*-
"""
.. module:: transporter_early_strategy
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for transporter_early_strategy

.. moduleauthor:: Simon Stepputtis <stepputtis@cmu.edu>

This intervention intends to suggests the transporter to mark rooms at the beginning of the game,
if they enter a room instead. While there are general strategy interventions for the transporter, 
they usually need some time to observe the team. This intervention is rather weak, 
and only active for the first 90 seconds of the game.

"""

import re

from .. import Intervention
from .. import InterventionTrigger

from MinecraftBridge.messages import PlanningStageEvent, LocationEvent
from ..components import InterventionScheduler as Scheduler

class TransporterEarlyStrategyIntervention(Intervention):
    """
    A TransporterEarlyStrategyIntervention is an Intervention which suggests a
    strategy for the transporter in the early stages of the game
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        timeout : int
            Timeout in seconds describing for how long the intervention should be active
        """
        Intervention.__init__(self, manager, **kwargs)
        self._message = kwargs.get('message', "No message for <TransporterEarlyStrategyIntervention>")
        self._active_time = kwargs.get('active_time', 180)
        self._participants = [self.manager.participants['Transport_Specialist'].id]

        # Create a timeout for this intervention to only cover the start of the mission
        self._discarded = False
        self._scheduler = Scheduler(self)
        self._scheduler.wait(self._onDeactivate, delay=self._active_time)

        # Subscribe to the location events
        self.add_minecraft_callback(LocationEvent, self.__onLocationEvent)


    def __onLocationEvent(self, message):
        """
        Callbaclk when the player changes their location
        """
        # Check if this intervention is done
        if self._discarded:
            return

        # Make sure this is the player we are interested in
        if message.playername != self._participants[0]:
            return

        # Check the rooms:
        for room in message.locations:
            room_id = room["id"]
            # If we see the transporter enters a room, issue the intervention
            if re.match(r"^[A-Z][0-9][A-Z]?$", room_id):
                self.queueForResolution()


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        """
        pass
    
    def _onDeactivate(self):
        """
        When the timeout is reached, this intervention will be deactivated
        """
        self._discarded = True
        self.logger.info(
                "%s: Transporter early strategy advice was not necessary. Discarding",
                self
            )

        self.discard()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': self._participants,
            'default_message': self._message,
        }



class TransporterEarlyStrategyTrigger(InterventionTrigger):
    """
    Class that generates TransporterEarlyStrategyIntervention. 
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Register the trigger to receive FoV messages
        self.add_minecraft_callback(PlanningStageEvent, self.__onPlanningStageEvent)


    def __onPlanningStageEvent(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.PlanningStageEvent
            Received PlanningStageEvent message
        """
        self.logger.debug(f"{self}: Received PlanningStage Message")

        # Check to see if this is a Planning Stage end message
        if message.state == PlanningStageEvent.PlanningState.Stop:
            intervention = TransporterEarlyStrategyIntervention(self.manager, **self.kwargs)
            self.logger.info(f"{self}:  Spawning {intervention}.")
            self.manager.spawn(intervention)
