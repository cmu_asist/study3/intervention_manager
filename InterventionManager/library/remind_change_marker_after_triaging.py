# -*- coding: utf-8 -*-
"""
.. module:: remind_change_marker_after_triaging
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for reminding medic to remove
              "regular victim" markers placed by teammates after triaging

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

* Trigger:  Medic triages a regular victim in a room that has been marked "regular victim"

* Discard States:  Medic removes the old marker

* Resolve State:  Medic leaves proximity of victim room without removing old marker

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..mixins import ComplianceMixin, DistanceMixin
from MinecraftBridge.messages import (
    MarkerDestroyedEvent, MarkerPlacedEvent, MarkerRemovedEvent, TriageEvent,
)



class RemindChangeMarkerIntervention(Intervention, DistanceMixin):
    """
    Trigger for a RemindChangeMarkerIntervention.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        player : string
            Key for the relevant player (e.g. participant ID, playername, etc)
        location : string
            Room ID for victim room
        distance_threshold : float, default=5
            Maximum distance from room to marker block
        """
        Intervention.__init__(self, manager, **kwargs)
        DistanceMixin.__init__(self, manager, **kwargs, timeout=None)

        self.kwargs = kwargs
        self.participant = manager.participants[kwargs.get('player')]
        self.victim_room = kwargs.get('location')

        self.addRelevantParticipant(self.participant.participant_id)

        # Add Minecraft callbacks
        self.add_minecraft_callback(MarkerRemovedEvent, self.__onMarkerRemoved)
        self.add_minecraft_callback(MarkerDestroyedEvent, self.__onMarkerRemoved)


    def _onDistanceExceeded(self):
        """
        Callback for when the player has exceeded the distance threshold.
        """
        self.logger.info(f"{self}:  Medic has left proximity of victim room. Queuing for resolution.")
        self.queueForResolution()


    def __onMarkerRemoved(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.MarkerRemovedEvent
            Received MarkerRemovedEvent message
        """
        if 'regular' in message.type.name:
            if self.manager.semantic_map.closest_room(message.location) == self.victim_room:
                self.logger.info(f"{self}:  Medic removed old marker block. Discarding.")
                self.discard()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        message = (
            "Medic, after triaging, "
            "remember to remove 'regular victim' marker blocks left by your teammates."
        )
        return {
            'default_recipients': [self.participant.id],
            'default_message': message            
        }



class RemindChangeMarkerTrigger(InterventionTrigger):
    """
    Trigger for a RemindChangeMarkerIntervention.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        distance_threshold : float, default=5
            Maximum distance from room to marker block
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Set attributes
        self._rooms_marked_regular = set()
        self._triaged_rooms = set()

        # Add Minecraft callbacks
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onMarkerPlaced)
        self.add_minecraft_callback(TriageEvent, self.__onTriageEvent)


    def __onTriageEvent(self, message):
        """
        Callback when an TriageEvent message is received.
        """

        # Ignore critical victims
        if message.type == 'victim_c':
            return

        # Ignore rooms already triaged in or that haven't previously been marked
        room = self.manager.semantic_map.rooms[message.victim_location]
        if room in self._triaged_rooms or room not in self._rooms_marked_regular:
            return

        # Spawn a RemindChangeMarkerIntervention
        if message.triage_state == TriageEvent.TriageState.SUCCESSFUL:
            self._triaged_rooms.add(room)
            intervention = RemindChangeMarkerIntervention(
                self.manager,
                player=message.participant_id,
                location=self.manager.semantic_map.rooms[message.victim_location],
                **self.kwargs,
            )
            self.logger.info(f"{self}:  Spawning Intervention {intervention}")
            self.manager.spawn(intervention)


    def __onMarkerPlaced(self, message):
        """
        Callback when an MarkerPlacedEvent message is received.
        """
        if 'regular' in message.type.name:
            room = self.manager.semantic_map.closest_room(
                message.location, exclude={None, *self.manager.semantic_map.hallways})
            self._rooms_marked_regular.add(room)



class RemindChangeMarkerFollowup(Followup, ComplianceMixin):
    """
    Followup to a RemindChangeMarkerIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=10
            Number of seconds in the time period of follow up validation
        """

        duration = kwargs.get("duration", 10)

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """
        self._onNonCompliance()


    def __onMarkerRemoved(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.MarkerRemovedEvent
            Received MarkerRemovedEvent message
        """
        if 'regular' in message.type.name:
            if self.semantic_map.closest_room(message.location) == self.intervention.victim_room:
                self._onCompliance()
