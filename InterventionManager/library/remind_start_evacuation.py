# -*- coding: utf-8 -*-
"""
.. module:: remind_start_evacuation
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger that reminds the team
              to start evacuating triaged victims

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to remind the team to start evacuating triaged victims.
"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..components import InterventionScheduler as Scheduler
from ..mixins import ComplianceMixin

from MinecraftBridge.messages import TriageEvent, VictimEvacuated, VictimPickedUp, VictimPlaced



class TriageMonitor:
    """
    Object that monitors triage victim locations and estimates
    of how long it would take to evacuate them.
    """

    def __init__(self, component, **kwargs):
        """
        Arguments
        ---------
        component : BaseComponent
            The Intervention manager component to connect this monitor to

        Keyword Arguments
        -----------------
        speed : float, default=3.75
            Average player speed (Minecraft block-lengths per second)
        """

        # Set attributes
        self._component = component
        self._speed = kwargs.get('speed', 3.75)
        self._dist_func = component.manager.semantic_map.distance
        self._triaged_victim_locations = {}
        self._triaged_victim_types = {}
        self._evacuation_zones = {
            'victim_a': ['taan', 'taas'],
            'victim_b': ['tabn', 'tabs'],
            'victim_c': ['tacn', 'tacs'],
            'victim_saved_a': ['taan', 'taas'],
            'victim_saved_b': ['tabn', 'tabs'],
            'victim_saved_c': ['tacn', 'tacs'],
        }

        # Add Minecraft callbacks through component
        component.add_minecraft_callback(TriageEvent, self._set_victim_location)
        component.add_minecraft_callback(VictimEvacuated, self._remove_victim_location)
        component.add_minecraft_callback(VictimPickedUp, self._remove_victim_location)
        component.add_minecraft_callback(VictimPlaced, self._set_victim_location)


    def copy(self, component, **kwargs):
        tm = TriageMonitor(component, **kwargs)
        tm._triaged_victim_locations = self._triaged_victim_locations
        tm._triaged_victim_types = self._triaged_victim_types
        return tm


    def evacuation_time_estimate(self):
        """
        Return a time estimate on the time required to evacuate all triaged victims.
        """
        num_players = len(self._component.manager.participants)
        return self._evacuation_distance_upper_bound() / num_players / self._speed


    def _evacuation_distance_upper_bound(self):
        """
        Return an upper bound on the minimum total travel distance required
        to evacuate all triaged victims.
        """
        dist = 0
        for victim_id, victim_location in self._triaged_victim_locations.items():
            # Calculate max distance from (any) evacuation zone 
            # (i.e. distance traveled before victim pickup)
            all_zones = sum(self._evacuation_zones.values(), [])
            dist += max(self._dist_func(victim_location, zone) for zone in all_zones)

            # Calculate distance to nearest (correct) evacuation zone
            # (i.e. distance traveled from picking up to placing down the victim)
            victim_type = self._triaged_victim_types[victim_id]
            victim_zones = self._evacuation_zones[victim_type]
            dist += min(self._dist_func(victim_location, zone) for zone in victim_zones)

        return dist


    def _set_victim_location(self, message):
        """
        Arguments
        ---------
        message : TriageEvent or VictimPlaced
            Instance of received message
        """

        # Ignore VictimPlaced events for victims that have not been triaged
        if isinstance(message, VictimPlaced):
            if message.victim_id not in self._triaged_victim_types.keys():
                return

        location = (message.victim_x, message.victim_y, message.victim_z)
        self._triaged_victim_locations[message.victim_id] = location
        self._triaged_victim_types[message.victim_id] = message.type


    def _remove_victim_location(self, message):
        """
        Arguments
        ---------
        message : VictimEvacuated or VictimPickedUp
            Instance of received message
        """
        self._triaged_victim_locations.pop(message.victim_id, None)



class StartEvacuationIntervention(Intervention):
    """
    Intervention designed to encourage team to start evacuating triaged victims.
    """

    def __init__(self, manager, triage_monitor, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        triage_monitor : TriageMonitor
            Monitor for triaged victims
        """
        Intervention.__init__(self, manager, **kwargs)
        self.triage_monitor = triage_monitor.copy(self)

        # Indicate that all participants are relevant to this intervention
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.participant_id)        


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        self.queueForResolution()


    def getInterventionInformation(self):
        return {
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': "Team, please start evacuating all the triaged victims on the map, you don't have much time left."            
        }



class StartEvacuationTrigger(InterventionTrigger):
    """
    Trigger that spawns StartEvacuationIntervention instances.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        speed : float, default=3.75
            Average player speed (Minecraft block-lengths per second)
        time_buffer : float, default=15
            Maximum number of seconds of extra time the team has to evacuate 
            all triaged victims before triggering an intervention
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Trigger-specific attributes
        self._triage_monitor = TriageMonitor(self, **kwargs)
        self._time_buffer = kwargs.get('time_buffer', 15)
        self._has_triggered = False

        # Schedule triage monitor checks
        self._scheduler = Scheduler(self)
        self._scheduler.repeat(self._check_triage_monitor, interval=15)


    def spawn_intervention(self):
        if not self._has_triggered:
            intervention = StartEvacuationIntervention(self.manager, self._triage_monitor)
            self.logger.info(
                "%s:  %s %s %s",
                self,
                f"Evacuation time estimate is {self._triage_monitor.evacuation_time_estimate()},",
                f"and there are {self._scheduler.time_remaining} seconds remaining.",
                f"Spawning {intervention}.",
            )
            self.manager.spawn(intervention)
            self._has_triggered = True


    def _check_triage_monitor(self):
        """
        Check the triage monitor to decide whether to spawn an intervention.
        """

        # Check that we are over halfway through the mission
        if self._scheduler.time >= self._scheduler.time_remaining:
            # Calculate the time required to evacuate all triaged victims
            time_required = self._triage_monitor.evacuation_time_estimate()

            # Trigger an intervention if the amount of extra time is less than
            # our time buffer
            if self._scheduler.time_remaining - time_required <= self._time_buffer:
                self.spawn_intervention()
                self._scheduler.reset()



class StartEvacuationFollowup(Followup, ComplianceMixin):
    """
    Followup to an StartEvacuationIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=60
            The duration of the followup validation period (in seconds)
        """

        duration = kwargs.get("duration", 60)

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)

        # Initialize triage monitor
        self._triage_monitor = intervention.triage_monitor.copy(self)
        self._initial_time_estimate = self._triage_monitor.evacuation_time_estimate()


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """

        # Check if the evacuation time estimate has gone down
        # (i.e. has the team triaged new victims without evacuating old ones?)
        if self._triage_monitor.evacuation_time_estimate() < self._initial_time_estimate:
            self._onCompliance()
        else:
            self._onNonCompliance()



class StartEvacuationIndividualComplianceFollowup(Followup, ComplianceMixin):
    """
    Followup to determine if an individual complied with advice from a
    StartEvacuationIntervention.  Individuals are considered compliant if they
    are responsible for evacuating a victim within a certain time period.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=60
            The duration of the followup validation period (in seconds)
        """

        duration = kwargs.get("duration", 60)

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)

        self._history = []

        # Add callback for VictimEvacuated messages
        self.add_minecraft_callback(VictimEvacuated, self.__onVictimEvacuated)


    def participant_evacuated_victim(self, participant_id):
        """
        Callback from a linked followup when a participant has evacuated a victim.
        """

        pass


    def __onVictimEvacuated(self, message):
        """
        Callback when a VictimEvacuated message is received.  Check to see if
        participant that evacuated the victim is the target participant of the
        followup, and record compliance if so.

        Arguments
        ---------
        message : VictimEvacuated
        """

        if message.participant_id == self._target_participant:
            self._history.append(message)

            # Inform the linked followups that this participant evacuated a 
            # victim
            for followup in self._linked_followups:
                followup.participant_evacuated_victim(self._target_participant)


    def _onTimeout(self):
        """
        If the participant has not evacuated a single victim within the given
        timeframe.
        """

        if len(self._history) > 0:
            self._onCompliance()


class StartEvacuationTeamComplianceFollowup(Followup, ComplianceMixin):
    """
    Followup to determine if a team complied with advice from a
    StartEvacuationIntervention.  Teams are considered compliant if the number
    of participants who complied individually and the total number of evacuated
    victims exceeds some threshold.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=60
            The duration of the followup validation period (in seconds)
        min_number_participants : int
            The minimum number of participants contributing to evacuation to 
            consider the team compliant
        """

        duration = kwargs.get("duration", 60)

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)

        self._compliant_participants = set()

        self._min_number_participants = kwargs.get('min_number_participants', 2)



    def participant_evacuated_victim(self, participant_id):
        """
        Callback from a linked followup when a participant has evacuated a victim.
        """

        self._compliant_participants.add(participant_id)


    def _onTimeout(self):
        """
        If the participant has not evacuated a single victim within the given
        timeframe, consider them non-compliant
        """

        if len(self._compliant_participants) >= self._min_number_participants:
            self._onCompliance()
        else:
            self._onNonCompliance()
