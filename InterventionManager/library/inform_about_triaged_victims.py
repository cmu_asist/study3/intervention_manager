# -*- coding: utf-8 -*-
"""
.. module:: inform_about_triaged_victims
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for informing the engineer or transporter
              about a nearby room containing triaged victims.

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to inform the engineer or transporter of nearby
rooms containing triaged victims.

* Trigger:  Engineer or transporter passes a new room containing triaged victims

* Discard States:
    -  Player enters victim room
    -  Player is aware room has triaged victims (according to ToM)

* Resolve States: 
    -  Player leaves proximity of victim room without awareness of triaged
       victims (according to ToM)
    -  A given time interval elapses without player awareness of triaged
       victims (according to ToM)
"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..components import InterventionScheduler as Scheduler
from ..mixins import BeliefMixin, ComplianceMixin, DistanceMixin
from ..utils import VictimMonitor

from collections import defaultdict
from MinecraftBridge.messages import LocationEvent, PlayerState



class InformAboutTriagedVictimsIntervention(Intervention, BeliefMixin, DistanceMixin):
    """
    Intervention designed to inform the engineer or transporter of nearby
    rooms containing triaged victims.
    """

    def __init__(self, manager, participant_id, room, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        participant_id : string
            Participant ID of the intended recipient of this intervention
        room : string
            ID for room containing triaged victims

        Keyword Arguments
        -----------------
        distance_threshold : float, default=5
            Threshold distance for a room to be considered "left"
        timeout : float, default=15
            Maximum mission time (in seconds) without a discard before checking
            teammates knowledge to determine whether or not to resolve
        redis_timeout : float, default=1.0
            Timeout for a Redis request on the internal bridge
        belief_threshold : float, default=0.99
            The minimum number of expected triaged victims in a room from the belief state
            that is considered being "aware"
        """
        Intervention.__init__(self, manager, **kwargs)
        BeliefMixin.__init__(self, manager, **kwargs)
        DistanceMixin.__init__(self, manager, player=participant_id, location=room, **kwargs)

        self.kwargs = kwargs
        
        # Set attributes
        self.participant = manager.participants[participant_id]
        self.room = room

        self.addRelevantParticipant(self.participant.participant_id)

        # Add Minecraft callback
        self.add_minecraft_callback(LocationEvent, self.__onLocationEvent)


    def _onDistanceExceeded(self):
        """
        Callback for when the player has exceeded the distance threshold.
        """
        self.logger.info(f"{self}:  Player left proximity of triaged victim room.")
        self._check_awareness_of_triaged_victims()


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without a discard or resolution.
        """
        self.logger.info(f"{self}:  Intervention timed out.")
        self._check_awareness_of_triaged_victims()


    def _check_awareness_of_triaged_victims(self):
        """
        Determine whether to resolve or discard based on player's awareness that
        the room contains triaged victims.
        """

        # Queue for resolution if player is unaware, otherwise discard.
        if self.believe(self.participant, 'treated_victims', self.room):
            self.logger.info(
                "%s:  %s %s",
                self,
                "According to ToM, Player is aware of triaged victim.",
                "Discarding intervention.",
            )
            self.discard()
        else:
            self.logger.info(
                "%s:  %s %s",
                self,
                "According to ToM, Player is still unaware of triaged victim.",
                "Queuing for resolution.",
            )
            self.queueForResolution()


    def __onLocationEvent(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.LocationEvent
            Received LocationEvent message
        """
        if self.manager.participants[message.participant_id] == self.participant:
            locations = set(loc['id'].rsplit('_', 1)[0] for loc in message.locations)
            if self.room in locations:
                self.logger.info(f"{self}:  Player entered triaged victim room. Discarding intervention.")
                self.discard()


    def getInterventionInformation(self):
        message = f"{self.participant.callsign}, there is a victim nearby that can be moved to the evacuation point.  It is visible on your map."
        return {
            'default_recipients': [self.participant.id],
            'default_message': message,
        }



class InformAboutTriagedVictimsTrigger(InterventionTrigger):
    """
    Trigger that spawns InformAboutTriagedVictimsIntervention instances.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        distance_threshold : float, default=5
            Threshold distance for a room to be considered "approached" or "left"
        timeout : float, default=15
            Maximum mission time (in seconds) without a discard before checking
            teammates knowledge to determine whether or not to resolve
        redis_timeout : float, default=1.0
            Timeout for a Redis request on the internal bridge
        belief_threshold : float, default=0.99
            The minimum number of expected triaged victims in a room from the belief state
            that is considered being "aware"
        """
        InterventionTrigger.__init__(self, manager, **kwargs)

        # Trigger-specific attributes
        self._distance_threshold = kwargs.get('distance_threshold', 5)
        self._player_locations = {} # indexed by participant ID
        self._approached_triaged_rooms = defaultdict(set) # indexed by participant ID
        self._victim_monitor = VictimMonitor(self)

        # Add Minecraft callback
        self.add_minecraft_callback(PlayerState, self.__onPlayerState)

        # Schedule proximity checks
        self._scheduler = Scheduler(self)
        self._scheduler.repeat(self._check_proximity_to_triaged_victim_rooms, interval=1)


    def spawn_intervention(self, participant_id, room):
        intervention = InformAboutTriagedVictimsIntervention(self.manager, participant_id, room)
        self.logger.info(
            f"{self}: {participant_id} passed room with triaged victims. Spawning {intervention}")
        self.manager.spawn(intervention)


    def _check_proximity_to_triaged_victim_rooms(self):
        """
        Check if any players are near rooms with triaged victims.
        """

        # Get the set of rooms containing unevacuated triaged victims
        triaged_locations = set(
            self._victim_monitor.victim_locations[victim_id]
            for victim_id in self._victim_monitor.triaged_victim_ids
            if victim_id not in self._victim_monitor.evacuated_victim_ids)
        triaged_locations.discard(None)
        triaged_rooms = set(
            self.manager.semantic_map.rooms[victim_location]
            for victim_location in triaged_locations)
        triaged_rooms.discard(None)

        # Check if players are close to any *new* rooms with triaged victims
        for player, player_location in self._player_locations.items():
            for room in triaged_rooms:
                if room not in self._approached_triaged_rooms[player]:
                    dist = self.manager.semantic_map.distance(player_location, room)
                    if dist <= self._distance_threshold:
                        self._approached_triaged_rooms[player].add(room)
                        self.spawn_intervention(player, room)


    def __onPlayerState(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.PlayerState
            Received PlayerState message
        """
        if message.elapsed_milliseconds <= 0:
            return

        # Ignore PlayerState messages for the medic
        participant = self.manager.participants[message.participant_id]
        if participant is None or participant.role in {None, 'Medical_Specialist'}:
            return

        # Update player location
        self._player_locations[message.participant_id] = message.position


class InformAboutTriagedVictimsFollowup(Followup, BeliefMixin, ComplianceMixin):
    """
    Follow up for TeamSawVictimMarkerIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=60
            Number of seconds in the time period of follow up validation
        """

        duration = kwargs.get("duration", 60)

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)
        BeliefMixin.__init__(self, manager, **intervention.kwargs)
        self.participant = intervention.participant
        self.room = intervention.room

    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """
        if self.believe(self.participant, 'treated_victims', self.room):
            self._onCompliance()
        else:
            self._onNonCompliance()