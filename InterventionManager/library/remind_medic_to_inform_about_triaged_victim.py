# -*- coding: utf-8 -*-
"""
.. module:: remind_medic_to_inform_about_triaged_victim
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for reminding the medic to inform all
              teammates about the location of a triaged victim.

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to ask the medic to inform all teammates about the
location of a triaged victim.

* Trigger:  Medic triages a victim

* Discard States:
    -  Medic places a marker block in proximity of victim
    -  Victim is picked up
    -  Teammates become aware of the victim (according to ToM)

* Resolve States: 
    -  A given time interval elapses without discard
"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..mixins import BeliefMixin, ComplianceMixin, VictimMarkerMixin
from MinecraftBridge.messages import TriageEvent



class RemindMedicToInformAboutTriagedVictimIntervention(Intervention, BeliefMixin, VictimMarkerMixin):
    """
    An Intervention designed to remind the medic to inform all teammates
    about the location of a triaged victim.
    """

    def __init__(self, manager, *args, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the intervention manager in charge of this intervention

        Keyword Arguments
        -----------------
        triage_event : TriageEvent
            Message that triggered this intervention
        timeout : float, default=15
            Maximum mission time (in seconds) without a discard before checking
            teammates knowledge to determine whether or not to resolve
        redis_timeout : float, default=1.0
            Timeout for a Redis request on the internal bridge
        belief_threshold : float, default=0.99
            The minimum number of expected victims in the belief state of the
            triaged victim type that is considered being "aware"
        """
        Intervention.__init__(self, manager, **kwargs)
        BeliefMixin.__init__(self, manager, **kwargs)
        VictimMarkerMixin.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Set attributes
        triage_event = kwargs.get('triage_event')
        self.participant = manager.participants[triage_event.participant_id]
        self.victim_type = triage_event.type
        self.victim_room = manager.semantic_map.rooms[triage_event.victim_location]

        self.addRelevantParticipant(self.participant.participant_id)        


    def _onVictimMarkerCallback(self, callback):
        """
        Default behavior for all VictimMarkerMixin callbacks unless they are overriden.
        """
        discard_states = {'_onCorrectMarkerPlaced', '_onIncorrectMarkerPlaced', '_onVictimPickedUp'}
        if callback.__name__ in discard_states:
            self.logger.info(f"{self}:  Triggered {callback.__name__}. Discarding intervention.")
            self.discard()


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without a discard or resolution.
        """
        teammates = [p for p in self.manager.participants if p != self.participant]
        if self.believe(teammates, 'treated_victims', self.victim_room):
            self.logger.info(
                "%s:  %s %s",
                self,
                "According to ToM, Teammates are aware of triaged victim.",
                "Discarding intervention.",
            )
            self.discard()
        else:
            self.logger.info(
                f"{self}:  Intervention has timed out. Queuing intervention for resolution.")
            self.queueForResolution()


    def getInterventionInformation(self):
        return {
            'default_recipients': [self.participant.id],
            'default_message': (
                "Medic, if your team was informed that a victim had been triaged, "
                "it would help your team evacuate them"
            )            
        }



class RemindMedicToInformAboutTriagedVictimTrigger(InterventionTrigger):
    """
    Trigger for a RemindMedicToInformAboutTriagedVictimIntervention.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        timeout : float, default=15
            Maximum mission time (in seconds) without a discard before checking
            teammates knowledge to determine whether or not to resolve
        redis_timeout : float, default=1.0
            Timeout for a Redis request on the internal bridge
        belief_threshold : float, default=0.99
            The minimum number of expected victims in the belief state of the
            triaged victim type that is considered being "aware"
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Add Minecraft callback
        self.add_minecraft_callback(TriageEvent, self.__onTriageEvent)


    def __onTriageEvent(self, message):
        """
        Callback when an TriageEvent message is received.
        """
        if message.triage_state == TriageEvent.TriageState.SUCCESSFUL:
            intervention = RemindMedicToInformAboutTriagedVictimIntervention(
                self.manager,
                triage_event=message,
                **self.kwargs,
            )
            self.logger.info(f"{self}:  Spawning Intervention {intervention}")
            self.manager.spawn(intervention)



class RemindMedicToInformAboutTriagedVictimFollowup(
    Followup, ComplianceMixin, BeliefMixin, VictimMarkerMixin):
    """
    Followup to a RemindMedicToInformAboutTriagedVictimIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=10
            Number of seconds in the time period of follow up validation
        """

        duration = kwargs.get("duration", 10)

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)
        BeliefMixin.__init__(self, manager, **intervention.kwargs)
        VictimMarkerMixin.__init__(self, manager, **dict(intervention.kwargs, timeout=None))


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """
        teammates = [p for p in self.manager.participants if p != self.intervention.participant]
        if self.believe(teammates, 'treated_victims', self.intervention.victim_room):
            self._onCompliance()
        else:
            self._onNonCompliance()


    def _onCorrectMarkerPlaced(self, message):
        """
        Callback for when a correct marker block is placed near the victim.
        """
        self._onCompliance()


    def _onVictimPickedUp(self):
        """
        Callback for when the victim is picked up.
        """
        self._onNonCompliance()
