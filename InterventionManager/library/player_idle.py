# -*- coding: utf-8 -*-
"""
.. module:: player_idle
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for encouraging players to move if they
              have been idle for too long.

.. moduleauthor:: Max Chis <>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to encourage players to move if they have been too idle.
"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup
###from ..follow_through import PredictionFollowThrough

from ..mixins import ComplianceMixin

import numpy as np
import math

from MinecraftBridge.utils import Loggable

from MinecraftBridge.messages import (
    ItemDropEvent, 
    ItemEquippedEvent, 
    MarkerPlacedEvent, 
###    MissionStateEvent,
    PlanningStageEvent,
    PlayerState, 
    PlayerSwingingEvent, 
    RubbleDestroyedEvent, 
    TriageEvent, 
    ToolUsedEvent, 
    VictimPlaced, 
    VictimPickedUp
)



class PlayerIdleIntervention(Intervention):
    """
    This intervention will alert players when they have been idle for an
    extended period of time.  Player idleness is indicated by the lack of all
    of the following behaviors:

    1.  Moving a minimal distance within a given timeframe
    2.  Changing equipment
    3.  Destroying Rubble
    4.  Stabalizing a victim
    5.  Placing a marker
    6.  Picking up or placing a victim
    7.  Using a tool
    8.  Attempting to use a tool (swinging)


    * Trigger: Triggered initially at the MisisonStart.  If presented to the
               player, a new instance is created and spawned.

    * Discard States:  None

    * Resolve State:  After time start_time + check_interval, player has 
                      neither engaged in event nor moved at least min_dist

    
    Attributes
    ----------
    participant : BaseAgent.Participant
        Participant being monitored for idle behavior
    behavior_history : list of tuples
        List of tuples summarizing the events that 
    """

    def __init__(self, manager, participant, activation_time, 
                       idle_time_threshold, idle_distance_threshold, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the intervention manager that should control this
            intervention
        participant : Participant
            Participant being monitored for idle behavior
        activation_time : float
            Earliest time (in elapsed seconds) that this intervention can start
            monitoring for idle behavior
        idle_time_threshold : float
            Time (in seconds) that the player needs to idle for to resolve the
            intervention
        idle_distance_threshold : float
            Maximum distance an player can move within the idle_time_threshold
            and still be considered idle
        """

        Intervention.__init__(self, manager, **kwargs)

        # Store the arguments.  Convert the time arguments to milliseconds, to
        # simplify comparison with elapsed milliseconds
        self.participant = participant
        self.activation_time = int(activation_time * 1000)
        self.idle_time_threshold = int(idle_time_threshold * 1000)
        self.idle_distance_threshold = idle_distance_threshold

        # Store attributes needed to check for idleness.  These will be updated
        # by PlayerState messages
        self._player_position = (0, 0)      # (x,z) position of the player
        self._current_time = 0              # elapsed_milliseconds

        # Set the initial state for idleness checks.  idle_reference_position
        # is the location of the last non-idle behavior of the participant, 
        # while is_idle_time indicates the future time that the participant will
        # be considered idle.
        self.idle_reference_position = (0, 0)    
        self.idle_resolve_time = self.activation_time + self.idle_time_threshold

        # Time (elapsed milliseconds) when the intervention was queued for 
        # resolution.  `current_time` could probably be used, but there may be
        # a PlayerState message received before the manager disconnects this 
        # from the bus.
        self.resolve_time = -1

        # The intervention will maintain a history of the events and their 
        # corresponding occurance time which resulted in a reset of the idle
        # monitoring.
        # NOTE:  elapsed_milliseconds may not be needed, since the messages 
        #        contain the elapsed_milliseconds?
        self._behavior_history = []

        self.addRelevantParticipant(self.participant.participant_id)


        self.add_minecraft_callback(PlayerState, self.__onPlayerStateMessage)
        self.add_minecraft_callback(ItemEquippedEvent, self.__onEventWithPlayername)
        self.add_minecraft_callback(RubbleDestroyedEvent, self.__onEvent)
        self.add_minecraft_callback(TriageEvent, self.__onEvent)
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onEvent)
        self.add_minecraft_callback(VictimPlaced, self.__onEvent)
        self.add_minecraft_callback(VictimPickedUp, self.__onEvent)
        self.add_minecraft_callback(ToolUsedEvent, self.__onEventWithPlayername)
        self.add_minecraft_callback(PlayerSwingingEvent, self.__onEventWithPlayername)
        #This event is not implemented yet:
        # self.register_callback(ProximityBlockInteraction, self.__onProximityBlockInteraction)

        self.logger.info("%s:  Intervention initialized", self)
        self.logger.info("%s:    Activation Time: %d ms", self, self.activation_time)
        self.logger.info("%s:    Resolution Time: %d ms", self, self.idle_resolve_time)


    def __str__(self):
        """
        String representation of the intervention
        """

        return f"{self.__class__.__name__} @ {self.participant.participant_id}"



    @property
    def behavior_history(self):
        """
        A list of tuples summarizing the events that reset the participant's
        idle state, and at which time.  Tuples indicate the time and message 
        resulting in an idle reset: (elapsed_milliseconds, message)
        """

        return self._behavior_history
    

    def _reset_idle_status(self, message=None):
        """
        Resets the attributes used to determine if the participant is idle, i.e.,
        set the idle reference position to where the player is standing now, and
        the resolve time to the current time plus the idle time threshold.

        Attributes
        ----------
        message : MinecraftBridge.messages, default=None
            Message that caused the reset of idle status, to be stored in the
            behavior history
        """

        # Reset the reference position and future resolve time to the provided
        self.idle_resolve_time = self._current_time + self.idle_time_threshold
        self.idle_reference_position = self._player_position

        # Store the message in the history for any later analysis
        self._behavior_history.append((self._current_time, message))



    def __onPlayerStateMessage(self, message):
        """
        """

        # Does this apply to the participant of interest?
        if message.participant_id != self.participant.participant_id:
            return

        # Update the time and player position
        self._player_position = (message.x, message.z)
        self._current_time = message.elapsed_milliseconds

        # Should this message be ignored?  If we haven't reached the activation
        # time yet, no need to check for idleness
        if self._current_time < self.activation_time:
            return

        # Check to see if the participant has moved sufficiently far.  If so, 
        # then reset the idle
        distance = math.sqrt((self.idle_reference_position[0] - message.x)**2 +
                             (self.idle_reference_position[1] - message.y)**2)

        if distance > self.idle_distance_threshold:
            self._reset_idle_status(message)

        # Check to see if it is time to resolve the the intervention
        if self._current_time >= self.idle_resolve_time:
            self.resolve_time = self._current_time
            self.logger.info("%s:  Resolving, resolve time %d", self, self.resolve_time)
            self.queueForResolution()


    def __onEvent(self, message):
        """
        Callback when a relevant event-type message is received.

        message : MinecraftBridge.message
            Message instance received
        """

        # Does this apply to the participant of interest?
        if message.participant_id != self.participant.participant_id:
            return

        self.logger.debug("%s: Received %s", self, message)
        self._reset_idle_status(message)


    def __onEventWithPlayername(self, message):
        """
        Callback when a relevant event-type message is received.  This is a 
        hack to handle messages that do not necessarily have `participant_id`
        as a property.

        message : MinecraftBridge.message
            Message instance received
        """

        # Unfortunately for now, ItemEquippedEvent uses playername, which may
        # (?) be the participant_id, depending when the message was produced.
        if message.playername != self.participant.playername and \
           message.playername != self.participant.participant_id:
           return

        self.logger.debug("%s:  Received %s", self, message)
        self._reset_idle_status()        



    def getInterventionInformation(self, **kwargs):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.

        The generated dictionary *must* have the properties listed in the 
        `Required Fields` below.

        NOTE:  This method is going to be deprecated soon...

        Return
        -------
        intervention_info : dictionary
            Mapping of relevant intervention properties to needed information

        Required Fields
        ---------------
        playernames : list of strings
            List of playernames that the intervention should be sent to
        text_message : string
            A (default) text representation of the intervention
        """


        intervention_info = {
            "default_recipients": [ self.participant.participant_id ],
            "default_message": f"{self.participant.callsign}, you could help your team by being more active."
        }

        return intervention_info



class PlayerIdleTrigger(InterventionTrigger):
    """
    Class that generates PlayerIdle Interventions.  Interventions are triggered
    for each participant at the end of the planning phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        idle_time_threshold : int, default=30
            Minimum number of seconds that must pass before the player is
            considered idle
        idle_distance_threshold : float, default=50
            Maximum distance that the participant can travel while still being
            considered idle
        initial_activation_time : int, default=0
            Activation time, in elapsed_milliseconds
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        # Store the keyword arguments for later intervention construction
        self.idle_time_threshold = kwargs.get("idle_time_threshold", 30)
        self.idle_distance_threshold = kwargs.get("idle_distance_threshold", 50)
        self.initial_activation_time = kwargs.get("initial_activation_time", 0)

        # Register to receive the following messages
        self.add_minecraft_callback(PlanningStageEvent, self.__onPlanningStageEvent)


    def __onPlanningStageEvent(self, message):
        """
        Callback when PlanningStage event messages are received.  If the mission
        state is a start, then a PlayerIdleIntervention should be spawned for 
        each participant.

        Arguments
        ---------
        message : PlanningStageEvent
            PlanningStage Event message
        """
        
        # If the planning stage is ending, then spawn an Intervention for each
        # participant.
        if message.state == PlanningStageEvent.PlanningState.Stop:

            for participant in self.manager.participants:
                # Create an intervention for the participant, setting the
                # activation time to 0 (can start monitoring immediately)
                intervention = PlayerIdleIntervention(self.manager, participant,
                                                      self.initial_activation_time, 
                                                      self.idle_time_threshold,
                                                      self.idle_distance_threshold,
                                                      parent=self)

                self.logger.info("%s:  Spawning Intervention: %s", self, intervention)

                self.manager.spawn(intervention)




class PlayerIdleResolution(Loggable):
    """
    A resolution that creates and spawns a new PlayerIdle resolution.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        activation_delay : float, default=10
            Number of seconds to wait until the intervention is
        compliance_timeout : float, default=15
            Number of seconds until the participant is considered non-compliant
            to the resolution
        """

        self.manager = manager

        # How much time (in seconds) before the intervention can be deemed active
        self.activation_delay = 1000 * kwargs.get("activation_delay", 10.0)
        self._compliance_timeout = kwargs.get("compliance_timeout", 15.0)


    def resolve(self, intervention):
        """
        Arguments
        ---------
        intervention : PlayerIdleIntervention
            Instance of a player idle intervention
        """

        self.logger.info("%s:  Resolving PlayerIdleIntervention with resolution time %d", self, intervention.resolve_time)

        # Create and initiate a followup to check for compliance
        followup = PlayerIdleComplianceFollowup(intervention, self.manager,
                                                timeout=self._compliance_timeout)
        self.manager.initiateFollowup(followup)

        # Note the dumb seconds - milliseconds stuff
        next_activation_time = (intervention.resolve_time + self.activation_delay) / 1000

        # Spawn the next player_idle intervention for this participant
        new_intervention = PlayerIdleIntervention(self.manager, intervention.participant,
                                                  next_activation_time, 
                                                  intervention.idle_time_threshold / 1000,
                                                  intervention.idle_distance_threshold,
                                                  parent=intervention)

        self.logger.info("%s:  Spawning %s with activation time %d", self, new_intervention, next_activation_time)

        self.manager.spawn(new_intervention)




class PlayerIdleComplianceFollowup(Followup, ComplianceMixin):
    """
    Followup indicating whether or not the player was compliant with being
    advised to stop being idle.

    Attributes
    ----------
    intervention : PlayerIdleIntervention
        Intervention this followup is following up on
    compliance_message : Message or None
        Message which results in the participant being considered no longer
        idle.  None, if the followup times out prior to compliance
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the Intervention Manager that the presenter is
            associated with

        Keyword Arguments
        -----------------
        timeout : float
            Number of seconds before the participant is considered noncompliant
        """

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=kwargs.get("timeout", 15.0))

        self.participant = intervention.participant

        self.compliance_message = None

        # Temporary, should be moved to the Compliance mixin (and maybe set to
        # None in Followup?)
        self.was_complied_with = False

        self.add_minecraft_callback(PlayerState, self.__onPlayerStateMessage)
        self.add_minecraft_callback(ItemEquippedEvent, self.__onEventWithPlayername)
        self.add_minecraft_callback(RubbleDestroyedEvent, self.__onEvent)
        self.add_minecraft_callback(TriageEvent, self.__onEvent)
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onEvent)
        self.add_minecraft_callback(VictimPlaced, self.__onEvent)
        self.add_minecraft_callback(VictimPickedUp, self.__onEvent)
        self.add_minecraft_callback(ToolUsedEvent, self.__onEventWithPlayername)
        self.add_minecraft_callback(PlayerSwingingEvent, self.__onEventWithPlayername)

        self.logger.info("%s:  Followup Initialized", self)      


    def _onActive(self):
        """
        Callback from the InterventionManager when the Followup is made active
        """
        # This followup observes if the wrong markerblock has been removed, 
        # and the correct one placed instead of it

        self.logger.info("{}: Initiating followup for idle player {}".format(self, self.participant.participant_id))



    def __onPlayerStateMessage(self, message):
        """
        Check to see if the player has moved far enough to be considered compliant
        """

        # Does this apply to the participant of interest?
        if message.participant_id != self.participant.participant_id:
            return

        self.logger.debug("%s: Received %s", self, message)

        # Check to see if the participant has moved sufficiently far.  If so, 
        # then reset the idle
        distance = math.sqrt((self.intervention._player_position[0] - message.x)**2 +
                             (self.intervention._player_position[1] - message.y)**2)

        if distance > self.intervention.idle_distance_threshold:
            self._onCompliance()
            self.complete()


    def __onEvent(self, message):
        """
        Callback when a relevant event-type message is received.

        message : MinecraftBridge.message
            Message instance received
        """

        # Does this apply to the participant of interest?
        if message.participant_id != self.participant.participant_id:
            return

        self.logger.debug("%s: Received %s", self, message)

        self._onCompliance()
        self.complete()


    def __onEventWithPlayername(self, message):
        """
        Callback when a relevant event-type message is received.  This is a 
        hack to handle messages that do not necessarily have `participant_id`
        as a property.

        message : MinecraftBridge.message
            Message instance received
        """

        # Unfortunately for now, ItemEquippedEvent uses playername, which may
        # (?) be the participant_id, depending when the message was produced.
        if message.playername != self.participant.playername and \
           message.playername != self.participant.participant_id:
           return

        self.logger.debug("%s:  Received %s", self, message)

        self._onCompliance()
        self.complete()


    def _onTimeout(self):
        """
        If the followup has timed out before any additional behavior, consider
        the intervention not complied with.
        """

        self._onNonCompliance()



###class PlayerIdlePredictionM1(PredictionFollowThrough, Loggable):
###    """
###    A follow-through making predictions on how a resolved / followed up 
###    player idle intervention will influence M1 / team score.
###
###    This FollowThrough assumes that the received Followup is an instance of
###    `PlayerIdleComplianceFollowup`
###    """
###
###    def __init__(self, manager, **kwargs):
###        
###        PredictionFollowThrough.__init__(self, manager, **kwargs)
###
###
###    def predict(self, followup):
###        """
###        Create the prediction based on the contents of the followup
###        """
###
###        # Pull out the intervention
###        intervention = followup.intervention
###
###        # Who was being idle?
###        participant_id = intervention.participant.participant_id
###
###        # Extract some features (this is just for initial testing, so may not
###        # be actually what is later used).  The features will be the mean and
###        # standard deviation of time between non-idle behaviors
###        behavior_times = [b[0] for b in intervention.behavior_history]
###
###        # Make sure that there's something to calculate
###        if len(behavior_times) < 2:
###            behavior_times = [0,0]
###
###        idle_times = []
###        for i in range(1,len(behavior_times)):
###            idle_times.append(behavior_times[1] - behavior_times[0])
###
###        # Features will be 
###        # 1.  what is the average duration of "idle" times observed during the
###        #     lifecycle of the intervention
###        # 2.  what is the standard deviation of "idle" times observed during the
###        #     lifecycle of the intervention
###        # 3.  what is the average duration of "idle" times observed, in terms
###        #     of percentage of the idle time threshold
###        mean_idle_time = np.mean(idle_times)
###        std_idle_time = np.std(idle_times)
###        mean_idle_time_percentage = mean_idle_time / intervention.idle_time_threshold
###
###        # Estimate the decrease in score, if the followup was complied with or
###        # not.  If the player complied with the followup, then 
###        if followup.was_complied_with:
###            score_loss = 0
###        else:
###            score_loss = -10 * mean_idle_time_percentage
###
###
###        prediction = {
###            "mean_idle_time": mean_idle_time,
###            "std_idle_time": std_idle_time,
###            "mean_idle_time_percentage": mean_idle_time_percentage,
###            "M1_difference": score_loss
###        }
###
###        return prediction