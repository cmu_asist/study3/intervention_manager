# -*- coding: utf-8 -*-
"""
.. module:: player_saw_victim
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for querying player if they saw a victim

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to ask a player if they saw a victim.

* Trigger:  Player sees a new victim

* Discard States:
    - Player has "seen" victim (according to ToM)
    - The number of pixels for victim block exceeds some threshold

* Resolve State:  Player moves away from victim without discard

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..mixins import BeliefMixin, DistanceMixin, SawVictimMixin
from MinecraftBridge.messages import FoVSummary



class PlayerSawVictimIntervention(Intervention, BeliefMixin, DistanceMixin):
    """
    Intervention which is designed to alert the player to a potentially unperceived
    victim.
    """

    def __init__(self, manager, participant_id, victim, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : string
            Unique ID of the participant who may or may not have seen a victim
        victim : dict
            Dictionary of victim info with keys for 'id', 'location', and 'type'

        Keyword Arguments
        -----------------
        angle_threshold : float, default=135.0
            Minimum angle between the location and direction of player
            movement to consider the player "moving away"
        pixel_threshold : int, default=40000
            Number of pixels required to assume the victim was seen
        use_tom : bool, default=True
            Whether or not to use the ToM to check player beliefs
        redis_timeout : float, default=1.0
            Timeout for a Redis request on the internal bridge
        belief_threshold : float, default=0.7
            The minimum number of expected victims in the belief state of the
            triaged victim type that is considered being "aware"
        """
        kwargs.setdefault('belief_threshold', 0.7)
        Intervention.__init__(self, manager, **kwargs)
        BeliefMixin.__init__(self, manager, **kwargs)
        DistanceMixin.__init__(
            self, manager, **kwargs, player=participant_id, location=victim['location'])

        # Set attributes
        self.participant = manager.participants[participant_id]
        self.victim_id = victim['id']
        self.victim_location = victim['location']
        self.pixel_threshold = kwargs.get('pixel_threshold', 40000)
        self.use_tom = kwargs.get('use_tom', True)

        self.addRelevantParticipant(self.participant.participant_id)

        # Add Minecraft callback
        self.add_minecraft_callback(FoVSummary, self.__onFoVMessage)


    def _onMovingAway(self):
        """
        Callback for when the player begins moving away from the location.
        """
        if self.use_tom and self.believe(self.participant, 'seen', self.victim_location):
            self.logger.info(f"According to ToM, player has seen victim. Discarding.")
            self.discard()
        else:
            self.logger.info(f"{self}:  Player moving away from victim. Queuing for resolution.")
            self.queueForResolution()


    def __onFoVMessage(self, message):
        """
        Arguments
        ---------
        message : MincraftBridge.messages.FoVSummary
            Received FoV message
        """

        # Ignore this FoV message if it's not associated with the participant
        if self.manager.participants[message.playername].id != self.participant.id:
            return

        # Check to see if the victim block is in the message
        for block in message.blocks:
            if block['id'] == self.victim_id:
                self.victim_location = block['location']
                if block['number_pixels'] >= self.pixel_threshold:
                    self.logger.info(
                        f"{self}: Discarding, under assumption that the victim has been seen.")
                    self.discard()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {
            'default_recipients': [self.participant.id],
            'default_message': f"{self.participant.callsign}, did you notice that victim?  You should indicate the presence of the victim verbally or with a marker block to let your team know.",
        }



class PlayerSawVictimTrigger(InterventionTrigger, SawVictimMixin):
    """
    Class that generates PlayerSawVictim Interventions.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        angle_threshold : float, default=135.0
            Minimum angle between the location and direction of player
            movement to consider the player "moving away"
        pixel_threshold : int, default=40000
            Number of pixels required to assume the victim was seen
        use_tom : bool, default=True
            Whether or not to use the ToM to check player beliefs
        redis_timeout : float, default=1.0
            Timeout for a Redis request on the internal bridge
        belief_threshold : float, default=0.7
            The minimum number of expected victims in the belief state of the
            triaged victim type that is considered being "aware"
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        SawVictimMixin.__init__(self, manager, **kwargs)
        self.kwargs = kwargs


    def _onNewVictimSeen(self, participant_id, victim):
        """
        Callback for when a victim is first seen by a player.
        """
        intervention = PlayerSawVictimIntervention(
            self.manager, participant_id=participant_id, victim=victim, **self.kwargs)
        self.logger.info(f"{self}: Victim in FoV of {participant_id}. Spawning {intervention}.")
        self.manager.spawn(intervention)
