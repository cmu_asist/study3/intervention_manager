# -*- coding: utf-8 -*-
"""
.. module:: notify_about_trapped_victim
   :platform: Linux, Windows, OSX
   :synopsis: Intervention instructing Medic to tell the Engineer about
              a trapped victim

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an
intervention designed to instruct the Medic to notify the Engineer of a
trapped victim.

* Trigger:  Medic sees a trapped victim that the engineer has not seen

* Discard States:
    - Medic places an SOS marker block
    - Medic communicates about rubble
    - Engineer sees trapped victim
    - Victim is no longer trapped

* Resolve States:
    - Medic moves away from victim
    - A given time interval elapses without discard / resolution
"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..mixins import ComplianceMixin, DistanceMixin, TrappedMixin
from MinecraftBridge.messages import MarkerPlacedEvent



class NotifyAboutTrappedVictimIntervention(Intervention, DistanceMixin, TrappedMixin):
    """
    Intervention instructing the the Medic to notify the Engineer of a trapped victim.
    """

    def __init__(self, manager, victim, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        victim : dict
            Dictionary of victim info with keys for 'id', 'location', and 'type'

        Keyword Arguments
        -----------------
        angle_threshold : float, default=135.0
            Minimum angle between the location and direction of player
            movement to consider the player "moving away"
        timeout : float, default=15
            Maximum mission time (in seconds) without a discard / resolution
            before queuing for resolution
        """
        Intervention.__init__(self, manager, **kwargs)
        DistanceMixin.__init__(
            self, manager, **kwargs, player='Medical_Specialist', location=victim['location'])
        TrappedMixin.__init__(self, manager, **kwargs)

        # Set attributes
        self.victim_id = victim['id']
        self.victim_location = victim['location']

        self.addRelevantParticipant(self.manager.participants['Medical_Specialist'].participant_id)

        # Add Minecraft callback
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onMarkerPlaced)

        # Add Redis callback
        self.add_redis_callback(self.__onNLP, 'nlp')


    def _onMovingAway(self):
        """
        Callback for when the player begins moving away from the location.
        """
        self.logger.info(f"{self}:  Player is moving away from victim. Queuing for resolution.")
        self.queueForResolution()


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without a discard or resolution.
        """
        self.logger.info(f"{self}:  Intervention has timed out.")
        if self.is_trapped(self.victim_location):
            self.logger.info(f"{self}:  Victim is still trapped. Queuing for resolution.")
            self.queueForResolution()
        else:
            self.logger.info(f"{self}:  Victim is no longer trapped. Discarding.")
            self.discard()


    def _onVictimTrapped(self, participant, victim):
        """
        Callback for when a trapped victim is first seen by a player.
        """
        if participant.role == 'Engineering_Specialist' and victim['id'] == self.victim_id:
            self.logger.info(f"{self}:  Engineer saw trapped victim. Discarding.")
            self.discard()
            

    def __onMarkerPlaced(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.MarkerPlacedEvent
            Received MarkerPlacedEvent message
        """
        participant = self.manager.participants[message.playername]
        if participant.role == 'Medical_Specialist' and 'sos' in message.type.name:
            self.logger.info(f"{self}:  Player placed SOS marker block. Discarding.")
            self.discard()


    def __onNLP(self, message):
        """
        Arguments
        ---------
        message : dict
            Received message from the internal Redis 'nlp' channel
        """
        speaker = self.manager.participants[message.data['participant_id']]
        if speaker.role == 'Medical_Specialist':
            if 'level_2' in message.data['codes']:
                if 'rubble' in message.data['codes']['level_2']:
                    self.logger.info(f"{self}:  Player spoke about rubble. Discarding.")
                    self.discard()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        message = "Medic, it would help the engineer to know there is a victim trapped in rubble here."
        return {
            'default_recipients': [self.manager.participants['Medical_Specialist'].participant_id],
            'default_message': message,
        }



class NotifyAboutTrappedVictimTrigger(InterventionTrigger, TrappedMixin):
    """
    Class that generates NotifyAboutTrappedVictimIntervention instances.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        angle_threshold : float, default=135.0
            Minimum angle between the location and direction of player
            movement to consider the player "moving away"
        timeout : float, default=15
            Maximum mission time (in seconds) without a discard / resolution
            before queuing for resolution
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        TrappedMixin.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Set attributes
        self._engineer_seen_trapped_victims = set()


    def _onVictimTrapped(self, participant, victim):
        """
        Callback for when a trapped victim is first seen by a player.
        """
        if participant.role == 'Engineering_Specialist':
            self._engineer_seen_trapped_victims.add(victim['id'])
        elif participant.role == 'Medical_Specialist':
            if victim['id'] not in self._engineer_seen_trapped_victims:
                intervention = NotifyAboutTrappedVictimIntervention(self.manager, victim, **self.kwargs)
                self.logger.info(
                    f"{self}: Medic saw trapped victim at {victim['location']}. Spawning {intervention}.")
                self.manager.spawn(intervention)



class NotifyAboutTrappedVictimFollowup(Followup, ComplianceMixin, TrappedMixin):
    """
    Followup to an NotifyAboutTrappedVictimIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=15
            The duration of the followup validation period (in seconds)
        """

        duration = kwargs.get("duration", 15)

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)
        TrappedMixin.__init__(self, manager)

        # Add Minecraft callback
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onMarkerPlaced)

        # Add Redis callback
        self.add_redis_callback(self.__onNLP, 'nlp')


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance (or the mission ends).
        """
        if self.is_trapped(self.intervention.victim_location):
            self._onNonCompliance()
        else:
            self._onCompliance()


    def __onMarkerPlaced(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.MarkerPlacedEvent
            Received MarkerPlacedEvent message
        """
        participant = self.manager.participants[message.playername]
        if participant.role == 'Medical_Specialist' and 'sos' in message.type.name:
            self._onCompliance()


    def __onNLP(self, message):
        """
        Arguments
        ---------
        message : dict
            Received message from the internal Redis 'nlp' channel
        """
        speaker = self.manager.participants[message.data['participant_id']]
        if speaker.role == 'Medical_Specialist':
            if 'level_2' in message.data['codes']:
                if 'rubble' in message.data['codes']['level_2']:
                    self._onCompliance()
