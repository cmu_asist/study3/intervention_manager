# -*- coding: utf-8 -*-
"""
.. module:: kit_recommendation
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for KIT results

.. moduleauthor:: Simon Stepputtis <stepputtis@cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to recommend KIT results to the players

* Trigger: The intervention is triggered at the start of the misison

* Discard States:  N/A

* Resolve State:  The intervention is immediately queued for a resolution

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup
from ..mixins import ComplianceMixin
from ..components import KITAnalyzer
from ..components import InterventionScheduler as Scheduler

from MinecraftBridge.messages import PlanningStageEvent
from MinecraftBridge.messages import ASR_Message
from MinecraftBridge.mqtt.parsers import MessageType, MessageSubtype
from MinecraftBridge.messages import (
    AgentPredictionGroupProperty,
    AgentStatePrediction,
    AgentStatePredictionMessage,
    BusHeader,
    MessageHeader
)

class KitRecommendationIntervention(Intervention):
    """
    A TeamWelcomeMessageIntervention is an Intervention which simply presents
    a welcome message to all participants in the team.
    """

    def __init__(self, manager, text, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        text : string
            The text to analyze for KIT information

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """
        Intervention.__init__(self, manager, **kwargs)
        self._message = kwargs.get('message', "<KITRecommendation> Needs a message!")

        # Indicate that all participants are relevant to this intervention
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.participant_id)     

        self._kit = KITAnalyzer()  
        self._text = text

    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        kit_has_result = self._kit.parse(self._text)
        kit_global_result = (self._kit.summarizeResults())

        # Publish the results to the message bus
        kit_message = AgentStatePrediction(
            subject="team",
            subject_type="team",
            predicted_property="string",
            prediction="KIT Result",
            probability=1.0,
            confidence=1.0,
            explanation=kit_global_result)
        message_group = AgentPredictionGroupProperty(start=kit_message.start, duration=kit_message.duration)
        bus_message = AgentStatePredictionMessage(group=message_group)
        bus_message.add(kit_message)
        bus_message.finalize()

        # Add needed message headers
        bus_header = BusHeader(MessageType.agent)
        msg_header = MessageHeader(MessageSubtype.Prediction_State,
                                   self._manager.trial_info["experiment_id"],
                                   self._manager.trial_info["trial_id"],
                                   self._manager._agent_name,
                                   replay_id=self._manager.trial_info["replay_id"]
                                   )

        bus_message.addHeader("header", bus_header)
        bus_message.addHeader("msg", msg_header)

        self._manager.minecraft_bridge.publish(bus_message)

        # Run our interventions
        if not kit_has_result:
            self.discard()

        # Make sure we have something useful to say
        consensus = self._kit.getConsensus()
        self._suggestion = self._kit.getSuggestion()
        # If ATLAS found something they players didn't:
        relevant = False
        for s in self._suggestion:
            if s not in consensus:
                relevant = True
        if not relevant:
            self.discard()    
            return
                    
        # Seems like we have something to say
        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        self._message += ", ".join(self._suggestion)
        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self._message,
        }


class KitRecommendationTrigger(InterventionTrigger):
    """
    Class that generates KitRecommendationIntervention.  Interventions are
    triggered at the start of a trial.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs
        self._initial_observation_period = kwargs.get('additional_observation_period', 30)

        # Register the trigger to receive planning messages
        self.add_minecraft_callback(PlanningStageEvent, self.__onPlanningStageEvent)
        self.add_minecraft_callback(ASR_Message, self.__onAsrMessage)
        self._collect_asr = False
        self._asr = []

        # Wait a little beyond the end of the planning phase
        self._scheduler = Scheduler(self)

    def __onAsrMessage(self, message):
        """
        Collects the utterances during the planning phase
        """
        if not self._collect_asr:
            return

        self._asr.append(message.text)
        
    def __onPlanningStageEvent(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.PlanningStageEvent
            Received PlanningStageEvent message
        """
        self.logger.debug(f"{self}: Received PlanningStage Message")

        # Check to see if this is a Planning Stage start message
        if message.state == PlanningStageEvent.PlanningState.Start:
            self._collect_asr = True

        # Check to see if this is a Planning Stage end message
        if message.state == PlanningStageEvent.PlanningState.Stop:
            self._scheduler.wait(self._enableIntervention, delay=self._initial_observation_period)

    def _enableIntervention(self):
        self._collect_asr = False
        intervention = KitRecommendationIntervention(self.manager, ". ".join(self._asr), **self.kwargs)
        self.logger.info(f"{self}:  Spawning {intervention}.")
        self.manager.spawn(intervention)


class KitRecommendationIndividualComplianceFollowup(Followup, ComplianceMixin):
    """
    Class to monitor for compliance of an individual to a 
    KitRecommencationIntervention.  An individual will be considered compliant
    if they enter one of the recommended rooms.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : KitRecommendationIntervention
            Instance of the intervention being followup up upon
        manager : InterventionManager
            Manager of this followup and intervention

        Keyword Arguments
        -----------------
        timeout : float, default = 60
            Amount of time to allow the participant to go to one of the 
            recommended rooms
        """

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, kwargs.get("timeout", 60))


    def _onTimeout(self):
        """
        """

        pass


class KitRecommendationTeamComplianceFollowup(Followup, ComplianceMixin):
    """
    Class to monitor for compliance of the team to a
    KitRecommendationIntervention.  The team is considered compliant if the
    medic and one other team member enter the _same_ recommended room.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : KitRecommendationIntervention
            Instance of the intervention being followup up upon
        manager : InterventionManager
            Manager of this followup and intervention

        Keyword Arguments
        -----------------
        timeout : float, default = 60
            Amount of time to allow the participant to go to one of the 
            recommended rooms
        """

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, kwargs.get("timeout", 60))


    def _onTimeout(self):
        """
        """

        pass
