# -*- coding: utf-8 -*-
"""
.. module:: nlp_dummy
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger responding to tomcat labels

.. moduleauthor:: Noel Chen <noelchen@cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to test nlp triggers
"""

from .. import Intervention
from .. import InterventionTrigger

from MinecraftBridge.messages import ASR_Message, TriageEvent
from MinecraftBridge.components import Participant, ParticipantCollection



class NLPIntervention(Intervention):
    """
    A NlpIntervention is an Intervention which is designed to test NLP interventions

    * Trigger: The intervention is triggered when receiving utterance talking about direction.

    * Discard States:  N/A

    * Resolve State:  N/A

    Attributes
    ----------

    Methods
    -------

    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------

        """

        Intervention.__init__(self, manager, **kwargs)

        self._participants = kwargs.get("participants", ParticipantCollection())

        # Register callbacks for when the intervention is connected
        self.register_callback(ASR_Message, self.__onASR_Message)
        self.register_callback(TriageEvent, self.__onTriageEvent)

        self.message = "I AM A MESSAGE"


    def __onASR_Message(self, message):
        """
        Callback when the Intervention receives a ASR_Message message.

        Arguments
        ---------
        message : MinecraftBridge.message.ASR_Message
            Received ASR_Message message
        """

        self.logger.info("Received ASR Message")

    def __onTriageEvent(self, message):
        self.logger.info("Intervention Received TriageEvent")

    def _onActivated(self):
        """
        Callback when the Intervention is activated.  This intervention should
        be queued for resolution.
        """

        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.

        The generated dictionary *must* have the properties listed in the
        `Required Fields` below.

        TODO:  Identify a set of universal intervention properties, so that the
               returned information is sufficient for whichever type of
               intervention.

        Return
        -------
        intervention_info : dictionary
            Mapping of relevant intervention properties to needed information

        Required Fields
        ---------------
        participant_ids : list of strings
            List of participant_ids that the intervention should be sent to
        text_message : string
            A (default) text representation of the intervention
        """

        intervention_info = {}

        intervention_info["participant_ids"] = [ participant.participant_id for participant in self._participants ]
        intervention_info["text_message"] = self.message

        return intervention_info


class NLPTrigger(InterventionTrigger):
    """
    Class that generates NLP-dummy Interventions.  Interventions are 
    triggered the remaining time is less than t minutes.

    Arguments
    ---------

    Methods
    -------

    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        remaining_time_threshold : int
            Minutes left in the trial for the intervention to happen.
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        self.register_callback(ASR_Message, self.__onASR_Message)
        self.register_callback(TriageEvent, self.__onTriageEvent)

    
    def __onASR_Message(self, message):
        self.logger.info("NLP Received ASR_Message Message")
        self.logger.info(message.toDict())
        intervention = NLPIntervention(self.manager, participants=self.manager.participants)
        self.manager.spawn(intervention)

    def __onTriageEvent(self, message):
        self.logger.info("NLPTrigger Received TriageEvent")