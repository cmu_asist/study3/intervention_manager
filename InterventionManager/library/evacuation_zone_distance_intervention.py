# -*- coding: utf-8 -*-
"""
.. module:: evacuation_zone_distance_intervetnion
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for introductory mmessage for team

.. moduleauthor:: Simon Stepputtis <stepputtis@cmu.edu>

"""

import numpy as np

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..components import InterventionScheduler as Scheduler

from ..mixins import ComplianceMixin

from MinecraftBridge.messages import VictimPickedUp, VictimEvacuated, VictimPlaced



class EvacuationZoneDistanceIntervention(Intervention):
    """
    The strategy of this intervention is to monitor where victims that are carried around go.
    Particularly, we want victims to be evacuated to the closet valid evacuation zone.

    If a player evacuates the victims "suboptimally" (defined by a distance threshold of min_distance_differnce),
    the intervention will is inform the player to use their map to find more optimal evacuation zones
    """

    def __init__(self, manager, pickup_message, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        pickup_message : VictimPickedUp
            Message that caused this intervetion

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        min_distance_differnce : int
            Minimal distance required to consider the evacuation zone suboptimal
        """
        Intervention.__init__(self, manager, **kwargs)
        self._message = kwargs.get('message', "No message for <EvacuationZoneDistanceIntervention>")
        self._min_difference = kwargs.get('min_distance_differnce', 30)
        self._event = pickup_message
        self._start_pos = self._event.location
        self._type = self._event.type.split("_")[-1]
        self._id = int(self._event.victim_id)
        self._saved = self._event.type.split("_")[-2] == "saved"
        self._participant = self._event.participant_id    

        # Was this victim evacuated?
        self._evacuated = False

        self.add_minecraft_callback(VictimEvacuated, self.__onVictimEvacuated)
        self.add_minecraft_callback(VictimPlaced, self.__onVictimPlaced)
        self._zones = {
            "a_1": (-2117, 60, 62),
            "a_2": (-2187, 60, -4),
            "b_1": (-2187, 60, 62),
            "b_2": (-2117, 60, -4),
            "c_1": (-2155, 60, 62),
            "c_2": (-2155, 60, -4)
        }

    def _dist(self, pos, pos2=None):
        """
        Implementation of what ever distance metric we want to use
        Here: Euclidean distance
        """
        if not pos2:
            pos2 = self._start_pos
        return np.linalg.norm(np.asarray(pos) - np.asarray(pos2))


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        
        # Find the closest evac zone
        distances = {k: self._dist(v) for k, v in self._zones.items() if self._type in k}
        self._target_area = min(distances, key=distances.get)
        
        # Check if they are too close together
        values = list(distances.values())
        if abs(values[0] - values[1]) < self._min_difference:
            self.discard()

    def __onVictimPlaced(self, message):
        """
        A victim was placed.
        Now we need to figure out if it was evacuated or not. Give it 5 seconds for the 
        evacuated message to show up, or we will discard this intervetnion
        """
        # Move on if not our victim
        if int(message.victim_id) != self._id:
            return
        
        # If this victim was just dropped somewhere, discard this intervention
        # and wait for the next pickup
        if not self._evacuated:
            self.discard()  
    
    def __onVictimEvacuated(self, message):
        """
        Call back when a victim got evacuated
        """
        # Move on if not our victim
        if int(message.victim_id) != self._id:
            return        
        
        # Make sure we don't discard it here...
        self._evacuated =  True
        
        # Find the actual area:
        distances = {k: self._dist(v, message.victim_location) for k, v in self._zones.items()}
        used_area = min(distances, key=distances.get)

        # Check if this was the correct zone:
        if self._type not in used_area:
            # Placed in wrong evac zone
            # Not the problem if this intervention
            self.discard()
            return

        # Check if placed in "optimal" zone:
        if self._target_area == used_area:
            # If so, let's discard
            self.discard()
            return

        # Seems like it was placed in a sub-optimnal zone, resolve!
        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [self._participant],
            'default_message': self._message,
        }



class EvacuationZoneDistanceTrigger(InterventionTrigger):
    """
    Class that generates EvacuationZoneDistanceIntervention. 
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Register the trigger to receive FoV messages
        self.add_minecraft_callback(VictimPickedUp, self.__onVictimPickedUpEvent)


    def __onVictimPickedUpEvent(self, message):
        """
        Creates a new intervention when ever a victim is picked up

        Arguments
        ---------
        message : MinecraftBridge.messages.PlanningStageEvent
            Received PlanningStageEvent message
        """
        intervention = EvacuationZoneDistanceIntervention(self.manager, pickup_message=message, **self.kwargs)
        self.logger.info(f"{self}:  Spawning {intervention}.")
        self.manager.spawn(intervention)



class EvacuationZoneDistanceFollowup(Followup, ComplianceMixin):
    """
    Class to monitor for compliance of the team to a
    KitRecommendationIntervention.  The team is considered compliant if the
    medic and one other team member enter the _same_ recommended room.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : KitRecommendationIntervention
            Instance of the intervention being followup up upon
        manager : InterventionManager
            Manager of this followup and intervention

        Keyword Arguments
        -----------------
        timeout : float, default = 60
            Amount of time to allow the participant to go to one of the 
            recommended rooms
        """

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, kwargs.get("timeout", 60))


    def _onTimeout(self):
        """
        """

        pass
