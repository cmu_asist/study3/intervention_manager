# -*- coding: utf-8 -*-
"""
.. module:: marker_place_after_saw_victim
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for reminding the engineer to place a marker after
   detecting a threat room 

.. moduleauthor:: Long Le <lnle@umass.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to ask the engineer to place a marker after detecting a 
threat room for the first time.
"""

import re

import MinecraftElements
from MinecraftBridge.messages import LocationEvent, MarkerPlacedEvent

from .. import Intervention
from .. import InterventionTrigger

THREAT_ROOMS = set()  # a set of currently known threat room names


class SawThreatRoomMarkerIntervention(Intervention):
    """
    A SawThreatRoomMarkerIntervention is an Intervention which is designed to remind
    the engineer if he/she forget to place a marker after detecting a threatroom for the first
    time.

    * Trigger: The intervention is triggered when a threat room is newly detected by an engineer.

    * Discard States:  The intervention is discarded if a marker is placed (and the intervention has not been
    queued for resolution).

    * Resolve State:  The intervention is queued for a resolution if the engineer has moved to another location (without placing a marker).

    Attributes
    ----------

    Methods
    -------

    """

    def __init__(self, manager, current_location, participant_id, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        current_location : string
            The location of the participant when they detected the threat room.
        participant_id : string
            Unique ID of the engineer participant 
        """

        Intervention.__init__(self, manager, **kwargs)

        self.current_location = current_location
        self.participant_id = participant_id
        self.player_display_name = Intervention.get_player_display_name(participant_id)

        # Register callbacks for when the intervention is connected
        self.add_minecraft_callback(
            MarkerPlacedEvent, self.__onMarkerPlacedMessage)  # discard
        self.add_minecraft_callback(
            LocationEvent, self._onLocationEvent)  # resolution


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.

        The generated dictionary *must* have the properties listed in the
        `Required Fields` below.

        TODO:  Identify a set of universal intervention properties, so that the
               returned information is sufficient for whichever type of
               intervention.

        Return
        -------
        intervention_info : dictionary
            Mapping of relevant intervention properties to needed information

        Required Fields
        ---------------
        participant_ids : list of strings
            List of participant_ids that the intervention should be sent to
        text_message : string
            A (default) text representation of the intervention
        """

        intervention_info = {}

        intervention_info["participant_ids"] = [self.participant_id]
        intervention_info["text_message"] = self.player_display_name + \
            ", did you put a marker block for the threat room you just detected?"

        return intervention_info

    def __onMarkerPlacedMessage(self, message):
        # Ignore this Marker Placed message if it's not associated with the participant
        if message.playername != self.participant_id:
            return
        # Ignore this Marker Placed message if the marker placed is not a threat room marker
        threat_markers = [MinecraftElements.MarkerBlock.red_threat,
                          MinecraftElements.MarkerBlock.blue_threat,
                          MinecraftElements.MarkerBlock.green_threat]
        if message.marker_type not in threat_markers:
            return
        self.logger.info(
            f"{self}: Marker placed for {message.room_name}. Discarding intervention.")
        self.discard()

    def _onLocationEvent(self, message):
        # Ignore this Location Event message if it's not associated with the participant
        if message.playername != self.participant_id:
            return
        # check if the player has moved to a new location
        if self.current_location in message.locations:
            return
        self.logger.info(
            f"{self}: Player has moved to another location i.e. {message.locations[0]}. Queueing intervention.")
        self.queueForResolution()


class SawThreatRoomMarkerTrigger(InterventionTrigger):
    """
    Class that generates SawThreatRoomMarkerIntervention.  Interventions are
    triggered when a threat room is newly detected.

    Arguments
    ---------

    Methods
    -------

    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        # NOTE: for now, there's no message for threat_room_detected yet.
        # Hopefully, we can get this message in the future spiral.
        self.add_minecraft_callback('threat_room_detected',
                               self.__onThreatRoomDetectedMessage)


    def __onThreatRoomDetectedMessage(self, message):
        """
        Callback when an 'threat_room_detected' message is received (e.g.
        in the internal bus)

        Arguments
        ---------
        message : dict
        """
        threat_room = message['threat_room_name']
        participant_id = message['participant_id']

        # only create intervention if the threat room is not already detected before
        # and the participant is an engineer
        if threat_room not in THREAT_ROOMS and message['role'] == 'engineer':
            THREAT_ROOMS.add(threat_room)
            intervention = SawThreatRoomMarkerIntervention(self.manager,
                                                           message['current_location'],
                                                           participant_id)
            self.logger.info(
                f"{self}: Spawning Intervention. Participant {participant_id} detected threat room: {threat_room}")
            self.manager.spawn(intervention)
