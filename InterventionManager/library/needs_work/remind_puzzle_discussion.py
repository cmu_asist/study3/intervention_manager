# -*- coding: utf-8 -*-
"""
.. module:: remind_puzzle_discussion
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger to discuss the meeting room puzzle

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed remind players to discuss the meeting room puzzle.

* Trigger:  A given interval of time elapses and no keywords related to
            the meeting room puzzle have been found in player utterances.

* Discard States:  N/A

* Resolve State:  Immediately queued for resolution.

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..components import InterventionScheduler as Scheduler
from ..mixins import ComplianceMixin

from MinecraftBridge.messages import ASR_Message



class PuzzleDiscussionIntervention(Intervention):
    """
    Intervention to remind team to discuss the puzzle.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        keywords : list of string, default=[]
            List of keywords that indicate discussion of the puzzle
        """
        Intervention.__init__(self, manager, **kwargs)
        self.keywords = kwargs.get('keywords', [])


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {
            'participant_ids': [p.id for p in self.manager.participants],
            'text_message': (
                "There was a meeting that happened before the incident; "
                "there may be a large number of victims in that meeting room. "
                "Do you guys want to discuss about it?"
            )
        }



class PuzzleDiscussionTrigger(InterventionTrigger):
    """
    Class that generates PuzzleDiscussionIntervention instances.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        reminder_time : float, default=300
            Number of seconds into the mission to remind team to discuss
            the puzzle (unless discussion of the puzzle is already detected)
        keywords : list of string, default=[]
            List of keywords that indicate discussion of the puzzle
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Extract kwargs
        self._reminder_time = kwargs.get('reminder_time', 300)
        self._keywords = kwargs.get('keywords', [])

        # Register Minecraft callback
        self.add_minecraft_callback(ASR_Message, self.__onASR_Message)

        # Schedule an intervention
        self._scheduler = Scheduler(self)
        self._scheduler.schedule(self.spawn_intervention, time=self._reminder_time)


    def spawn_intervention(self):
        intervention = PuzzleDiscussionIntervention(self.manager, **self.kwargs)
        self.logger.info(
            f"{self}:  {self._reminder_time} seconds have elapsed. Spawning {intervention}.")
        self.manager.spawn(intervention)


    def __onASR_Message(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.ASR_Message
            The received ASR_Message
        """

        # If puzzle discussion detected, cancel the scheduled intervention
        if any(word in message.text for word in self._keywords):
            self.logger.info(f"{self}:  {message.participant_id} said {message.text}")
            self._scheduler.reset()



class PuzzleDiscussionFollowup(Followup, ComplianceMixin):
    """
    Followup to a PuzzleDiscussionIntervention.
    """

    def __init__(self, intervention, manager, duration=60):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=60
            Number of seconds in the time period of follow up validation
        """
        Followup.__init__(self, intervention, manager)
        ComplianceMixin.__init__(self, timeout=duration)

        # Set attributes
        self._keywords = self.intervention.keywords

        # Register Minecraft callback
        self.add_minecraft_callback(ASR_Message, self.__onASR_Message)


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """
        super()._onTimeout()
        self._onNonCompliance()


    def __onASR_Message(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.ASR_Message
            The received ASR_Message
        """

        # If puzzle discussion detected, record compliance
        if any(word in message.text for word in self._keywords):
            self._onCompliance()
