# -*- coding: utf-8 -*-
"""
.. module:: suggest_player_update_when_victim_moved
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for if Player A moves a victim without
              announcing it and Player B’s belief state about a victim doesn't
              update, to move it themselves

.. moduleauthor:: Max Chis <>

Definition of an Intervention class and InterventionTrigger class for an
intervention designed to encourage players to update that they have moved
a victim if other players are not aware that this has occurred.

* Trigger:  Player picks up victim and places it in a different room.

* Discard States:  Within t seconds, other player's belief states regarding
                   the victim update to acknowledge it is no longer at the
                   prior location.

* Resolve State:  After t seconds, at least one other player's belief state
                  regarding the victim has not updated.

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..mixins import BeliefMixin, ComplianceMixin, VictimMarkerMixin

from collections import defaultdict
from MinecraftBridge.messages import VictimPickedUp, VictimPlaced



class PlayerMovedVictimIntervention(Intervention, BeliefMixin):
    """
    This intervention will alert players when they have moved a victim
    but other players are not aware the victim has been moved.

    Attributes
    ----------
    participant : BaseAgent.Participant
        Participant that moved the victim
    """
    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        trigger_message : MinecraftBridge.messages.VictimPlaced
            The VictimPlaced event that triggered this intervention
        timeout : float, default=15
            Maximum mission time (in seconds) without a discard / resolution
            before queuing for resolution
        """
        Intervention.__init__(self, manager, **kwargs)
        BeliefMixin.__init__(self, manager, **kwargs)
        VictimMarkerMixin.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Set attributes
        trigger_message = kwargs.get('trigger_message')
        self.participant = manager.participants[trigger_message.participant_id]
        self.victim_room = manager.semantic_map.rooms[trigger_message.location]


    def __str__(self):
        """
        String representation of the intervention.
        """
        return f"{self.__class__.__name__} @ {self.participant.id}"


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without a discard or resolution.
        """

        # Queue for resolution if ANY players still believe victim is at prior location
        if self.believe(self.manager.participants, 'victims', self.victim_location, mode='any'):
            self.logger.info(
                f"{self}:  A participant believes victim still present in room. Queuing intervention for resolution.")
            self.queueForResolution()

        else:
            return

            self.logger.info(f"{self}:  Intervention has timed out. Discarding.")
            self.discard()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {
            'default_recipients': [self.participant.id],
            'default_message': (
                f"{self.participant.callsign}, did you inform the other participants "
                "that you moved the victim?"
            ),
        }



class PlayerMovedVictimTrigger(InterventionTrigger):
    """
    Class that generates PlayerMovedVictimIntervention instances.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        timeout : float, default=15
            Maximum mission time (in seconds) without a discard / resolution
            before queuing for resolution
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Set attributes
        self._victim_pickup_room = defaultdict(type(None)) # indexed by participant ID

        # Add Minecraft callbacks
        self.add_minecraft_callback(VictimPlaced, self.__onVictimPlaced)
        self.add_minecraft_callback(VictimPickedUp, self.__onVictimPickedUp)


    def __onVictimPickedUp(self, message):
        room = self.manager.semantic_map.rooms[message.location]
        self._victim_pickup_room[message.participant_id] = room


    def __onVictimPlaced(self, message):
        dropoff_room = self.manager.semantic_map.rooms[message.location]
        pickup_room = self._victim_pickup_room[message.participant_id]

        if dropoff_room != pickup_room:
            participant = self.manager.participants[message.participant_id]
            intervention = PlayerMovedVictimIntervention(
                self.manager, victim_placed_event=message, **self.kwargs)
            self.logger.info(
                f"{self}: {participant.role} has moved a victim. Spawning {intervention}.")
            self.manager.spawn(intervention)
