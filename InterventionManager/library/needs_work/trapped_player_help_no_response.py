# -*- coding: utf-8 -*-
"""
.. module:: trapped_player_help_no_response
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for reminding team about a trapped
   player's unanswered request for help

.. moduleauthor:: Ryan Aponte <ryanaponte00@gmail.com>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to remind the team about unanswered help request.

* Trigger: A player is trapped

* Discard States:
    - a team member approaches the trapped player
    - a team member clears rubble near the trapped player

* Resolve States: 
    The intervention is queued for resolution if trapped player has done one of:
    - requested help verbally
    - placed an SOS block
    And no team member has done one of:
    - approached that player
    - cleared rubble near the playe
    Within threshold seconds

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..mixins import TrappedMixin, ComplianceMixin

from math import pow, sqrt
from MinecraftBridge.messages import MarkerPlacedEvent, PlayerState, RubbleDestroyedEvent



class TrappedPlayerHelpNoResponseIntervention(Intervention):
    """
    A TrappedPlayerHelpNoResponseIntervention is an intervention which
    reminds the team about a player that has requested help but the
    team does not appear to be moving to help that player.

    The intervention is queued for resolution if no team member has moved 
    toward the player or started clearing rubble near that player within 
    threshold seconds.
    """

    def __init__(self, manager, participant, loc, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        participant_id : string
            Participant ID of the intended recipient of this intervention
        loc : tuple of floats
            Location of trapped player (x, y, z)

        Keyword Arguments
        -----------------
        near_player_distance_threshold : float, default=5
            Threshold for team member to be near player (considered when clearing rubble)
        timeout_threshold : float, default=10
            Maximum mission time (in seconds) without a discard before queuing for resolution
        approach_player_distance_threshold: float, default=5
            Threshold for a team member to be considered approaching player
        """
        Intervention.__init__(self, manager, **kwargs)

        # Set attributes
        self.kwargs = kwargs
        self.participant_id = participant.id
        self.loc = loc
        self.help_request_time = None
        # used to track team members to see if they move closer to trapped player
        self.team_member_locs = {p.id: None for p in self.manager.participants}
        self.initial_distances = {p.id: None for p in self.manager.participants}
    
        # Add Minecraft callbacks
        self.add_minecraft_callback(MarkerPlacedEvent,self.__onMarkerPlacedEvent)
        self.add_minecraft_callback(PlayerState,self.__onPlayerStateEvent)
        self.add_minecraft_callback(MarkerPlacedEvent,self.__onMarkerPlacedEvent)
        self.add_minecraft_callback(RubbleDestroyedEvent,self.__onRubbleDestroyedEvent)

        # Add Redis callback 
        self.add_redis_callback(self.__onNLP, 'nlp')


    def __onPlayerStateEvent(self, message):
        """
        Used to track team member locations to see whether the trapped player is being approached.

        As these messages are received frequently, we check if the time for a team member to help
        trapped participant has been exceeded.
        """
        if self.help_request_time is not None:
            if message.time - self.help_request_time > self.kwargs["timeout_threshold"]:
                self.queueForResolution()

        elif message.participant_id != self.participant_id:
            if self.initial_distances[message.participant_id] is None:
                self.initial_distances[message.participant_id] = \
                    sqrt(pow(self.loc[0] - message.x, 2) + 
                    pow(self.loc[2] - message.z, 2))
            else:
                if self.team_member_locs[message.participant_id] is None:
                    return

                # check if distance has decreased by threshold amount, leading to intervention discard
                if (
                    sqrt(pow(self.team_member_locs[message.participant_id][0] - self.loc[0], 2) +
                    pow(self.team_member_locs[message.participant_id].loc[2] - self.loc[2], 2)
                    ) - self.initial_distances[message.participant_id]
                    < self.kwargs["approaching_player_distance_threshold"]
                ):
                    self.discard()


    def __onNLP(self,message):
        """
        Arguments
        ---------
        message : dict
            Received message from the internal Redis 'nlp' channel
        """
        speaker = self.manager.participants[message.data['participant_id']]
        if speaker == self.participant_id:
            if 'help' or 'trapped' in message.data['message']:
                self.logger.info(f"{self}: Trapped player has requested help verbally at time {message.time}.\
                Settting requested help to True.")
                self.help_request_time = message.time


    def __onMarkerPlacedEvent(self,message):
        if self.participant_id == self.manager.participants[message.data['partipant_id']] and 'sos' in message.type.name:
            self.logger.info(f"{self}: Trapped player has requested help with SOS block at time {message.time}.\
                Setting requested help to True.")
            self.help_request_time = message.time


    def __onRubbleDestroyedEvent(self,message):
        """
        If rubble destroyed near target player, discard intervention
        """
        if (sqrt(pow(message.rubble_x - self.loc[0],2) + pow(message.rubble_z - self.loc[2],2))) < \
            self.kwargs["near_player_distance_threshold"]:
            self.discard()


    def getInterventionInformation(self):
        return {
            'default_recipients': self.manager.participants['Engineering_Specialist'],
            'default_message': "A trapped player has requested help, but no team member has gone to them."
        }


class TrappedPlayerHelpNoResponseTrigger(InterventionTrigger, TrappedMixin):
    """
    Trigger for a TrappedPlayerHelpNoResponse intervention.
    """

    def __init__(self,manager,**kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        near_player_distance_threshold : float, default=5
            Threshold for team member to be near player (considered when clearing rubble)
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        TrappedMixin.__init__(self, manager, **kwargs)
        self.kwargs = kwargs
    

    def _onPlayerTrapped(self, participant, loc):
        """
        Callback for when a player is trapped
        """
        intervention = TrappedPlayerHelpNoResponseIntervention(
            self.manager, participant, loc, **self.kwargs)
        self.logger.info(f"{self}:  {participant.role} is trapped. Spawning {intervention}")
        self.manager.spawn(intervention)



class TrappedPlayerHelpNoResponseFollowup(Followup, ComplianceMixin, TrappedMixin):
    """
    A followup to a TrappedPlayerNoResponseIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=10
            Number of seconds in the time period of follow up validation
        near_player_distance_treshold: float, default=5
            Distance for a rubble being cleared to be considered near player
        """
        Followup.__init__(self, intervention, manager)
        ComplianceMixin.__init__(self, timeout=kwargs.get('duration', 10))

        self.add_minecraft_callback(PlayerState, self.__onPlayerState)
        self.add_minecraft_callback(RubbleDestroyedEvent, self.__onRubbleDestroyedEvent)
        self.add_minecraft_callback(ComplianceMixin, self.__onTimeout)


    def __onRubbleDestroyedEvent(self,message):
        """
        If rubble destroyed near target player, compliance
        """
        if (sqrt(pow(message.rubble_x - self.loc[0],2) + pow(message.rubble_z - self.loc[2],2))) < \
            self.kwargs["near_player_distance_threshold"]:
            self._onCompliance()


    def __onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """
        self.logger.info(f"{self}: Timeout for helping trapped player. This is noncompliance.")
        self._onNonCompliance()
    