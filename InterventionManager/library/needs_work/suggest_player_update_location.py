# -*- coding: utf-8 -*-
"""
.. module:: suggest_player_update_location
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for if Player A moves a victim
              without announcing it and Player B’s belief state about
              a victim doesn't update, to move it themselves

.. moduleauthor:: Max Chis <>

Definition of an Intervention class and InterventionTrigger class for an
intervention designed to encourage players to update that they have moved a victim if other players are not aware that this has occurred.
"""

from .. import Intervention
from .. import InterventionTrigger
from ..mixins import BeliefMixin

from MinecraftBridge.messages import (
    MarkerPlacedEvent,
    PlanningStageEvent,
    PlayerState,

)

class playerUpdateLocationIntervention(Intervention, BeliefMixin):
    """
    This intervention will alert players when they have moved a victim but other players are not aware the victim has been moved.

    * Trigger: Player picks up victim.

    * Discard States:  Within t seconds, other player's belief states regarding the victim update to acknowledge it is no longer at the prior location.

    * Resolve State:  After t seconds, at least one other player's belief state regarding the victim has not updated.

    Attributes
    ----------
    participant : BaseAgent.Participant
        Participant that picked up victim
    unknown_loc_time_threshold : float
        Time (in seconds) that the player's location must be unknown by at least one other player for intervention to resolve
    """
    def __init__(self, manager, participant, activation_time, unknown_loc_time_threshold,  **kwargs):
        """
                Arguments
        ---------

        """
        Intervention.__init__(self, manager, **kwargs)
        BeliefMixin.__init__(self, manager, **kwargs)

        pickup_event = kwargs.get('pickup_event')
        self.participant = manager.participants[pickup_event.participant_id]
        self.victim_room = manager.semantic_map.rooms[pickup_event.victim_location]

        # Store the arguments.  Convert the time arguments to milliseconds, to
        # simplify comparison with elapsed milliseconds
        self.participant = participant
        self.activation_time = int(activation_time * 1000)
        self.unknown_loc_time_threshold = int(unknown_loc_time_threshold * 1000)

        # Store attributes needed to check for other player's knowledge of player's location.  These will be updated
        # by PlayerState messages
        self._player_position = (0, 0)      # (x,z) position of the player
        self._current_time = 0              # elapsed_milliseconds

        self.unknown_loc_resolve_time = self._current_time + self.unknown_loc_time_threshold

        # Time (elapsed milliseconds) when the intervention was queued for
        # resolution.  `current_time` could probably be used, but there may be
        # a PlayerState message received before the manager disconnects this
        # from the bus.
        self.resolve_time = -1

        # The intervention will maintain a history of the events and their
        # corresponding occurance time which resulted in a reset of the idle
        # monitoring.
        # NOTE:  elapsed_milliseconds may not be needed, since the messages
        #        contain the elapsed_milliseconds?
        self._behavior_history = []

        self.add_minecraft_callback(PlayerState, self.__onPlayerStateMessage)
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onEvent)

        self.logger.info("%s:  Intervention initialized", self)
        self.logger.info("%s:    Activation Time: %d ms", self, self.activation_time)
        self.logger.info("%s:    Resolution Time: %d ms", self, self.idle_resolve_time)

    def __str__(self):
        """
        String representation of the intervention
        """

        return f"{self.__class__.__name__} @ {self.participant.participant_id}"

    @property
    def behavior_history(self):
        """
        A list of tuples summarizing the events that reset the participant's
        idle state, and at which time.  Tuples indicate the time and message
        resulting in an idle reset: (elapsed_milliseconds, message)
        """

        return self._behavior_history

    def _reset_loc_knowledge_status(self, message=None):
        """
        Resets the attributes used to determine if the participant's location is known', i.e.,
        set the resolve time to the current time plus the idle time threshold.

        Attributes
        ----------
        message : MinecraftBridge.messages, default=None
            Message that caused the reset of idle status, to be stored in the
            behavior history
        """

        # Reset the reference position and future resolve time to the provided
        self.unknown_loc_resolve_time = self._current_time + self.unknown_loc_time_threshold

        # Store the message in the history for any later analysis
        self._behavior_history.append((self._current_time, message))

    def __onPlayerStateMessage(self, message):
        """
        """

        # Does this apply to the participant of interest?
        if message.participant_id != self.participant.participant_id:
            return

        # Update the time and player position
        self._player_position = (message.x, message.z)
        self._player_room = self.manager.semantic_map.rooms[self._player_position]
        self._current_time = message.elapsed_milliseconds

        # Should this message be ignored?  If we haven't reached the activation
        # time yet, no need to continue
        if self._current_time < self.activation_time:
            return

        # If player is
        if message.participant_id == self.manager.participants['Transport_Specialist'].id: return 'player_green'

        # Dictionary to match player id's to their color, for beliefMixin
        dic = {
            self.manager.participants['Transport_Specialist'].id: 'player_green',
            self.manager.participants['Engineering_Specialist_Specialist'].id: 'player_blue',
            self.manager.participants['Medical_Specialist'].id: 'player_red'
        }

        # For each player...
        for participant in self.manager.participants:
            # If other players belief about where they are located contradicts with their actual location...
            for other_participant in self.manager.participants:
                if participant != other_participant and not self.believe(other_participant, dic[message.participant_id], self._player_room):
                    # Check to see if it is time to resolve the the intervention
                    if self._current_time >= self.unknown_loc_resolve_time:
                        self.resolve_time = self._current_time
                        self.logger.info("%s:  Resolving, resolve time %d", self, self.resolve_time)
                        self.queueForResolution()
                else:
                    self._reset_loc_knowledge_status(message)

    def getInterventionInformation(self, **kwargs):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.

        The generated dictionary *must* have the properties listed in the
        `Required Fields` below.

        NOTE:  This method is going to be deprecated soon...

        Return
        -------
        intervention_info : dictionary
            Mapping of relevant intervention properties to needed information

        Required Fields
        ---------------
        playernames : list of strings
            List of playernames that the intervention should be sent to
        text_message : string
            A (default) text representation of the intervention
        """

        intervention_info = {
            "participant_ids": [ self.participant.participant_id ],
            "text_message": "Consider informing other players, verbally or with a marker block, of your location.",
            "default_recipients": [ self.participant.participant_id ],
            "default_message": "Consider informing other players, verbally or with a marker block, of your location."
        }
        return intervention_info


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without a discard or resolution.
        """
        # If all players' ToMs recognize victim is no longer in room
        for participant in self.manager.participant:
            if self.believe(participant, 'victims', self.victim_room):
                self.logger.info(f"{self}:  {participant} believes victim still present in room. Queuing intervention for resolution.")
                self.queueForResolution()
                return
        self.logger.info(
            f"{self}:  Intervention has timed out. Discarding.")
        self.discard()
        return


class playerUpdateLocationTrigger(InterventionTrigger):
    """
    Class that generates playerMoved Interventions.  Interventions are triggered
    for each participant at the end of the planning phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        unknown_loc_time_threshold : float, default=30
            Time (in seconds) that the player's location must be unknown by at least one other player for intervention to resolve
        initial_activation_time : int, default=0
            Activation time, in elapsed_milliseconds
        """
        InterventionTrigger.__init__(self, manager, **kwargs)

        self.idle_time_threshold = kwargs.get("unknown_loc_time_threshold", 30)
        self.initial_activation_time = kwargs.get("initial_activation_time", 0)

        self.kwargs = kwargs
        # Register to receive the following messages
        self.add_minecraft_callback(PlanningStageEvent, self.__onPlanningStageEvent)

    def __onPlanningStageEvent(self, message):
        """
        Callback when PlanningStage event messages are received.  If the mission
        state is a start, then a PlayerIdleIntervention should be spawned for
        each participant.

        Arguments
        ---------
        message : PlanningStageEvent
            PlanningStage Event message
        """

        # If the planning stage is ending, then spawn an Intervention for each
        # participant.
        if message.sttate == PlanningStageEvent.PlanningState.Stop:
            for participant in self.manager.participants:
                # Create an intervention for the participant, setting the
                # activation time to 0 (can start monitoring immediately)
                intervention = playerUpdateLocationIntervention(self.manager, participant,
                                                    self.initial_activation_time,
                                                    self.unknown_loc_time_threshold,
                                                    parent=self)
                self.logger.info("%s:  Spawning Intervention: %s", self, intervention)
                self.manager.spawn(intervention)

