# -*- coding: utf-8 -*-
"""
.. module:: encourage_verbal
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger to encourage verbal communication

.. moduleauthor:: Noel Chen <noelchen@cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed remind players to talk more in the trial.

* Trigger:  The intervention is triggered when the players don't speak
            to each other for more than some given period of time.

* Discard States:  N/A

* Resolve State:  Immediately queued for resolution

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..components import InterventionScheduler as Scheduler
from ..mixins import ComplianceMixin

from MinecraftBridge.messages import (
    PlanningStageEvent,
    CMU_TA2_TED
)



class EncourageVerbalInterventionTED(Intervention):

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        minimum_elapsed_time : double, default=120.0
            Minimum amount of time (in seconds) that the intervention must be
            active before being able to be resolved
        communication_rate_threshold : double, default=0.75
            Threshold of communication rate required for resolution
        """

        Intervention.__init__(self, manager, **kwargs)

        self._minimum_elapsed_time = kwargs.get("minimum_elapsed_time", 120.0)
        self._communication_rate_threshold = kwargs.get("communication_rate_threshold", 0.75)

        # What is the earliest that this resolution can be resolved
        self._earliest_resolution_time = self.manager.mission_clock.elapsed_milliseconds + 1000*self._minimum_elapsed_time

        # Indicate that all participants are relevant to this intervention
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.participant_id)

        # Add callback to recieve information from the TED agent
        self.add_minecraft_callback(CMU_TA2_TED, self.__onTED)


    def __onTED(self, message):
        """
        Callback when a TED message is received

        Arguments
        ---------
        message : MinecraftBridge.messages.CMU_TA2_TED
        """

        # Is it too early to consider messages?
        if message.elapsed_ms < self._earliest_resolution_time:
            return

        # Otherwise, check to see if the players have been communicating below
        # the threshold rate
        if (1000*message.comms_total_words / message.elapsed_ms) < self._communication_rate_threshold:

            # Participants have not been talking!  Spawn a new instance of this intervention
            # for future monitoring, and resolve
            self.manager.spawn(EncourageVerbalInterventionTED(self.manager,
                                                              minimum_elapsed_time = self._minimum_elapsed_time,
                                                              communication_rate_threshold = self._communication_rate_threshold))
            self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': (
                "Team, you should communicate more."
            )            
        }



class EncourageVerbalTriggerTED(InterventionTrigger):
    """
    Class that generates EncourageVerbal Interventions.  Interventions are 
    triggered when the Team stay quiet for a given period of time.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        silence_time_threshold : float, default=120
            Minimum number seconds the players can remain silent before intervening
            to encourage verbal communication
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs


        # Add Minecraft callback
        self.add_minecraft_callback(PlanningStageEvent, self.__onPlanningStage)


    def __onPlanningStage(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.PlanningStageEvent
            The received PlanningStageEvent message
        """

        # If the planning stage has ended, then spawn the initial intervention
        if message.state == PlanningState.Stop:
            self.manager.spawn(EncourageVerbalInterventionTED(self.manager, 
                                                              minimum_elapsed_time=kwargs.get("minimum_elapsed_time", 120.0),
                                                              communication_rate_threshold=kwargs.get("communication_rate_threshold", 0.75)))


class EncourageVerbalIndividualComplianceFollowupTED(Followup, ComplianceMixin):
    """
    Followup to determine if an individual has complied with the advice from
    EncourageVerbalIntervention.  An individual is considered compliant if they
    are the source of an utterance within a certain period of time.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=60
            Number of seconds in the time period of follow up validation
        """

        duration = kwargs.get("duration", 60)


        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)

        # Set attributes
        self._duration = duration

        # List of events relevant to this followup
        self._history = []

        # Add Minecraft callback
        self.add_minecraft_callback(ASR_Message, self.__onASR_Message)


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """

        self._onNonCompliance()


    def participant_spoke(self, participant_id):
        """
        Callback from linked followups when a particular participant has spoken

        Arguments
        ---------
        participant_id : string
            ID of the participant who had spoke
        """

        # Not used by the individual intervention
        pass


    def __onASR_Message(self, message):
        """
        Callback when an ASR message is received.  If the ASR message is from
        the target participant, then consider that participant as compliant.

        Arguments
        ---------
        message : ASR_Message
            Received ASR Message
        """

        # Who did the message come from
        if message.participant_id == self._target_participant:

            self._history.append(message)

            # Let any linked followups know that this participant has spoken
            for followup in self._linked_followups:
                followup.participant_spoke(self._target_participant)

            # Indicate that this message is compliant
            self._onCompliance()


class EncourageVerbalTeamComplianceFollowupTED(Followup, ComplianceMixin):
    """
    Followup to determine if a team has complied with the advice from
    EncourageVerbalIntervention.  An team is considered compliant if a minimum
    number of participants have spoken within the followup time.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=60
            Number of seconds in the time period of follow up validation
        min_number_speakers : int, default=2
            Required number of participants to have spoken to have the team be
            considered compliant
        """

        duration = kwargs.get("duration", 60)

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)

        # Set attributes
        self._duration = duration
        self._min_number_speakers = kwargs.get('min_number_speakers', 2)
        self._speakers = set()


    def participant_spoke(self, participant_id):
        """
        Callback from linked followups when a participant has spoken.  Keep
        track of the fact that that the participant has said something, for use
        when determining final team compliance.
        """

        self._speakers.add(participant_id)


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """

        if len(self._speakers) >= self._number_speakers:
            self._onCompliance()
        else:
            self._onNonCompliance()

