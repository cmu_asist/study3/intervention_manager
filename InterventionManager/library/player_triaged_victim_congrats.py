# -*- coding: utf-8 -*-
"""
.. module:: player_triaged_victim_congrats
   :platform: Linux, Windows, OSX
   :synopsis: Test Intervention that congratulates a player for triaging

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention that congratulates a player if they triage a victim.  This 
intervention is for test purposes only.
"""

from .. import Intervention
from .. import InterventionTrigger

from MinecraftBridge.messages import TriageEvent



class PlayerTriagedVictimCongratsIntervention(Intervention):
    """
    A PlayerSawVictimIntervention is an Intervention which is designed to alert
    the player to a potentially unperceived victim.  

    * Trigger: The intervention is triggered when a victim enters the player's
               FoV with fewer than N pixels

    * Discard States:  The intervention is discarded if the number of pixels
                       the victim exceeds the threshold N

    * Resolve State:  The intervention is queued for a resolution if when the
                      victim is no longer in the player's FoV and the player
                      moves away from the victim, and the intervention has not
                      been discarded.

    Attributes
    ----------

    Methods
    -------

    """

    def __init__(self, manager, participant_id, victim_type, 
                       triage_message, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : string
            Unique ID of the participant who may or may not have seen a victim
        victim_type : string
            Type of victim triaged
        triage_message : MinecraftBridge.messages.TriageEvent
            Message that triggered this intervention
        """

        Intervention.__init__(self, manager, **kwargs)

        # Intervention specific attributes
        self.participant_id = participant_id
        self.victim_type = victim_type
        self.triage_message = triage_message

        self.victim_type = self.triage_message.type

        self.addRelevantParticipant(self.participant_id)


    def _onActivated(self):
        """
        Callback from the manager when the intervention is activated.  When
        activated, the intervention should directly be queued for resolution.
        """

        self.logger.info("%s: Queuing for resolution", self)
        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.

        Return
        -------
        intervention_info : dictionary
            Mapping of relevant intervention properties to needed information

        Fields
        ------
        participant_ids : list of strings
            List of participant_ids that the intervention should be sent to
        text_message : string
            A (default) text representation of the intervention
        default_recipieints : list of strings
            Default list of participant_ids to present the intervention to
        default_message : string
            Default text representation of the intervention
        target_participant_id : string
            ID of the participant this intervention is monitoring
        victim_type : string
            Type of victim triaged

        """


        victim_type = { "victim_a": "Regular",
                        "victim_b": "Regular",
                        "victim_c": "Critical" }.get(str(self.victim_type), "some kind of")

        intervention_info = {
            "default_recipients": [ self.participant_id ],
            "default_message": "Good Job!  You saved a {} victim!".format(victim_type),
            "target_participant_id": self.participant_id,
            "victim_type": str(self.victim_type),
            "victim_location": self.triage_message.victim_location
        }                        


        return intervention_info



class PlayerTriagedVictimCongratsTrigger(InterventionTrigger):
    """
    Class that generates PlayerTriagedVictimCongratsInterventions.  
    Interventions are triggered when a victim is triaged

    Arguments
    ---------

    Methods
    -------

    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        # Register the trigger to receive FoV messages
        self.add_minecraft_callback(TriageEvent, self.__onTriage)


    def __onTriage(self, message):
        """
        Callback when a triage event message is received.

        Arguments
        ---------
        message : MinecraftBridge.messages.TriageEvent
            Instance of the TriageEvent message
        """

        self.logger.debug("%s: Received TriageEvent Message", self)

        # Check if the TriageEvent type is SUCCESSFUL
        if message._triage_state == TriageEvent.TriageState.SUCCESSFUL:
            intervention = PlayerTriagedVictimCongratsIntervention(self.manager,
                                                                   message.participant_id,
                                                                   message.type,
                                                                   message)
            self.logger.info("%s: Spawning Intervention %s", self, intervention)
            self.manager.spawn(intervention)

