# -*- coding: utf-8 -*-
"""
.. module:: time_elapsed
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for reminding players when a limited
              amount of time remains

.. moduleauthor:: Noel Chen <noelchen@cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to remind players that a limited amount of time remains.

* Trigger:  The intervention is triggered when the remaining time for a
            a trial is less than t minutes.

* Discard States:  N/A

* Resolve State:  Immediately queued for resolution

"""

from .. import Intervention
from .. import InterventionTrigger

from ..components import InterventionScheduler as Scheduler



class TimeElapsedIntervention(Intervention):
    """
    An Intervention which is designed to alert the team
    that there is only a few minutes left in the mission.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        minutes_left_threshold : int
            Minutes left in the trial for the intervention to happen
        """
        Intervention.__init__(self, manager, **kwargs)
        self._minutes_left_threshold = kwargs.get('minutes_left_threshold', 3)

        # Indicate that all participants are relevant to this intervention
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.participant_id)


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': f"There are {self._minutes_left_threshold} minutes left.",            
        }



class TimeElapsedTrigger(InterventionTrigger):
    """
    Class that generates TimeElapsedIntervention instances.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        minutes_left_threshold : int
            Minutes left in the trial for the intervention to happen
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Trigger-specific attributes
        self._minutes_left_threshold = kwargs.get('minutes_left_threshold', 3)

        # Schedule an intervention
        self._scheduler = Scheduler(self)
        self._scheduler.countdown(
            self.spawn_intervention, t_minus=60*self._minutes_left_threshold)


    def spawn_intervention(self):
        """
        Spawn a TimeElapsedIntervention.
        """
        intervention = TimeElapsedIntervention(self.manager, **self.kwargs)
        self.logger.info(
            f"{self}:  Spawning {intervention}, {self._minutes_left_threshold} minutes left!")
        self.manager.spawn(intervention)
