# -*- coding: utf-8 -*-
"""
.. module:: encourage_sync_triage_evacuation
   :platform: Linux, Windows, OSX
   :synopsis: This intervention will monitor the number of unevacuated triaged
              victims, and suggest the team to synchronize triage and evacuation
              if this number becomes too large.

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

* Trigger:  The number of unevacuated triaged victims exceeds some threshold

* Discard States:  N/A

* Resolve State:  Immediately queued for resolution

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..mixins import ComplianceMixin
from ..utils import VictimMonitor
from MinecraftBridge.messages import VictimEvacuated



class SyncTriageAndEvacuationIntervention(Intervention):
    """
    Intervention designed to encourage team to synchronize triage and evacuation.
    """

    def __init__(self, manager, victim_monitor, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        victim_monitor : VictimMonitor
            Monitor for triaged / evacuated victims
        """
        Intervention.__init__(self, manager, **kwargs)
        self.victim_monitor = victim_monitor.copy(self)

        # Indicate that all participants are relevant to this intervention
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.participant_id)
        
        self._message = kwargs.get("message", "Team, I would suggest evacuating victims immediately after triaging them.")


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        self.queueForResolution()


    def getInterventionInformation(self):
        message = (
            "Team, I would suggest evacuating victims immediately after triaging them."
        )
        return {
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self._message,
        }



class SyncTriageAndEvacuationTrigger(InterventionTrigger):
    """
    Trigger that spawns SyncTriageAndEvacuationIntervention instances.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        triage_excess_threshold : int, default=5
            Minimum number of unevacuated triaged victims needed to trigger an intervention
        """
        InterventionTrigger.__init__(self, manager, **kwargs)

        # Trigger-specific attributes
        self._triage_excess_threshold = kwargs.get('triage_excess_threshold', 5)
        self._victim_monitor = VictimMonitor(self, on_triage=self._on_triage)
        self._has_triggered = False

        # Disable the intervention trigger for x seconds after each victim 
        self._evac_silence = kwargs.get("evac_silence", 45)
        self.add_minecraft_callback(VictimEvacuated, self._silenceIntervention)
        self._last_evacuated = 0
    
    def _silenceIntervention(self, message):
        """
        When any team member evacuated a victim within the past _evac_silence seconds, this intervention will not trigger
        """
        self._last_evacuated = message.elapsed_milliseconds


    def spawn_intervention(self):
        if not self._has_triggered: # only triggers once
            intervention = SyncTriageAndEvacuationIntervention(self.manager, self._victim_monitor)
            self.logger.info(
                f"{self}: There are many unevacuated triaged victims. Spawning {intervention}")
            self.manager.spawn(intervention)
            self._has_triggered = True


    def _on_triage(self, msg):
        silenced = (msg.elapsed_milliseconds - self._last_evacuated) < self._evac_silence * 1000
        if self._victim_monitor.triage_excess() >= self._triage_excess_threshold and not silenced:
            self.spawn_intervention()



class SyncTriageAndEvacuationFollowup(Followup, ComplianceMixin):
    """
    Followup to an SyncTriageAndEvacuationIntervention.
    """

    def __init__(self, intervention, manager, duration=90, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=90
            The duration of the followup validation period (in seconds)
        """
        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)

        # Initialize rescue monitor
        self._victim_monitor = intervention.victim_monitor.copy(self)
        self._initial_triage_excess = self._victim_monitor.triage_excess()


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """
        if self._victim_monitor.triage_excess() < self._initial_triage_excess:
            self._onCompliance()
        else:
          self._onNonCompliance()


### NOTE:  remind_start_evacuation.StartEvacuationIndividualCompliantFollowup
###        and remind_start_evacuation.StartEvacuationTeamComplianceFollowup
###        may apply for individual and team compliance with this intervention,
###        at least in its current formulation.  CompliantInitiator could 
###        simply use those classes, if desired.