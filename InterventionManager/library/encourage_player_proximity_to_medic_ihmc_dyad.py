# -*- coding: utf-8 -*-
"""
.. module:: encourage_proximity
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for team to work in an appropriate proximity

.. moduleauthor:: Simon Stepputtis <stepputtis@andrew.edu>
"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..components import InterventionScheduler as Scheduler
from ..mixins import ComplianceMixin, SawVictimMixin

import numpy as np
from datetime import datetime

from itertools import combinations
from MinecraftBridge.messages import PlayerState, MarkerPlacedEvent, VictimEvacuated, ScoreboardEvent
from MinecraftBridge.messages.analytic_components.ihmc_ta2_dyad import IHMC_Dyad
import MinecraftElements


class DyadMonitor(object):
    """
    Monitor for the Dyad AC
    This monitor is updated by the trigger and interventions are created every two minutes
    evaluate the Dyad statistics
    """
    def __init__(self, **kwargs):
        """
        Initializer for the Dyad monitor

        Arguments:
        ---------
        discard_period : int
            Duration in seconds to discard dyads by counted against the elapsed milliseconds of the trial
        """

        # Rate of support the engineer should supply
        self._eng_dependance_rate = kwargs.get("eng_dependance_rate", 0.8)

        # Rate of support the transporter should supply to individual players
        self._transporter_team_focus_rate = kwargs.get("transporter_team_focus_rate", 0.3)

        # Teamwork realtion factor
        self._teamwork_relation_factor = kwargs.get("teamwork_relation_factor", 2.0)

        # Store discard period and converd to milliseconds
        self._discard_period = kwargs.get("discard_period", 300)
        self._discard_period = self._discard_period * 1000

        # Track the active and completed dyads
        self._dyad = {}
        # Track the milliseconds to assess team performance globally
        self._last_dyad_update = 0

        # Continuous Metrics:
        # M-T: Medic vs. Transporter
        # M-E: Medic vs. Engineer
        # T-E: Transporter vs Engineer
        # value: Tracks the effective cooperation time
        self._metrics = {"M-T": 0.0, "M-E": 0.0, "T-E": 0.0 }

        # Keep track of which role has which player ID
        self._role_to_pid = {}

    def update(self, message):
        """
        Adds, updates or completes a dyad and tracks its metrics

        Arguments:
        message : IHMC_Dyad
            Message tracking updating ongoing dyads
        """
        time = message.elapsed_milliseconds
        self._last_dyad_update = time
        
        # If this is a new message, initialize it and store the participants
        # Note, the "start" event is not reliable here since there can be single-instance events.
        # In such cases, "end" is issued immediately
        if message.id not in self._dyad.keys():
            self._dyad[message.id] = {
                "participants": message.participants, 
                "duration": -1, 
                "probability": [], 
                "start_time": time, 
                "last_update": time, 
                "effective_time": [],
                "complete": False
            }
            # Hold a dict of role-to-player-id
            for p in message.participants:
                if p.role not in self._role_to_pid.keys():
                    self._role_to_pid[p.role] = p.participant_id
        
        # Calculate the length of the last segment
        time_since_last_update = np.abs(self._dyad[message.id]["last_update"] - time)
        # the probability tells us how close the cooperation is. 
        # Multiply that with the length of the last segment if this is not the first one
        # However, check first if we had previous updates to this team interaction
        if len(self._dyad[message.id]["probability"]) > 0:
            self._dyad[message.id]["effective_time"].append(time_since_last_update * self._dyad[message.id]["probability"][-1])
        # Track the teamwork probability for the current segment
        self._dyad[message.id]["probability"].append(message.in_dyad_probability)

        # If this was the last message, finalize this dyad
        if message.event_type == IHMC_Dyad.EventType.end:
            self._dyad[message.id]["duration"] = message.duration
            self._dyad[message.id]["complete"] = True
            # If this was the first and last message, take the duration as effective time:
            if len(self._dyad[message.id]["probability"]) > 0 and message.duration is not None:
                self._dyad[message.id]["effective_time"].append(message.duration * message.in_dyad_probability)
            if time > self._discard_period:
                self._addToMetrics(self._dyad[message.id])


        # Finally, recognize the last update to this dyad
        self._dyad[message.id]["last_update"] = time

    def _addToMetrics(self, dyad):
        """
        Adds a completed dyad to the cummulative metrics
        """
        # Quickly check if this dyad is valid:
        if not dyad["complete"]:
            return

        # Calculate the effective time of this dyad:
        effective_time = 0 if len(dyad["effective_time"]) == 0 else np.mean(dyad["effective_time"])

        # Figure out which participants were part of this dyad:
        if np.all([p.role in ["Medical_Specialist", "Transport_Specialist"] for p in dyad["participants"]]):
            self._metrics["M-T"] += effective_time
        elif np.all([p.role in ["Medical_Specialist", "Engineering_Specialist"] for p in dyad["participants"]]):
            self._metrics["M-E"] += effective_time
        elif np.all([p.role in ["Transport_Specialist", "Engineering_Specialist"] for p in dyad["participants"]]):
            self._metrics["T-E"] += effective_time
        
    def _getTargetScore(self):
        """
        Based on high performing teams, we expect a certain coordination between the team members
        Excluding the first 2 minutes of KIT, we can approximate the teamwork as a linear equation:
        - We want ~75k teamwork score between the medic and engineer after 17 minutes.
        - Data based on Spiral-4 HSR data
        """
        m = 0.08
        b = -10000
        return m * self._last_dyad_update + b

    def getOptimality(self):
        """
        Evaluates the team's relative performance regarding their support of the medic
        There are three main criteria:
        - Is the cooperation between the engineer and medic within the expected range (80%)
        - Is the cooperation between the transporter and medic/engineer similar (within 30%)
        - Is the absolute cooperation between the medic and engineer significant (at least factor 2)

        Returns the optimality of the Engineer and Transporter
        """
        desired_score = self._getTargetScore()
        
        # Check if we have enough data yet:
        if self._metrics["M-E"] <= 0.01:
            return True, True
        
        # Evaluate the engineer's performance (Evaluates if the team is generally on track)
        eng_perf = self._metrics["M-E"] / desired_score
        eng_perf = eng_perf > self._eng_dependance_rate

        # The transporter should not cooperate with anyone (test similarity)
        tns_perf_relative = np.abs(1.0 - (self._metrics["M-T"] / self._metrics["T-E"]))

        # Interactions involving the transporter should be significantly smaller than the ones between the medic and engineer
        tns_perf = self._metrics["M-E"] / ((self._metrics["M-T"] + self._metrics["T-E"]) / 2.0)

        # Combine the two above metrics
        tns_perf = tns_perf_relative < self._transporter_team_focus_rate and tns_perf > self._teamwork_relation_factor

        return eng_perf, tns_perf
    
    def getPlayerIDFromRole(self, role):
        """
        Convers a role description to a player ID
        """
        return self._role_to_pid[role]

    def resetMetrics(self):
        """
        Resets the metrics, e.g., after the KIT
        """
        self._metrics = {"M-T": 0.0, "M-E": 0.0, "T-E": 0.0 }

    
class EncouragePlayerProximityToMedicIHMCDyad(Intervention):
    """
    An Intervention designed to encourage / discourage team proximity
    if the team's behaviour is deemed suboptimal.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        dyad_monitor : ProximityMonitor
            Team proximity history
        trans_last_marker_thresh : int(second), default=20
            Threshold of time duration since transporter last marked rooms required for resolution
        trans_last_evac_thresh : int(second), default=20
            Threshold of time duration since transporter last evacuated victims required for resolution
        since_transporter_last_marked_room_time: int(second)
            Time duration since transporter last marked rooms
        since_transporter_last_evac_time : int
            Time duration since transporter last evacuated victims 
        """
        Intervention.__init__(self, manager, **kwargs)

        # Set attributes
        self._dyad_monitor = kwargs.get("dyad_monitor", None)
        self.trans_last_marker_thresh = kwargs.get("trans_last_marker_thresh", 20)
        self.trans_last_evac_thresh = kwargs.get("trans_last_evac_thresh", 20)
        self.since_transporter_last_marked_room_time = kwargs.get("since_transporter_last_marked_room_time", 100)
        self.since_transporter_last_evac_time = kwargs.get("since_transporter_last_evac_time", 100)


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately determine whether to resolve or discard.
        """

        # If the dyad monitor is somehow invalid, discard
        if self._dyad_monitor is None:            
            self.discard()

        # Evaluate team performance
        eng_perf, tns_perf = self._dyad_monitor.getOptimality()
    
        # If both players are performant, nothing needs to be done
        if eng_perf and tns_perf:
            self.logger.info(
                "%s: Discarding strategy advice. Team is performing well!",
                self
            )
            self.discard()
        elif eng_perf and not tns_perf:
            if self.since_transporter_last_marked_room_time < self.trans_last_marker_thresh:
                self.logger.info(
                    "%s: Discarding strategy advice. Transporter is marking rooms.",
                    self
                )
                self.discard()
            
            elif self.since_transporter_last_evac_time < self.trans_last_evac_thresh:
                self.logger.info(
                    "%s: Discarding strategy advice. Transporter is evacuating victims.",
                    self
                )
                self.discard()

            else:
                self.logger.info(
                    "%s: Transporter is underperforming, since_transporter_last_marked_room_time: %d\tsince_transporter_last_evac_time: %d",
                    self, self.since_transporter_last_marked_room_time, self.since_transporter_last_evac_time
                )

                self._text_message = "Transporters focusing on marking rooms and evacuating victims usually leads to higher scores."
                self._target_player = [self._dyad_monitor.getPlayerIDFromRole("Transport_Specialist")]
                self.queueForResolution()
        elif not eng_perf and tns_perf:
            self.logger.info(
                "%s: Strategy advice: Engineer is underperforming",
                self
            )

            self._text_message = "Engineers focusing on helping the medic triaging victims usually leads to higher scores."
            self._target_player = [self._dyad_monitor.getPlayerIDFromRole("Engineering_Specialist")]
            self.queueForResolution()
        elif not eng_perf and not tns_perf:
            self.logger.info(
                "%s: Engineer and Transporter are underperforming, since_transporter_last_marked_room_time: %d\tsince_transporter_last_evac_time: %d",
                self, self.since_transporter_last_marked_room_time, self.since_transporter_last_evac_time
            )

            if self.since_transporter_last_marked_room_time < self.trans_last_marker_thresh or self.since_transporter_last_evac_time < self.trans_last_evac_thresh:
                self._text_message = "Engineers and Medics working closely together usually leads to higher scores."
                self._target_player = [self._dyad_monitor.getPlayerIDFromRole("Engineering_Specialist"), self._dyad_monitor.getPlayerIDFromRole("Medical_Specialist")]
            else:
                self._text_message = "Team, generally, Transporters marking rooms and Engineers working closely together with the Medic to triage critical victims leads to higher scores."            
                self._target_player = [p.id for p in self.manager.participants]
            self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {
            'default_recipients': self._target_player,
            'default_message': self._text_message,            
        }



class EncouragePlayerProximityToMedicIHMCDyadTrigger(InterventionTrigger, SawVictimMixin):
    """
    Class that generates an intervention to encourage player proximity.
    Interventions are triggered when a specified time interval has elapsed 
    without team progress, with progess defined as any of the following:

        1. Discovering new victims
        2. Triaging victims
        3. Evacuating victims
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        initial_observation_period : int, default=300
            The time we start check the dyad scores after mission starts (in seconds)
        player_checkin_period : int, default=120
            How often do we check the dyad values (in seconds)
        full_score : int, default=950
            Maximum score a team can get
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        SawVictimMixin.__init__(self, manager, **kwargs)
        self.kwargs = kwargs
        self._initial_observation_period = kwargs.get('initial_observation_period', 300)
        self._player_checkin_period = kwargs.get('player_checkin_period', 120)
        self._full_score = kwargs.get('full_score', 950)
        
        # Keep track of the Dyads
        self._dyad_monitor = DyadMonitor(**kwargs)

        # Add Minecraft callbacks
        self.add_minecraft_callback(IHMC_Dyad, self._updateDyad)
        self.add_minecraft_callback(MarkerPlacedEvent, self._onMarkerPlacedEvent)
        self.add_minecraft_callback(VictimEvacuated, self._onVictimEvacuated)
        self.add_minecraft_callback(PlayerState, self._onPlayerState)
        self.add_minecraft_callback(ScoreboardEvent, self._onScoreboardEvent)

        # Initially, we want to observe the first 5 minutes of gameplay without intervening
        self._scheduler = Scheduler(self)
        # Subtract the player_checkin_period as the repeat immediately triggers
        self._scheduler.wait(self._enableIntervention, delay=self._initial_observation_period)

        self.TRANSPORTER_SIGNAL_MARKERS = set([MinecraftElements.Block.green_regularvictim,
                                               MinecraftElements.Block.green_criticalvictim,
                                               MinecraftElements.Block.green_novictim])
        # initialize time
        self.transporter_last_marked_room_time = datetime(2000, 1, 1)
        self.transporter_last_evac_time = datetime(2000, 1, 1)

        self.score = 0

    def _onScoreboardEvent(self, message):
        self.score = message.scoreboard["TeamScore"]

    def _onPlayerState(self, message):
        # this callback is used only to catch the current timestamp
        self.timestamp = message.headers["msg"].timestamp

    def _onMarkerPlacedEvent(self, message):
        timestamp = message.headers["msg"].timestamp
        markerblock_type = message.type
        # if transporter placed a markerblock for victim signals
        if markerblock_type in self.TRANSPORTER_SIGNAL_MARKERS:
            self.transporter_last_marked_room_time = timestamp


    def _onVictimEvacuated(self, message):
        if message.success == True:
            timestamp = message.headers["msg"].timestamp
            participant_id = message.participant_id
            # if player is transporter
            if participant_id == self._dyad_monitor.getPlayerIDFromRole("Transport_Specialist"):
                self.transporter_last_evac_time = timestamp


    def _updateDyad(self, message): 
        """
        Callback when receiving a Dyad message. Even though we do not want to look at initially completed teamwork,
        active dyads should still be tracked for alter use. 
        """
        self._dyad_monitor.update(message)        

    def _enableIntervention(self):
        """
        After we waited for 5 minutes, we check in with the players every two minutes
        """
        self._dyad_monitor.resetMetrics()
        self._scheduler.repeat(self._spawn_intervention, interval=self._player_checkin_period)

    def _spawn_intervention(self):
        since_transporter_last_marked_room_time = (self.timestamp - self.transporter_last_marked_room_time).total_seconds()
        since_transporter_last_evac_time = (self.timestamp - self.transporter_last_evac_time).total_seconds()

        # stop sending interventions when team reaches full score
        if self.score == self._full_score:
            return

        intervention = EncouragePlayerProximityToMedicIHMCDyad(
            self.manager, dyad_monitor=self._dyad_monitor,
            since_transporter_last_marked_room_time=since_transporter_last_marked_room_time, 
            since_transporter_last_evac_time=since_transporter_last_evac_time,
            **self.kwargs)
        self.manager.spawn(intervention)



class EncouragePlayerProximityToMedicIHMCDyadFollowup(Followup, ComplianceMixin):
    """
    Followup to an EncourageProximityIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=60
            Number of seconds in the time period of follow up validation
        """

        duration = kwargs.get("duration", 60)

        Followup.__init__(self, intervention, manager)
        ComplianceMixin.__init__(self, timeout=duration)

    def __acCallback(self, message):
        self._current_value = 0


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """

        # Evaluate the team response to the intervention
        if self._initial_value < self._current_value:
            self._onCompliance()
        else:
            self._onNonCompliance()
