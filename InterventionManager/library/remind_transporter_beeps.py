# -*- coding: utf-8 -*-
"""
.. module:: remind_transporter_beeps
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for reminding transporter about beeps

.. moduleauthor:: Ryan Aponte <ryanaponte00@gmail.com>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to remind the transporter to pay attention to beeps.
Contains many similarities to medic_forgot_to_place_markerblock intervention.

* Trigger:  A beep signal is given due to a victim in proximity to the transporter

* Discard States:  Transporter places correct marker block in proximity of the signal

* Resolve States:
    -  Transporter leaves proximity of the victim
    -  A given time interval elapses without discard / resolution
"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from MinecraftBridge.utils import Loggable
import MinecraftElements

from ..mixins import ComplianceMixin
from MinecraftBridge.messages import (
    FoVSummary,
    MarkerDestroyedEvent,
    MarkerPlacedEvent,
    MarkerRemovedEvent,
    PlayerState,
    TriageEvent,
    VictimPickedUp,
    VictimPlaced,
    VictimSignal
)

from collections import defaultdict

import numpy as np
from ..utils import distance

from ..components import MarkerBlocksMonitor
from ..components import InterventionScheduler

# NOTE:  Marker blocks (for Study 3) will always have a y position of 60
DEFAULT_MARKER_Y = 60


VICTIM_MARKER_BLOCKS = { MinecraftElements.Block.red_regularvictim,
                         MinecraftElements.Block.red_criticalvictim,
                         MinecraftElements.Block.red_novictim,                         
                         MinecraftElements.Block.green_regularvictim,
                         MinecraftElements.Block.green_criticalvictim,
                         MinecraftElements.Block.green_novictim,
                         MinecraftElements.Block.blue_regularvictim,
                         MinecraftElements.Block.blue_criticalvictim,
                         MinecraftElements.Block.blue_novictim
                         }

###VICTIM_MARKER_TO_SIGNAL_MAP = { MinecraftElements.Block.red_regularvictim: "Regular Victim Detected",
###                                MinecraftElements.Block.red_criticalvictim: "Critical Victim Detected",
###                                MinecraftElements.Block.red_novictim: "No Victim Detected",
###                                MinecraftElements.Block.green_regularvictim: "Regular Victim Detected",
###                                MinecraftElements.Block.green_criticalvictim: "Critical Victim Detected",
###                                MinecraftElements.Block.green_novictim: "No Victim Detected",
###                                MinecraftElements.Block.blue_regularvictim: "Regular Victim Detected",
###                                MinecraftElements.Block.blue_criticalvictim: "Critical Victim Detected",
###                                MinecraftElements.Block.blue_novictim: "No Victim Detected"
###                              }



def is_correct_block_for_signal(block, signal_message):
    """
    Arguments
    ---------
    block : MinecraftElements.Block
        Block placed
    signal_message : string
        Message from victim signal

    Returns
    -------
    True if the block is correct for the given message, False otherwise
    """

    # NOTE:  Depending on the version of the testbed, the signal may put out,
    #        e.g., either "Regular Victim Here" or "Regular Victim Detected".
    #        This dictionary simply maps to the critical component of the 
    #        phrase in an attempt to be robust to these kinds of variations.
    KEYWORD = { MinecraftElements.Block.red_regularvictim: "Regular Victim",
                MinecraftElements.Block.red_criticalvictim: "Critical Victim",
                MinecraftElements.Block.red_novictim: "No Victim",
                MinecraftElements.Block.green_regularvictim: "Regular Victim",
                MinecraftElements.Block.green_criticalvictim: "Critical Victim",
                MinecraftElements.Block.green_novictim: "No Victim",
                MinecraftElements.Block.blue_regularvictim: "Regular Victim",
                MinecraftElements.Block.blue_criticalvictim: "Critical Victim",
                MinecraftElements.Block.blue_novictim: "No Victim"
              }

    if not block in KEYWORD:
        return False

    return KEYWORD[block] in signal_message




################################################################################
################################################################################


class RemindTransporterBeepsTrigger(InterventionTrigger):
    """
    Class that generates RemindTransporterBeepsIntervention Interventions.
    Interventions are triggered when the transporter receives a beep.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        marker_distance_threshold : float, default=1
            Maximum distance a marker can be from the signal location to be
            considered as marking the signal.
        distance_threshold : float, default=5
            Maximum distance from victim that a player can go
            before the victim is considered to have been "left"
        timeout : float, default=None
            Maximum mission time (in seconds) without a discard / resolution
            before queuing for resolution
        """

        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Create a MarkerBlocksMonitor instance to track information about
        # placed marker blocks
        self._victims_info = None #VictimsMonitor(self)
        self._markers_info = MarkerBlocksMonitor(self)

        # Set attributes
        self._prev_beep_rooms = set()

        # Add Minecraft callback
        self.add_minecraft_callback(VictimSignal, self.__onVictimSignal)

        self._marker_distance_threshold = kwargs.get("marker_distance_threshold", 1)
        self._timeout = kwargs.get("timeout", None)
        self._distance_threshold = kwargs.get("distance_threshold", 5)


    def __onVictimSignal(self, message):
        """
        Callback when a MarkerPlacedEvent message is received.

        Arguments
        ---------
        message : MinecraftBridge.messages.VictimSignal
            Instance of the VictimSignal message
        """

        if message.roomname not in self._prev_beep_rooms:
            self._prev_beep_rooms.add(message.roomname)
            self.logger.debug(f"{self}:  Received VictimSignal message")

            intervention = RemindTransporterBeepsIntervention(self.manager, message, 
                                                              self._victims_info, self._markers_info, 
                                                              distance_threshold=self._distance_threshold,
                                                              timeout=self._timeout,
                                                              marker_distance_threshold=self._marker_distance_threshold,
                                                              parent=self)
            self.logger.info(
                f"{self}:  Transporter heard beep at {message.location}. Spawning {intervention}.")
            self.manager.spawn(intervention)



class RemindTransporterBeepsIntervention(Intervention):
    """
    A RemindTransporterBeepsIntervention is an Intervention which
    reminds the transporter to place marker blocks when a beep is given.
    The intervention is queued for resolution if the transporter has not placed
    a marker block within threshold seconds of receiving a beep.
    """

    def __init__(self, manager, signal_message, victims_info, markers_info, 
                 **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for thiis intervention
        signal_message : MinecraftBridge.messages.VictimSignal
            VictimSignal message that triggered this intervention
        victims_info : VictimsInfo
            Data structure containing information about victims
        markers_info : MarkerBlocksInfo

        Keyword Arguments
        -----------------
        marker_distance_threshold : int, default=1
            Maximum distance from the signal that the marker is considered 
            "part of" the signal location
        distance_threshold : float, default=5
            Maximum distance from victim that a player can go
            before the victim is considered to have been "left"
        timeout : float, default=None
            Maximum mission time (in seconds) without a discard / resolution
            before queuing for resolution
        """

        Intervention.__init__(self, manager, **kwargs)

        # Store the original message, and the data structures containing the
        # victim and marker block information
        self.signal_message = signal_message
        self._signal_location = self.signal_message.location
        self._victims_info = victims_info
        self._markers_info = markers_info

        # Keyword arguments
        self._distance_threshold = kwargs.get("distance_threshold", 5)
        self._marker_distance_threshold = kwargs.get("marker_distance_threshold", 2)
        self._timeout = kwargs.get("timeout", None)

        # Who received the signal message?  As a sanity check, make sure that
        # the ID of the participant matches that of the transport specialist
        self._participant_id = self.signal_message.participant_id
        if self._participant_id != self.manager.participants['Transport_Specialist'].participant_id:
            self.logger.warning("%s:  participant_id in signal message (%s) does not equal the transporter's participant id (%s)", 
                                self, self.signal_message.participant_id, 
                                self.manager.participants['Transport_Specialist'].participant_id)

        # Transporter is the only relevant participant
        self.addRelevantParticipant(self.manager.participants['Transport_Specialist'].participant_id)            

        # If a timeout is provided, then set a resolution time in the future
        # by the given timeout, based on the time the signal message was 
        # published
        self._resolve_time = None
        if self._timeout is not None:
            self._resolve_time = self.signal_message.elapsed_milliseconds + 1000 * self._timeout

        # Generate a list of marker locations that would be considered in the
        # surrounding of the signal location based on the marker distance
        # threshold
        self._surrounding_locations = [(self._signal_location[0]+dx, DEFAULT_MARKER_Y, self._signal_location[2]+dz) 
                                       for dx in range(-self._marker_distance_threshold, self._marker_distance_threshold+1)
                                       for dz in range(-self._marker_distance_threshold, self._marker_distance_threshold+1)
                                       if distance((0,0), (dx,dz)) <= self._marker_distance_threshold 
                                       and (dx != 0 or dz != 0)]

        # Register callbacks for monitoring for marker block placement and 
        # player movement
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onMarkerPlaced)
        self.add_minecraft_callback(PlayerState, self.__onPlayerState)


    def __str__(self):
        """
        String representation of the intervention
        """

        return "%s @ %s" % (self.__class__.__name__, self._signal_location)



    def _onActivated(self):
        """
        Callback from the manager when this intervention is actived.  Checks to
        see if there already exists a marker block next to the signal location,
        and, if it's a victim-type block, simply discards this intervention due
        to be irrelevant.
        """

        should_discard = False

        for location in self._surrounding_locations:

            # Check if there's a marker in the location.  If so, check to see
            # if the marker block is "correct", i.e., it's a marker indicating
            # the presence of a victim.
            # NOTE:  _markers_info gives a string of the marker, not the 
            #        Block enumeration, so some conversion is needed.
            marker = self._markers_info.marker_blocks.get(location, None)
###            if marker is not None and MinecraftElements.Block[marker] in VICTIM_MARKER_BLOCKS:
            if marker is not None and marker in VICTIM_MARKER_BLOCKS:                
                should_discard=True

                # TODO: Put this in evidence, when that gets merged in
                self.logger.debug("%s: Discarding on Activation:  Marker present: %s", self, marker)

        if should_discard:
            self.discard()


    def __onMarkerPlaced(self, message):
        """
        Callback when a marker block is placed.

        Arguments
        ---------
        message : MarkerPlacedEvent
        """

        # It may be plausible that the Transporter tells another player to place
        # a marker block (though unlikely); so, don't simply ignore this message
        # if the marker wasn't placed by the Transporter.

        # Check if the marker was placed within the surrounding locations of
        # the signal.  If so, then there's no advice to give to the player.
        if message.location in self._surrounding_locations and message.marker_type in VICTIM_MARKER_BLOCKS:

            # TODO: Put this in as evidence, when that gets merged in
            self.logger.debug("%s:  Discarding:  Marker placed @ %s: %s", self, message.location, message.marker_type)

            # Is the marker correct?
###            if VICTIM_MARKER_TO_SIGNAL_MAP.get(message.marker_type) != self.signal_message.message:
            if is_correct_block_for_signal(message.marker_type, self.signal_message.message):
                # Incorrect marker placed!
                self.logger.debug("%s:  Victim placed incorrect marker block (%s) for given signal (%s)", self, message.marker_type, self.signal_message.message)

                # Create an intervention to correct the placed merker block
                correction_intervention = TransporterCorrectMarkerIntervention(self.manager, 
                                                                               self.signal_message.message, 
                                                                               self._signal_location,
                                                                               self._surrounding_locations,
                                                                               message.location,
                                                                               self._participant_id,
                                                                               parent=self)
                self.manager.spawn(correction_intervention)

            self.discard()


    def __onPlayerState(self, message):
        """
        Callback when a player moves

        Arguments
        ---------
        message : PlayerState
        """

        # Is this about the player the intervention is monitoring?
        if message.participant_id != self._participant_id:
            return

        # Has enough time passed to resolve the intervention?
        if self._resolve_time is not None and message.elapsed_milliseconds >= self._resolve_time:

            # TODO:  Put this in as evidence, when that gets merged in
            self.logger.debug("%s: Queueing for Resolution:  Exceeded resolve time", self)

            self.queueForResolution()


        # Has the player moved far enough from the signal location?
        if distance(self._signal_location, message.position) > self._distance_threshold:

            # TODO:  Put this in as evidence, when that gets merged in
            self.logger.debug("%s:  Queueing for Resolution:  Distance exceeded threshold: %f; Player @ %s", self, distance(self._signal_location, message.position), message.position)

            self.queueForResolution()



    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {
            'default_recipients': [self._participant_id],
            'default_message': "Transporter, remember to mark rooms when you are given a signal to help your team find victims.",
        }



################################################################################
################################################################################


class RemindTransporterBeepsFollowup(Followup, ComplianceMixin):
    """
    Followup to a RemindTransporterBeepsIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=10
            Number of seconds in the time period of follow up validation
        """

        duration = kwargs.get("duration", 10)

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)

        self._surrounding_locations = self.intervention._surrounding_locations


        # Register callbacks for monitoring for marker block placement and 
        # player movement
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onMarkerPlaced)


    def __onMarkerPlaced(self, message):
        """
        Callback when a marker block is placed.  Indicates that the player
        complied (either directly or indirectly, by informing another player)
        with the advice

        Arguments
        ---------
        message : MarkerPlacedEvent
        """

        # It may be plausible that the Transporter tells another player to place
        # a marker block (though unlikely); so, don't simply ignore this message
        # if the marker wasn't placed by the Transporter.

        # Check if the marker was placed within the surrounding locations of
        # the signal.  If so, then the player has complied with the advice
        if message.location in self._surrounding_locations and message.marker_type in VICTIM_MARKER_BLOCKS:

            # TODO: Put this in as evidence, when that gets merged in
            self.logger.debug("%s:  Marker placed @ %s: %s", self, message.location, message.marker_type)

            # Is the marker correct?
###            if VICTIM_MARKER_TO_SIGNAL_MAP.get(message.marker_type) != self.intervention.signal_message.message:
            if is_correct_block_for_signal(message.marker_type, self.intervention.signal_message.message):
                # Incorrect marker placed!
                self.logger.debug("%s:  Victim placed incorrect marker block (%s) for given signal (%s)", self, message.marker_type, self.intervention.signal_message.message)

                # Create an intervention to correct the placed merker block
                correction_intervention = TransporterCorrectMarkerIntervention(self.manager, 
                                                                               self.intervention.signal_message.message, 
                                                                               self.intervention._signal_location,
                                                                               self.intervention._surrounding_locations,
                                                                               message.location,
                                                                               self.intervention._participant_id,
                                                                               parent=self)
                self.manager.spawn(correction_intervention)

            
            self._onCompliance()


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """

        # TODO: Put this in as evidence, when that gets merged in

        self._onNonCompliance()



################################################################################
################################################################################

class TransporterCorrectMarkerIntervention(Intervention):
    """
    An intervention to inform the transporter that they have placed a marker
    that is incorrect in the context of the signal that was presented to the
    transporter.
    """

    def __init__(self, manager, signal_message, signal_location, 
                       surrounding_locations, marker_location, participant_id,
                       **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for thiis intervention
        signal_message : string
            Signal received by the transporter
        signal_location : tuple
            (x,y,z) location of where the signal occured
        surrounding_locations : list
            List of (x,y,z) locations consider valid for marker placement
        marker_location : tuple
            (x,y,z) location of where the marker was placed
        participant_id : string
            ID of the participant that received the signal intervention


        Keyword Arguments
        -----------------
        delay : float, default = 2.0
            Amount of time to wait before resolving this intervention
###        marker_distance_threshold : int, default=1
###            Maximum distance from the signal that the marker is considered 
###            "part of" the signal location
###        timeout : float, default=None
###            Maximum mission time (in seconds) without a discard / resolution
###            before queuing for resolution
        """

        Intervention.__init__(self, manager, **kwargs)

        self._signal_message = signal_message
        self._signal_location = signal_location
        self._marker_location = marker_location
        self._participant_id = participant_id
        self._surrounding_locations = surrounding_locations

        self._delay = kwargs.get("delay", 2.0)
###        self._marker_distance_threshold = kwargs.get("marker_distance_threshold", 1)
###        self._timeout = kwargs.get("timeout", None)

        # When was the marker placed?  Schedule a callback `delay` seconds in
        # the future to resolve if the marker isn't replaced
        self._scheduler = InterventionScheduler(self)
        self._scheduler.wait(self._onTimeout, delay=self._delay)

        # Flags to maintain to check if we should discard or resolve at timeout
        self.marker_removed = False
        self.marker_replaced = False
        self.marker_correct = False

        # Register to listen for marker removed and marker placed events
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onMarkerPlaced)
        self.add_minecraft_callback(MarkerRemovedEvent, self.__onMarkerRemoved)
        self.add_minecraft_callback(MarkerDestroyedEvent, self.__onMarkerRemoved)



    def __str__(self):
        """
        String representation of the intervention
        """

        return "%s @ %s" % (self.__class__.__name__, self._signal_location)


    def _onTimeout(self):
        """
        Callback when the amount of time to wait has expired
        """

        # Clear out the scheduler (though there shouldn't be anything else...)
        self._scheduler.reset()

        # Is the marker correct?  Then no need to discard.
        if self.marker_correct:

            # Add evidence

            self.logger.debug("%s:  Player has replaced marker with correct one.", self)
            self.discard()


        elif self.marker_replaced:

            self.logger.debug("%s:  Marker has been replaced, but is still incorrect.", self)
            self.queueForResolution()

        elif self.marker_removed:

            # Add evidence

            self.logger.debug("%s:  Marker has been removed, but not replaced.", self)
            self.queueForResolution()

        else:

            # Add evidence

            self.logger.debug("%s:  Player did nothing to fix marker", self)
            self.queueForResolution()


    def __onMarkerPlaced(self, message):
        """
        Callback when the player places a marker
        """

        # If the placement is in the region surround the signal, _and_ it's a
        # victim identification block, then notate that a marker has been placed
        if message.location in self._surrounding_locations:
            self.logger.debug("%s:  Player placed new marker", self)
            self.marker_placed = True

        # Also check to see if the marker is correct
###        if VICTIM_MARKER_TO_SIGNAL_MAP.get(message.marker_type, None) == self._signal_message:
        if is_correct_block_for_signal(message.marker_type, self._signal_message):
            self.logger.debug("%s:    Marker is correct", self)
            self.marker_correct = True


    def __onMarkerRemoved(self, message):
        """
        Callback when the player removes / destroyes a marker
        """

        # If it's the marker of interest, then note that the marker has been
        # removed
        if message.location == self._marker_location:
            self.logger.debug("%s:  Player removed erroneous marker", self)
            self.marker_removed = True


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {
            'default_recipients': [self._participant_id],
            'default_message': "Transporter, you have placed a marker block incongruent with the signal you received.",
        }
