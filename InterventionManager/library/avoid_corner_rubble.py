# -*- coding: utf-8 -*-
"""
.. module:: avoid_corner_rubble
   :platform: Linux, Windows, OSX
   :synopsis: Intervention to suggest avoiding clearing corner rubble blocks

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an
intervention designed to suggest the Engineer avoid clearing corner rubble blocks.

* Trigger:  Engineer clears rubble block in the corner of a room

* Discard States:  N/A

* Resolve State:  Immediately queued for resolution

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..mixins import ComplianceMixin
from ..utils import neighbors

from MinecraftBridge.messages import RubbleDestroyedEvent



class CornerRubble:
    """
    Mixin that reports when a corner rubble block has been cleared.
    """

    def __init__(self, manager, **kwargs):
        self.__semantic_map = manager.semantic_map
        self.add_minecraft_callback(RubbleDestroyedEvent, self.__onRubbleDestroyed)


    def _onCornerRubbleBlockCleared(self):
        """
        Callback for when a corner rubble block has been cleared.
        """
        pass


    def __onRubbleDestroyed(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.RubbleDestroyedEvent
            Received RubbleDestroyedEvent message
        """
        num_adjacent_walls = 0
        for loc in neighbors(message.location):
            if self.__semantic_map.is_in_bounds(loc) and self.__semantic_map.basemap[loc] == 'wall':
                num_adjacent_walls += 1

        if num_adjacent_walls >= 2:
            self._onCornerRubbleBlockCleared()



class AvoidCornerRubbleIntervention(Intervention):
    """
    Intervention designed to suggest the Engineer avoid clearing corner rubble blocks.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """
        Intervention.__init__(self, manager, **kwargs)

        # The relevant participant is the engineer
        self.addRelevantParticipant(self.manager.participants['Engineering_Specialist'].participant_id)


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        message = "Engineer, you probably don't need to clear rubble blocks in the corner of the room."
        return {
            'default_recipients': [self.manager.participants['Engineering_Specialist'].participant_id],
            'default_message': message,
        }



class AvoidCornerRubbleTrigger(InterventionTrigger, CornerRubble):
    """
    Class that generates AvoidCornerRubbleIntervention instances.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        CornerRubble.__init__(self, manager, **kwargs)


    def _onCornerRubbleBlockCleared(self):
        """
        Callback for when a corner rubble block has been cleared.
        """
        intervention = AvoidCornerRubbleIntervention(self.manager)
        self.logger.info(f"{self}:  Corner rubble block cleared. Spawning {intervention}.")
        self.manager.spawn(intervention)



class AvoidCornerRubbleFollowup(Followup, ComplianceMixin, CornerRubble):
    """
    Followup to an AvoidCornerRubbleIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup
        """
        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self)
        CornerRubble.__init__(self, manager)


    def _onCornerRubbleBlockCleared(self):
        """
        Callback for when a corner rubble block has been cleared.
        """
        self._onNonCompliance()


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance (or the mission ends).
        """
        self._onCompliance()
