# -*- coding: utf-8 -*-
"""
.. module:: encourage_verbal
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger to encourage verbal communication

.. moduleauthor:: Noel Chen <noelchen@cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed remind players to talk more in the trial.

* Trigger:  The intervention is triggered when the players don't speak
            to each other for more than some given period of time.

* Discard States:  N/A

* Resolve State:  Immediately queued for resolution

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..components import InterventionScheduler as Scheduler
from ..mixins import ComplianceMixin

from MinecraftBridge.messages import ASR_Message



class EncourageVerbalIntervention(Intervention):

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """
        Intervention.__init__(self, manager, **kwargs)

        self.kwargs = kwargs

        # Indicate that all participants are relevant to this intervention
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.participant_id)


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': (
                "Team, you have not communicated much this round. "
                "You will help your score, if you communicate more often."
            )            
        }



class EncourageVerbalTrigger(InterventionTrigger):
    """
    Class that generates EncourageVerbal Interventions.  Interventions are 
    triggered when the Team stay quiet for a given period of time.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        silence_time_threshold : float, default=120
            Minimum number seconds the players can remain silent before intervening
            to encourage verbal communication
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Set attributes
        self._silence_time_threshold = kwargs.get('silence_time_threshold', 120)
        self._has_triggered = False

        # Add Minecraft callback
        self.add_minecraft_callback(ASR_Message, self.__onASR_Message)

        # Schedule an intervention
        self._scheduler = Scheduler(self)
        self._scheduler.wait(self.spawn_intervention, delay=self._silence_time_threshold)


    def spawn_intervention(self):
        intervention = EncourageVerbalIntervention(self.manager, **self.kwargs)
        self.logger.info(
            "%s:  %s. %s.",
            self,
            f"Team has not spoken in {self._silence_time_threshold} seconds",
            f"Spawning {intervention}",
        )
        self.manager.spawn(intervention)
        self._has_triggered = True


    def __onASR_Message(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.ASR_Message
            The received ASR_Message
        """

        # Cancel the scheduled intervention
        self._scheduler.reset()

        # If not yet triggered, reschedule the intervention
        if not self._has_triggered:
            self._scheduler.wait(self.spawn_intervention, delay=self._silence_time_threshold)



class EncourageVerbalFollowup(Followup, ComplianceMixin):
    """
    Followup to an EncourageVerbalIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=60
            Number of seconds in the time period of follow up validation
        """

        duration = kwargs.get("duration", 60)


        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)

        # Set attributes
        self._duration = duration

        # Add Minecraft callback
        self.add_minecraft_callback(ASR_Message, lambda msg: self._onCompliance())


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """

        followup_intervention = EncourageVerbalIntervention(self.intervention.manager, 
                                                            **self.intervention.kwargs)
        self.logger.info(
            "%s:  %s. %s",
            self,
            f"Team still has not spoken in {self._duration} seconds.",
            f" Spawning {followup_intervention}.",
        )

        self.manager.spawn(followup_intervention)

        self._onNonCompliance()        



class EncourageVerbalIndividualComplianceFollowup(Followup, ComplianceMixin):
    """
    Followup to determine if an individual has complied with the advice from
    EncourageVerbalIntervention.  An individual is considered compliant if they
    are the source of an utterance within a certain period of time.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=60
            Number of seconds in the time period of follow up validation
        """

        duration = kwargs.get("duration", 60)


        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)

        # Set attributes
        self._duration = duration

        # List of events relevant to this followup
        self._history = []

        # Add Minecraft callback
        self.add_minecraft_callback(ASR_Message, self.__onASR_Message)


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """

        self._onNonCompliance()


    def participant_spoke(self, participant_id):
        """
        Callback from linked followups when a particular participant has spoken

        Arguments
        ---------
        participant_id : string
            ID of the participant who had spoke
        """

        # Not used by the individual intervention
        pass


    def __onASR_Message(self, message):
        """
        Callback when an ASR message is received.  If the ASR message is from
        the target participant, then consider that participant as compliant.

        Arguments
        ---------
        message : ASR_Message
            Received ASR Message
        """

        # Who did the message come from
        if message.participant_id == self._target_participant:

            self._history.append(message)

            # Let any linked followups know that this participant has spoken
            for followup in self._linked_followups:
                followup.participant_spoke(self._target_participant)

            # Indicate that this message is compliant
            self._onCompliance()


class EncourageVerbalTeamComplianceFollowup(Followup, ComplianceMixin):
    """
    Followup to determine if a team has complied with the advice from
    EncourageVerbalIntervention.  An team is considered compliant if a minimum
    number of participants have spoken within the followup time.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=60
            Number of seconds in the time period of follow up validation
        min_number_speakers : int, default=2
            Required number of participants to have spoken to have the team be
            considered compliant
        """

        duration = kwargs.get("duration", 60)

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)

        # Set attributes
        self._duration = duration
        self._min_number_speakers = kwargs.get('min_number_speakers', 2)
        self._speakers = set()


    def participant_spoke(self, participant_id):
        """
        Callback from linked followups when a participant has spoken.  Keep
        track of the fact that that the participant has said something, for use
        when determining final team compliance.
        """

        self._speakers.add(participant_id)


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """

        if len(self._speakers) >= self._number_speakers:
            self._onCompliance()
        else:
            self._onNonCompliance()

