# -*- coding: utf-8 -*-
"""
.. module:: correct_victim_misidentification
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for informing medic
              when they have misidentified a victim

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

* Trigger:  Medic triages a victim

* Discard States:
    -  Medic places correct marker block in proximity of victim
    -  Medic leaves proximity of victim
    -  Medic triages another victim
    -  Victim is picked up
    -  A given time interval elapses without discard / resolution

* Resolve State:  Medic places incorrect marker block near victim

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..mixins import ComplianceMixin, VictimMarkerMixin
from MinecraftBridge.messages import MarkerDestroyedEvent, MarkerRemovedEvent, TriageEvent



class VictimMisidentificationIntervention(Intervention, VictimMarkerMixin):
    """
    Intervention for a misidentified triaged victim.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the intervention manager in charge of this intervention

        Keyword Arguments
        -----------------
        triage_event : TriageEvent
            Message that triggered this intervention
        distance_threshold : float, default=5
            Maximum distance from victim that a player can go
            before the victim is considered to have been "left"
        timeout : float, default=15
            Maximum mission time (in seconds) without a discard / resolution
            before discarding
        """
        Intervention.__init__(self, manager, **kwargs)
        VictimMarkerMixin.__init__(self, manager, **kwargs)

        self.kwargs = kwargs
        self.participant = manager.participants[self.kwargs.get('triage_event').participant_id]
        self.marker_location = None

        self.addRelevantParticipant(self.participant)


    def _onVictimMarkerCallback(self, callback):
        """
        Default behavior for all other VictimMarkerMixin callbacks unless they are overriden.
        """
        self.logger.info(f"{self}:  Triggered {callback.__name__}. Discarding intervention.")
        self.discard()


    def _onIncorrectMarkerPlaced(self, message):
        """
        Callback for when an incorrect marker block is placed near the victim.

        Arguments
        ---------
        message : MinecraftBridge.message.MarkerPlacedEvent
            Received MarkerPlacedEvent message
        """
        self.marker_location = message.location
        self.logger.info(f"{self}:  Incorrect marker block placed. Queuing intervention for resolution.")
        self.queueForResolution()


    def getInterventionInformation(self):
        return {
            'default_recipients': [self.participant.id],
            'default_message': "Medic, you may have identified that victim with the wrong marker block."            
        }



class VictimMisidentificationTrigger(InterventionTrigger):
    """
    Trigger for a VictimMisidentificationIntervention.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        distance_threshold : float, default=5
            Maximum distance from victim marker block can be placed before discard
        timeout : float, default=15
            Maximum mission time (in seconds) without a discard / resolution
            before discarding
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Add Minecraft callback
        self.add_minecraft_callback(TriageEvent, self.__onTriageEvent)


    def __onTriageEvent(self, message):
        """
        Callback when an TriageEvent message is received.
        """
        if message.triage_state == TriageEvent.TriageState.SUCCESSFUL:
            intervention = VictimMisidentificationIntervention(
                self.manager,
                triage_event=message,
                **self.kwargs,
            )
            self.logger.info(f"{self}:  Spawning Intervention {intervention}")
            self.manager.spawn(intervention)



class VictimMisidentificationFollowup(Followup, ComplianceMixin, VictimMarkerMixin):
    """
    Followup to a VictimMisidentificationIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=10
            Number of seconds in the time period of follow up validation
        """

        duration = kwargs.get("duration", 10)


        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)
        VictimMarkerMixin.__init__(self, manager, **dict(intervention.kwargs, timeout=None))

        # Followup-specific attributes
        self._correct_marker_placed = False
        self._incorrect_marker_removed = False

        # Add Minecraft callbacks
        self.add_minecraft_callback(MarkerRemovedEvent, self.__onMarkerRemoved)
        self.add_minecraft_callback(MarkerDestroyedEvent, self.__onMarkerRemoved)


    def _onCompliance(self):
        """
        Callback for when team is compliant with the intervention.
        """
        super()._onCompliance()

        # Spawn a "thank you" intervention
        thank_you_intervention = VictimMisidentificationThanksIntervention(
            self.manager, self.intervention.participant.id)
        self.manager.spawn(thank_you_intervention)


    def _onNonCompliance(self):
        """
        Callback for when team is noncompliant with the intervention.
        """
        super()._onNonCompliance()

        # If player removed wrong marker but did not correctly replace it,
        # spawn a TriagedVictimMarkerIntervention
        if self._incorrect_marker_removed and not self._correct_marker_placed:
            from .marker_place_after_triaging_victim import TriagedVictimMarkerIntervention
            new_intervention = TriagedVictimMarkerIntervention(self.manager, **self.intervention.kwargs)
            self.manager.spawn(new_intervention)


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """
        self._onNonCompliance()


    def _onVictimMarkerCallback(self, callback):
        """
        Default behavior for all VictimMarkerMixin callbacks unless they are overriden.
        """
        self.logger.info(
            f"{self}:  Triggered {callback.__name__}. Recording noncompliance.")
        self._onNonCompliance()


    def _onCorrectMarkerPlaced(self, message):
        """
        Callback for when a correct marker block is placed near the victim.
        """
        self._correct_marker_placed = True

        # Record compliance if the incorrect marker has already been removed
        if self._incorrect_marker_removed:
            self._onCompliance()


    def _onIncorrectMarkerPlaced(self, message):
        """
        Callback for when an incorrect marker block is placed near the victim.
        """

        # Player placed another incorrect marker, spawn another VictimMisidentificationIntervention
        new_intervention = VictimMisidentificationIntervention(self.manager, **self.intervention.kwargs)
        self.logger.info(
            f"{self}:  Player placed another incorrect marker. Spawning another {new_intervention}.")
        self.manager.spawn(new_intervention)
        new_intervention._onIncorrectMarkerPlaced(message)


    def __onMarkerRemoved(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.MarkerRemovedEvent
            Received MarkerRemovedEvent message
        """

        # Check if the incorrect marker was removed
        if message.location == self.intervention.marker_location:
            self._incorrect_marker_removed = True

            # Record compliance if the correct marker has already been placed
            if self._correct_marker_placed:
                self._onCompliance()



class VictimMisidentificationThanksIntervention(Intervention):
    """
    A simple intervention that is spawned after the player is complies with a
    VictimMisidentifificationFollowup.  This Intervention simply issues a 
    "thank you" message immediately.
    """

    def __init__(self, manager, participant_id, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager controlling this intervention
        participant_id : string
            ID of the participant this intervention is targeting
        """
        Intervention.__init__(self, manager, **kwargs)
        self._participant_id = participant_id

        self.addRelevantParticipant(self._participant_id)


    def _onActivated(self):
        """
        Callback when the Intervention is activated.  This intervention should
        be immediately queued for resolution.
        """
        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing the default "thanks" message, and the
        provided participant_id as the default recipient.
        """
        return { 
            'default_message': 'Thank you for correcting the marker!',
            'default_recipients': [self._participant_id],
        }
