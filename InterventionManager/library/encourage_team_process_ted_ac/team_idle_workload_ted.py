# -*- coding: utf-8 -*-
"""
.. module:: player_idle
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for encouraging players to increase
              their effort based on TED workload.

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to encourage players to increase their effort if they
have been too idle, based on the output of the CMU-TA2 TED AC.
"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..mixins import ComplianceMixin

from MinecraftBridge.utils import Loggable
from MinecraftBridge.messages import (
    PlanningStageEvent,
    CMU_TA2_TED
)



class TED_WorkloadIntervention(Intervention):
    """
    This intervention alerts the team when their workload drops below their
    average previous performance.

    * Trigger: Triggered initially at the end of the Planning stage.  If 
               resolved, a new version can be triggered to continue monitoring.

    * Discard States:  None

    * Resolve State:  A minimum of `minimum_history` seconds has passed since,
                      and the workload has dropped below some threshold of the
                      moving average of the history of measurements.
    
    Attributes
    ----------
    behavior_history : list of tuples
        List of tuples summarizing the events that 
    """

    def __init__(self, manager, smoothing_factor, threshold, 
                       minimum_resolve_time):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the intervention manager that should control this
            intervention
        smoothing_factor : double, range=[0,1]
            Amount of smoothing performed on the workload history, as a slow 
            averaging parameter.  0 indicates no smoothing, while 1 will result
            in the first value being held as a constant value.
        minimum_resolve_time : int
            Minimum value of elapsed_milliseconds before this intervention can
            be resolved.
        """

        Intervention.__init__(self, manager, **kwargs)

        self._smoothing_factor = smoothing_factor
        self._threshold = threshold
        self._minimum_resolve_time = minimum_resolve_time

        # The intervention will maintain a history of the workload output from
        # the TED AC.  Maintains both the raw and smoothed values.
        self._raw_workload_history = []
        self._smoothed_workload = None

        # Register to receive the TED AC message
        self.add_minecraft_callback(CMU_TA2_TED, self.__onTED)


    def __str__(self):
        """
        String representation of the intervention
        """

        return f"{self.__class__.__name__}"


    def __onTED(self, message):
        """
        Callback when a TED message is received.

        message : messages.CMU_TA2_TED
            Received TED message
        """

        workload = message.process_workload_burnt

        # If this is the first measurement, then simply record it as the
        # smoothed workload value
        if self._smoothed_workload is None:
            self._smoothed_workload = workload

        # Is the workload below the threshold, and has enough time passed?
        if message.elapsed_milliseconds > self._minimum_resolve_time and workload < self._threshold * self._smoothed_workload:
            self.queueForResolution()
            return

        # Not resolved -- record the workload value and update the smoothed 
        # version
        self._raw_workload_history.append(workload)
        self._smooth_workload = self.smoothing_factor*self._smoothed_workload + \
                                (1-self._smoothing_factor)*workload



class TED_WorkloadTrigger(InterventionTrigger):
    """
    Class that generates the initial TED_WorkloadIntervention.  Interventions 
    are triggered at the end of the planning phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        smoohting_factor : double
            Smoothing factor to use with the Intervention
        threshold : double, range = [0,1)
            Lower threshold to resolve the intervention, as a percentage of the
            previous workload measure.
        """

        InterventionTrigger.__init__(self, manager, **kwargs)


        # Register to receive the following messages
        self.add_minecraft_callback(PlanningStageEvent, self.__onPlanningStageEvent)


    def __onPlanningStageEvent(self, message):
        """
        Callback when PlanningStage event messages are received.  If the mission
        state is a start, then a TED_WorkloadIntervention should be spawned.

        Arguments
        ---------
        message : PlanningStageEvent
            PlanningStage Event message
        """
        
        # If the planning stage is ending, then spawn an Intervention for each
        # participant.
        if message.state == PlanningStageEvent.PlanningState.Stop:

            # Create the intervetion
            intervention = TED_WorkloadIntervention(self.manager)

            self.logger.info("%s:  Spawning Intervention: %s", self, intervention)

            self.manager.spawn(intervention)


class TED_WorkloadFollowup(Followup, ComplianceMixin):
    """
    Followup indicating whether or not the player was compliant with being
    advised to increase workload.

    Attributes
    ----------
    intervention : TED_WorkloadIntervention
        Intervention this followup is following up on
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the Intervention Manager that the presenter is
            associated with

        Keyword Arguments
        -----------------
        timeout : float
            Number of seconds before the participant is considered noncompliant
        """

        Followup.__init__(self, intervention, manager)
        ComplianceMixin.__init__(self, timeout=kwargs.get("timeout", 15.0))

        self.participant = intervention.participant

        self.compliance_message = None

        self.logger.info("%s:  Followup Initialized", self)      


    def _onActive(self):
        """
        Callback from the InterventionManager when the Followup is made active
        """
        # This followup observes if the wrong markerblock has been removed, 
        # and the correct one placed instead of it

        self.logger.info("{}: Initiating followup for idle player {}".format(self, self.participant.participant_id))



    def __onPlayerStateMessage(self, message):
        """
        Check to see if the player has moved far enough to be considered compliant
        """

        # Does this apply to the participant of interest?
        if message.participant_id != self.participant.participant_id:
            return

        self.logger.debug("%s: Received %s", self, message)

        # Check to see if the participant has moved sufficiently far.  If so, 
        # then reset the idle
        distance = math.sqrt((self.intervention._player_position[0] - message.x)**2 +
                             (self.intervention._player_position[1] - message.y)**2)

        if distance > self.intervention.idle_distance_threshold:
            self._onCompliance()
            self.complete()


    def __onEvent(self, message):
        """
        Callback when a relevant event-type message is received.

        message : MinecraftBridge.message
            Message instance received
        """

        # Does this apply to the participant of interest?
        if message.participant_id != self.participant.participant_id:
            return

        self.logger.debug("%s: Received %s", self, message)

        self._onCompliance()
        self.complete()


    def __onEventWithPlayername(self, message):
        """
        Callback when a relevant event-type message is received.  This is a 
        hack to handle messages that do not necessarily have `participant_id`
        as a property.

        message : MinecraftBridge.message
            Message instance received
        """

        # Unfortunately for now, ItemEquippedEvent uses playername, which may
        # (?) be the participant_id, depending when the message was produced.
        if message.playername != self.participant.playername and \
           message.playername != self.participant.participant_id:
           return

        self.logger.debug("%s:  Received %s", self, message)

        self._onCompliance()
        self.complete()


    def _onTimeout(self):
        """
        If the followup has timed out before any additional behavior, consider
        the intervention not complied with.
        """

        self._onNonCompliance()
