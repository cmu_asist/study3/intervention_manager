# -*- coding: utf-8 -*-
"""
.. module:: team_welcome_message
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for introductory mmessage for team

.. moduleauthor:: Simon Stepputtis <stepputtis@cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to remind players to share their individual puzzle information

Puzzle information includes (randomly given to players):
- Rooms and damange
- Rooms and meeting names
- Meeting names and attendees

* Trigger: The intervention is triggered at the start of the planning phase

* Discard States:  N/A

* Resolve State:  The intervention is immediately queued for a resolution

"""

from .. import Intervention
from .. import InterventionTrigger

from MinecraftBridge.messages import PlanningStageEvent
from MinecraftBridge.messages import ASR_Message
from ..components import InterventionScheduler as Scheduler


class RemindPlayersToSharePuzzleInformationIntervention(Intervention):
    """
    A TeamWelcomeMessageIntervention is an Intervention which simply presents
    a welcome message to all participants in the team.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        message : string
            Message to be sent to the team
        """
        Intervention.__init__(self, manager, **kwargs)
        self._message = kwargs.get('message', "No message for <RemindPlayersToSharePuzzleInformationIntervention>")


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        print("----------")
        print(self._message)
        print("----------")
        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self._message,
        }



class RemindPlayersToSharePuzzleInformationTrigger(InterventionTrigger):
    """
    Class that generates RemindPlayersToSharePuzzleInformation. This intervention
    is triggered at the start of the trial, with an N second delay. If there is no
    discussion detected, this intervention will remind the players to share their information
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        initial_wait : int
            Delaying the message for a few seconds before presenting it
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Register the trigger to receive FoV messages
        self.add_minecraft_callback(PlanningStageEvent, self.__onPlanningStageEvent)

        # Create a scheduler
        self._scheduler = Scheduler(self)
        self._initial_wait = kwargs.get("initial_wait", 30)

        # Check if the team is saying something
        self._team_discussing = False
        self.add_minecraft_callback(ASR_Message, self.__onAsrMessage)


    def __onPlanningStageEvent(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.PlanningStageEvent
            Received PlanningStageEvent message
        """
        self.logger.debug(f"{self}: Received PlanningStage Message")

        # Check to see if this is a Planning Stage end message
        if message.state == PlanningStageEvent.PlanningState.Start:
            self._scheduler.wait(self._enableIntervention, delay=self._initial_wait)


    def __onAsrMessage(self, message):
        """
        Callback when we get an asr/final message
        """
        self._team_discussing = True

    
    def _enableIntervention(self):
        """
        Creates the intervention if no discussion was detected
        """
        if not self._team_discussing:
            intervention = RemindPlayersToSharePuzzleInformationIntervention(self.manager, **self.kwargs)
            self.logger.info(f"{self}:  Spawning {intervention}.")
            self.manager.spawn(intervention)
