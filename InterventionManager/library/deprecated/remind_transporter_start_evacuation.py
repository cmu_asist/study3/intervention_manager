# -*- coding: utf-8 -*-
"""
.. module:: remind_transporter_start_evacuation
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger that reminds transporter to start evacuating triaged victims

.. moduleauthor:: Noel Chen <yunhsua3@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to remind transporter to start evacuating triaged victims
"""

from collections import defaultdict
from .. import Intervention
from .. import InterventionTrigger

import math
import re

from MinecraftBridge.messages import (PlayerState, TriageEvent,
                                      VictimPickedUp, VictimPlaced,
                                      SemanticMapInitialized, VictimEvacuated)


class TransporterStartEvacuationIntervention(Intervention):
    """
    A TransporterStartEvacuationIntervention is an Intervention which is designed to
    remind the transporter to start evacuating triaged victims.

    * Trigger: TODO: more on this later

    * Discard States:  The intervention is discarded if transporter when the
                      transporter starts transporting triaged victims. 

    * Resolve State:  The intervention is queued for a resolution if when the
                      transporter doesn't start transporting after 30 seconds
                      since the trigger.

    Attributes
    ----------

    Methods
    -------

    """

    def __init__(self, manager, participant_id, trigger_message, start_time, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : string
            Unique ID of the participant who may or may not have seen a victim
        start_time : mission_timer format (min, sec)
            The mission timer when the intervention was triggered

        NOTE: if player is currently moving a victim, dont send intervention 


        Keyword Arguments
        -----------------
        angle_threshold : float, default=135.0
            Minimum angle between the victim's location and direction of player
            movement to consider the player
        """

        Intervention.__init__(self, manager, **kwargs)


        # Register callbacks for when the intervention is connected
        self.add_minecraft_callback(PlayerState, self.__processPlayerState)
        self.add_minecraft_callback(VictimPickedUp, self.__processVictimPickedUp)
        self.add_minecraft_callback(VictimPlaced, self.__processVictimPlaced)
        self.add_minecraft_callback(TriageEvent, self.__processTriageEvent)


        # Intervention specific attributes
        self.participant_id = participant_id
        self.player_display_name = Intervention.get_player_display_name(participant_id)
        self.start_time = start_time


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.

        The generated dictionary *must* have the properties listed in the 
        `Required Fields` below.

        TODO:  Identify a set of universal intervention properties, so that the
               returned information is sufficient for whichever type of 
               intervention.

        Return
        -------
        intervention_info : dictionary
            Mapping of relevant intervention properties to needed information

        Required Fields
        ---------------
        participant_ids : list of strings
            List of participant_ids that the intervention should be sent to
        text_message : string
            A (default) text representation of the intervention
        """

        intervention_info = {}

        intervention_info["participant_ids"] = [ self.participant_id ]
        intervention_info["text_message"] = self.player_display_name + ", \
            please be aware of the time remaining, you should start \
            evacuating all the triaged victims on the map!"

        return intervention_info


    def calculate_time_diff(timeA, timeB):
        """
        timeA, timeB : mission_timer format (min, sec)
        returns time difference timeA - timeB in seconds
        """
        if timeA[0] > timeB[0]:
            if timeA[1] < timeB[1]:
                return timeA[1] + 60 - timeB[1]
            else:
                return timeA[1] - timeB[1]
        else:
            if timeA[1] > timeB[1]:
                return timeA[1] - 60 - timeB[1]
            else:
                return timeA[1] - timeB[1]

    def __processPlayerState(self, message):
        """
        Callback when the Intervention receives a PlayerState message.

        Arguments
        ---------
        message : MinecraftBridge.message.PlayerState
            Received PlayerState message
        """
        # Ignore the message if it is not related to the player that this 
        # intervention was created for
        if message.playername != self.participant_id:
            return
        
        # self.logger.info("message.mission_timer:{}, self.start_time:{}, time diff:{}".format(message.mission_timer, self.start_time, self.calculate_time_diff(message.mission_timer, self.start_time)))
        if self.calculate_time_diff(message.mission_timer, self.start_time) > 30:
            self.logger.info("Queuing for resolution, transporter hasn't evacuated any victim for 30 seconds")
            self.queueForResolution()
        


    def __processVictimPickedUp(self, message):
        """
        Callback when the Intervention receives an VictimPickedUp message.

        Arguments
        ---------
        message : MincraftBridge.messages.VictimPickedUp
            Received VictimPickedUp message
        """

        # self.logger.debug("Received VictimPickedUp Message")

        # Ignore this FoV message if it's not associated with the participant
        if message.playername != self.participant_id:
            return

        self.logger.info("Discarding, Transporter picked up a victim")
        self.discard()


    def __processVictimPlaced(self, message):
        """
        Callback when the Intervention receives a VictimPlaced message.

        Arguments
        ---------
        message : MinecraftBridge.message.VictimPlaced
            Received VictimPlaced message
        """

        # self.logger.debug("Received VictimPlaced Message")

        # Ignore the message if it is not related to the player that this 
        # intervention was created for
        if message.playername != self.participant_id:
            return


    def __processTriageEvent(self, message):
        """
        Callback when the Intervention receives a TriageEvent message.

        Arguments
        ---------
        message : MinecraftBridge.message.TriageEvent
            Received TriageEvent message
        """

        # self.logger.debug("Received TriageEvent Message")

        # Ignore the message if it is not related to the player that this 
        # intervention was created for
        if message.playername != self.participant_id:
            return


class TransporterStartEvacuationTrigger(InterventionTrigger):
    """
    Class that generates TransporterStartEvacuation Interventions. 
    TODO: add descriptions here later

    Arguments
    ---------

    Methods
    -------

    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------

        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        # Register the trigger to receive FoV messages
        self.add_minecraft_callback(VictimPickedUp, self.__processVictimPickedUp)
        self.add_minecraft_callback(VictimPlaced, self.__processVictimPlaced)
        self.add_minecraft_callback(TriageEvent, self.__processTriageEvent)
        self.add_minecraft_callback(PlayerState, self.__processPlayerState)
        self.add_minecraft_callback(VictimEvacuated, self.__processVictimEvacuated)
        self.add_minecraft_callback(SemanticMapInitialized, self.__processSemanticMap)

        # transporter/evacuator id
        self.participant_id = None
        self.holding_a_victim = False

        self.evacuation_zoneids = ["taan", "taas", "tabn", "tabs", "tacn","tacs"]
        self.zone_for_victim_type = {"victim_saved_a": ["taan", "taas"],
                                    "victim_saved_b": ["tabn", "tabs"],
                                    "victim_saved_c": ["tacn", "tacs"]
                                    }
        self.zone = {}
        self.zone_center = {}

        # keep track of non-evacuated and triaged victims
        # key: victim_id, value: tuple (victim_type, location of victim)
        self.victim_queue = {}

        # Time to start reminder (Intervention will not be spawned in the first half of the mission)
        self.remaining_time_thresh = 5

        # keep track of interventions and avoid sending multiple interventions within the same second
        self.remind = defaultdict(lambda: False)


    def calculateZoneCenters(self, zone):
        zone_center = {}
        for name, bbox in zone.items():
            zone_center[name] = ((bbox[0]["x"]+bbox[1]["x"])/2, (bbox[0]["z"]+bbox[1]["z"])/2)
        return zone_center
    
    def whichEvacuationZone(self, x,z):
        """
        returns evacuation zone id given x,z coordinates
        returns False if the x,z is not in a evacuation zone
        """
        for name, bbox in self.zone.items():
            if bbox[0]["x"] <= x <= bbox[1]["x"] and bbox[0]["z"] <= z <= bbox[1]["z"]:
                return name
        return False

    def victimType_matches_zone(self, vtype, zone):
        if zone in self.zone_for_victim_type[vtype]:
            return True
        else:
            return False

    def __processVictimPickedUp(self, message):
        """
        Callback when an VictimPickedUp message is received.

        Arguments
        ---------
        message : MinecraftBridge.messages.VictimPickedUp
            Instance of the VictimPickedUp message
        """

        # Ignore the message if it is not related to the transporter/evacuator
        if self.participant_id != message.playername:
            return
        
        self.holding_a_victim = True


    def __processSemanticMap(self, message):
        for location in message.semantic_map["locations"]:
            loc_id = location["id"]
            if loc_id in self.evacuation_zoneids:
                self.zone[location["id"]]= location["bounds"]["coordinates"]
        self.zone_center = self.calculateZoneCenters(self.zone)

    def __processVictimEvacuated(self, message):
        self.logger.info("VictimEvacuated location {} | type: {} | id: {} | success :{}".format(message.victim_location, message.type, message.victim_id, message.success))

    def __processVictimPlaced(self, message):
        """
        Callback when an VictimPlaced message is received.

        Arguments
        ---------
        message : MinecraftBridge.messages.VictimPlaced
            Instance of the VictimPlaced message
        """
        # not a triaged victim, just other players moving the victim around
        if message.victim_id not in self.victim_queue:
            return

        self.logger.info("Received VictimPlaced Message | victim id : {} | victim type:{} | location: ({}, {})".format(message.victim_id, message.type, message.victim_x, message.victim_z))

        victim_type = message.type


        placed_zone = self.whichEvacuationZone(message.victim_x, message.victim_z)
        if placed_zone:
            if self.victimType_matches_zone(victim_type, placed_zone):
                self.logger.info("[Evacuation success] Placed {}(id:{}) at {}".format(victim_type, message.victim_id, placed_zone))
                del self.victim_queue[message.victim_id]
            else:
                self.logger.info("[Evacuation type mismatch] Placed {} at {}".format(victim_type, placed_zone))
                self.victim_queue[message.victim_id] = ((message.victim_x, message.victim_y, message.victim_z), victim_type)
        else:
            self.logger.info("Placed triaged victim at a new location ({}, {})".format(message.victim_x, message.victim_z))
            self.victim_queue[message.victim_id] = ((message.victim_x, message.victim_y, message.victim_z), victim_type)

        self.holding_a_victim = False

    def __processTriageEvent(self, message):
        """
        Callback when an TriageEvent message is received.

        Arguments
        ---------
        message : MinecraftBridge.messages.TriageEvent
            Instance of the TriageEvent message
        """
        # assign participant_id on first triage event
        if not self.participant_id:
            self.participant_id = message.playername

        # Ignore the message if it is not related to the transporter/evacuator
        if self.participant_id and self.participant_id != message.playername:
            return
        
        if message._triage_state != TriageEvent.TriageState.SUCCESSFUL:
            # self.logger.info("Triage state:{}".format(message._triage_state))
            return
        
        self.logger.info("{} triaged {} victim(id:{}) at {}, mission_timer: {}, elapsed_milliseconds:{}".format(message.playername, message.type, message.victim_id, message.victim_location, message.mission_timer, message.elapsed_milliseconds))

        # add to queue
        self.victim_queue[message.victim_id] = (message.victim_location, message.type)
    
    def distance(self, loc_a, loc_b):
        """
        returns Euclidean distance between loc_a and loc_b
        """
        a_x, a_z = loc_a[0], loc_a[2]
        b_x, b_z = loc_b[0], loc_b[2]
        return math.sqrt((a_x - b_x)**2 + (a_z - b_z)**2)

    def distance_to_nearest_zone(self, victim_loc, victim_type):
        """
        returns Euclidean distance between victim_loc and the closest evacuation zone
        """
        smallest_dist = float("inf")
        if victim_type in self.zone_for_victim_type:
            for zone_id in self.zone_for_victim_type[victim_type]:
                zone_bbox = self.zone_center[zone_id]
                # zone_bbox is (x,z)
                # victim_loc is (x,y,z)
                smallest_dist = min(smallest_dist, math.sqrt((zone_bbox[0] - victim_loc[0])**2 + (zone_bbox[1] - victim_loc[2])**2))
        return smallest_dist

    def mission_timer_to_seconds(self, mission_timer):
        """
        converts mission_timer to seconds
        """
        return mission_timer[0] * 60 + mission_timer[1]

    def __processPlayerState(self, message):
        """
        Callback when the Intervention receives a PlayerState message.

        Arguments
        ---------
        message : MinecraftBridge.message.PlayerState
            Received PlayerState message
        """
        # Ignore the message if it is not related to the transporter/evacuator
        if self.participant_id != message.playername:
            return
        
        # Ignore the message if there is no victim triaged
        if len(self.victim_queue) == 0:
            return

        # Ignore the message if the player is currently transporting a victim
        if self.holding_a_victim:
            return

        # Ignore the message if still in first half of the mission
        if message.mission_timer[0] > self.remaining_time_thresh:
            return

        remaining_seconds = self.mission_timer_to_seconds(message.mission_timer)

        if remaining_seconds < 0:
            return

        # check every 15 seconds (don't want to annoy the player)
        if remaining_seconds % 15 != 0:
            return

        # only remind once in the same second
        if self.remind[remaining_seconds]:
            return
        self.remind[remaining_seconds] = True

        # calculate total distance
        total_dist = 0
        for vic_id, (vic_loc, vic_type) in self.victim_queue.items():
            player_to_victim_dist = self.distance(message.position, vic_loc)
            victim_to_zone_dist = self.distance_to_nearest_zone(vic_loc, vic_type)
            total_dist += player_to_victim_dist + victim_to_zone_dist

        # speed = math.sqrt(message.motion_x **2 + message.motion_z ** 2)
        speed = 2
        time_needed = total_dist / speed if speed !=0 else 10
        self.logger.info(">>>speed:{}, total_distance:{}, time_needed:{}, remaining_seconds:{}".format(speed, total_dist, time_needed, remaining_seconds))

        if time_needed > remaining_seconds:
            self.logger.info("Spawning Intervention: time needed to transport victims: {} > time remaining:{}".format(time_needed, remaining_seconds))
            intervention = TransporterStartEvacuationIntervention(self.manager,
                                                                    message.participant_id, 
                                                                    message,
                                                                    message.mission_timer)

            self.manager.spawn(intervention)