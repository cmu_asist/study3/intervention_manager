from .. import Intervention
from .. import InterventionTrigger

from MinecraftBridge.messages import MarkerPlacedEvent, TriageEvent, PlayerState, LocationEvent



class PlayerTriaggedVictimIntervention(Intervention):
    """
    A PlayerSawVictimIntervention is an Intervention which is designed to alert
    the player to a potentially unplaced markerblock.

    * Trigger: The intervention is triggered when a player triages a victim,
               leaves the room, and forgets to place a marker block.

    * Discard States:  The intervention is discarded if the player places the 
                       correct marker block.

    * Resolve State:  The intervention is queued for a resolution ???

    Attributes
    ----------

    Methods
    -------

    """

    def __init__(self, manager, participant_id, victim_type, room_id, rooms, 
                 victims_location, trigger_message, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : string
            Unique ID of the participant who triaged the victim
        victim_type : string
            The type of the victim being triaged
        room_id: string
            String ID of the room
        rooms: dictionary **** This probably shouldn't be here
            Mapping of Unique room_ids to Room class
        victims_location : dictionary **** This probably shouldn't be here
            Mapping of victim location to room_id
        trigger_message : MinecraftBridge.messages.TriageEvent
            Message that triggered this intervention

        Keyword Arguments
        -----------------
        markerblock_to_door_threshold: float, default=5.0
            Distance threshold for the markerblock to be identified as close to the door.
        player_to_door_threshold: float, default=5.0
            Distance threshold given to the player for him/her to place the marker block.
        """

        Intervention.__init__(self, manager, **kwargs)

        # Register callbacks for when the intervention is connected
        self.add_minecraft_callback(PlayerState, self.__onPlayerStateMessage)
        self.add_minecraft_callback(LocationEvent, self.__onLocationEventMessage)
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onMarkerPlacedEventMessage)

        # Intervention specific attributes
        self.victims_location = victims_location
        self.rooms = rooms
        self.trigger_message = trigger_message
        self.participant_id = participant_id
        self.room_id = room_id
        self.victim_type = victim_type

        # Whether the marker has been placed for this victim. Initializes as False.
        self.marker_placed = False
        
        self.markerblock_to_door_threshold = kwargs.get("markerblock_to_door_threshold", 5.0)
        self.player_to_door_threshold = kwargs.get("player_to_door_threshold", 5.0)


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.

        The generated dictionary *must* have the properties listed in the 
        `Required Fields` below.

        TODO:  Identify a set of universal intervention properties, so that the
               returned information is sufficient for whichever type of 
               intervention.

        Return
        -------
        intervention_info : dictionary
            Mapping of relevant intervention properties to needed information

        Required Fields
        ---------------
        participant_ids : list of strings
            List of participant_ids that the intervention should be sent to
        text_message : string
            A (default) text representation of the intervention
        """

        intervention_info = {}

        intervention_info["participant_ids"] = [ self.participant_id ]
        intervention_info["text_message"] = "Didn't you forget to put down a marker in front of the door?"

        return intervention_info


    def __onMarkerPlacedEventMessage(self, message):
        """
        Callback when the Intervention receives an MarkerPlacedEvent message.

        Arguments
        ---------
        message : MincraftBridge.messages.MarkerPlacedEvent
            Received MarkerPlacedEvent message
        """
        self.logger.debug("%s: Received Marker Placed Event Message", self)

        # Ignore this MarkerPlacedEvent message if it's not associated with the participant
        if message.playername != self.participant_id:
            return

        # If marker block is placed outside of the room_id,
        # and the marker block is correct,
        # change the marker_placed state to True.
        # currently assuming Markerblock2 means the room is clear.
        # TODO:
        # 1. don't know door location

        if distance(message.location, door.location) < self.markerblock_to_door_threshold and message.marker_type == MarkerBlockType.MarkerBlock1:
            self.marker_placed = True

    def __onLocationEventMessage(self, message):
        """
        Callback when the Intervention receives an LocationEvent message.

        Arguments
        ---------
        message : MincraftBridge.messages.LocationEvent
            Received LocationEvent message
        """
        if message.playername != self.participant_id:
            return
        
        # I don't see where we can find the door xyz in this event?
        for location in message.exited_locations:
            if self.room_id == location['id']:
                self.logger.debug("%s: Player left the room %s", self, self.room_id)


    def __onPlayerStateMessage(self, message):
        """
        Callback when the Intervention receives a PlayerState message.

        Arguments
        ---------
        message : MinecraftBridge.message.PlayerState
            Received PlayerState message
        """

        self.logger.debug("%s: Received PlayerState Message", self)

        # Ignore the message if it is not related to the player that this 
        # intervention was created for
        if message.playername != self.participant_id:
            return
        
        # If the player location is too far from the door location
        # and the marker block is not yet put
        # queue the intervention for resolution
        # TODO: don't know door's location
        distance_to_door = distance(message.position, door.location)
        if self.marker_placed == False and distance_to_door > self.player_to_door_threshold:
            self.logger.info("%s: Queuing for resolution, player-door distance (%f) exceeds threshold (%f)", self, distance_to_door, self.player_to_door_threshold)
            self.queueForResolution()



class PlayerTriaggedVictimTrigger(InterventionTrigger):
    """
    Class that generates PlayerTriaggedVictim Interventions.  Interventions are 
    triggered when a player triages a victim.

    Arguments
    ---------

    Methods
    -------

    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        # Register the trigger to receive TriageEvent messages
        self.add_minecraft_callback(TriageEvent, self.__onTriageEventMessage)


    def __onTriageEventMessage(self, message):
        """
        Callback when an TriageEvent message is received.

        Arguments
        ---------
        message : MinecraftBridge.messages.TriageEvent
            Instance of the TriageEvent message
        """

        self.logger.debug("%s: Received TriageEvent Message", self)

        # Spawns an intervention and provide to the manager when a player
        # triages a victim
        if message._triage_state == TriageEvent.TriageState.SUCCESSFUL:
            room_id = victims_location[message._victim_location]
            intervention = PlayerTriaggedVictimIntervention(self.manager, 
                                                            message.participant_id,
                                                            message._type,
                                                            room_id,
                                                            rooms,                                                             
                                                            victims_location,
                                                            message)

            self.logger.info("%s: Spawning Intervention %s", self, intervention)
            self.manager.spawn(intervention)
