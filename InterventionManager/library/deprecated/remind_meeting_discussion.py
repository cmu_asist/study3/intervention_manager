# -*- coding: utf-8 -*-
"""
.. module:: nlp_dummy
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger responding to tomcat labels

.. moduleauthor:: Noel Chen <noelchen@cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed remind players to talk more in the trial.
"""

from .. import Intervention
from .. import InterventionTrigger

from MinecraftBridge.messages import ASR_Message, TriageEvent, PlayerState, MissionStateEvent, Trial
from BaseAgent.components import ParticipantCollection
from MinecraftBridge.mqtt.parsers import MessageType, MessageSubtype



class MeetingDiscussionIntervention(Intervention):
    """
    A MeetingDiscussionIntervention is an Intervention which is designed to remind players
    to discuss about the meeting room puzzle.

    * Trigger: If t minutes pass, and no meeting room related labels in utterances are found. 

    * Discard States:  When an utterance includes meeting room related labels.

    * Resolve State:  Players do not respond to discuss meeting room's announcement.

    Attributes
    ----------

    Methods
    -------

    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------

        """

        Intervention.__init__(self, manager, **kwargs)

        self._participants = kwargs.get("participants", ParticipantCollection())
        self.first_intervention_timestamp = kwargs.get("first_intervention", None)

        # Register callbacks for when the intervention is connected
        self.add_minecraft_callback(ASR_Message, self.__onASR_Message)
        self.add_minecraft_callback(PlayerState, self.__onPlayerState_Message)

        self.message = "There was a meeting that happened before the incident; there may be a large number of victims in that meeting room. Do you guys want to discuss about it?”"


    def messageContainsKeyword(self, text):
        if any(word in text for word in self.meetingroom_keywords):
            return True
        else:
            return False

    def __onASR_Message(self, message):
        """
        Callback when the Intervention receives a ASR_Message message.

        Arguments
        ---------
        message : MinecraftBridge.message.ASR_Message
            Received ASR_Message message
        """
        if self.messageContainsKeyword(message.text):
            self.mentioned_keyword = True
            self.logger.debug("Discarded. Someone started discussing about the puzzle")
            self.discard()


    def __onPlayerState_Message(self, message):
        self.logger.debug("Recieved PlayerState message")

        curent_timeStamp = message.headers["msg"].timestamp
        seconds_passed_since_intervention = int((curent_timeStamp - self.first_intervention_timestamp).total_seconds())

        # reminds periodically
        self.logger.info("seconds_passed_since_intervention :{}".format(seconds_passed_since_intervention))
        if seconds_passed_since_intervention % 60 == 0:
            self.logger.info("Keep reminding.")
            self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.

        The generated dictionary *must* have the properties listed in the
        `Required Fields` below.

        TODO:  Identify a set of universal intervention properties, so that the
               returned information is sufficient for whichever type of
               intervention.

        Return
        -------
        intervention_info : dictionary
            Mapping of relevant intervention properties to needed information

        Required Fields
        ---------------
        participant_ids : list of strings
            List of participant_ids that the intervention should be sent to
        text_message : string
            A (default) text representation of the intervention
        """

        intervention_info = {}

        intervention_info["participant_ids"] = [ participant.participant_id for participant in self._participants ]
        intervention_info["text_message"] = self.message

        return intervention_info


class MeetingDiscussionTrigger(InterventionTrigger):
    """
    Class that generates EncourageVerbal Interventions.  Interventions are 
    triggered when the Team stay quiet for time `minimum_silence_time_sec`. 

    Arguments
    ---------

    Methods
    -------

    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        minimum_silence_time_sec : int
            Minimum seconds the players can remain silent before ATLAS encourages verbal
            communication.
        reminder_interval_sec : int
            The interval between MeetingDiscussionIntervention if players do not speak after
            after the first intervention.
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        self._participants = manager.participants

        self.add_minecraft_callback(ASR_Message, self.__onASR_Message)
        self.add_minecraft_callback(PlayerState, self.__onPlayerState_Message)
        self.add_minecraft_callback(MissionStateEvent, self.__MissionStateEvent_Message)


        self.reminder_time_sec = kwargs.get("reminder_time_sec", 60)
        self.meetingroom_keywords = kwargs.get("keywords", [])

        self.mission_start = False
        self.mentioned_keyword = False
        self.last_intervention_timestamp = False
    

    def messageContainsKeyword(self, text):
        if any(word in text for word in self.meetingroom_keywords):
            return True
        else:
            return False

    def __onASR_Message(self, message):
        if not self.mission_start:
            return
        
        if self.messageContainsKeyword(message.text):
            self.mentioned_keyword = True
            self.logger.info("[meeting room discussion]{} said {}".format(message.participant_id, message.text))
        
    def __onPlayerState_Message(self, message):
        if not self.mission_start:
            return

        curent_timeStamp = message.headers["msg"].timestamp
        sec_since_mission_start = (curent_timeStamp - self.mission_start_timestamp).total_seconds()

        if not self.last_intervention_timestamp and not self.mentioned_keyword and sec_since_mission_start > self.reminder_time_sec:
            intervention = MeetingDiscussionIntervention(self.manager,
                                                        first_intervention=curent_timeStamp,
                                                        participants=self._participants)
            self.logger.info("Remind players to discuss meeting room.")
            self.manager.spawn(intervention)
            self.last_intervention_timestamp = curent_timeStamp
        
    def __MissionStateEvent_Message(self, message):
        if message.state == MissionStateEvent.MissionState.START or message.state == MissionStateEvent.MissionState.Start:
            self.mission_start_timestamp = message.headers["msg"].timestamp
            self.mission_start = True