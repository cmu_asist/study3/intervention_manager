"""
Remind to coordinate trap-clearing with teammates

Summary
Only Engineers can identify and clear traps. If Non-Engineer players arebeing trapped by rubble, that suggests there is not a coordinated trap-clearing strategy. This intervention would remind Engineer to coordinate trap clearing with other players after non-Engineers fall into traps.

Trigger
Non-Engineer player gets trapped by rubble-trap and must be rescued. 

Intervention
Request that Engineer coordinate location of hazard areas with other players so that they can avoid falling into trap, or else be nearby when other players are exploring to shorten time required for rescue.

Validation
Whether or not Engineer communicates location of Engineer areas to other players afterwards, OR whether Engineer stays closer to other players afterwards.
"""

"""
TODO:
-Figure out why logger is showing the queue for resolution occurring only three times, but the intervention is appearing in the metadata much more often
"""
from .. import Intervention
from .. import InterventionTrigger

import math

from MinecraftBridge.messages import PlayerState



class CoordinateTrapClearingIntervention(Intervention):
    """
    Request that Engineer coordinate location of hazard areas with other players so that they can avoid falling into trap, or else be nearby when other players are exploring to shorten time required for rescue.

    * Trigger: Non-Engineer player gets trapped by rubble-trap and must be rescued. 

    * Discard States:  Engineer is nearby and/or indicates verbally that they are going to help the Engineer within a certain timeframe

    * Resolve State:  Engineer is NOT nearby and fails to indicate verbally that they are going to help the Engineer within a certain timeframe.

    """

    def __init__(self, manager, engineer_id, trap_id, trap_x, trap_z, time_check_threshold, nearness_threshold, intervention_start_time, trap_keywords, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the intervention manager that should control this
            intervention
        engineer_id: str
        	participant_id of Engineer
        trap_id: int
        	ID of triggering trap
        time_check_threshold: int
			Maximum amount of time (in seconds) intervention will check conditions before either presenting or discarding
        nearness_threshold: float
        	Maximum distance engineer can be from trap to be considered close to the trap
        intervention_start_time: datetime.datetime
        	Time at which intervention was first initiated. 
        trap_keywords: list of str
        	keywords indicated as referencing a trap.


        """
        Intervention.__init__(self, manager, **kwargs)

        self._manager = manager

        self._state = Intervention.State.INITIALIZED

        #ARGUMENT-SET VALUES
        self.time_check_threshold = time_check_threshold
        self.nearness_threshold = nearness_threshold
        self.trap_keywords = trap_keywords

        #VARIABLES
        #A set that contains all words spoken in dialogue over a period of time.
        self.dialogue_set = set()

        self.participant_id = participant_id

        self.add_minecraft_callback(PlayerState, self.__onPlayerStateMessage)


    def __onDialogue(self, message):
    	#[TODO] Register callback should dialogue be added to message bus.

    	#Check how much time has elapsed since intervention was initialized
    	#If time passed is greater than time_check_threshold

    	#Add message contents to dialogue set
    	#[TODO] Implement this when above TODO is met.


    def near_trap(self, eng_x, eng_z):
    	dist = math.sqrt((eng_x - self.trap_x)**2 + (eng_z - self.trap_z)**2)
    	return (dist <= self.nearness_threshold)

    def trap_mentioned(self):
    	#Check if any dialogue mentioned the trap

    def time_remaining(self, message)
    	current_time = message.timestamp
        time_elapsed = (current_time - self.intervention_start_time).total_seconds() 
        self.logger.info("time_elapsed: %d", time_elapsed)
        return (time_elapsed <= self.time_check_threshold)


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.

        The generated dictionary *must* have the properties listed in the 
        `Required Fields` below.

        Return
        -------
        intervention_info : dictionary
            Mapping of relevant intervention properties to needed information

        Required Fields
        ---------------
        participant_ids : list of strings
            List of participant_ids that the intervention should be sent to
        text_message : string
            A (default) text representation of the intervention
        """

        intervention_info = {}

        intervention_info["participant_ids"] = [ self.participant_id ]
        intervention_info["text_message"] = "Engineer: Another player recently triggered a trap. Coordinating trap-clearing with teammates will reduce the likelihood of this happening"

        return intervention_info


    def __onPlayerStateMessage(self, message):
    	if (self.time_remaining(message) and message.participant_id == self.engineer_id):
    		if self.near_trap(message.x, message.y):
    			#Discard Intervention

class CoordinateTrapClearingTrigger(InterventionTrigger):
    """
    Class that generates CoordinateTrapClearing Interventions.  Interventions are triggered at regular intervals of time trigger_interval

    Arguments
    ---------

    Methods
    -------

    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        # Register the trigger to receive PlayerState messages
        self.add_minecraft_callback(PlayerState, self.__onPlayerStateMessage)


    def spawn_intervention(self, pid, message):
            time_of_trigger = message.timestamp
            self.intervention_dict[pid] = CoordinateTrapClearingIntervention(
                self.manager,
                )
            self.logger.info("%s: Spawning Intervention %s", self, self.intervention_dict[pid])
            self.manager.spawn(self.intervention_dict[pid])

    def __onPlayerStateMessage(self, message):
        """
        Callback when an PlayerState message is received.

        Arguments
        ---------
        message : MinecraftBridge.messages.PlayerState
            Instance of the PlayerState message
        """
        self.logger.debug("%s: Received PlayerState Message", self)

        pid = message.participant_id
        #If participant does not have an active intervention in the intervention dictionary
        #OR
        #If participant's entry in the intervention dictionary is inactive
        if (pid not in self.intervention_dict.keys()) or (self.intervention_dict[pid]._state != Intervention.State.ACTIVE):
            #[TODO] Check if above is how I can check state. As of now, it appears the intervention is only being spawned once per participant, but it should be occurring more often.
            self.spawn_intervention(pid, message)
