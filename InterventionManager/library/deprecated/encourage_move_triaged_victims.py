# -*- coding: utf-8 -*-
"""
.. module:: encourage_move_triaged_victims
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for encouraging team to focus on moving victims when there is an excess of triaged non-evacuated victims

.. moduleauthor:: Max Chis <>

Definition of an Intervention class and InterventionTrigger class for when medic misidentifies a victim.
"""

"""
TODO:
-Figure out why logger is showing the queue for resolution occurring only three times, but the intervention is appearing in the metadata much more often
"""

from .. import Intervention
from .. import InterventionTrigger

from MinecraftBridge.messages import TriageEvent, VictimsRescued



class MoveTriagedVictimsIntervention(Intervention):
    """
    This intervention will alert players when they (or the team as a whole) have been idle for an extended period of time.

    * Trigger: 

    * Discard States:  

    * Resolve State:  

    """

    def __init__(self, manager, 
         playername, 
         **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the intervention manager that should control this
            intervention
        """
        Intervention.__init__(self, manager, **kwargs)

        #ARGUMENT-SET VALUES
        # self.triage_excess_threshold = triage_excess_threshold


        self.playername = playername

        self.queueForResolution();

        #This event is not implemented yet:
        # self.register_callback(ProximityBlockInteraction, self.__onProximityBlockInteraction)


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.

        The generated dictionary *must* have the properties listed in the 
        `Required Fields` below.

        Return
        -------
        intervention_info : dictionary
            Mapping of relevant intervention properties to needed information

        Required Fields
        ---------------
        playernames : list of strings
            List of playernames that the intervention should be sent to
        text_message : string
            A (default) text representation of the intervention
        """

        intervention_info = {}

        intervention_info["participant_ids"] = [ participant.participant_id for participant in self._manager.participants ]
        intervention_info["text_message"] = "There is a high number of triaged but unevacuated victims. Consider focusing more on evacuating existing victims rather than triaging additional ones."

        return intervention_info




class MoveTriagedVictimsTrigger(InterventionTrigger):
    """
    Class that generates PlayerIdle Interventions.  Interventions are triggered at regular intervals of time trigger_interval

    Arguments
    ---------

    Methods
    -------

    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        #CONSTANTS

        self.triage_excess_threshold = kwargs.get("triage_excess_threshold", 5);

        #VARIABLES
        self.triage_excess = 0;

        # Register to receive the following messages
        self.add_minecraft_callback(TriageEvent, self.__onTriageEvent)
        self.add_minecraft_callback(VictimsRescued, self.__onVictimsRescued)


    def spawn_intervention(self, message):
            intervention = MoveTriagedVictimsIntervention(
                self.manager,
                message.player_name,
                )
            # self.logger.info("%s: Spawning Intervention %s", self, self.intervention_dict[pid])
            # logging.info("%s: Spawning Intervention %s for %s at time %s", self, (self.next_iid-1), pid, (time_of_trigger/1000))
            self.manager.spawn(intervention)

    def __onTriageEvent(self, message):
        if message._triage_state == "SUCCESSFUL":
            self.triage_excess += 1
        if self.triage_excess > self.triage_excess_threshold:
            self.spawn_intervention(message)
            self.triage_excess = 0

    def __onVictimsRescued(self, message):
        self.triage_excess -= 1
