# -*- coding: utf-8 -*-
"""
.. module:: player_saw_victim
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for querying player if they saw a victim

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to ask a player if they saw a victim.
"""

from .. import Intervention
from .. import InterventionTrigger

import math
import numpy as np
import re

import MinecraftElements
from MinecraftBridge.messages import FoVSummary, PlayerState



### Constants

UNTRIAGED_VICTIM_BLOCKS = {'block_victim_1', 'block_victim_2', 'block_victim_proximity'}



### Helpers

def block_is_seen(manager, block, playername, prob_threshold=0.5, pixel_threshold=40000):
    """
    Queries the TToM model to determine whether or not a block has been seen,
    above the given probability threshold.
    """
    query = {
        'players': [playername],
        'cells': 'seen',
        'x': block['location'][0],
        'z': block['location'][2],
    }
    response = manager.agent_bridge.request(query, 'belief', timeout=1.0)

    if response is not None:
        # Use the TToM model's response
        return response.data[playername] >= prob_threshold
    else:
        # Fall back to using the pixel threshold
        return block['number_pixels'] >= pixel_threshold


def angle_between(v1, v2, degrees=True):
    """
    Returns the angle between two vectors in Euclidean space.
    """
    unit_vector = lambda v: np.array(v) / np.linalg.norm(v)
    u1, u2 = unit_vector(v1), unit_vector(v2)
    angle = np.arccos(np.clip(np.dot(u1, u2), -1.0, 1.0))

    if degrees:
        angle = np.degrees(angle)
    return angle



### Trigger

class PlayerSawVictimTrigger(InterventionTrigger):
    """
    Class that generates PlayerSawVictim Interventions.  Interventions are
    triggered when a victim enters the player's FoV, and the number of pixels
    is below a certain threshold.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        pixel_threshold : int, default=40000
            Minimum number of pixels required to consider the victim as seen
        use_tom : bool, default=True
            Whether or not to use the ToM model to determine whether blocks were seen
        """
        InterventionTrigger.__init__(self, manager, **kwargs)

        # Store pixel threshold
        self.pixel_threshold = kwargs.get('pixel_threshold', 40000)

        # Store flag of whether or not to use the ToM
        self.use_tom = kwargs.get('use_tom', True)
        if self.use_tom:
            assert self.manager.agent_bridge is not None

        # The Trigger maintains a local set of tuples indicating which victim blocks
        # each player has seen, to check if a potential intervention should be
        # initiated.
        self._victims_seen_map = set()

        # Register the trigger to receive FoV messages
        self.register_callback(FoVSummary, self.__onFoVMessage)


    def __onFoVMessage(self, message):
        """
        Callback when an FoV message is received.

        Arguments
        ---------
        message : MinecraftBridge.messages.FoVSummary
            Instance of the FoV message
        """
        self.logger.debug(f"{self}: Received FoV Message")

        # Check if there are victims in the FoV message, and do the following:
        # 1.  See if the player has seen the victim before,
        # 2.  Produce an intervention and provide to the manager if not.
        for block in message.blocks:

            # Is this an (untriaged) victim block?
            if block['type'] in UNTRIAGED_VICTIM_BLOCKS:
                self.logger.debug(f"{self}: Victim Block in FoV message: {block['type']}")

                # Check if the player has seen this block, and if not, spawn a
                # new intervention.  For each player / victim combination, only
                # a single intervention needs be spawned---the consequence of
                # the intervention is that either the player "sees" the victim
                # (number of pixels exceeds threshold), or is informed by the
                # Intervention presentation.
                if not (message.playername, block['id']) in self._victims_seen_map:
                    self._victims_seen_map.add((message.playername, block['id']))

                    # Only produce the intervention if the block wasn't seen
                    seen = block_is_seen(
                        self.manager, block, message.playername,
                        prob_threshold=0.5, pixel_threshold=self.pixel_threshold,
                    )
                    if not seen:
                        intervention = PlayerSawVictimIntervention(
                            self.manager,
                            message.participant_id,
                            block['id'],
                            self.pixel_threshold,
                            message,
                            use_tom=self.use_tom,
                        )

                        self.logger.info(f"{self}: Spawning Intervention {intervention}")
                        self.manager.spawn(intervention)



### Intervention

class PlayerSawVictimIntervention(Intervention):
    """
    A PlayerSawVictimIntervention is an Intervention which is designed to alert
    the player to a potentially unperceived victim.  

    * Trigger: The intervention is triggered when a victim enters the player's
               FoV with fewer than N pixels

    * Discard States:  The intervention is discarded if the number of pixels
                       the victim exceeds the threshold N

    * Resolve State:  The intervention is queued for a resolution if when the
                      victim is no longer in the player's FoV and the player
                      moves away from the victim, and the intervention has not
                      been discarded.
    """

    def __init__(self, manager, participant_id, victim_id, pixel_threshold, 
                       trigger_message, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : string
            Unique ID of the participant who may or may not have seen a victim
        victim_id : int
            Unique ID of the victim that may or may not have been seen
        pixel_threshold : int
            Number of pixels required to assume the victim was seen
        trigger_message : MinecraftBridge.messages.FoVSummary
            Message that triggered this intervention

        Keyword Arguments
        -----------------
        angle_threshold : float, default=135.0
            Minimum angle between the victim's location and direction of player
            movement to consider the player
        use_tom : bool, default=True
            Whether or not to use the ToM model to determine whether blocks were seen
        """

        Intervention.__init__(self, manager, **kwargs)

        # Register callbacks for when the intervention is connected
        self.register_callback(PlayerState, self.__onPlayerStateMessage)
        self.register_callback(FoVSummary, self.__onFoVMessage)

        # Intervention specific attributes
        self.participant_id = participant_id
        self.victim_id = victim_id
        self.pixel_threshold = pixel_threshold
        self.trigger_message = trigger_message

        # The angle threshold defines the minimum angle between the victim
        # and the direction of player motion to be considered "walking away
        # from".  This value is in degrees, and should not exceed 180.   
        self.angle_threshold = kwargs.get("angle_threhold", 135.0)

        # Store flag of whether or not to use the ToM
        self.use_tom = kwargs.get('use_tom', True)
        if self.use_tom:
            assert self.manager.agent_bridge is not None

        # Determine the location of the victim from the provided message
        self.victim_location = None
        for block in self.trigger_message.blocks:
            if block["id"] == self.victim_id:
                self.victim_location = np.array(block["location"])

        # Sanity check:  Make sure that the victim location exists
        if self.victim_location is None:
            # Raise an error here?
            pass


    @property
    def player_display_name(self):
        """
        Participant display name used when intervening.
        """
        match = re.search(r'\w+_ASIST1', self.participant_id)
        if match:
            return self.participant_id.split('_')[0].title()
        else:
            return self.participant_id


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.

        The generated dictionary *must* have the properties listed in the
        `Required Fields` below.

        TODO:  Identify a set of universal intervention properties, so that the
               returned information is sufficient for whichever type of 
               intervention.

        Return
        -------
        intervention_info : dictionary
            Mapping of relevant intervention properties to needed information

        Required Fields
        ---------------
        participant_ids : list of strings
            List of participant_ids that the intervention should be sent to
        text_message : string
            A (default) text representation of the intervention
        """
        return {
            'participant_ids': [self.participant_id],
            'text_message': f'{self.player_display_name}, did you notice that victim?',
        }


    def __onFoVMessage(self, message):
        """
        Callback when the Intervention receives an FoV message.

        Arguments
        ---------
        message : MincraftBridge.messages.FoVSummary
            Received FoV message
        """
        self.logger.debug(f"{self}: Received FoV Message")

        # Ignore this FoV message if it's not associated with the participant
        if message.playername != self.participant_id:
            return

        # Check to see if the victim block is in the message
        for block in message.blocks:
            if block['id'] == self.victim_id:
                # Check to see if the the victim has been seen
                seen = block_is_seen(
                    self.manager, block, message.playername,
                    prob_threshold=0.5, pixel_threshold=self.pixel_threshold,
                )

                # If the participant is assumed to have "seen" the victim,
                # change the state of the intervention to DISCARD
                self.logger.info('{self}: Discarding, under assumption that the victim has been seen.')
                self.discard()


    def __onPlayerStateMessage(self, message):
        """
        Callback when the Intervention receives a PlayerState message.

        Arguments
        ---------
        message : MinecraftBridge.message.PlayerState
            Received PlayerState message
        """
        self.logger.debug(f"{self}: Received PlayerState Message", self)

        # Ignore the message if it is not related to the player that this 
        # intervention was created for
        if message.playername != self.participant_id:
            return

        # Check to see if the player is moving away from the victim.  This is
        # calculated based on the dot product between the displacement vector
        # between the victim and player's location, and the velocity of the
        # player.  Alternatively, the orientation of the player could also be
        # used.  NOTE:  We technically only need the (x, z) location of the 
        # player and victim
        victim_player_displacement = self.victim_location - message.position
        player_victim_angle = angle_between(message.velocity, victim_player_displacement)

        # If the resulting angle exceeds the player threshold, then queue the
        # intervention for resolution
        if player_victim_angle > self.angle_threshold:
            self.logger.info(
                f"{self}: Queuing for resolution, player-victim angle ({player_victim_angle}) exceeds threshold ({self.angle_threshold})")
            self.queueForResolution()
