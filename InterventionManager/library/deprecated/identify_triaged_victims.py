# -*- coding: utf-8 -*-
"""
.. module:: identify_triaged_victims
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for transporters to locate triaged victims

.. moduleauthor:: Huao Li <hul52@pitt.edu>

Definition of an Intervention class and InterventionTrigger class for an
intervention designed to suggest smaller or larger distance between members
depending on team performance and team proximity
"""

from .. import Intervention
from .. import InterventionTrigger

import math

from MinecraftBridge.messages import (
    FoVSummary, MarkerPlacedEvent, PlayerState,
    TriageEvent, VictimPickedUp, RoleSelectedEvent,
)

MEMORY = []

class IdentifyTriagedVictimIntervention(Intervention):
    """
    A IdentifyTriagedVictimIntervention is an Intervention which is designed to help
    the transporter locate victims that were triaged by the medic earlier.

    * Trigger: The intervention is triggered when the transporter approach a triaged victim

    * Discard States:  The intervention is discarded if the transporter sees the victim

    * Resolve State:  The intervention is queued for a resolution if the transporter leaves the victim
                    , and the intervention has not been discarded.

    Attributes
    ----------

    Methods
    -------

    """

    def __init__(self, manager, participant_id, victim_id,victim, pixel_threshold,
                 trigger_message, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : string
            Unique ID of the participant who may or may not have seen a victim
        victim_id : int
            Unique ID of the victim that may or may not have been seen
        victim : dict
            Information about the victim that triggers this intervention
        pixel_threshold : int
            Number of pixels required to assume the victim was seen
        trigger_message : MinecraftBridge.message.PlayerState
            Received PlayerState message

        Keyword Arguments
        -----------------
        angle_threshold : float, default=135.0
            Minimum angle between the victim's location and direction of player
            movement to consider the player
        """

        Intervention.__init__(self, manager, **kwargs)

        # Register callbacks for when the intervention is connected
        self.add_minecraft_callback(PlayerState, self.__onPlayerStateMessage)
        self.add_minecraft_callback(FoVSummary, self.__onFoVMessage)

        # Intervention specific attributes
        self.participant_id = participant_id
        self.victim_id = victim_id
        self.victim = victim
        self.pixel_threshold = pixel_threshold
        self.trigger_message = trigger_message

        self.player_display_name = Intervention.get_player_display_name(participant_id)

        # The angle threshold defines the minimum angle between the victim
        # and the direction of player motion to be considered "walking away
        # from".  This value is in degrees, and should not exceed 180.
        self.angle_threshold = kwargs.get("angle_threshold", 135.0)


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.

        The generated dictionary *must* have the properties listed in the
        `Required Fields` below.

        TODO:  Identify a set of universal intervention properties, so that the
               returned information is sufficient for whichever type of
               intervention.

        Return
        -------
        intervention_info : dictionary
            Mapping of relevant intervention properties to needed information

        Required Fields
        ---------------
        participant_ids : list of strings
            List of participant_ids that the intervention should be sent to
        text_message : string
            A (default) text representation of the intervention
        """

        intervention_info = {}

        intervention_info["participant_ids"] = [self.participant_id]
        intervention_info["text_message"] = self.player_display_name + ", there is a triaged victim nearby you can transport."

        return intervention_info


    def __onFoVMessage(self, message):
        """
        Callback when the Intervention receives an FoV message.

        Arguments
        ---------
        message : MincraftBridge.messages.FoVSummary
            Received FoV message
        """

        self.logger.debug("%s: Received FoV Message", self)

        # Ignore this FoV message if it's not associated with the participant
        if message.playername != self.participant_id:
            return

        # Check to see if the victim block is in the message
        victim_block = None

        for block in message.blocks:
            if block["id"] == self.victim_id:
                victim_block = block

        # Check to see if the number of pixels has exceeded the threshold
        if victim_block is not None and block["number_pixels"] >= self.pixel_threshold:
            # The participant is assumed to have "seen" the victim; change the
            # state of the intervention to DISCARD

            self.logger.info("%s: Discarding, number of pixels (%d) exceeds threshold (%d)", self, block["number_pixels"], self.pixel_threshold)
            self.discard()


    def __onPlayerStateMessage(self, message):
        """
        Callback when the Intervention receives a PlayerState message.

        Arguments
        ---------
        message : MinecraftBridge.message.PlayerState
            Received PlayerState message
        """

        self.logger.debug("%s: Received PlayerState Message", self)

        # Ignore the message if it is not related to the player that this
        # intervention was created for
        if message.playername != self.participant_id:
            return

        # Check to see if the player is moving away from the victim.  This is
        # calculated based on the dot product between the displacement vector
        # between the victim and player's location, and the velocity of the
        # player.  Alternatively, the orientation of the player could also be
        # used.  NOTE:  We only need the (x,z) location of the player and
        # victim

        victim_player_displacement = (self.victim['x'] - message.x,
                                      self.victim['z'] - message.z)

        dot_product = victim_player_displacement[0]*message.motion_x + \
                      victim_player_displacement[1]*message.motion_z

        # Normalize the dot product with the magnitude of the two vectors, and
        # calculate the arc cosine of the result
        denominator1 = math.sqrt(victim_player_displacement[0]**2 +
                                     victim_player_displacement[1]**2)
        denominator2 = math.sqrt(message.motion_x**2 + message.motion_z**2)
        denominator = denominator1 * denominator2

        # The denominator may be zero, in which case, set the victim angle to 0
        # so that the resolution is not inadvertantly resolved
        if denominator <= 1e-8:
            player_victim_angle = 0.0
        else:
            player_victim_angle = (180.0/math.pi)*math.acos(dot_product / denominator)

        # If the resulting angle exceeds the player threshold, then queue the
        # intervention for resolution
        if player_victim_angle > self.angle_threshold:
            self.logger.info("%s: Queuing for resolution, player-victim angle (%f) exceeds threshold (%f)", self, player_victim_angle, self.angle_threshold)
            self.queueForResolution()


class IdentifyTriagedVictimTrigger(InterventionTrigger):
    """
    Class that generates IdentifyTriagedVictimIntervention.  Interventions are
    triggered when the distance between transporter and any triaged victim is below a
    certain threshold.

    Arguments
    ---------

    Methods
    -------

    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        Keyword Arguments
        -----------------
        distance_threshold : int default = 10
            Minimum distance for this intervention to be triggered
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        # Keep the link to the distance threshold
        self.distance_threshold = kwargs.get("distance_threshold", 10)
        self.pixel_threshold = kwargs.get("pixel_threshold", 40000)
        # The Trigger maintains a set of victim ids indicating which triaged victims
        # the transporter has been closed to. Intervention is only triggered when the transporter
        # approach a victim for the first time
        self.nearby_victims = set()
        self.triaged_victim = {}
        self.transporter_location = [0, 0]
        self.roles = {}

        # Register the trigger to receive FoV messages
        self.add_minecraft_callback(VictimPickedUp, self.__onVictimPickedUp)
        self.add_minecraft_callback(TriageEvent, self.__onTriageEvent)
        self.add_minecraft_callback(PlayerState, self.__onPlayerStateMessage)
        self.add_minecraft_callback(RoleSelectedEvent, self.__onRoleSelectedEvent)


    def __onPlayerStateMessage(self, message):
        """
        Callback when the Trigger receives an Player state message.

        Arguments
        ---------
        message : MincraftBridge.messages.PlayerState
            Received PlayerState
        """

        self.logger.debug("%s: Received Player state Message", self)


        self.transporter_location = [message.x, message.z]
        for victim in self.triaged_victim.keys():
            distance = math.sqrt(sum(i * i for i in [self.transporter_location[0] - self.triaged_victim[victim]['x'],
                                                   self.transporter_location[1] - self.triaged_victim[victim]['z']]))
            if distance < self.distance_threshold and (message.playername,victim) not in self.nearby_victims and self.roles[message.playername] == 'Transport_Specialist':
                intervention = IdentifyTriagedVictimIntervention(self.manager,
                                                                 message.playername,
                                                                 victim,
                                                                 self.triaged_victim[victim],
                                                                 self.pixel_threshold,
                                                                message)
                self.logger.info("%s: Spawning Intervention %s", self, intervention)
                self.manager.spawn(intervention)
                self.nearby_victims.add((message.playername,victim))


    def __onRoleSelectedEvent(self, message):
        """
        Callback when the Trigger receives an Role selected message.

        Arguments
        ---------
        message : MincraftBridge.messages.RoleSelectedEvent
            Received RoleSelectedEvent
        """

        self.logger.debug("%s: Received RoleSelectedEvent Message", self)
        self.roles[message.playername] = message.new_role


    def __onVictimPickedUp(self, message):
        """
        Callback when the Intervention receives an VictimPickedUp message.

        Arguments
        ---------
        message : MincraftBridge.messages.VictimPickedUp
            Received VictimPickedUp
        """
        if 'saved' not in message.type:
            return
        victim_id = message.victim_id
        self.triaged_victim.pop(victim_id, None)


    def __onVictimPlaced(self, message):
        """
        Callback when the Intervention receives an VictimPlaced message.

        Arguments
        ---------
        message : MincraftBridge.messages.VictimPlaced
            Received VictimPickedUp
        """
        if 'saved' not in message.type:
            return

        victim_x = message.victim_x
        victim_z = message.victim_z
        timestamp = message.elapsed_milliseconds
        victim_type = message.type
        victim_id = message.victim_id

        self.triaged_victim[victim_id] = {'x':victim_x,'z':victim_z,'timestamp':timestamp,'type':victim_type}

    def __onTriageEvent(self, message):
        """
        Callback when the Intervention receives an TriageEvent message.

        Arguments
        ---------
        message : MincraftBridge.messages.TriageEvent
            Received TriageEvent
        """

        victim_x = message.victim_x
        victim_z = message.victim_z
        timestamp = message.elapsed_milliseconds
        victim_type = message.type
        victim_id = message.victim_id

        self.triaged_victim[victim_id] = {'x': victim_x, 'z': victim_z, 'timestamp': timestamp, 'type': victim_type}


class IdentifyTriagedVictimsFollowup(InterventionTrigger):
    """
    Class that follow up the consequence of IdentifyTriagedVictimsIntervention.  Follow-ups are
    whether or not the transporter go check the victim or pass by, after receiving the intervention.

    Arguments
    ---------

    Methods
    -------

    """

    def __init__(self, manager, participant_id, victim_id, victim, pixel_threshold,
                 trigger_message, **kwargs):
            """
            Arguments
            ---------
            manager : InterventionManager
                Manager for this intervention
            participant_id : string
                Unique ID of the participant who may or may not have seen a victim
            victim_id : int
                Unique ID of the victim that may or may not have been seen
            victim : dict
                Information about the victim that triggers this intervention
            pixel_threshold : int
                Number of pixels required to assume the victim was seen
            trigger_message : MinecraftBridge.message.PlayerState
                Received PlayerState message

            Keyword Arguments
            -----------------
            angle_threshold : float, default=135.0
                Minimum angle between the victim's location and direction of player
                movement to consider the player
            """

            InterventionTrigger.__init__(self, manager, **kwargs)

            # Register callbacks for when the intervention is connected
            self.add_minecraft_callback(PlayerState, self.__onPlayerStateMessage)
            self.add_minecraft_callback(FoVSummary, self.__onFoVMessage)

            # Intervention specific attributes
            self.participant_id = participant_id
            self.victim_id = victim_id
            self.victim = victim
            self.pixel_threshold = pixel_threshold
            self.trigger_message = trigger_message

            # The interval define the time period of follow up validation
            self.interval = kwargs.get("interval", 100000)


    def __onFoVMessage(self, message):
        """
        Callback when the Intervention receives an FoV message.

        Arguments
        ---------
        message : MincraftBridge.messages.FoVSummary
            Received FoV message
        """

        self.logger.debug("%s: Received FoV Message", self)

        # Ignore this FoV message if it's not associated with the participant
        if message.playername != self.participant_id:
            return

        # Check to see if the victim block is in the message
        victim_block = None

        for block in message.blocks:
            if block["id"] == self.victim_id:
                victim_block = block

        # Check to see if the number of pixels has exceeded the threshold
        if victim_block is not None and block["number_pixels"] >= self.pixel_threshold:
            # The participant is assumed to have "seen" the victim; change the
            # state of the intervention to DISCARD
            self.logger.info("%s: Follow up result is True, the transporter approaches to the nearby victim.",
                             self)
            MEMORY.append(1)
            self.discard()

    def __onPlayerStateMessage(self, message):
        """
        Callback when the Intervention receives a PlayerState message.

        Arguments
        ---------
        message : MinecraftBridge.message.PlayerState
            Received PlayerState message
        """

        self.logger.debug("%s: Received PlayerState Message", self)

        # Ignore the message if it is not related to the player that this
        # intervention was created for
        if message.playername != self.participant_id:
            return

        if message.elapsed_milliseconds - self.trigger_message.elapsed_milliseconds > self.interval:
            self.logger.info("%s: Follow up result is False, the transporter does not approach to the nearby victim.", self)
            MEMORY.append(0)
            self.discard()

