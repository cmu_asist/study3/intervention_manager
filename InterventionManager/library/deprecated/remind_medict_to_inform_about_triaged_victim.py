# -*- coding: utf-8 -*-
"""
.. module:: marker_place_after_saw_victim
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for reminding the medic to inform all teammates
   about the location of a triaged victim.

.. moduleauthor:: Long Le <lnle@umass.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to ask the medic to inform all teammates about the location of a triaged victim.
"""

import math
import re

import MinecraftElements
from MinecraftBridge.messages import (MarkerPlacedEvent, PlayerState,
                                      RoleSelectedEvent, TriageEvent)

from .. import Intervention
from .. import InterventionTrigger

THREAT_ROOMS = set()  # a set of currently known threat room names



class RemindMedicToInformAboutTriagedVictim(Intervention):
    """
    A RemindMedicToInformTransporterAboutTriagedVictim is an Intervention which is designed to remind
    the medic to inform the transporter about the location of a triaged victim if 
    (1) the intervention agent believes that a teammate is unaware about the victim's location
    AND
    (2) no marker has been placed for the victim's location

    * Trigger: The intervention is triggered when a victim is newly triaged.

    * Discard States:  The intervention is discarded if a marker is placed near the victim's location or the teammate 
    has become aware of the victim's location (as determined by our internal ToM tracker) e.g. through NLP inform.

    * Resolve State:  The intervention is queued for a resolution if the teammate still is far away from the victim (as determined by `distance_threshold`)
    after `acceptable_delay_threshold` seconds, and the intervention has not been discarded (i.e. no marker has been placed for the victim's location, and
    the teammate is still unaware of the victim).

    Attributes
    ----------

    Methods
    -------

    """

    def __init__(self, manager, victim_location, trigger_time, medic_participant_id,
                 teammate_participant_id, confidence_threshold, distance_threshold, marker_distance_threshold, acceptable_delay_threshold, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        victim_location: tuple of ints (x, y, z)
            The location of the triaged victim.
        trigger_time : float (in miliseconds)
            The time this intervention was triggered (i.e. the time the victim was triaged)
        medic_participant_id : string
            Unique ID of the medic participant
        teammate_participant_id: string
            Unique ID of the teammate 
        confidence_threshold: float
            The minimum probability of the transporter being aware of the victim at the victim location to discard the intervention
        distance_threshold : float
            The distance threshold between the transporter and the triaged victim for the intervention to be queued for resolution
        marker_distance_threshold: float
            The distance threshold between the marker placed and the triaged victim for the intervention to be discarded
        acceptable_delay_threshold : float (in miliseconds)
            The time threshold for the intervention to be queued for resolution
        """

        Intervention.__init__(self, manager, **kwargs)

        self.victim_location = victim_location
        #self.victim_room_location = self.get_room_location(self.victim_location)
        # NOTE: get_room_location is not implemented yet because we currently don't have a
        # way to map (x, y, z) to a room name.
        self.victim_room_location = self.victim_location
        self.trigger_time = trigger_time
        self.medic_participant_id = medic_participant_id
        self.teammate_participant_id = teammate_participant_id

        self.confidence_threshold = confidence_threshold
        self.distance_threshold = distance_threshold
        self.marker_distance_threshold = marker_distance_threshold
        self.acceptable_delay_threshold = acceptable_delay_threshold

        # Register callbacks for when the intervention is connected
        self.add_minecraft_callback(
            MarkerPlacedEvent, self.__onMarkerPlacedMessage)  # discard
        # internal ToM bus. discard
        self.add_minecraft_callback('belief', self.__onBeliefMessage)
        self.add_minecraft_callback(
            PlayerState, self.__onPlayerStateMessage)  # resolution


    def get_room_location(self, location):
        """
        Get the room name of the location (x, y, z)
        """
        raise NotImplementedError


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.

        The generated dictionary *must* have the properties listed in the
        `Required Fields` below.

        TODO:  Identify a set of universal intervention properties, so that the
               returned information is sufficient for whichever type of
               intervention.

        Return
        -------
        intervention_info : dictionary
            Mapping of relevant intervention properties to needed information

        Required Fields
        ---------------
        participant_ids : list of strings
            List of participant_ids that the intervention should be sent to
        text_message : string
            A (default) text representation of the intervention
        """

        intervention_info = {}

        intervention_info["participant_ids"] = [self.participant_id]
        medic_display_name = Intervention.get_player_display_name(
            self.medic_participant_id)
        teammate_display_name = Intervention.get_player_display_name(
            self.teammate_participant_id)
        intervention_info["text_message"] = f"{medic_display_name}, please inform {teammate_display_name} about the location of the triaged victim at {self.victim_room_location}."

        return intervention_info

    def get_distance(location_1, location_2):
        '''
        Euclidian distance between two locations
        '''
        assert len(location_1) == 3
        assert len(location_2) == 3
        # don't care about the y coordinate (height)
        return math.sqrt(
            (location_1[0] - location_2[0]) ** 2 +
            (location_1[2] - location_2[2]) ** 2)

    def __onMarkerPlacedMessage(self, message):
        '''
        Discard the intervention if a victim marker is placed near the victim's location
        (as determined by `marker_distance_threshold`).
        '''
        # Ignore this Marker Placed message if the marker placed is not a victim marker
        victim_triaged_markers = [MinecraftElements.MarkerBlock.red_regularvictim,
                                  MinecraftElements.MarkerBlock.red_criticalvictim,
                                  MinecraftElements.MarkerBlock.red_abrasion,
                                  MinecraftElements.MarkerBlock.red_bonedamage,
                                  MinecraftElements.MarkerBlock.green_regularvictim,
                                  MinecraftElements.MarkerBlock.green_criticalvictim,
                                  MinecraftElements.MarkerBlock.green_abrasion,
                                  MinecraftElements.MarkerBlock.green_bonedamage,
                                  MinecraftElements.MarkerBlock.blue_regularvictim,
                                  MinecraftElements.MarkerBlock.blue_criticalvictim,
                                  MinecraftElements.MarkerBlock.blue_abrasion,
                                  MinecraftElements.MarkerBlock.blue_bonedamage]
        if message.marker_type not in victim_triaged_markers:
            return
        marker_location = message.location
        # Ignore this Marker Placed message if the marker placed is not close to the victim
        if self.get_distance(marker_location, self.victim_location) > self.marker_distance_threshold:
            return
        self.logger.info(
            f"{self}: Marker {message.marker_type} placed at {marker_location} near victim location at {self.victim_location}. Discarding intervention.")
        self.discard()

    def __onBeliefMessage(self, message):
        '''
        Discard the intervention if the transporter is aware of the victim
        with high confidence (as defined by the confidence_threshold)
        '''
        import pickle

        from tom.belief.views import Cell, GridBeliefView

        # Load the belief state from the message
        msg = pickle.loads(message['data'])
        transporter_belief = msg['beliefs'][self.transporter_participant_id]
        # check if the transporter is aware of the victim at the victim location
        x, z = self.victim_location
        for victim in Cell.treated_victims():
            victim_prob = GridBeliefView.get_belief_value(
                transporter_belief, feature=victim)[x, z]
            if victim_prob > self.confidence_threshold:
                self.logger.info(
                    f"{self}: Transporter {self.transporter_participant_id} is aware of victim {victim} at {self.victim_location} with confidence {victim_prob}.")
                self.discard()
                return

    def __onPlayerStateMessage(self, message):
        '''
        Queue the intervention for the resolution if certain time 
        has passed since the intervention was triggered (as defined by the acceptable_delay_threshold)
        AND the transporter is far enough away from the victim (as defined by the distance_threshold)
        '''
        # Ignore this Player State message if the player is not the transporter
        if message.participant_id != self.transporter_participant_id:
            return

        transporter_location = message.position
        state_time = message.elapsed_milliseconds
        if self.get_distance(transporter_location, self.victim_location) > self.distance_threshold \
                and state_time - self.trigger_time > self.acceptable_delay_threshold:
            self.logger.info(
                f"{self}: Transporter {self.transporter_participant_id} is too close to victim {self.victim_location}.")
            self.queueForResolution()


class RemindMedicToInformAboutTriagedVictimTrigger(InterventionTrigger):
    """
    Class that generates RemindMedicToInformTransporterAboutTriagedVictim. Interventions are
    triggered when a victim is triaged by the medic.

    Arguments
    ---------

    Methods
    -------

    """

    def __init__(self, manager, confidence_threshold, distance_threshold,
                 marker_distance_threshold, acceptable_delay_threshold, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        confidence_threshold: float
            The minimum probability of the transporter being aware of the victim at the victim location to discard the intervention
        distance_threshold : float
            The distance threshold between the transporter and the triaged victim for the intervention to be queued for resolution
        marker_distance_threshold: float
            The distance threshold between the marker placed and the triaged victim for the intervention to be discarded
        acceptable_delay_threshold : float (in miliseconds)
            The time threshold for the intervention to be queued for resolution
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        self.confidence_threshold = confidence_threshold
        self.distance_threshold = distance_threshold
        self.marker_distance_threshold = marker_distance_threshold
        self.acceptable_delay_threshold = acceptable_delay_threshold

        self.medic_participant_id = None
        self.transporter_participant_id = None
        self.engineeer_participant_id = None

        self.add_minecraft_callback(RoleSelectedEvent,
                               self.__onRoleSelectedEventMessage)  # trigger
        self.add_minecraft_callback(TriageEvent,
                               self.__onTriageEventMessage)  # track medic and transporter ids


    def __onRoleSelectedEventMessage(self, message):
        '''
        Track the medic and transporter ids
        '''
        self.logger.info(
            f"{self}: Role selected event received for participant {message.participant_id} with role {message.new_role}")
        if message.new_role == 'Medical_Specialist':
            self.medic_participant_id = message.participant_id
        elif message.new_role == 'Transport_Specialist':
            self.transporter_participant_id = message.participant_id
        else:
            self.engineeer_participant_id = message.participant_id

    def _onTriageEventMessage(self, message):

        self.logger.debug("%s: Received TriageEvent Message", self)

        if self.medic_participant_id is None or self.transporter_participant_id is None or self.engineeer_participant_id is None:
            self.logger.debug(
                "%s: Waiting for both participant ids for medic and transporter to be set", self)
            return

        # When the medic successfully triages a victim,
        # spawn an intervention to the manager
        if message._triage_state == TriageEvent.TriageState.SUCCESSFUL:
            self.logger.info("{}: {} triaged {} victim(id:{}) at {}, mission_timer: {}, elapsed_milliseconds:{}".format(
                self, message.playername, message.type, message.victim_id, message.victim_location, message.mission_timer, message.elapsed_milliseconds))
            for temmate_participant_id in [self.transporter_participant_id, self.engineeer_participant_id]:
                intervention = RemindMedicToInformAboutTriagedVictim(self.manager,
                                                                                message._victim_location,
                                                                                message.elapsed_milliseconds,
                                                                                self.medic_participant_id,
                                                                                temmate_participant_id,
                                                                                self.confidence_threshold,
                                                                                self.distance_threshold,
                                                                                self.marker_distance_threshold,
                                                                                self.acceptable_delay_threshold)
                self.manager.spawn(intervention)
