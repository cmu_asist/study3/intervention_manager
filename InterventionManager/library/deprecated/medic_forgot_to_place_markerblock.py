# -*- coding: utf-8 -*-
"""
.. module:: medic_forgot_to_place_markerblock
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for reminding the medic to put the
              correct markerblock after triagging a victim

.. moduleauthor:: Noel Chen <noelchen@cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to remind medic to put the correct markerblock
"""

from .. import Intervention
from .. import InterventionTrigger

import math

import MinecraftElements
from MinecraftBridge.messages import (
    MarkerPlacedEvent,
    TriageEvent,
    PlayerState,
    VictimPickedUp
)

class MedicForgottoPlaceMarkerBlockIntervention(Intervention):
    """
    A MedicForgottoPlaceMarkerBlockIntervention is an Intervention which is 
    designed to alert the medic when he/she forgets to place marker block
    after triaging a victim.

    * Trigger: The intervention is triggered when the medic triages a victim.

    * Discard States:  The intervention is discarded when the medic places the
                       correct markerblock in proximity with the triagged victim

    * Resolve State:  The intervention is queued for resolution when the medic 
                      leaves the victim without placing the correct markerblock.

    Attributes
    ----------

    Methods
    -------

    """

    def __init__(self, manager, playername, participant_id, victim_type, victim_id,
                       victim_location, triaged_time, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        playername : string
            Name of the player performing the triage
        participant_id : string
            Participant ID of the player performing the triage
        victim_type : MinecraftBridge.messages.FoVSummary
            The type of victim that has been triaged. Used to check whether
            the correct markerblock has been placed.
        victim_id : int
            Unique identifier for the victim being triaged
        victim_location : tuple of ints
            Location of the victim being triaged
        triaged_time : int
            The elapsed_milliseconds at the time of triage.

        Keyword Arguments
        -----------------

        """

        Intervention.__init__(self, manager, **kwargs)

        # Register callbacks for when the intervention is connected
        self.add_minecraft_callback(PlayerState, self.__onPlayerStateMessage)
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onMarkerPlacedEventMessage)
        self.add_minecraft_callback(TriageEvent, self.__onTriageEventMessage)
        self.add_minecraft_callback(VictimPickedUp, self.__onPickupEvent)

        # if the distance between the triaged victim and player exceeds this threshold, 
        # it will trigger the intervention
        self.player_victim_distance_threshold = 5 

        # the minimum distance between the placed marker and the triaged victim for a 
        # marker block to be considered placed for that particular victim
        self.marker_victim_distance_threshold = 5

        # if (current_time - triaged_time) exceeds idle_time, spawn intervention
        # unit : second
        self.idle_time = 5

        # Intervention specific attributes
        self.playername = playername
        self.victim_type = victim_type
        self.victim_location = victim_location
        self.triaged_time = triaged_time
        self.participant_id = participant_id

        self.victim_id = victim_id
        self.reminded = False
    

    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.

        The generated dictionary *must* have the properties listed in the 
        `Required Fields` below.

        TODO:  Identify a set of universal intervention properties, so that the
               returned information is sufficient for whichever type of 
               intervention.

        Return
        -------
        intervention_info : dictionary
            Mapping of relevant intervention properties to needed information

        Required Fields
        ---------------
        participant_ids : list of strings
            List of participant_ids that the intervention should be sent to
        text_message : string
            A (default) text representation of the intervention
        """

        intervention_info = {}

        intervention_info["participant_ids"] = [ self.participant_id ]
        intervention_info["text_message"] = "Remember to place marker block for {}".format(self.victim_type)

        return intervention_info


    def distance(self, loc_a, loc_b):
        """
        returns Euclidean distance between loc_a and loc_b
        """
        a_x, a_z = loc_a[0], loc_a[2]
        b_x, b_z = loc_b[0], loc_b[2]
        return math.sqrt((a_x - b_x)**2 + (a_z - b_z)**2)

    def markerblock_matches_victim(self, markerblock_type, victim_type):
        """
        checks whether the markerblock type placed matches the victim type
        e.g.
        if victim type is abrasion, return True if the markerblock placed is abrasion
                                    return False if other markerblocks are placed
        """
        # victim_type should be "victim_a", "victim_b", "victim_c", according to https://gitlab.asist.aptima.com/asist/testbed/-/blob/develop/MessageSpecs/Triage/triage_event_message.md
        # but in study 3 spiral 1, the victim_types are still "REGULAR" and "CRITICAL"
        # TODO: once the victim_types are updated, need to change this part

        # remove the "marker<color>" part
        # this leaves only "abrasion", "bonedamage", "critical", "regularvictim", "rubble", "threat", "wildcard"
        # modified date: 11/18/2021 https://gitlab.asist.aptima.com/asist/testbed/-/blob/develop/MessageSpecs/MarkerPlaced/marker_placed_event_message.md
        markerblock_type = markerblock_type.split("_")[1]
        
        if victim_type == "victim_a" and markerblock_type == "abrasion":
            return True
        if victim_type == "victim_b" and markerblock_type == "bonedamage":
            return True
        if victim_type == "victim_c" and markerblock_type == "critical":
            return True
        
        # older version that needs to be removed once updated ---->
        if victim_type == "REGULAR" and markerblock_type == "abrasion":
            return True
        if victim_type == "REGULAR" and markerblock_type == "bonedamage":
            return True
        if victim_type == "CRITICAL" and markerblock_type == "critical":
            return True
        # <---------
        return False


    def __onTriageEventMessage(self, message):
        pass

    def __onPickupEvent(self, message):
        if message.victim_x == self.victim_location[0] and message.victim_z == self.victim_location[2]:
            self.discard()

    def __onMarkerPlacedEventMessage(self, message):
        self.logger.debug("%s: Received MarkerPlacedEvent Message", self)

        if message.playername == self.playername:
            self.logger.info("{}: correct victim_type: {} | {} placed marker{} at {}".format(self, self.victim_type, message.playername, message.type.name, message.location))

            markerblock_distance = self.distance(message.location, self.victim_location)
            # Discard intervention if (1)the distance between the placed markerblock and the victim is within
            # threshold and (2) the markerblock placed matches the victim_type
            if markerblock_distance < self.marker_victim_distance_threshold and self.markerblock_matches_victim(message.type.name, self.victim_type):
                self.logger.info("%s: Discarding, Markerblock distance (%d) is within threshold (%d)", self, markerblock_distance, self.marker_victim_distance_threshold)
                self.discard()

    def __onPlayerStateMessage(self, message):
        """
        Callback when the Intervention receives a PlayerState message.

        Arguments
        ---------
        message : MinecraftBridge.message.PlayerState
            Received PlayerState message
        """

        self.logger.debug("%s: Received PlayerState Message", self)

        # Ignore the message if it is not related to the player that this 
        # intervention was created for
        if message.playername != self.playername:
            return
        
        # if player (1)leaves the victim without placing the correct markerblock
        # (2) distance exceeds threshold, and (3) haven't been reminded,
        # queue for resolution
        player_victim_distance = self.distance(message.position, self.victim_location)
        if not self.reminded and (player_victim_distance > self.player_victim_distance_threshold
                                  or (message.elapsed_milliseconds - self.triaged_time) > self.idle_time*1000):
#            print("!!Remember to place marker block for {}, at time {}".format(self.victim_location, message.elapsed_milliseconds))
            self.reminded = True
            self.logger.info("%s: Queuing for resolution, player-victim distance (%f) exceeds threshold (%f)", self, player_victim_distance, self.player_victim_distance_threshold)
            self.queueForResolution()



class MedicForgottoPlaceMarkerBlockTrigger(InterventionTrigger):
    """
    Class that generates MedicForgottoPlaceMarkerBlock Interventions.  Interventions are 
    triggered when the medic triages a victim.

    Arguments
    ---------

    Methods
    -------

    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        remaining_time_threshold : int
            Minutes left in the trial for the intervention to happen.
        total_trial_time : int
            Total time (in minutes) of a trial. (I remember it is 15 min for study 2 and 10 min for study 3?)
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        # Register the trigger to receive TriageEvent messages
        self.add_minecraft_callback(TriageEvent, self.__onTriageEventMessage)


    def __onTriageEventMessage(self, message):
        """
        Callback when an TriageEvent message is received.

        Arguments
        ---------
        message : MinecraftBridge.messages.TriageEvent
            Instance of the TriageEvent message
        """

        self.logger.debug("%s: Received TriageEvent Message", self)
        
        # When the medic successfully triages a victim,
        # spawn an intervention to the manager
        if message._triage_state == TriageEvent.TriageState.SUCCESSFUL:
            self.logger.info("{}: {} triaged {} victim(id:{}) at {}, mission_timer: {}, elapsed_milliseconds:{}".format(self, message.playername, message.type, message.victim_id, message.victim_location, message.mission_timer, message.elapsed_milliseconds))
            intervention = MedicForgottoPlaceMarkerBlockIntervention(self.manager, 
                                                                    message.playername,
                                                                    message.participant_id,
                                                                    message.type,
                                                                    message.victim_id,
                                                                    message._victim_location,
                                                                    message.elapsed_milliseconds)
            self.manager.spawn(intervention)