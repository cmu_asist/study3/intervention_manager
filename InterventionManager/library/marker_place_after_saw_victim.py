# -*- coding: utf-8 -*-
"""
.. module:: marker_place_after_saw_victim
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for reminding any members to place marker
              blocks after they see a victim for the first time

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.edu>, Max Chis <mac372@pitt.edu>

Definition of an Intervention class and InterventionTrigger class for an
intervention designed to ask a player to place a marker block outside a room
after seeing a victim.

* Trigger:
    - [Non-ToM]  A victim first enters the team's FoV through the transporter or the engineer
    - [ToM]  Belief state discrepancy about victims arises between team members following
             discovery of victim by one member.

* Discard States:
    -  A marker block is placed near the victim
    -  Victim is picked up
    -  Victim is triaged

* Resolve States:
    -  Player leaves proximity of the victim
    -  A given time interval elapses without discard / resolution
"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..mixins import BeliefMixin, ComplianceMixin, SawVictimMixin, VictimMarkerMixin

from collections import defaultdict
from MinecraftBridge.messages import MarkerPlacedEvent
from MinecraftElements import Block



class TeamSawVictimMarkerIntervention(Intervention, BeliefMixin, VictimMarkerMixin):
    """
    An intervention designed to remind the transporter and engineer
    to place markers after discovering victims.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        player : string
            Key for the relevant player (e.g. participant ID, playername, etc)
        victim : dict
            Dictionary of victim info with keys for 'id', 'location', and 'type'
        timeout : float, default=15
            Maximum mission time (in seconds) without a discard / resolution
            before queuing for resolution
        use_tom : bool, default=True
            Whether or not to use the ToM to check player beliefs
        redis_timeout : float, default=1.0
            Timeout for a Redis request on the internal bridge
        belief_threshold : float, default=0.99
            The minimum number of expected victims in the belief state of the
            triaged victim type that is considered being "aware"
        """
        Intervention.__init__(self, manager, **kwargs)
        BeliefMixin.__init__(self, manager, **kwargs)
        VictimMarkerMixin.__init__(self, manager, **kwargs)

        self.kwargs = kwargs
        self.participant = manager.participants[kwargs.get('player')]
        self.use_tom = self.kwargs.get('use_tom', True)
        self.victim_room = self.manager.semantic_map.rooms[tuple(kwargs.get('victim')['location'])]

        self.addRelevantParticipant(self.participant.participant_id)


    def _onVictimMarkerCallback(self, callback):
        """
        Default behavior for all VictimMarkerMixin callbacks unless they are overriden.
        """
        self.logger.info(f"{self}:  Triggered {callback.__name__}. Discarding intervention.")
        self.discard()


    def _onIncorrectMarkerPlaced(self, message):
        """
        Callback for when an incorrect marker block is placed near the victim.
        """

        # Do nothing, as this marker may be for another nearby victim
        pass


    def _onDistanceExceeded(self):
        """
        Callback for when player has left proximity of the victim.
        """
        if self.use_tom and self.believe(self.manager.participants, 'victims', self.victim_room):
            self.logger.info(
                "%s:  %s %s",
                self,
                "According to ToM, all players are aware of victim.",
                "Discarding intervention.",
            )
            self.discard()
        else:
            self.logger.info(
                f"{self}:  Player has left victim proximity. Queuing intervention for resolution.")
            self.queueForResolution()


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without a discard or resolution.
        """
        if self.use_tom and self.believe(self.manager.participants, 'victims', self.victim_room):
            self.logger.info(
                "%s:  %s %s",
                self,
                "According to ToM, all players are aware of victim.",
                "Discarding intervention.",
            )
            self.discard()
        else:
            self.logger.info(
                f"{self}:  Intervention has timed out. Queuing intervention for resolution.")
            self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {
            'default_recipients': [self.participant.id],
            'default_message': f"{self.participant.callsign}, remember to place a marker block after spotting a victim."
        }



class TeamSawVictimMarkerTrigger(InterventionTrigger, SawVictimMixin):
    """
    Class that generates TeamSawVictimMarkerIntervention instances.
    Triggered when a victim first enters the FoV of any player in the team.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        timeout : float, default=15
            Maximum mission time (in seconds) without a discard / resolution
            before queuing for resolution
        use_tom : bool, default=True
            Whether or not to use the ToM to check player beliefs
        redis_timeout : float, default=1.0
            Timeout for a Redis request on the internal bridge
        belief_threshold : float, default=0.99
            The minimum number of expected victims in the belief state of the
            triaged victim type that is considered being "aware"
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        SawVictimMixin.__init__(self, manager, **kwargs, victim_types=Block.injured_victims())
        self.kwargs = kwargs

        # Set attributes
        self._team_seen_victim_ids = set()
        self._room_markers = defaultdict(set) # indexed by room ID

        # Add Minecraft callback
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onMarkerPlaced)


    def _onNewVictimSeen(self, participant_id, victim):
        """
        Callback for when a victim is first seen by a player.

        Arguments
        ---------
        participant_id : string
            ID of the participant seeing the victim for the first time
        victim : dict
            Dictionary of victim info with keys for 'id', 'location', and 'type'
        """

        # Replace regular victim blocks types with "block_victim_regular"
        if victim['type'] in {'victim_a', 'victim_b', 'block_victim_1', 'block_victim_1b'}:
            victim['type'] = 'block_victim_regular'

        # Check to see if victim has not been seen by team
        if victim['id'] not in self._team_seen_victim_ids:
            self._team_seen_victim_ids.add(victim['id'])

            # Check to see if the victim room has already been marked
            room_id = self.manager.semantic_map.rooms[tuple(victim['location'])]
            if victim['type'] == 'Regular Victim Here' and 'regular' in self._room_markers[room_id]:
                return
            elif victim['type'] == 'victim_c' and 'critical' in self._room_markers[room_id]:
                return

            # Spawn an intervention if victim was discovered by
            # the engineer or the transporter
            participant = self.manager.participants[participant_id]
            if participant.role in {'Engineering_Specialist', 'Transport_Specialist'}:
                intervention = TeamSawVictimMarkerIntervention(
                    self.manager, player=participant_id, victim=victim, **self.kwargs)
                self.logger.info(
                    f"{self}: {participant.role} has seen a new victim. Spawning {intervention}.")
                self.manager.spawn(intervention)


    def __onMarkerPlaced(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MarkerPlacedEvent
            Received MarkerPlacedEvent message
        """
        room_id = self.manager.semantic_map.closest_room(message.location)
        if any(s in message.type.name for s in {'abrasion', 'bonedamage', 'regular'}):
            self._room_markers[room_id].add('regular')
        elif 'critical' in message.type.name:
            self._room_markers[room_id].add('critical')



class TeamSawVictimMarkerFollowup(Followup, BeliefMixin, ComplianceMixin, VictimMarkerMixin):
    """
    Follow up for TeamSawVictimMarkerIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=10
            Number of seconds in the time period of follow up validation
        """

        duration = kwargs.get("duration", 10)

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)
        BeliefMixin.__init__(self, manager, **intervention.kwargs)
        VictimMarkerMixin.__init__(self, manager, **dict(intervention.kwargs, timeout=None))

        # Set attributes
        self._use_tom = intervention.use_tom
        self._victim_room = intervention.victim_room


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """
        if self._use_tom and self.believe(self.manager.participants, 'victims', self._victim_room):
            self._onCompliance()
        else:
            self._onNonCompliance()


    def _onVictimMarkerCallback(self, callback):
        """
        Default behavior for all VictimMarkerMixin callbacks unless they are overriden.
        """
        self.logger.info(
            f"{self}:  Triggered {callback.__name__}. Recording noncompliance.")
        self._onNonCompliance()


    def _onCorrectMarkerPlaced(self, message):
        """
        Callback for when a correct marker block is placed near the victim.
        """
        self._onCompliance()


    def _onIncorrectMarkerPlaced(self, message):
        """
        Callback for when an incorrect marker block is placed near the victim.
        """

        # Do nothing, as this marker may be for another nearby victim
        pass
