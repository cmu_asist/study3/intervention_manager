# -*- coding: utf-8 -*-
"""
.. module:: c8_passage
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for encouraging participants to inform
              others about C8 being a passage.

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention classes and InterventionTrigger class that
encourages participants to inform others about C8 being a passage between 
map sections when discovered.
"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..mixins import ComplianceMixin

import math

from MinecraftBridge.messages import (
    ASR_Message,
###    ChatAnalysis,
    LocationEvent
)


class PassageKnowledgeStatus:
    """
    A lightweight object used by the C8 Passage Intervention to store status of
    which participants are aware of the ability to pass through C8.

    Attributes
    ----------

    Methods
    -------

    """

    def __init__(self, participants):
        """
        Arguments
        ---------
        participants : ParticipantCollection
            Collection of participants
        """

        # Keep a list of all participants in the collection
        self._participants = set([participant.participant_id for participant in participants])

        # Initialize who is aware of the C8 passage
        self._team_informed = False
        self._informed_participants = set()

        # Allow observers to be registered to receive callbacks when someone 
        # new is made aware of the room
        self._observers = set()


    def register_observer(self, observer):
        """
        """

        # TODO:  Add in warnings, etc.

        self._observers.add(observer)


    def deregister_observer(self, observer):
        """
        """

        if observer in self._observers:
            self._observers.remove(observer)


    @property
    def team_informed(self):
        return self._team_informed


    @property
    def informed_participants(self):
        return self._informed_participants


    def add_informed_participant(self, participant_id):
        """
        Indicate that the participant with the given id is aware of passage
        ability in C8

        Attributes
        ----------
        participant_id : string
            ID of the participant that understands that C8 is a passage
        """

        self._informed_participants.add(participant_id)

        # Does the whole team know now?
        if self._informed_participants == self._participants:
            self._team_informed = True





class C8PassageTrigger(InterventionTrigger):
    """
    This trigger is used to detect when a participant has entered C8, and 
    spawns an Intervention in anticipation that 

    Attributes
    ----------
    team_informed : boolean
        Indicate if the team has been informed of the presence of the passage
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        # IDs of the room and adjacent connections
        self.__room_id = kwargs.get("room_id", "C8")
        self.__west_connections = kwargs.get("west_connections", ["c_26_-75_27_-74",
                                                                  "c_26_-73_27_-72"])
        self.__east_connections = kwargs.get("east_connections", ["c_16_-70_17_-69"])

        # Keep track of whether or not the team has been informed of that C8
        # is a passage.  Also, keep track of *who* should already know of the
        # passage, based on either behavior or passing through the C8 directly.
        # This will avoid needless spawning in interventions in the case that
        # a participant already is aware of the passage (though not compliant
        # with informing others).
        self._knowledge_status = PassageKnowledgeStatus(manager.participants)


        # Register to receive the needed messages to determine when a player
        # enters C8
        self.add_minecraft_callback(LocationEvent, self._onLocationEvent)


    @property
    def knowledge_status(self):
        return self._knowledge_status
    

    def _onLocationEvent(self, message):
        """
        Callback when a participant changes their location.  Used to check when
        a participant enters a room, or exits.

        Arguments
        ---------
        message : LocationEvent
            LocationEvent message
        """

        # Has the team been informed of C8?  Then we don't need to do anything
        # Also, does the participant know if the passage?  Then don't inform 
        # with needless information
        if message.participant_id in self.knowledge_status.informed_participants:
            return

        if self.__room_id in [l["id"] for l in message.locations]:

            # Need to identify which (if any) connections were used to enter the
            # room, and note the exit connections
            exit_connections = None

            # Did the participant come from the west or east?
            for connection_id in [l["id"] for l in message.exited_connections]:

                if connection_id in self.__west_connections:

                    self.logger.debug("%s:  Participant %s entered room C8", self, message.participant_id)
                    self.logger.debug("%s:    Entered from west connection: %s", self, connection_id)
                    self.logger.debug("%s:    Exit connections of interest: %s", self, str(self.__east_connections))

                    exit_connections = self.__east_connections


                elif connection_id in self.__east_connections:

                    self.logger.debug("%s:  Participant %s entered room C8", self, message.participant_id)
                    self.logger.debug("%s:    Entered from east connection: %s", self, connection_id)
                    self.logger.debug("%s:    Exit connections of interest: %s", self, str(self.__west_connections))

                    exit_connections = self.__west_connections
                else:
                    # There are inter-room connections that should be ignored
                    pass

            if exit_connections is not None:
                # Spawn an intervention, if an exit_connection is identified
                intervention = C8PassageIntervention(self.manager,
                                                     self,
                                                     self.knowledge_status, 
                                                     message,
                                                     exit_connections)
                self.manager.spawn(intervention)



class C8PassageIntervention(Intervention):
    """
    """

    def __init__(self, manager, parent, knowledge_status, entry_message, exit_connections):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager controlling this intervention
        parent
            Parent component that is responsible for creation of this intervention
        knowledge_status : PassageKnowledgeStatus
            Object containing which particpants are aware of the passage
        entry_message : LocationEvent
            Message indicating that the participant entered C8
        exit_connections : list
            List of connections that are exits to the room that the participant
            did not enter from
        """

        Intervention.__init__(self, manager, parent=parent)

        # Hold on to the trigger, so that the intervention can later indicate 
        # that the participant is aware of the passage (even if they don't 
        # discuss with other participants).  For convenience, also keep track
        # of the participant this intervention is targeting (this is also the
        # relevant participant)
        self._knowledge_status = knowledge_status
        self._participant_id = entry_message.participant_id
        self.addRelevantParticipant(self._participant_id)

        # Hang on to the exit_connections -- if the participant enters one 
        # these areas, they will be considered to have passed through C8, and 
        # the intervention can be resolved.
        self._exit_connections = exit_connections

        # Keep a list of messages relevant to the evolution of this intervention
        # for possible later analysis
        self._behavior_history = [entry_message]

        self.logger.debug("%s:  Created intervention for participant %s", self, entry_message.participant_id)


        # Register to receive LocationEvent messages (to see if the participant
        # has exited the room) and ASR / DialogAgent messages (to see if someone
        # else who has been through the room has mentioned the room)
        self.add_minecraft_callback(LocationEvent, self._onLocationEvent)
        self.add_minecraft_callback(ASR_Message, self._onASRMessage)
###        self.add_minecraft_callback(ChatAnalysis, onChatAnalysis)

    @property
    def behavior_history(self):
        return self._behavior_history


    @property
    def monitored_participant_id(self):
        """
        Returns the id of the participant that is being monitored by this 
        intervention
        """
        return self._participant_id
    


    def _onLocationEvent(self, message):
        """
        Callback when a Location Monitor Event message is received

        Arguments
        ---------
        message : LocationEvent
            Location monitor event message
        """

        # Is this event about the participant?  If not, ignore
        if message.participant_id != self._participant_id:
            return

        # Check with the trigger to see if 1) the participant knows about the
        # passage, or 2) the whole team knows about the passage.  If so, then
        # discard
        # TODO:  How to handle non-messages in behavior history?  Create
        #        enumerated "pseudo-events"?
        if self._participant_id in self._knowledge_status.informed_participants:
            self.logger.debug("%s:  Participant %s knows that C8 is a passage", self, self._participant_id)
            self._behavior_history.append("PARTICIPANT KNOWS ABOUT PASSAGE")
            self.discard()
            return

        if self._knowledge_status.team_informed:
            self.logger.debug("%s:  Team knows that C8 is a passage", self)
            self._behavior_history.append("TEAM KNOWS ABOUT PASSAGE")
            self.discard()            
            return


        # Did the participant exit through one of the locations of 
        # interest?
        is_exit = False
        exit_location = "<UNKNOWN>"
        for connection in message.connections:
            if connection["id"] in self._exit_connections:
                is_exit = True
                exit_location = connection["id"]

        # If so, need to put the message in the history, inform the trigger
        # that this participant knows that C8 is a passage, and queue this
        # intervention for resolution
        if is_exit:
            self.logger.debug("%s:  Partcipant %s exited through %s", self, self._participant_id, exit_location)
            self._behavior_history.append(message)
            self._knowledge_status.add_informed_participant(self._participant_id)
            self.queueForResolution()


    def _onASRMessage(self, message):
        """
        Callback when an ASR message is received.  If the message is about C8
        being a passage, then discard this intervention

        Arguments
        ---------
        message : ASR_Message
        """

        __C8_PHRASES = { "c8", "c 8", "c eight", "c ate",
                         "see 8", "see eight", "see ate",
                         "sea 8", "sea eight", "sea ate" }


        # Check to see if any of the "C8" phrases has been uttered
        uttered = False

        for phrase in __C8_PHRASES:

            # Check if the phrase is in the message or any alternative
            if phrase in message.text.lower():
                uttered = True
            for alternative in message.alternatives:
                if phrase in alternative.text.lower():
                    uttered = True

        # Was "C8" uttered?
        if uttered:
            self._behavior_history.append(message)

            self._knowledge_status.add_informed_participant(self._participant_id)

            self.discard()




###    def _onChatAnalysis(self, message):
###        """
###        Callback when a ChatAnalysis message is received
###
###        Arguments
###        ---------
###        message : ChatAnalysis
###        """
###
###        pass


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information related to the
        intervention


        Return
        -------
        intervention_info : dictionary
            Mapping of relevant intervention properties to needed information

        Required Fields
        ---------------
        default_recipients : list of strings
            List of participant_ids that the intervention should be sent to
        default_message : string
            A (default) text representation of the intervention
        """

        intervention_info = {
            "default_recipients": [ self._participant_id ],
            "default_message":  "If our teammates do not yet know, you can help the team by telling about the C8 shortcut.",
        }

        return intervention_info



class C8PassageFollowup(Followup, ComplianceMixin):
    """
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : C8PassageIntervention
            Instance of the intervention that is being followed up upon
        manager : InterventionManager
            Manager of this followup and intervention

        Keyword Arguments
        -----------------
        timeout : float, default=20
            Amount of time to allow the participant to mention C8 before
            recording non-compliance
        """

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, kwargs.get("timeout", 20))

        # Extract from the intervention the participant that is being monitored
        self._participant_id = intervention.monitored_participant_id

        # Register to receive ASR messages and ChatAnalysis messages to identify
        # when a participant discusses the C8 passage
        self.add_minecraft_callback(ASR_Message, self._onASRMessage)
###        self.add_minecraft_callback(ChatAnalysis, self._onChatAnalysis)


    def _onASRMessage(self, message):
        """
        Callback when an ASR message is received.  If the message is about C8
        being a passage, then discard this intervention

        Arguments
        ---------
        message : ASR_Message
        """

        __C8_PHRASES = { "c8", "c 8", "c eight", "c ate",
                         "see 8", "see eight", "see ate",
                         "sea 8", "sea eight", "sea ate" }

        # Is the message coming from the participant of interest?
        if message.participant_id != self._participant_id:
            return


        # Check to see if any of the "C8" phrases has been uttered
        uttered = False

        for phrase in __C8_PHRASES:

            # Check if the phrase is in the message or any alternative
            if phrase in message.text.lower():
                uttered = True
            for alternative in message.alternatives:
                if phrase in alternative.text.lower():
                    uttered = True

        # Was "C8" uttered?
        if uttered:

            # Assume that all participants as heard
            for participant in self.manager.participants:
                self._knowledge_status.add_informed_participant(participant.participant_id)

            self._onCompliance()


    def _onTimeout(self):
        """
        Player hasn't mentioned the room, consider them non-compliant
        """

        self._onNonCompliance()