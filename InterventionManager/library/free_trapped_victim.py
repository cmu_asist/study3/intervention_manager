# -*- coding: utf-8 -*-
"""
.. module:: free_trapped_victim
   :platform: Linux, Windows, OSX
   :synopsis: Intervention instructing Engineer to free a trapped victim

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an
intervention designed to instruct the Engineer to free a trapped victim.

* Trigger:  Engineer sees a trapped victim

* Discard States:  Victim is no longer trapped after a given time interval

* Resolve States:
    - Engineer moves away from victim
    - Victim is still trapped after a given time interval
"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..mixins import ComplianceMixin, DistanceMixin, TrappedMixin



class FreeTrappedVictimIntervention(Intervention, DistanceMixin, TrappedMixin):
    """
    Intervention instructing the Engineer to free a trapped victim.
    """

    def __init__(self, manager, victim, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        victim : dict
            Dictionary of victim info with keys for 'id', 'location', and 'type'

        Keyword Arguments
        -----------------
        angle_threshold : float, default=135.0
            Minimum angle between the location and direction of player
            movement to consider the player "moving away"
        timeout : float, default=15
            Maximum mission time (in seconds) without a discard / resolution
            before queuing for resolution
        """
        Intervention.__init__(self, manager, **kwargs)
        DistanceMixin.__init__(
            self, manager, **kwargs, player='Engineering_Specialist', location=victim['location'])
        TrappedMixin.__init__(self, manager, **kwargs)
        self.victim_location = victim['location']

        self.addRelevantParticipant(self.manager.participants["Engineering_Specialist"].participant_id)


    def _onMovingAway(self):
        """
        Callback for when the player begins moving away from the location.
        """
        self.logger.info(f"{self}:  Player is moving away from victim. Queuing for resolution.")
        self.queueForResolution()


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without a discard or resolution.
        """
        self.logger.info(f"{self}:  Intervention has timed out.")
        if self.is_trapped(self.victim_location):
            self.logger.info(f"{self}:  Victim is still trapped. Queuing for resolution.")
            self.queueForResolution()
        else:
            self.logger.info(f"{self}:  Victim is no longer trapped. Discarding.")
            self.discard()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        message = "Engineer, there is still a trapped in the room you were in."
        return {
            'default_recipients': [self.manager.participants['Engineering_Specialist'].participant_id],
            'default_message': message,
        }



class FreeTrappedVictimTrigger(InterventionTrigger, TrappedMixin):
    """
    Class that generates FreeTrappedVictimIntervention instances.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        angle_threshold : float, default=135.0
            Minimum angle between the location and direction of player
            movement to consider the player "moving away"
        timeout : float, default=15
            Maximum mission time (in seconds) without a discard / resolution
            before queuing for resolution
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        TrappedMixin.__init__(self, manager, **kwargs)
        self.kwargs = kwargs


    def _onVictimTrapped(self, participant, victim):
        """
        Callback for when a trapped victim is first seen by a player.
        """
        if participant.role == 'Engineering_Specialist':
            intervention = FreeTrappedVictimIntervention(self.manager, victim, **self.kwargs)
            self.logger.info(
                f"{self}: Engineer saw trapped victim at {victim['location']}. Spawning {intervention}.")
            self.manager.spawn(intervention)



class FreeTrappedVictimFollowup(Followup, ComplianceMixin, TrappedMixin):
    """
    Followup to an FreeTrappedVictimIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=15
            The duration of the followup validation period (in seconds)
        """

        duration = kwargs.get("duration", 15)


        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)
        TrappedMixin.__init__(self, manager)


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance (or the mission ends).
        """
        if self.is_trapped(self.intervention.victim_location):
            self._onNonCompliance()
        else:
            self._onCompliance()
