# -*- coding: utf-8 -*-
"""
.. module:: encourage_triage_critical
   :platform: Linux, Windows, OSX
   :synopsis: This intervention will monitor the ratio of critical and
              regular victims, and suggest the team to focus on critical
              victims if they appear to be ignoring the critical ones

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

* Trigger:  Given a minimum number of total triaged victims, the ratio
            of regular-to-critical exceeds some threshold.

* Discard States:  N/A

* Resolve State:  Immediately queued for resolution

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..mixins import ComplianceMixin
from ..utils import VictimMonitor



class TriageCriticalVictimsIntervention(Intervention):
    """
    Intervention designed to encourage team to focus more on
    triaging critical victims.
    """

    def __init__(self, manager, victim_monitor, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        victim_monitor : VictimMonitor
            Monitor for triaged / evacuated victims
        """
        Intervention.__init__(self, manager, **kwargs)
        self.victim_monitor = victim_monitor.copy(self)

        # The medic is the relevant participant, no other teammates can
        # triage critical victims
        self.addRelevantParticipant(self.manager.participants['Medical_Specialist'].participant_id)



    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        self.queueForResolution()


    def getInterventionInformation(self):
        message = (
            "Team, you seem to be neglecting high-value critical victims. "
            "Stabilizing more critical victims would likely result in a higher score."
        )
        return {
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': message,
        }



class TriageCriticalVictimsTrigger(InterventionTrigger):
    """
    Trigger that spawns TriageCriticalVictimsIntervention instances.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        min_ratio : float, default=1.5
            Minimum ratio of regular-to-critical triaged victims needed
            to trigger an intervention
        min_triaged : int, default=5
            Minimum number of triaged victims needed to trigger an intervention
        """
        InterventionTrigger.__init__(self, manager, **kwargs)

        # Trigger-specific attributes
        self._min_ratio = kwargs.get('min_ratio', 2.0)
        self._min_triaged = kwargs.get('min_triaged', 5)
        self._victim_monitor = VictimMonitor(self, on_triage=self._on_triage)
        self._has_triggered = False


    def spawn_intervention(self):
        if not self._has_triggered: # only triggers once
            intervention = TriageCriticalVictimsIntervention(self.manager, self._victim_monitor)
            self.logger.info(
                f"{self}: Team not triaging enough critical victims. Spawning {intervention}")
            self.manager.spawn(intervention)
            self._has_triggered = True


    def _on_triage(self, msg):
        total = len(self._victim_monitor.triaged_victim_ids)
        ratio = self._victim_monitor.triaged_ratio()
        if total >= self._min_triaged and ratio >= self._min_ratio:
            self.spawn_intervention()



class TriageCriticalVictimsFollowup(Followup, ComplianceMixin):
    """
    Followup to an TriageCriticalVictimsIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=90
            The duration of the followup validation period (in seconds)
        """

        duration = kwargs.get("duration", 90)


        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)

        # Copy victim monitor from intervention
        self._victim_monitor = intervention.victim_monitor.copy(self)
        self._initial_ratio = self._victim_monitor.triaged_ratio()


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """
        if self._victim_monitor.triaged_ratio() <= self._initial_ratio:
            self._onCompliance()
        else:
            self._onNonCompliance()
