# -*- coding: utf-8 -*-
"""
.. module:: player_search_rooms
   :platform: Linux, Windows, OSX
   :synopsis: Intervention designed to guide the team towards promising
              unsearched areas

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

* Trigger:  Player enters a hallway for the first time

* Discard States:  N/A

* Resolve State:  Immediately queued for resolution

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

import numpy as np

from ..mixins import ComplianceMixin
from ..utils import angle_between, compass_direction

from MinecraftBridge.messages import LocationEvent, PlayerState



class PlayerSearchRoomsIntervention(Intervention):
    """
    Intervention designed to guide the team towards promising unsearched areas.
    """

    def __init__(self, manager, participant_id, location, direction, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """
        Intervention.__init__(self, manager, **kwargs)
        self.participant_id = participant_id
        self.participant = manager.participants[participant_id]
        self.location = location
        self.direction = direction

        # Specify relevant participant
        self.addRelevantParticipant(self.participant_id)


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        self.queueForResolution()


    def getInterventionInformation(self):
        message = (
            f"{self.participant.callsign}, try exploring {compass_direction(self.direction)} from here. "
            f"Rooms in this area may not yet have been explored by the team."
        )
        return {
            'default_recipients': [self.participant.id],
            'default_message': message,
        }



class PlayerSearchRoomsTrigger(InterventionTrigger):
    """
    Trigger that spawns PlayerSearchRoomsIntervention instances.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """
        InterventionTrigger.__init__(self, manager, **kwargs)

        # Trigger-specific attributes
        self._player_locations = {}
        self._visited_rooms = set()

        # Add Minecraft callbacks
        self.add_minecraft_callback(LocationEvent, self.__onLocationEvent)
        self.add_minecraft_callback(PlayerState, self.__onPlayerState)


    def spawn_intervention(self, participant_id):
        if participant_id not in self._player_locations:
            return

        location = self._player_locations[participant_id]
        direction = self._suggested_explore_direction(participant_id)

        if direction is not None:
            intervention = PlayerSearchRoomsIntervention(
                self.manager, participant_id, location, direction)
            self.logger.info(
                f"{self}:  Entered previously unvisited hallway. Spawning {intervention}")
            self.manager.spawn(intervention)


    def _suggested_explore_direction(self, participant_id):
        if participant_id not in self._player_locations:
            return None

        # Get unvisited rooms
        all_rooms = self.manager.semantic_map.rooms.unique() - self.manager.semantic_map.hallways
        unvisited_rooms = all_rooms - self._visited_rooms - {None}

        # Calculate the average direction towards unvisited rooms
        location = self._player_locations[participant_id]
        unvisited_vector = np.mean([
            self.manager.semantic_map.direction(location, room) 
            for room in unvisited_rooms], axis=0)

        # Calculate the average direction towards teammates
        if len(self._player_locations) > 1:
            teammates_vector = np.mean([
                self.manager.semantic_map.direction(location, self._player_locations[pid]) 
                for pid in self._player_locations if pid != participant_id], axis=0)
        else:
            teammates_vector = [0, 0]

        # Suggest an exploration direction vector
        angle = angle_between(teammates_vector, unvisited_vector)
        if angle is None or angle >= 90:
            return unvisited_vector


    def __onLocationEvent(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.LocationEvent
            Received LocationEvent message
        """
        for loc in message.locations:
            room = self.manager.semantic_map.get_room()
            if room not in self._visited_rooms:
                self._visited_rooms.add(room)
                if room in self.manager.semantic_map.hallways:
                    self.spawn_intervention(message.participant_id)


    def __onPlayerState(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.PlayerState
            Received PlayerState message
        """
        if self.manager.semantic_map.is_in_bounds(message.position):
            self._player_locations[message.participant_id] = message.position



class PlayerSearchRoomsFollowup(Followup, ComplianceMixin):
    """
    Followup to an PlayerSearchRoomsIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=90
            The duration of the followup validation period (in seconds)
        """

        duration = kwargs.get("duration", 90)

        Followup.__init__(self, intervention, manager, **kwargs)
        ComplianceMixin.__init__(self, timeout=duration)

        # Add Minecraft callback
        self.add_minecraft_callback(LocationEvent, self.__onLocationEvent)


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """
        self._onNonCompliance()


    def __onLocationEvent(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.LocationEvent
            Received LocationEvent message
        """
        if len(message.locations) > 0:
            if message.participant_id == self.intervention.participant_id:
                # Relative to the initial intervention location, calculate the
                # angle between suggested direction and direction of the entered toom
                room = message.locations[0]['id'].rsplit('_', 1)[0]
                direction = self.manager.semantic_map.direction(self.intervention.location, room)
                angle = angle_between(direction, self.intervention.direction)

                # Record compliance if player entered a room within 45 degrees of suggested direction
                if angle is not None and round(angle, 6) <= 45:
                    self._onCompliance()
###                    self.complete()

