# -*- coding: utf-8 -*-
"""
.. module:: encourage_transporter_scout
   :platform: Linux, Windows, OSX
   :synopsis: Intervention designed to encourage transporter to explore
                unvisited rooms and leave markers

.. moduleauthor:: Huao Li <hul52@pitt.edu>

* Trigger:  Team has a slow explore rate and the transporter did not place no_victim markers

* Discard States:  N/A

* Resolve State:  Immediately queued for resolution

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

import numpy as np

from ..mixins import ComplianceMixin
from ..utils import angle_between, compass_direction

from MinecraftBridge.messages import LocationEvent, PlayerState, MarkerPlacedEvent, VictimSignal,VictimEvacuated



class EncourageTransporterScoutIntervention(Intervention):
    """
    Intervention designed to encourage transporter to explore unvisited rooms and leave markers
    """

    def __init__(self, manager, suggestion, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """
        Intervention.__init__(self, manager, **kwargs)
        self.suggestion = suggestion
        self.participant_id = self.manager.participants['Transport_Specialist'].id



    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        if self.suggestion == 'explore':
            self.queueForResolution()
        else:
            self.discard()


    def getInterventionInformation(self):
        return {
            'default_recipients': [self.manager.participants['Transport_Specialist'].id],
            'default_message': "Transporter, try to explore more areas and mark rooms for your team.",
        }



class EncourageTransporterScoutTrigger(InterventionTrigger):
    """
    Trigger that spawns EncourageTransporterScoutIntervention instances.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """
        InterventionTrigger.__init__(self, manager, **kwargs)



        self._optimal_explore_rate = kwargs.get('optimal_explore_rate', 2.7)
        self._optimal_exploit_rate = kwargs.get('optimal_exploit_rate', 1.7)

        # Trigger-specific attributes
        self._player_locations = {}
        self._visited_rooms = set()
        self._explore = 0
        self._exploit = 0
        self._first_intervention = False
        self._second_intervention = False

        # Add Minecraft callbacks
        self.add_minecraft_callback(LocationEvent, self.__onLocationEvent)
        self.add_minecraft_callback(PlayerState, self.__onPlayerState)
        self.add_minecraft_callback(VictimSignal, self.__onVictimSignal)
        self.add_minecraft_callback(VictimEvacuated, self.__onVictimEvacuated)


    def __onLocationEvent(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.LocationEvent
            Received LocationEvent message
        """
        for loc in message.locations:
            room = loc['id'].rsplit('_', 1)[0]
            if room not in self._visited_rooms:
                self._visited_rooms.add(room)
                self.logger.info(
                    f"{self}:  The team just visited {room}.")
                self._explore +=1

    def __onVictimEvacuated(self, message):
        """
        Callback when a VictimEvacuated message is received.

        Arguments
        ---------
        message : MinecraftBridge.messages.VictimEvacuated
            Instance of the VictimEvacuated message
        """
        if message.success:
            self.logger.debug(f"{self}:  Received VictimEvacuated message")
            self._exploit += 1


    def __onVictimSignal(self, message):
        """
        Callback when a MarkerPlacedEvent message is received.

        Arguments
        ---------
        message : MinecraftBridge.messages.VictimSignal
            Instance of the VictimSignal message
        """
        if message.roomname not in self._visited_rooms:
            self._visited_rooms.add(message.roomname)
            self.logger.debug(f"{self}:  Received VictimSignal message")
            self.logger.info(
                f"{self}:  Transporter heard beep at {message.roomname}.")
            self._explore += 1


    def __onPlayerState(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.PlayerState
            Received PlayerState message
        """
        if message.mission_timer[0] == 5 and not self._first_intervention:
            self.spawn_intervention(message)
            self._first_intervention = True
        if message.mission_timer[0] == 10 and not self._first_intervention:
            self.spawn_intervention(message)
            self._first_intervention = True


    def spawn_intervention(self, message):
        t = message.elapsed_milliseconds
        explore_rate = self._explore / t * 60 * 1000
        exploit_rate = self._exploit / t * 60 * 1000
        if explore_rate / self._optimal_explore_rate >= 1 and exploit_rate / self._optimal_exploit_rate >= 1:
            return
        elif explore_rate / self._optimal_explore_rate >= exploit_rate / self._optimal_exploit_rate:
            intervention = EncourageTransporterScoutIntervention(self.manager, 'exploit')
            self.logger.info(f"{self}:  explore_rate is greater than exploit_rate. Spawning {intervention}")
            self.manager.spawn(intervention)
        elif explore_rate / self._optimal_explore_rate < exploit_rate / self._optimal_exploit_rate:
            intervention = EncourageTransporterScoutIntervention(self.manager, 'explore')
            self.logger.info(f"{self}:  exploit_rate is greater than explore_rate. Spawning {intervention}")
            self.manager.spawn(intervention)


class EncourageTransporterScoutFollowup(Followup, ComplianceMixin):
    """
    Followup to an EncourageTransporterScoutIntervention.
    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention that is being followed up on
        manager : InterventionManager
            Instance of the manager in charge of this followup

        Keyword Arguments
        -----------------
        duration : float, default=90
            The duration of the followup validation period (in seconds)
        """

        duration = kwargs.get("duration", 90)
        self.suggestion = intervention.suggestion
        self.participant_id = intervention.participant_id
        Followup.__init__(self, intervention, manager)
        ComplianceMixin.__init__(self, timeout=duration)

        # Add Minecraft callback
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onMarkerPlacedMessage)
        self.add_minecraft_callback(VictimEvacuated, self.__onVictimEvacuated)


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without recording compliance or non-compliance.
        """
        self._onNonCompliance()

    def __onMarkerPlacedMessage(self, message):
        # Ignore this Marker Placed message if it's not associated with the participant
        if message.playername != self.participant_id:
            return
        # Ignore this Marker Placed message if the marker placed is not a threat room marker
        transporter_markers = [MinecraftElements.MarkerBlock.green_novictim,
                          MinecraftElements.MarkerBlock.green_regularvictim,
                          MinecraftElements.MarkerBlock.green_criticalvictim,
                          MinecraftElements.MarkerBlock.green_critical]
        if message.marker_type not in transporter_markers:
            return
        if self.suggestion == 'explore':
            self._onCompliance()

    def __onVictimEvacuated(self, message):
        """
        Callback when a VictimEvacuated message is received.

        Arguments
        ---------
        message : MinecraftBridge.messages.VictimEvacuated
            Instance of the VictimEvacuated message
        """
        if message.success:
            if message.playername != self.participant_id:
                return
            if self.suggestion == 'exploit':
                self._onCompliance()


