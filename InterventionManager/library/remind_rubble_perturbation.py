# -*- coding: utf-8 -*-
"""
.. module:: remind_rubble_perturbation
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for querying player if they see a rubble
    perturbation happen in front of the evacuation zones

.. moduleauthor:: Noel Chen <noelchen@cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to ask a (1) non-engineer player to ask for help or 
(2) engineer to clear the rubbles when they observe rubble perturbation.

* Trigger:  Player sees the evacuation zone is blocked

* Discard States:
    [If the player is non-engineer]
    - asked for engineer to come and help
    - mentioned this to the team
    - placed a marker block to indicate the location
    [If the player is engineer]
    - clears a rubble of the perturbation

* Resolve State:  Player moves away from pertubation area without discard

"""

from .. import Intervention
from .. import InterventionTrigger
from .. import Followup

from ..components import InterventionScheduler as Scheduler
from MinecraftBridge.messages import FoVSummary, ASR_Message, RubbleDestroyedEvent, MarkerPlacedEvent

### helpers
def loc_to_2D(loc):
    """
    Convert a location to an (x, z) tuple.
    """
    if type(loc) == list:
        loc = tuple(loc)

    if len(loc) == 2:
        x, z = loc
    elif len(loc) == 3:
        x, _, z = loc
    else:
        raise ValueError(f'Invalid argument: {loc}. Must be a tuple of length 2 or 3.')

    return x, z

def is_in_bounds(loc, bounds):
    """
    Return whether or not the given location in absolute coordinates
    is within bounds of this map.

    Arguments:
        - loc: an absolute location tuple, either (x, y, z) or (x, z)
        - bounds: a list containing two tuples,
                  [(top-left xz coordinates), (bottom-right xz coordinates)]
    """
    x, z = loc_to_2D(loc)
    return (x >= bounds[0][0] and x <= bounds[1][0]
        and z >= bounds[0][1] and z <= bounds[1][1])

class RemindRubblePerturbationIntervention(Intervention):
    """
    Intervention which is designed to remind (1) non-engineers to
    ask for help or (2) engineers to clear the rubbles in the
    event of rubble perturbation.
    """

    def __init__(self, manager, participant, blocked_zone, wait_time_threshold, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """
        Intervention.__init__(self, manager, **kwargs)

        self.kwargs = kwargs

        self.addRelevantParticipant(participant)

        # Add Minecraft callback
        self.add_minecraft_callback(ASR_Message, self.__onASR_Message)
        self.add_minecraft_callback(RubbleDestroyedEvent, self.__onRubbleDestroyedEvent)
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onMarkerPlacedEvent)

        self._wait_time_threshold = wait_time_threshold

        # Schedule an intervention
        self._scheduler = Scheduler(self)
        self._scheduler.wait(self.sendInterventionToChat, delay=self._wait_time_threshold)

        self.participant = participant
        self.blocked_zone = blocked_zone

        # pathway of the blocked zone
        # type: list of two tuples
       
        self.blocked_zone_pathway = {
                            'taan':[(-2190,-2), (-2186,0)],
                            'tacn':[(-2157,-2), (-2151,0)],
                            'tabn':[(-2123,-2), (-2116,0)],
                            'tabs':[(-2191,58), (-2185,60)], 
                            'tacs':[(-2158,58), (-2150,60)], 
                            'taas':[(-2123,58), (-2117,60)], 
                            }[blocked_zone]

        # words that indicate the player is either
        # (1) sharing the perturbation situation to the team, or
        # (2) asks the engineer for help
        self.discard_keywords = ["cover", "help", "block", "engineer"]

        # intervention and discard state will depend on participant's role
        if self.participant == self.manager.participants['Engineering_Specialist'].id:
            self.is_engineer = True
        else:
            self.is_engineer = False

    def __onASR_Message(self, message):
        # ignore ASR if the intervention is for engineer
        if message.participant_id != self.participant or self.is_engineer:
            return

        for keyword in self.discard_keywords:
            if keyword in message.text:
                self.logger.info(
                    f"{self}: Discarding, under assumption that the player is informing others about the perturbation.")
                self.discard()
    
    def __onMarkerPlacedEvent(self, message):
        # ignore marker place event if the intervention is for engineer
        if message.playername != self.participant or self.is_engineer:
            return

        room_id = self.manager.semantic_map.closest_room(message.location)

        #  if a rubble or sos marker block is placed near the blocked zone
        if room_id == self.blocked_zone and any(s in message.type.name for s in {'rubble', 'sos'}):
            self.logger.info(
                f"{self}: Discarding, placed marker block next to the blocked evacuation zone ({self.blocked_zone}).")
            self.discard()



    def __onRubbleDestroyedEvent(self, message):
        # only engineers can perform rubble destroy event
        # don't need to check self.is_engineer
        if message.playername != self.participant:
            return

        # if any block within blocked_zone_pathway is cleared
        if is_in_bounds(message.location, self.blocked_zone_pathway):
            self.logger.info(
                f"{self}: Discarding, one rubble destroyed in evacuation zone ({self.blocked_zone}).")
            self.discard()


    def sendInterventionToChat(self):
        if self.is_engineer:
            self.logger.info(
                f"{self}:  Engineer didn't clear rubble at evacuation zone after {self._wait_time_threshold} seconds. Queuing intervention for resolution.")
            self.queueForResolution()
        else:
            self.logger.info(
                f"{self}:  Player didn't inform team about rubble perturbation event at {self.blocked_zone}. Queuing intervention for resolution.")
            self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        if self.is_engineer:
            return {
                'default_recipients': [self.participant],
                'default_message': (
                    "Engineer, the evacuation zone seems to be blocked."
                    "You might want to clear a path for your teammates."
                )            
            }
        else:
            return {
                'default_recipients': [self.participant],
                'default_message': (
                    "The evacuation zone seems to be blocked. You should ask the engineer for help."
                )            
            }



class RemindRubblePerturbationTrigger(InterventionTrigger):
    """
    Class that generates RemindRubblePerturbation Interventions.
    Interventions are triggered when the player sees the evacuation zone is blocked.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        wait_time_threshold : float, default=30
            Minimum number seconds the player can respond to the rubble perturbation
            after they discover the evacuation zone is blocked.
        -----------------
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Set attributes
        self._wait_time_threshold = kwargs.get('wait_time_threshold', 30)

        self.evacuation_zones = ['taan', 'tabn', 'tacn',
                                 'taas', 'tabs', 'tacs']

        # the pathway to enter the zone, if rubbles appear within this region,
        # the zone will be considered as blocked.
        # [(top-left xz coordinates), (bottom-right xz coordinates)]
        self.zone_pathway = {
                            'taan':[(-2190,-2), (-2186,0)],
                            'tacn':[(-2157,-2), (-2151,0)],
                            'tabn':[(-2123,-2), (-2116,0)],
                            'tabs':[(-2191,58), (-2185,60)], 
                            'tacs':[(-2158,58), (-2150,60)], 
                            'taas':[(-2123,58), (-2117,60)], 
                            }
        
        # each zone will only be triggered once
        self.zone_triggered_status = {zone:False for zone in self.evacuation_zones}

        # Add Minecraft callback
        self.add_minecraft_callback(FoVSummary, self.__onFoVSummary)


    def spawn_intervention(self, participant, blocked_zone, wait_time):
        intervention = RemindRubblePerturbationIntervention(self.manager, participant, blocked_zone, wait_time, **self.kwargs)
        self.logger.info(
            "%s:  %s. %s.",
            self,
            f"{participant} discovered {blocked_zone} is blocked by rubbles.",
            f"Spawning {intervention}.",
        )
        self.manager.spawn(intervention)
        self.zone_triggered_status[blocked_zone] = True
    
    def zoneIsBlocked(self, rubble_location, zone):
        """
        if any rubble appears in the zone's pathway, we assume
        the entire entrance to the zone is blocked
        """
        zone_pathway = self.zone_pathway[zone]
        if is_in_bounds(rubble_location, zone_pathway):
            return True
        return False
    
    def __onFoVSummary(self, message):
        blocked_zone = None
        participant = message.playername

        # look for rubbles blocking the evacuation zones
        for block in message.blocks:
            block_type = block["type"]

            # if the block type is not rubble/gravel, then ignore
            if "gravel" not in block_type:
                continue
            x,y,z = block["location"]
            rubble_location = block["location"]

            # if the location of the rubble is blocking 
            # the entrance to the evacuation zones
            for zone in self.evacuation_zones:
                if self.zoneIsBlocked(rubble_location, zone):
                    blocked_zone = zone
                    break

        # if any evacuation zone is blocked
        if blocked_zone:
            # if this zone has already been triggered, ignore
            if self.zone_triggered_status[blocked_zone]:
                return

            self.spawn_intervention(participant, blocked_zone, self._wait_time_threshold)
