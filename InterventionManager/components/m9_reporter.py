# -*- coding: utf-8 -*-
"""
.. module:: components.m8_reporter
    :platform: Linux, Windows, OSX
    :synopsis: Simple addon for reporting ASI-M9 metric
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Defines an addon class for reporting ASI-M9, which is published at the end of
a Mission (to ensure that it is captured in the trial metadata)
"""

from MinecraftBridge.messages import MissionStateEvent, AgentMeasure
from MinecraftBridge.utils import Loggable



class M9_Reporter(Loggable):
	"""
	A simple class for calculating and reporting M9 score at the end of a
	mission.

	Attributes
	----------

	Methods
	-------
	"""


	def __init__(self, manager, **kwargs):
		

		# Keep a reference to the manager for access to the minecraft
		# interface and intervention queues
		self.manager = manager
		self._minecraft_interface = self.manager.minecraft_interface

		# Register to receive MissionState messages, to indicate when the 
		# measure should be calculated and published
		self._minecraft_interface.register_callback(MissionStateEvent, self._onMissionState)


	def __str__(self):
		"""
		String representation of the profiler
		"""

		return self.__class__.__name__


	def _onMissionState(self, message):
		"""
		Callback when a MissionStateEvent message is received.  If the mission
		is ending, then the M8 score (number of interventions issued) should be
		computed and published

		Arguments
		---------
		message : MissionStateEvent
			MissionStateEvent message received
		"""

		# Ignore start messages
		if message.state.is_start_state():
			return

		self.logger.warning("%s:  Generating and publishing M8 message", self)

		# Calculate the number of presented interventions.  These interventions
		# must be in the
		compliance = 0
		noncompliance = 0

		# Go through the completed followups and calculate how many followups
		# were complied with, and how many weren't
		# NOTE:  the `player_complied` property can take on the value of `None`
		#        to indicate that the followup has neither been complied or not
		#        complied with.  These should be ignored
		for followup in self.manager._completed_followup_queue:
			if followup.player_complied is True:
				compliance += 1
			if followup.player_complied is False:
				noncompliance += 1

		# Publish the final value
		compliance_rate = compliance / max((compliance + noncompliance), 1)

		# Publish the final value
		message = AgentMeasure(study_version=3,
			                   elapsed_milliseconds=0,
			                   qualifying_event_type="time",
			                   measure_id="ASI-M9",
			                   datatype="float",
			                   measure_value=compliance_rate,
			                   description="Compliance Rate",
			                   additional_data={})


		self.manager.publish_measure(message)

