# -*- coding: utf-8 -*-
"""
.. module:: components.intervention_profiler
    :platform: Linux, Windows, OSX
    :synopsis: Profiler for monitoring the lifecycle of interventions and
               followups
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Defines an addon for profiling the lifecycle of interventions and followups, 
upon the end of a Trial.
"""

from MinecraftBridge.messages import PlayerState, Trial, MissionStateEvent
from MinecraftBridge.utils import Loggable
from MinecraftBridge.mqtt.parsers import MessageSubtype

from ..interventions import Intervention
from ..followups import Followup

from collections import defaultdict

import os


class InterventionLifecycleProfiler(Loggable):
	"""
	An InterventionLifecycleProfiler monitors when an intervention or followup
	changes state.

	Attributes
	----------

	Methods
	-------
	"""

	# Need to keep a consistently ordered list of the intervention and followup
	# states.  There's probably a better way to do this...
	__ORDERED_INTERVENTION_STATES = [ Intervention.State.INITIALIZED,
	                                  Intervention.State.ACTIVE,
	                                  Intervention.State.DISCARDED,
	                                  Intervention.State.QUEUED_FOR_RESOLUTION,
	                                  Intervention.State.VALIDATING,
	                                  Intervention.State.RESOLVED
	                                 ]
	__ORDERED_FOLLOWUP_STATES = [ Followup.State.INITIATED,
	                              Followup.State.ACTIVE,
	                              Followup.State.COMPLETE
	                            ]

	__INTERVENTION_CSV_HEADER = "intervention_name,UUID,parent_UUID," + \
	                            ",".join(["{0}_mission_timer_minutes,{0}_mission_timer_seconds,{0}_elapsed_milliseconds".format(x.name) for x in __ORDERED_INTERVENTION_STATES]) + \
	                            ",recipients,message"
	__FOLLOWUP_CSV_HEADER = "followup_name,UUID,parent_UUID," + \
	                        ",".join(["{0}_mission_timer_minutes,{0}_mission_timer_seconds,{0}_elapsed_milliseconds".format(x.name) for x in __ORDERED_FOLLOWUP_STATES]) + \
	                        ",target_participant,compliance"
	                        

	def __init__(self, manager, **kwargs):
		"""
		Arguments
		---------
		manager : InterventionManager
			Handle to the manager the profiler is reporting on
		"""

		# Keep a reference to the interface, in case it needs to be used later
		self._manager = manager
		self._minecraft_interface = manager.minecraft_interface

		# Register to receive PlayerState messages
		self._minecraft_interface.register_callback(Trial, self._onTrial)
		self._minecraft_interface.register_callback(MissionStateEvent, self._onMissionState)


		# Keep track of filenames to write to, if any
		self._intervention_filename = kwargs.get("intervention_filename", None)
		self._followup_filename = kwargs.get("followup_filename", None)

		# Keep track of wheter or not the logs have already been written
		self._logging_complete = False


	def __str__(self):
		"""
		String representation of the profiler
		"""

		return self.__class__.__name__



	def _onMissionState(self, message):
		"""
		Callback when a MissionStateEvent message is received.  If the mission
		is ending, the logs should be written. This may overlap with the _onTrial;
		however, the trial message will not always be received. 
		"""

		# For consecutive mission runs, reset the logger
		if message.state.is_start_state():
			self._logging_complete = False
			self.logger.info("Resetting profiler for new run")

		# If the mission is over, dump the profiler results
		elif message.state.is_stop_state():
			self._dump_files(self._intervention_filename, self._followup_filename)


	def _onTrial(self, message):
		"""
		Callback when a Trial message is received.  Used to collect
		experiment and trial ids, and to write profile files when complete.

		Arguments
		---------
		message : MinecraftBridge.messages.Trial
			Received Trial message
		"""

		# For consecutive mission runs, reset the logger
		if message.headers["msg"].sub_type == MessageSubtype.start:
			self._logging_complete = False
			self.logger.info("Resetting profiler for new run")

		if message.headers["msg"].sub_type == MessageSubtype.stop:	
			self._dump_files(self._intervention_filename, self._followup_filename)
			

	def onInterventionManagerReset(self, manager):
		"""
		Callback when an intervention manager has been reset

		Arguments
		---------
		manager : InterventionManager
			InterventionManager that has been reset
		"""

		pass


	def __writeInterventionLine(self, intervention, csv_file):
		"""
		Write a single intervention to the provided csv file

		Arguments
		---------
		intervention : Intervention
			Intervention to extract information from and write
		csv_file : File
			File handle for the CSV file to write to
		"""

		csv_file.write(intervention.__class__.__name__ + ",")
		csv_file.write("{0},".format(intervention.uuid))

		# Does the parent of this component have a UUID?
		if hasattr(intervention.parent,"uuid"):
			csv_file.write("{0},".format(intervention.parent.uuid))
		else:
			csv_file.write("None,")

		# Write the entry time for each state
		for state in InterventionLifecycleProfiler.__ORDERED_INTERVENTION_STATES:
			csv_file.write("{0},{1},".format(*intervention.state_entry_times[state].mission_timer))
			csv_file.write("{0},".format(intervention.state_entry_times[state].elapsed_milliseconds))

		# Which state did the intervention end up in?  Either write the sent
		# message, or a suitable one.  Also, replace commas with semicolons, so
		# that the lines are easily parsable.
		if intervention.state == Intervention.State.RESOLVED:
			csv_file.write("{0},{1}\n".format(str(intervention.message_recipients).replace(',',';'),
				                              str(intervention.presented_message).replace(',',';')))
		elif intervention.state == Intervention.State.DISCARDED:
			csv_file.write("[],<DISCARDED>\n")
		else:
			csv_file.write("[],<UNPROVIDED>\n")


	def __writeFollowupLine(self, followup, csv_file):
		"""
		Write a single followup to the provided CSV file

		Arguments
		---------
		followup : Followup
			Followup to extract information from and write
		csv_file : File
			File handle for the CSV file to write to
		"""

		csv_file.write(followup.__class__.__name__ + ",")
		csv_file.write("{0},".format(followup.uuid))

		# Does the parent of this component have a UUID?
		if hasattr(followup.parent,"uuid"):
			csv_file.write("{0}".format(followup.parent.uuid))
		else:
			csv_file.write("None")		

		for state in InterventionLifecycleProfiler.__ORDERED_FOLLOWUP_STATES:
			csv_file.write(",{0},{1},".format(*followup.state_entry_times[state].mission_timer))
			csv_file.write("{0}".format(followup.state_entry_times[state].elapsed_milliseconds))

		# Write whether the followup was complied with or not
		if followup.player_complied is None:
			csv_file.write(",TBD,NA")
		elif followup.player_complied:
			csv_file.write(",TBD,complied")
		else:
			csv_file.write(",TBD,noncomplied")

		csv_file.write("\n")		



	def writeInterventionsToCSV(self, csv_path, include_header=True):
		"""
		Write the contents of the interventions recorded by the profiler to the
		provided path as a CSV file

		Arguments
		---------
		csv_path : string
			path to the CSV file to write to
		include_header : boolean, default=True
			Indicate whether to include a header line
		"""

		# Check to see if the file exists, and issue a warning if it is going
		# to be overwritten.  If the path is a directory, indicate so in a
		# warning and return, as it will not be able to be written to.
		if os.path.isfile(csv_path):
			self.logger.warning("%s:  Writing intervention information to existing file: %s", self, csv_path)
		if os.path.isdir(csv_path):
			self.logger.warning("%s:  Cannot write to provided CSV path (CSV path is a directory): %s", self, csv_path)
			return

		# Try to open the file and write the conents of the intervention
		with open(csv_path, "w") as csv_file:
			if include_header:
				csv_file.write(InterventionLifecycleProfiler.__INTERVENTION_CSV_HEADER + "\n")

			# Iterate through the database stored by the manager, and write
			# the relevant contents of the interventions
			for intervention_list in [ self._manager._initialization_queue,
			                           self._manager._active_queue,
			                           self._manager._response_queue,
			                           self._manager._discard_queue ]:

			    for intervention in intervention_list:

			    	self.__writeInterventionLine(intervention, csv_file)



	def writeFollowupsToCSV(self, csv_path, include_header=True):
		"""
		Write the contents of the followups recorded by the profiler to the
		provided path as a CSV file

		Arguments
		---------
		csv_path : string
			path to the CSV file to write to
		include_header : boolean, default=True
			Indicate whether to include a header line
		"""

		# Check to see if the file exists, and issue a warning if it is going
		# to be overwritten.  If the path is a directory, indicate so in a
		# warning and return, as it will not be able to be written to.
		if os.path.isfile(csv_path):
			self.logger.warning("%s:  Writing followup information to existing file: %s", self, csv_path)
		if os.path.isdir(csv_path):
			self.logger.warning("%s:  Cannot write to provided CSV path (CSV path is a directory): %s", self, csv_path)
			return

		# Try to open the file and write the conents of the followup
		with open(csv_path, "w") as csv_file:
			if include_header:
				csv_file.write(InterventionLifecycleProfiler.__FOLLOWUP_CSV_HEADER + "\n")


			# Iterate through the database stored by the manager, and write
			# the relevant contents of the interventions
			for followup_list in [ self._manager._initiated_followup_queue,
			                       self._manager._active_followup_queue,
			                       self._manager._completed_followup_queue ]:

			    for followup in followup_list:

			    	self.__writeFollowupLine(followup, csv_file)


	def _dump_files(self, intervention_filename, followup_filename):
		"""
		Write the intervention and followup profiles, if provided
		"""
		if self._logging_complete:
			self.logger.info("%s: Profiling data for this run have already been written", self)
			return

		self.logger.info("%s:  Exiting: Writing profile information to files", self)

		if intervention_filename is not None:
			self.writeInterventionsToCSV(intervention_filename)
		if followup_filename is not None:
			self.writeFollowupsToCSV(followup_filename)
		
		self._logging_complete = True
