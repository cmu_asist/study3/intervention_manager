# -*- coding: utf-8 -*-
"""
.. module:: components.m8_reporter
    :platform: Linux, Windows, OSX
    :synopsis: Simple addon for reporting ASI-M8 metric
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Defines an addon class for reporting ASI-M8, which is published at the end of
a Mission (to ensure that it is captured in the trial metadata)
"""

from MinecraftBridge.messages import (
	AgentMeasure,
	MissionStateEvent
)
###from MinecraftBridge.messages import BusHeader, MessageHeader
###from MinecraftBridge.mqtt.parsers import MessageType, MessageSubtype

from MinecraftBridge.utils import Loggable

from ..interventions import Intervention



class M8_Reporter(Loggable):
	"""
	A simple class for calculating and reporting M8 score at the end of a
	mission.

	Attributes
	----------

	Methods
	-------
	"""


	def __init__(self, manager, **kwargs):
		

		# Keep a reference to the manager for access to the minecraft
		# interface and intervention queues
		self.manager = manager
		self._minecraft_interface = self.manager.minecraft_interface

		# Register to receive MissionState messages, to indicate when the 
		# measure should be calculated and published
		self._minecraft_interface.register_callback(MissionStateEvent, self._onMissionState)


	def __str__(self):
		"""
		String representation of the profiler
		"""

		return self.__class__.__name__


	def _onMissionState(self, message):
		"""
		Callback when a MissionStateEvent message is received.  If the mission
		is ending, then the M8 score (number of interventions issued) should be
		computed and published

		Arguments
		---------
		message : MissionStateEvent
			MissionStateEvent message received
		"""

		# Ignore start messages
		if message.state.is_start_state():
			return

		self.logger.warning("%s:  Generating and publishing M8 message", self)

		# Calculate the number of presented interventions.  These interventions
		# must be in the 
		M8 = 0

		# Go through the interventions in the response queue and increment M8
		# if 1) the intervention is in the RESOLVED state, and 2) the
		# intervention has been presented.
		# NOTE:  There's probably a lovely list comprehension here...
		for intervention in self.manager._response_queue:
			if intervention.state == Intervention.State.RESOLVED and intervention.presented:
				M8 += 1

		# Publish the final value
		message = AgentMeasure(study_version=3,
			                   elapsed_milliseconds=0,
			                   qualifying_event_type="time",
			                   measure_id="ASI-M8",
			                   datatype="integer",
			                   measure_value=M8,
			                   description="Intervention Count",
			                   additional_data={})


		self.manager.publish_measure(message)

