# -*- coding: utf-8 -*-
"""
.. module:: kit_analyzer
   :platform: Linux, Windows, OSX
   :synopsis: Analyzes KIT (Knowledge Intigration Test) utterances given a text

.. moduleauthor:: Simon Stepputtis <stepputts@cmu.edu>
"""

import nltk
from nltk import word_tokenize, pos_tag
from nltk.corpus import stopwords
from absl import logging
import re
import numpy as np
from collections import Counter

class KITAnalyzer():
    def __init__(self):
        self._kit = {}
        self._rooms = []
        self._meetings = []
        self._attendee_matches = {}
        self._suggestion = []
        self._consensus = []
        self._consensus_weigth = 0
        self._discussed = False
        self._room_frequency = {}

    def parse(self, text):
        """
        Main entry point to parse a sequence of utterances
        Will return True if puzzle information was found, False if not
        
        Note: There will always be a consensus... for what it's worth.
        """
        self._kit = {}
        self._text = text

        self._consensus, self._consensus_weigth  = self.findConsensus()
        logging.info("Infered consensus (evidence {}): {}".format(self._consensus_weigth, self._consensus))

        self._rooms = self.findRooms()
        logging.info("Found {} rooms: {}".format(len(set(self._rooms)), str(set(self._rooms))))
        if len(set(self._rooms)) < 4:
            logging.info("Puzzle doesn't seem to be discussed (not enough rooms). Aborting")
            self._discussed = False
            return

        self.findMeetings()
        logging.info("Found {} meetings: {}".format(len(self._meetings), str(self._meetings)))
        if len(set(self._meetings)) < 4:
            logging.info("Puzzle insufficiently discussed (not enough meetings found). Aborting")
            self._discussed = False
            return
        self.matchMeetingsToRooms()

        self._attendees  = self.findAttendees()
        logging.info("Found {} attendee matchings".format(self._attendees))
        if self._attendees < 2:
            # Not a lot of data to go on about... Let's see if it is at least relevant
            max_attendees = max(self._kit.values(), key=lambda x: x[1])
            if max_attendees[1] < 2:
                logging.info("Unable to find enough attendee information. Aborting")
                self._discussed = False
                return False  

        # mapping    = mergeEvidence(text, rooms, meetings, attendees)
        self._suggestion = self.getSuggestion()
        logging.info("Proposed ATLAS suggestion: " + str(self._suggestion))
        self._discussed = True
        return self._discussed

    def cleanText(self, text):
        """
        Make everything lower-case and remove dots and commas
        """
        segments = text.split(" ")
        for i in range(len(segments)):
            segments[i] = segments[i].replace(".", "").replace(",", "")
            segments[i] = segments[i].lower()
        
        return " ".join(segments)


    def findRooms(self):
        """
        Finds rooms under the assumption they are <letter><number> or <letter><number><letter>
        """
        rooms = []
        for word in self._text.split(" "):
            word = word.replace(".", "").replace(",", "").strip()
            if re.match(r"^[A-Z,a-z][0-9]$", word) or re.match(r"^[A-Z,a-z][0-9][A-Z,a-z]$", word):
                rooms.append(word.upper())

        for r in rooms:
            if r not in self._kit.keys():
                self._kit[r] = ["", -1]
        return rooms

    def findMeetings(self):
        """
        Attempts to find meeting names. However, we are not allowed to know the names, thus
        this is depending on keywords like "meeting" and "training". Special cases are "IT Traiing"
        becayse "it" is not a noun usually, as well as allowing meetings to have numbers, e.g. Zoom2

        Strateyg: Find the keyword, then assume the preceeding nouns are in fact meeting names
        """
        tokens = word_tokenize(self._text)
        tagged = nltk.pos_tag(tokens)
        text = self.cleanText(self._text)

        meetings = []
        def addMeeting(name):
            name = name.replace("meeting", "")
            name = name.replace("training", "")
            name = name.strip()                          
            if len(name) < 4:
                return
            meetings.append(name)

        # Assume meetings end with "meeting" or "training"
        name_meetings = re.findall(r"[a-z]* [a-z]+[0-9]? meeting|[a-z]* [a-z]+[0-9]? training", text)
        # Now, clean them out for nouns only:
        for i in range(len(name_meetings)):
            tokens = word_tokenize(name_meetings[i])
            tagged = nltk.pos_tag(tokens)
            name = " ".join([w for w, t in tagged if t in ["NN", "NNP", "NNS", "JJ", "NNPS", "CD"] or w in ["meeting", "training", "it"]])
            name = name.strip()
            addMeeting(name)  

        # Assume there is something like "room is/was meeting" 
        # This structure seems unusual, but data is presented that way
        loc_meetings = re.findall(r"[a-z][0-9][a-z]? is ?a?[a-z]? ?[a-z]* ?[a-z]*|[a-z][0-9][a-z]? was ?a?[a-z]? ?[a-z]* ?[a-z]*", text)
        for i in range(len(loc_meetings)):
            name = " ".join(loc_meetings[i].split(" ")[2:])
            tokens = word_tokenize(name)
            tagged = nltk.pos_tag(tokens)
            name = []
            for w,t in tagged:
                if t in ["NN", "NNP", "NNS", "JJ", "NNPS", "CD"] or w in ["meeting", "training", "it", "zoom"]:
                    name.append(w)
                elif len(name) > 0:
                    break
            name = " ".join(name)
            name = name.strip()
            addMeeting(name)

            # Collect a little evidence already if it is about a room
            # Note: We already have all rooms. If we found something strange, e.g. M2
            # from Zoom2, just ignore it.
            if loc_meetings[i].split(" ")[0].upper() in self._kit.keys():
                self._kit[loc_meetings[i].split(" ")[0].upper()][0] = name.replace("meeting", "").strip()

        # People like to abreviate meetings, thus delete all one-word meetings that are a substring
        # of another identified two or three word meeting
        meetings = list(set(meetings))
        self._meetings = []
        for m in meetings:
            if len(m.split(" ")) == 1:
                duplicate = False
                for m2 in meetings:
                    segments = m2.split(" ")
                    if len(segments) > 1 and m in segments:
                        duplicate = True
                if not duplicate:
                    self._meetings.append(m)
            else:
                self._meetings.append(m)

        return self._meetings

    def findConsensus(self):
        """
        Tries to find the consensus of players given room frequencies and 
        certain keywords. E.g. "first", "priority", ...
        """
        sentences = self._text.split(".")
        text = ". ".join(sentences[-int(len(sentences)/2):])
        text = self.cleanText(text)

        # This is rather crude for now
        consensus = []
        # Most Mentioned Room:    
        all_rooms = self.findRooms()
        if len(all_rooms) == 0:
            return "None", None
        # count this evidence as "one", but sort it to give it preference
        all_rooms = sorted(all_rooms, key=Counter(all_rooms).get, reverse=True) 
        # Make values unique, but keep order
        for v in all_rooms:
            if v not in consensus:
                consensus.append(v)
        # Limit to two most common rooms:
        consensus = consensus[:2]

        # Some Keyword search
        consensus += re.findall(r"([a-z][0-9][a-z]?) first", text)
        consensus += re.findall(r"([a-z][0-9][a-z]?) instead", text)
        consensus += re.findall(r"start with ([a-z][0-9][a-z]?)", text)
        consensus += re.findall(r"start at ([a-z][0-9][a-z]?)", text)
        consensus += re.findall(r"priority [a-z]* ?[a-z]* ?([a-z][0-9][a-z]?)", text)
        consensus += re.findall(r"([a-z][0-9][a-z]?) [a-z]* ?[a-z]* ?priority", text)

        # Choose the one with the most evidence...
        self._consensus = [c.upper() for c in consensus]
        if len(self._consensus) == 0:
            return "None", None
        
        count_dictionary = {v: self._consensus.count(v) for v in set(self._consensus)}
        self._consensus = [k for k, v in count_dictionary.items() if v == max(count_dictionary.values())]
        # Return at most two elements
        return self._consensus[:2], max(count_dictionary.values())

    def _findClosestNumber(self, base, numbers, up_only=False):
        """
        Finds the closes number in `numbers` given the base in both directions
        """
        for i in range(np.max(numbers)+1):
            if base + i in numbers:
                return base + i
            if not up_only and base - i in numbers:
                return base - i
        return -1

    def findAttendees(self):
        """
        Tries to find out how many attendees are in one meeting. This is pretty difficult though, 
        so for now, we find the index of the attendee count and match the closes meeting name to it.
        If there is only one quantity in a sentence (and multiple meeting names), all of them are matched

        Drwaback: 
            This can't handle <quantity> <meeting> <meeting> <quantity> <meeting< <quantity>
        However, users seem to like doing that. Other common patterns we can't understand is Q&A
            How many people were in <meeting> <meeting>? .... Let me see, <quantity>!
        """
        attendee_assignment = 0
        self._attendees = {}
        # Maybe a cheap proximity approach gives us an idea. 
        sentences = self._text.split(".")
        sentences = [self.cleanText(s) for s in sentences]

        for s in sentences:
            two_reg = re.search(r"two or more attendee|two attendee|the most attendees", s)
            one_reg = re.search(r"one attendee", s)
            non_reg = re.search(r"not? [a-zA-Z0-9]* ?[a-zA-Z0-9]* ?[a-zA-Z0-9]* ?[a-zA-Z0-9]* ?attendee", s)

            meeting_dict = {}
            if two_reg or one_reg or non_reg:
                meeting_dict = self._findMeetingLocs(s)
                if len(meeting_dict.keys()) == 0:
                    continue
            else:
                continue

            meeting_list = [v for k, v in meeting_dict.items()]
            room_index = {}
            if two_reg:
                room_index[two_reg.start()] = 2
            if one_reg:
                room_index[one_reg.start()] = 1
            if non_reg:
                room_index[non_reg.start()] = 0

            room_indices = [k for k, v in room_index.items()]
            # if there is one quantity in a sentence, mark all rooms in that sentence with the quantity
            if len(room_indices) == 1:
                for meeting_name in meeting_dict.keys():
                    for k, v in self._kit.items():
                        if v[0] == meeting_name:
                            self._attendee_matches[v[0]] = room_index[room_indices[0]]
                        if v[0] == meeting_name and self._kit[k][1] == -1:
                            self._kit[k][1] = room_index[room_indices[0]]
                            attendee_assignment += 1
                            break
            # If there are multiple rooms and quantities, only match the closest one
            else:
                for rid in sorted(room_indices):
                    meeting_id = self._findClosestNumber(rid, meeting_list)

                    # Match the evidence into the self._kit
                    closest_meeting = [k for k, v in meeting_dict.items() if v == meeting_id]
                    if len(closest_meeting) == 0:
                        continue
                    meeting_name = closest_meeting[0]
                    for k, v in self._kit.items():
                        if v[0] == meeting_name:
                            self._attendee_matches[v[0]] = room_index[rid]
                        if v[0] == meeting_name and self._kit[k][1] == -1:
                            self._kit[k][1] = room_index[rid]
                            attendee_assignment += 1
                            break
        return attendee_assignment

    def _findMeetingLocs(self, sentence):
        """
        Finds the index of meeting names in a given sentence
        """
        result = {}
        for meeting in self._meetings:
            res = re.search(meeting, sentence)
            if res:
                result[meeting] = res.start()
        return result

    def matchMeetingsToRooms(self):
        """
        Attempts to matcht meetings for which we didn't find a room yet 
        (meetings for which it is not obvious) to rooms given a simple 
        distance metric. 
        """
        text = self.cleanText(self._text)
        # Now, find all the meeting indices not yet matched to a room
        unmatched_meetings = []
        for meeting in self._meetings:
            found = False
            for k, v in self._kit.items():
                if v[0] == meeting:
                    found = True
                    break
            if not found:
                unmatched_meetings.append(meeting)

        # Find the indices of all these meetings and make a dict out of them
        meeting_indices = {}
        for meeting in unmatched_meetings:
            meeting_mentioned_at_id = [m.start() for m in re.finditer(meeting, text)]
            for i in meeting_mentioned_at_id:
                meeting_indices[i] = meeting

        for room in self._kit.keys():
            # If we don't have an assignment yet, find the closest meeting name mentioned to the room name
            if self._kit[room][0] == "":
                room_mentioned_at_id = [m.start() for m in re.finditer(room.lower(), text)]
                distance = 30 # Just give up if it's too far away
                # Find the overall smallest distance for the candidate name
                for rmad in room_mentioned_at_id:
                    closest = self._findClosestNumber(rmad, [k for k, v in meeting_indices.items()])
                    diff = abs(rmad - closest)
                    if diff < distance:
                        distance = diff
                        self._kit[room][0] = meeting_indices[closest]

                # Prevent rooms from being assigned twice
                delete_list = [k for k, v in meeting_indices.items() if v == self._kit[room][0]]
                for key in delete_list:
                    meeting_indices.pop(key)
                
                # If we matched what we could, abort here
                if len(meeting_indices.keys()) == 0:
                    break


    def getSuggestion(self):
        """
        Calculates the suggestion that ATLAS will make
        """
        most_attendees = 0
        result = []
        for k in self._kit.keys():
            if self._kit[k][1] == most_attendees:
                result.append(k)
            elif self._kit[k][1] > most_attendees:
                result = [k]
                most_attendees = self._kit[k][1]
        return result

    def getConsensus(self):
        """
        Returns the infered consensus of the players
        """
        return self._consensus, self._consensus_weigth

    def summarizeResults(self):
        """
        Summarizes the result of the 
        """
        results = {}
        results["rooms"] = list(set(self._rooms))
        results["room_frequency"] = {}
        
        for r in self._rooms:
            if r not in results["room_frequency"]:
                results["room_frequency"][r] = 0
            results["room_frequency"][r] += 1

        results["meetings"] = self._meetings
        results["attendees"] = self._attendee_matches
        results["suggestion"] = self._suggestion
        results["result"] = self._kit
        results["consensus"] = self._consensus
        results["consensus_weights"] = self._consensus_weigth
        results["discussed"] = self._discussed
        return results