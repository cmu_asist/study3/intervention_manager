# -*- coding: utf-8 -*-
"""
.. module:: scheduler
   :platform: Linux, Windows, OSX
   :synopsis: A class for scheduling callbacks on Minecraft mission time.

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

This file provides a class suitable for scheduling callbacks to
synchronize with the Minecraft mission timer.
"""

from MinecraftBridge.messages import PlayerState
from MinecraftBridge.utils import BaseScheduler



class InterventionScheduler(BaseScheduler):
    """
    A Scheduler that receives mission time by attaching itself to an
    Intervention or Followup instance, and registering to receive
    PlayerState messages.

    Attributes
    ----------
    time : float
        Current mission time, in seconds

    Methods
    -------
    schedule(callback, time)
        Schedule a callback to be called at a particular timestamp
    wait(callback, delay)
        Schedule a callback to be called after a specified delay
    repeat(callback, interval)
        Schedule a callback to be called at regular intervals
    pause()
        Pause the scheduler
    resume()
        Resume the scheduler
    reset()
        Reset the mission time and clear any pending events
    receive(message)
        Update the scheduler from a MinecraftBridge message
    """

    # MinecraftBridge message types to register with
    MESSAGE_CLASSES = [PlayerState]


    def __init__(self, base_component, blocking=True):
        """
        Arguments:
            - base_component: a BaseComponent instance
            - blocking: boolean indicating whether scheduled callbacks should block
                other callbacks, as opposed to executing in a background thread
        """
        BaseScheduler.__init__(self, blocking=blocking)

        # Register to receive messages through the component
        for MessageClass in self.__class__.MESSAGE_CLASSES:
            base_component.add_minecraft_callback(MessageClass, self.receive)
