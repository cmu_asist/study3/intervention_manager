# -*- coding: utf-8 -*-
"""
.. module:: victims_monitor
   :platform: Linux, Windows, OSX
   :synopsis: Utility class for monitoring marker block types and locations

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a component to monitor marker block types and locations, which
can be used by Interventions / Triggers / Followups to be able to store 
quasi-global information about victims.
"""

from MinecraftBridge.utils import Loggable

from MinecraftBridge.messages import (
    MarkerDestroyedEvent,
    MarkerPlacedEvent,
    MarkerRemovedEvent
)

from collections import defaultdict



class MarkerBlocksMonitor(Loggable):
    """
    A simple class to track the location and status of victims.  

    Attributes
    ----------
    marker_blocks : dictionary
        Dictionary mapping (x,y,z) location to marker type
    evidence : dictionary
        Dictionary mapping (x,y,z) to list of evidence (messages)
    """


    def __init__(self, parent_component):

        self._parent_component = parent_component

        # Mapping of victim location to Victim instance
        self._marker_blocks = defaultdict(self.__new_marker)
        self._evidence = defaultdict(list)

        if self._parent_component is None:
            self.logger.warning("%s:  No parent component provided", self)
            return

        # Register callbacks to receive information regarding victims
        self._parent_component.add_minecraft_callback(MarkerDestroyedEvent, self.__onMarkerDestroyed)
        self._parent_component.add_minecraft_callback(MarkerPlacedEvent, self.__onMarkerPlaced)
        self._parent_component.add_minecraft_callback(MarkerRemovedEvent, self.__onMarkerDestroyed)
    

    def __str__(self):
        """
        String representation of this
        """

        return self.__class__.__name__


    def __new_marker(self):
        """
        Create a new Marker instance with `None` values for the attributes
        """

        self.logger.debug("%s:  Creating new marker", self)

        return "<UNKNOWN>"


    @property
    def marker_blocks(self):
        return self._marker_blocks


    def __onMarkerDestroyed(self, message):
        """
        Callback when a marker is destroyed.

        Arguments
        ---------
        message : MinecraftBridge.message.MarkerDestroyedEvent or
                  MinecraftBridge.message.MarkerRemovedEvent
        """

        self.logger.debug("%s:  Received MarkerDestroyed message:", self)
        self.logger.debug("%s:    location: %s", self, message.location)
        self.logger.debug("%s:    type: %s (%s)", self, message.type, message.type.__class__)

        if message.location not in self._marker_blocks:
            self.logger.warning("%s:  Removing / destroying unknown marker @ %s", self, message.location)
            return

        del self._marker_blocks[message.location]

        self._evidence[message.location].append(message)


    def __onMarkerPlaced(self, message):
        """
        Callback when a marker is placed.

        Arguments
        ---------
        message : MinecraftBridge.message.MarkerPlacedEvent
        """

        self.logger.debug("%s:  Received MarkerDestroyed message:", self)
        self.logger.debug("%s:    location: %s", self, message.location)
        self.logger.debug("%s:    type: %s (%s)", self, message.type, message.type.__class__)

        self._marker_blocks[message.location] = message.type

        self._evidence[message.location].append(message)

