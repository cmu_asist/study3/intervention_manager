# -*- coding: utf-8 -*-
"""
.. module:: followup.medic_forgot_to_place_markerblock_followup
    :platform: Linux, Windows, OSX
    :synopsis: This followups repeates the given intervention
.. moduleauthor:: Simon Stepputtis <sstepput@andrew.cmu.edu>

Creates a new MedicForgottoPlaceMarkerBlockIntervention with slightly increased tollerances in order to remind
the player again that no marker block was placed.
"""

from InterventionManager.followups import Followup
from InterventionManager.library.medic_forgot_to_place_markerblock import MedicForgottoPlaceMarkerBlockIntervention

"""
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Note: This follow-up might be depricated
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
"""

class MedicForgottoPlaceMarkerBlockFollowup(Followup):
    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the Intervention Manager that the presenter is
            associated with
        """
        super(MedicForgottoPlaceMarkerBlockFollowup, self).__init__(intervention, manager)
        
    def _onInitiated(self):
        """
        Callback from the InterventionManager when the Followup is Initiated.
        Changes the state of the followup and informs the manager to activate
        it.
        """
        super(MedicForgottoPlaceMarkerBlockFollowup, self)._onInitiated()


    def _onActive(self):
        """
        Callback from the InterventionManager when the Followup is made active
        """
        # In order to not duplicate logic, we just re-create the intervention
        # here and immediately queue the followp for completion, as the 
        # proper observation about whether or not a marker block is placed is
        # already present in the respective intervention

        self.logger.info("{}: Initiating followup for potentially forgotten markerblock for victim(id:{}) at {} to player {}".format(
            self, self.intervention.victim_id, self.intervention.victim_location, self.intervention.playername))
        new_intervetnion = MedicForgottoPlaceMarkerBlockIntervention(self.manager, 
                                                                self.intervention.playername,
                                                                self.intervention.participant_id,
                                                                self.intervention.type,
                                                                self.intervention.victim_id,
                                                                self.intervention._victim_location,
                                                                self.intervention.elapsed_milliseconds)
        # Increase the distance by two blocks. This will delay the intervention triggering
        # until the player moves further away
        new_intervetnion.player_victim_distance_threshold += 2

        # At some arbitrary point (here a distance of 10), the followup will give up
        if new_intervetnion.player_victim_distance_threshold < 10:
            self.manager.spawn(new_intervetnion)
        self.manager._completeFollowup(self)


    def _onComplete(self):
        """
        Callback from the InterventionManager when the Followup is completed
        """
        pass
    
 