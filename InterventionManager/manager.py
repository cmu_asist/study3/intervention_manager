# -*- coding: utf-8 -*-
"""
.. module:: interventions.manager
    :platform: Linux, Windows, OSX
    :synopsis: Definition of the top-level InterventionManager class
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Defines a top-level InterventionManager class.  The InterventionManager handles
the lifecycle of Intervention instances, delegation of Intervention creation to
one or more Trigger instances, delegation of Intervention resolution to one or
more delegates, and handgles connection to various data streams.
"""

from MinecraftBridge.messages import Trial
from MinecraftBridge.utils import Loggable

from MinecraftBridge.messages import AgentChatIntervention
from MinecraftBridge.messages import BusHeader, MessageHeader
from MinecraftBridge.mqtt.parsers import MessageType, MessageSubtype

from BaseAgent.utils import DependencyInjection
from BaseAgent.components import ParticipantCollection

from collections import defaultdict

from .components.intervention_statistics_thread import InterventionStatisticsThread
from .participant import InterventionParticipant

from .components.m8_reporter import M8_Reporter
from .components.m9_reporter import M9_Reporter


class InterventionManager(Loggable):
    """

    The InterventionManager stores instances of bridges to two message streams:

    * minecraft_bridge: Connection to the bus handling messages from the 
                        testbed
    * redis_bridge:     Internal message bus handling 

    The InterventionManager maintains a set of collections of Interventions 
    (Intervention queues), which store interventions based on their state.
    The semantics of each queue are:

    * _initialization_queue: Interventions that have been created, but are not
                             yet connected to message streams
    * _active_queue:         Interventions that have been triggered and are 
                             connected to the message stream(s)
    * _response_queue:       Interventions that have been triggered and are in
                             a completed state, and are awaiting response (e.g.,
                             presenting the intervention to the participant).
                             These interventions are no longer connected to the
                             message streams.
    * _followup_queue:       Interventions that have been resolved can
                             optionally result in a followup being spawned.
    * _discard_queue:        Interventions that are awaiting disposal.



    Attributes
    ----------
    minecraft_bridge : MinecraftBridge.Bridge
        Instance of the MinecraftBridge for communication to the testbed and
        other components
    redis_bridge : RedisBridge
        Instance of the internal Redis bridge for communication with internal
        components
    trial_info : dictionary
        Dictionary containing the current experiment_id, trial_id, and
        replay_id of the running trial
    participants : MinecraftBridge.ParticipantCollection
        Collection of the current participants in the trial

    
    Methods
    -------
    addTrigger(trigger)
        Adds an Intervention trigger, automatically connecting it to the buses
    addResolution(resolution, intervention_class)
        Adds a resolution to perform a particular action for interventions of a
        given class when queued for resolution.
    reset()
        Disconnects and removes all interventions and followups, preparing the
        manager for a new trial.
    spawn(intervention)
        Method used to register a newly created intervention to be managed by
        the manager.
    initiateFollowup(followup)
        Method used to register a followup to be managed by the manager.


    Private Callback Methods
    ------------------------
    _activate(intervention)
        Callback from an Intervention when it enters the active state.
    _queueForResolution(intervention)
        Callback from an Intervention when it enters a terminal state and 
        requires a response / presentation.
    _resolve(intervention)
        Callback from an Intervention once it has been resolved
    _discard(intervention)
        Callback from an Intervention when it is ready to be discarded
    _initiateFollowup(followup)
        Callback from a Followup instance when initially created.  Used ty the
        Followup instance to register themselves for management.
    _activateFollowup(followup)
        Callback from a Followup when it becomes active
    _completeFollowup(followup)
        Callback from a Followup when it is completed
    """

    def __init__(self, context, config, **kwargs):
        """
        Arguments
        ---------
        context : MissionContext
            context containing bridge interfaces and mission information
        config : dictionary
            Configuration dictionary of the intervention manager
        """

        # Name used for representation and logging
        if not "agent_name" in kwargs:
            self.logger.warning("%s:  No agent name provided in keyword arguments", self.__class__.__name__)

        self._agent_name = kwargs.get("agent_name", "ASI_CMU_TA1_ATLAS")
        self._string_name = "[%s (%s)]" % (self._agent_name, self.__class__.__name__)
        self.logger.debug("%s:  Initializing Manager", self._string_name)


        # Hang on to the bridges and config dictionary
        self._minecraft_bridge = context.minecraft_interface
        self._redis_bridge = context.redis_interface
        self._config = config

        # Mission context information
        self._participants = context.participants
        self._trial_info = context.trial_info
        self._semantic_map = context.semantic_map
        self._mission_clock = context.mission_clock

        # TO TEST:  Create a new set of participants & participant collection,
        #           to decorate the participants with characteristics and ToM
        #           model (read-only version) of the participant
        self._decorated_partcipants = ParticipantCollection()
        for participant in context.participants:
            self._decorated_partcipants.add(InterventionParticipant(participant))


        # The manager will maintain a set of collections of Interventions, and
        # update these based on each Intervention's change of state
        # NOTE: The _discarded_intervention queue 
        # NOTE: Since interventions *could* have priority, should priority
        #       queues be used instead of sets?
        self._initialization_queue = set()
        self._active_queue = set()
        self._response_queue = set()
        self._discard_queue = set()

        # The manager also maintains a set of Followups, and updates these
        # based on the state of the Followup
        self._initiated_followup_queue = set()
        self._active_followup_queue = set()
        self._completed_followup_queue = set()

        # The manager maintains a set of triggers
        self._triggers = set()

        # The resolution map provides a mapping from Intervention classes to a
        # list of resolutions to perform.  Similarly, the followup completion 
        # map provides a mapping from Followup classes to a list of classes to
        # handle what to do with followups when complete
        self._resolution_map = {}
        self._follow_through_map = {}

        # Start the thread to report intervention statistics, passing this
        # manager so that the thread has access
        self._intervention_statistics_thread = InterventionStatisticsThread(
            self,
            context.minecraft_interface,
            publish_interval=config.get("intervention_statistics_rate", 30),
            agent_name=self._agent_name,
        )
        if kwargs.get("run_intervention_statistics", True):
            self._intervention_statistics_thread.start()

        # Create reporters for M8 & M9
        self.measures = [M8_Reporter(self), M9_Reporter(self)]


        # Observers can register themselves to receive callbacks whenever an
        # intervention changes state
        self.__observers = set()

        # Load triggers and resolutions
###        self.__load_triggers_from_json(self._config.get("triggers", []))
###        self.__load_resolutions_from_json(self._config.get("resolvers", []))
###        self.__load_follow_throughs_from_json(self._config.get("follow_throughs", []))

        # Load in any add-ons, which connects to the manager by registering
        # as observers
        addons = []
        for addon_config in self._config.get("addons", []):
            addon = DependencyInjection.create_instance(addon_config["addon_class"],
                                                        self, **addon_config.get("parameters", {}))
            self.register_observer(addon)
            addons.append(addon)


    @property
    def minecraft_bridge(self):
        """
        Get the Minecraft bridge that the manager is connected to.

        Returns None if the agent does not exist.
        """

        return self._minecraft_bridge



    @property
    def redis_bridge(self):
        """
        Get the internal agent bridge that the manager is connected to

        Returns None if the agent does not exist
        """

        return self._redis_bridge


    @property
    def minecraft_interface(self):
        """
        Alias of `minecraft_bridge`
        """

        return self._minecraft_bridge



    @property
    def redis_interface(self):
        """
        Alias of `redis_bridge`
        """

        return self._redis_bridge


    @property
    def trial_info(self):
        """
        Pass the agent's trial_info attribute through, if the agent exists

        Returns None if the agent does not exist
        """

        return self._trial_info


    @property
    def participants(self):
        """
        Pass the agent's participants collection through, if the agent exists.

        Returns None if the agent does not exist
        """

        return self._participants


    @property
    def semantic_map(self):
        """
        Pass the agent's semantic map through, if the agent exists.

        Returns None if the agent does not exist
        """

        return self._semantic_map


    @property
    def mission_clock(self):
        return self._mission_clock
    


    def __str__(self):
        """
        Return a string representation of the manager.

        Returns
        -------
        string
            String representation of the intervention manager
        """

        return self._string_name


    def request_redis(self, *args, **kwargs):
        """
        Make a request through the Redis interface.
        """
        return self.redis_interface.request(*args, **kwargs)


    def publish_measure(self, message):
        """
        Publish an agent measure message

        Arguments
        ---------
        message : MinecraftBridge.messages.AgentMeasure
            Message to publish.  Does not necessarily need to include any
            header information
        """

        # Check to see if header information is present in the message, and add
        # add if not.
        if not "header" in message.headers:
            message.addHeader("header", BusHeader(MessageType.agent))
        if not "msg" in message.headers:
            message.addHeader("msg", MessageHeader(MessageSubtype.measures,
                                                   self.trial_info.get('experiment_id', 'UNKNOWN'),
                                                   self.trial_info.get('trial_id', 'UNKNOWN'),
                                                   self._agent_name,
                                                   replay_id = self.trial_info.get('replay_id', None)))

        self.minecraft_bridge.publish(message)


    def publish_chat_intervention(self, message, recipients, **kwargs):
        """
        Creates a ChatIntervention message and publishes to the message bus.

        Arguments
        ---------
        message : string
            Message to publish
        recipients : list
            List of participant identifiers, which can be participant instances,
            participant_id, playername, callsign, or role.  Ideally, this should
            be participant_id.

        Keyword Arguments
        -----------------
        start : integer, default=-1
            Time when the message should be presented, in elapsed_milliseconds
        duration : integer, default=10000
            Duration (in milliseconds) that the message should be shown for
        explanation : dictionary, default={}
            Additional explanatory data to be published with the message
        """

        # Get the participant_ids of the recipients.  
        _recipients = []

        for recipient in recipients:
            participant = self.participants[recipient]
            if participant is not None:
                _recipients.append(participant.participant_id)
            else:
                self.logger.warning("%s: Attempting to include unknown recipient in Chat Intervention: %s", self, recipient)

        explanation = kwargs.get("explanation", {})
        explanation["info"] = kwargs.get("intervention_info", {})

        # Create the chat message
        intervention_message = AgentChatIntervention(content=message,
                                                     source=self._agent_name,
                                                     start=kwargs.get("start", -1),
                                                     duration=kwargs.get("duration", 10000),
                                                     explanation=kwargs.get("explanation", explanation),
                                                     receivers=_recipients,
                                                     type="string",
                                                     renderers=["Minecraft_Chat"])

        intervention_message.addHeader("header", BusHeader(MessageType.agent))
        intervention_message.addHeader("msg", MessageHeader(MessageSubtype.Intervention_Chat,
                                                            self.trial_info.get('experiment_id', 'UNKNOWN'),
                                                            self.trial_info.get('trial_id', 'UNKNOWN'),
                                                            self._agent_name,
                                                            replay_id = self.trial_info.get('replay_id', None)))

        self.logger.debug("%s:  Publishing Chat Intervention: %s", self, intervention_message)
        self.minecraft_bridge.publish(intervention_message)



    def reset(self, **kwargs):
        """
        Clear out state of the manager in preparation for a new trial. Performs
        the following:

            1.  Disconnects active interventions from the Minecraft and Redis
                buses.
            2.  Disconnects active followups from the Minecraft and Redis
                buses.
            3.  Clears out the intervention and followup queues
        """

        self.logger.info("%s:  Resetting Intervention Manager", self)

        # Disconnect active interventions and followups from the buses
        for intervention in self._active_queue:
            self.__deregisterComponent(intervention)

        for followup in self._active_followup_queue:
            self.__deregisterComponent(followup)

        # Clear out all the queues
        self._initialization_queue.clear()
        self._active_queue.clear()
        self._response_queue.clear()
        self._discard_queue.clear()

        self._initiated_followup_queue.clear()
        self._active_followup_queue.clear()
        self._completed_followup_queue.clear()


        # HACK: Re-create the triggers, resolvers, and follow-throughs

        # Disconnect old triggers
        for trigger in self._triggers:
            self.__deregisterComponent(trigger)

        self._triggers.clear()
        self._resolution_map.clear()
        self._follow_through_map.clear()

        self.__load_triggers_from_json(self._config.get("triggers", []))
        self.__load_resolutions_from_json(self._config.get("resolvers", []))
        self.__load_follow_throughs_from_json(self._config.get("follow_throughs", []))

        # Let the observers know that the manager has been reset
        for observer in self.__observers:
            observer.onInterventionManagerReset(self)


    def register_observer(self, observer):
        """
        Add a component to receive callbacks whenever an intervention
        changes state

        Arguments
        ---------
        observer : object
            Component to receive state change callbacks
        """

        self.logger.info("%s:  Adding observer: %s", self, observer)

        if observer in self.__observers:
            self.logger.warning("%s:  Attemping to add already registered observer: %s", self, observer)
            return

        self.__observers.add(observer)


    def deregister_observer(self, observer):
        """
        Remove an observing component

        Arguments
        ---------
        observer : object
            Component to remove as an observer
        """

        self.logger.info("%s:  Removing observer: %s", self, observer)

        if observer not in self.__observers:
            self.logger.warning("%s:  Attempting to remove non-registered observer: %s", self, observer)
            return

        self.__observers.remove(observer)


    def __notify_of_intervention(self, intervention):
        """
        Notify the observers of the state change of the intervention

        Arguments
        ---------
        intervention : Intervention
            Intervention whose state has changed
        """

        for observer in self.__observers:
            observer.onInterventionStateChange(intervention)


    def __notify_of_followup(self, followup):
        """
        Notify the observers of the state change of the followup

        Arguments
        ---------
        followup : Followup
            Followup whose state has changed
        """

        for observer in self.__observers:
            observer.onFollowupStateChange(followup)


    def __registerComponent(self, component):
        """
        Register a component with the data bridges.

        Arguments
        ---------
        component : BaseComponent
        """

        # Connect the component to MinecraftBridge
        for message_class, callback in component._minecraft_callbacks:
            # Use a lower priority than the ToM model to ensure that the
            # component receives messages after the ToM has been updated
            self.logger.debug(f'{self}: Registering {component} to receive {message_class} messages')
            self._minecraft_bridge.register_callback(message_class, callback, priority=3)

        # Connect the component to the RedisBridge
        for callback, channel, message_type in component._redis_callbacks:
            self.logger.debug(f'{self}: Registering {component} to receive {channel} internal messages')
            self.redis_bridge.register_callback(callback, channel, message_type)


    def __deregisterComponent(self, component):
        """
        Deregister a component with the data bridges.

        Arguments
        ---------
        component : BaseComponent
        """

        # Disconnect the component from the MinecraftBridge
        if self._minecraft_bridge is not None:
            for message_class, callback in component._minecraft_callbacks:
                self._minecraft_bridge.deregister_callback(callback)

        # Disconnect the component from the RedisBridge
        if self.redis_bridge is not None:
            for callback, channel, message_type in component._redis_callbacks:
                self.redis_bridge.deregister_callback(callback)


    def addTrigger(self, trigger):
        """
        Add an Intervention trigger, and connect it to the data bridges

        Arguments
        ---------
        trigger : InterventionTrigger
            Trigger to add to the intervention manager
        """

        self._triggers.add(trigger)

        self.__registerComponent(trigger)
        self.logger.debug("%s: Added Trigger: %s", self, trigger)



    def addResolution(self, resolution, intervention_class):
        """
        Add a resolution to perform for the given intervention class.  The
        resolution will be queued to be performed _after_ all previously added
        resolutions.

        The `resolution` object must implement a `resolve` method, and accept
        an instance of an intervention as an argument.

        Arguments
        ---------
        resolution : resolution class
            The resolution to perform for the given intervention class
        intervention_class : Intervention class
            The class of intervention that the resolution is performed on
        """

        # TODO: Verify that resolution has a `resolve` method that takes a
        #       single argument

        # Start a list of resolutions for the given intervention class if it
        # doesn't exist
        if not intervention_class in self._resolution_map:
            self._resolution_map[intervention_class] = []

        # Add the resolution to the map for the class
        self._resolution_map[intervention_class].append(resolution)


    def addFollowThrough(self, follow_through, followup_class):
        """
        Add a class to handle followups onces they are completed.  The follow
        through classes will be performed in the order they are added for a
        given followup class.

        The `follow_through` object must implement an `onFollowupComplete`
        method, and accept an instance of a followup as an argument.

        Arguments
        ---------
        follow_through : follow through class
            The object to call when followups are completed
        followup_class : Followup class
            The class of followups that the completion object handles
        """

        if not followup_class in self._follow_through_map:
            self._follow_through_map[followup_class] = []

        # Add the follow through object to the map for the class
        self._follow_through_map[followup_class].append(follow_through)



    def spawn(self, intervention):
        """
        Callback from an Intervention when it is created.  Adds an intervention
        to the _initialization_queue.

        Arguments
        ---------
        intervention : Intervention
            Instance of the created intervention.
        """

        self._initialization_queue.add(intervention)

        intervention._setStateEntryTime(self.mission_clock.elapsed_milliseconds,
                                        self.mission_clock.mission_timer)

        self.logger.debug("%s:  Spawned Intervention: %s", self, intervention)

        # Notify observers that the intervention was spawned
        self.__notify_of_intervention(intervention)

        intervention._onSpawn()


    def _activate(self, intervention):
        """
        Callback from an Intervention when it enters the active state.  Moves
        the intervention to the _active_queue.

        Arguments
        ---------
        intervention : Intervention
            Instance of the intervention that has been made active.
        """

        # Check to make sure the intervention is in the initialization or 
        # response queue, and log a warning if it is not
        if intervention in self._initialization_queue:
            self._initialization_queue.remove(intervention)
        elif intervention in self._response_queue:
            self._response_queue.remove(intervention)
        else:
            self.logger.warning("%s: Activated intervention (%s) not in Initialization or Repsonse Queue", self, intervention)

        # Put the intervention in the active queue
        self._active_queue.add(intervention)

        intervention._setStateEntryTime(self.mission_clock.elapsed_milliseconds,
                                        self.mission_clock.mission_timer)        

        # Connect the intervention to the bridges
        # NOTE:  The spawn - activate sequence currently results in two of the
        #        same info messages being generated.
        self.__registerComponent(intervention)
        self.logger.debug("%s: Activating Intervention: %s", self, intervention)

        # Notify the observers that the intervetion was activated
        self.__notify_of_intervention(intervention)

        intervention._onActivated()


    def _queueForResolution(self, intervention):
        """
        Callback from an Intervention when it has entered a terminal state and
        requires a response / presentation.  Moves the intervention to the
        _response_queue

        Arguments
        ---------
        intervention : Intervention
            Instance of the intervention that has completed
        """

        # Check to make sure the intervention is either in the initialization 
        # or active queue, and log a warning if it is not in either
        if intervention in self._initialization_queue:
            self._initialization_queue.remove(intervention)
        elif intervention in self._active_queue:
            self._active_queue.remove(intervention)
        else:
            self.logger.warning("%s: Completed intervention (%s) not in Initialization or Active Queue", self, intervention)

        # Put the intervention in the response queue
        self._response_queue.add(intervention)

        intervention._setStateEntryTime(self.mission_clock.elapsed_milliseconds,
                                        self.mission_clock.mission_timer)        

        # Disconnect the intervention from the bus(es)
        self.__deregisterComponent(intervention)

        self.logger.debug("%s: Queuing Intervention for response: %s", self, intervention)

        intervention._onQueuedForResolution()

        # Notify observers that the intervention is queued for resolution
        self.__notify_of_intervention(intervention)

        intervention.resolve()


    def _resolve(self, intervention, severity=None):
        """
        Callback from an Intervention when it has been resolved.
        """

        self.__deregisterComponent(intervention)    

        # Perform all the resolutions on the intervention in the resolution map
        for resolution in self._resolution_map.get(intervention.__class__, []):
            resolution.resolve(intervention)

        # Now update the state entry time to indicate that the resolutions are
        # complete
        intervention._setStateEntryTime(self.mission_clock.elapsed_milliseconds,
                                        self.mission_clock.mission_timer)            

        # Notify observers that the intervention is resolved
        self.__notify_of_intervention(intervention)

        self.logger.debug("%s: Intervention resolved: %s", self, intervention)

        intervention._onResolved()


    def _discard(self, intervention):
        """
        Callback from an Intervention when it is ready to be discarded

        Arguments
        ---------
        intervention : Intervention
            Instance of the intervention to discard
        """

        self._discard_queue.add(intervention)

        intervention._setStateEntryTime(self.mission_clock.elapsed_milliseconds,
                                        self.mission_clock.mission_timer)        

        # Disconnect the intervention from the bus(es)
        self.__deregisterComponent(intervention)

        self.logger.debug("%s: Discarding Intervention: %s", self, intervention)

        # Notify observers that the intervention is discarded
        self.__notify_of_intervention(intervention)

        intervention._onDiscard()


    def initiateFollowup(self, followup):
        """
        Callback from a Followup when it is initially created.  Adds the
        followup to the _initiated_followup_queue

        Arguments
        ---------
        followup : Followup
            Followup that has been initated
        """

        self._initiated_followup_queue.add(followup)

        self.logger.debug("%s:  Initiaged Followup: %s", self, followup)

        followup._setStateEntryTime(self.mission_clock.elapsed_milliseconds,
                                    self.mission_clock.mission_timer)        

        # Notify observers of the followup being initiated
        self.__notify_of_followup(followup)

        followup._onInitiated()


    def _activateFollowup(self, followup):
        """
        Callback from a Followup when it becomes active.  Moves the followup
        from the initiated_followup_queue to the active_followup_queue

        Arguments
        ---------
        followup : Followup
        """

        # Check to make sure the followup is in the initated_followup queue,
        # and log a warning if it is not
        if followup in self._initiated_followup_queue:
            self._initiated_followup_queue.remove(followup)
        else:
            self.logger.warning("%s: Activated followup (%s) not in Initiated Queue", self, followup)

        # Put the followup in the active queue
        self._active_followup_queue.add(followup)

        followup._setStateEntryTime(self.mission_clock.elapsed_milliseconds,
                                    self.mission_clock.mission_timer)         

        # Connect the followup to the bridges
        self.__registerComponent(followup)
        self.logger.debug("%s: Activating Followup: %s", self, followup)

        # Notify observers of the followup being activated
        self.__notify_of_followup(followup)

        followup._onActive()


    def _completeFollowup(self, followup):
        """
        Callback from a Followup when it is completed.  Moves the followup
        from the active_followup_queue to the completed_followup_queue.  The
        followup is disconnected from the bus(es).

        Arguments
        ---------
        followup : Followup
            Completed followup
        """

        # Check to make sure the followup is in the active queue, and log a 
        # warning if not
        if followup in self._active_followup_queue:
            self._active_followup_queue.remove(followup)
        else:
            self.logger.warning("%s: Completed Followup (%s) not in Active Queue", self, followup)

        # Perform all the follow_throughs on the followup in the follow_through map
        for follow_through in self._follow_through_map.get(followup.__class__, []):
            follow_through.onFollowupComplete(followup)

        # Put the followup in the completed followup queue
        self._completed_followup_queue.add(followup)

        followup._setStateEntryTime(self.mission_clock.elapsed_milliseconds,
                                    self.mission_clock.mission_timer)         

        # Disconnect the followup from the bus(es)
        self.__deregisterComponent(followup)

        self.logger.debug("%s: Followup complete: %s", self, followup)

        # Notify observers of the completion of the followup
        self.__notify_of_followup(followup)

        followup._onComplete()



    def __load_resolutions_from_json(self, resolvers):
        """
        Configures the resolution pipeline for each resolution type listed in
        the configuration.  Each item in the resolvers list should contain a 
        dictionary with the following keys

        * resolution_class: namespaced import path to the resolution class
        * parameters:       object containing the arguments passed to the 
                            resolver class, as a set of keyword arguments
        * handles:          list of namespaced import paths to the classes that
                            the resolver is intended to handle

        Arguments
        ---------
        resolvers : list of dictionaries
            List of configurations for each resolver to create and add
        """

        self.logger.info("%s:  Adding Resolutions to Intervention Manager:", self)

        # Create and add each resolver in the provided list
        for config in resolvers:

            try:
                resolver = DependencyInjection.create_instance(config["resolution_class"], self,
                                                               **config["parameters"])

                self.logger.info("%s:   %s", self, resolver.__class__.__name__)
                self.logger.info("%s:   Handles Interventions:", self)

                # Add the resolver to the manager for each intervention class listed 
                for intervention_class in config["handles"]:
                    _, InterventionClass = DependencyInjection.get_module_and_class(intervention_class)
                    self.addResolution(resolver, InterventionClass)

                    self.logger.info("%s:      %s", self, InterventionClass.__name__)

            except Exception as e:

                self.logger.warning("%s:    -= Exception raised when attempting to add resolver: =-", self)
                self.logger.warning("%s:    %s", self, str(e))



    def __load_triggers_from_json(self, triggers):
        """
        Configures the pipeline for each intervention trigger in the provided
        list.  Each item in the triggers list should contain a dictionary with
        the following keys

        * trigger_class: namespaced import path to the class that triggers the
                         intervention
        * parameters:    object containing the arguments passed to the trigger
                         class initialization method, as a set of keyword 
                         arguments.

        Arguments
        ---------
        triggers : list of dictionaries
            List of configurations for each trigger to load
        """

        self.logger.info("%s:  Adding Triggers to Intervention Manager:", self)

        # Create and add each trigger in the intervention config
        for config in triggers:

            # try-catch in case there's some exception raised
            try:
                trigger = DependencyInjection.create_instance(config["trigger_class"], self,
                                                              **config.get("parameters", {}))
                self.addTrigger(trigger)

                self.logger.info("%s:    %s", self, trigger.__class__.__name__)

            except Exception as e:

                self.logger.warning("%s:    -= Exception raised when attempting to add trigger: =-", self)
                self.logger.warning("%s:    %s", self, str(e))


    def __load_follow_throughs_from_json(self, follow_throughs):
        """
        Configures the follow-through pipeline for each follow-through type
        listed in the configuration.  Each item in the completions list should
        contain a dictionary with the following keys

        * follow_through_class: namespaced import path to the follow through class
        * parameters:           object containing the arguments passed to the 
                                follow through class, as a set of keyword arguments
        * handles:              list of namespaced import paths to the classes that
                                the follow through is intended to handle

        Arguments
        ---------
        completions : list of dictionaries
            List of configurations for each follo _through to create and add
        """

        self.logger.info("%s:  Adding Followup Completion to Intervention Manager:", self)

        # Create and add each followup completion in the provided list
        for config in follow_throughs:

            try:
                follow_through = DependencyInjection.create_instance(config["follow_through_class"],
                                                                 self,
                                                                 **config["parameters"])

                self.logger.info("%s:   %s", self, follow_through.__class__.__name__)
                self.logger.info("%s:   Handles Followups:", self)

                # Add the completion to the manager for each followup class listed 
                for followup_class in config["handles"]:
                    _, FollowupClass = DependencyInjection.get_module_and_class(followup_class)
                    self.addFollowThrough(follow_through, FollowupClass)

                    self.logger.info("%s:      %s", self, FollowupClass.__name__)

            except Exception as e:

                self.logger.warning("%s:    -= Exception raised when attempting to add followup completion: =-", self)
                self.logger.warning("%s:    %s", self, str(e))