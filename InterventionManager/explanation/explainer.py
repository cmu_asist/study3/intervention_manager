# -*- coding: utf-8 -*-
"""
.. module:: explainer
    :platform: Linux, Windows, OSX
    :synopsis: Definition of the top-level InterventionManager class
.. moduleauthor:: Ying Chen <yingc4@andrew.cmu.edu>

Class definition for generating templated explanations for interventions.
"""

class Explainer:
    def __init__(self, expl_config):
        self.params = expl_config["params"]
        self.intervention_template = expl_config["intervention"]
        self.short_explanation_template = expl_config["short_explanation"]
    
    def fill_in_slots(self, text, intervention):
        # TODO: fill in slots in the text using the parameters of the intervention
        return text

    def get_intervention(self, intervention):
        return self.fill_in_slots(self.intervention_template, intervention)

    def get_short_explanation(self, intervention):
        return self.fill_in_slots(self.short_explanation_template, intervention)