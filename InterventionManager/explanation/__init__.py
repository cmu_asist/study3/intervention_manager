# -*- coding: utf-8 -*-
"""
.. module:: explanation
    :platform: Linux, Windows, OSX
    :synopsis: Definition of the top-level InterventionManager class
.. moduleauthor:: Ying Chen <yingc4@andrew.cmu.edu>

Module defining an Explainer class and ExplainerFactory.  Explainer instances
are used to inject natural language explanations into interventions, using
attributes of the intervention to populate / produce explanations
"""

from .explainer import Explainer
from .explainerfactory import ExplainerFactory

__all__ = [ "Explainer", "ExplainerFactory" ]