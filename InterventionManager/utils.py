import math
import numpy as np

from MinecraftBridge.messages import TriageEvent, VictimEvacuated, VictimPickedUp, VictimPlaced
from MinecraftBridge.utils import Loggable
from MinecraftElements import MarkerBlock



COMPASS_ROSE = {
    0: 'E', 45: 'NE', 90: 'N', 135: 'NW',
    180: 'W', 225: 'SW', 270: 'S', 315: 'SE',
}

VICTIM_TO_MARKER_TYPES = {
    'victim_a': {mb for mb in MarkerBlock if 'abrasion' in mb.name},
    'victim_b': {mb for mb in MarkerBlock if 'bonedamage' in mb.name},
    'victim_c': {mb for mb in MarkerBlock if 'critical' in mb.name},
    'block_victim_1': {mb for mb in MarkerBlock if 'abrasion' in mb.name},
    'block_victim_1b': {mb for mb in MarkerBlock if 'bonedamage' in mb.name},
    'block_victim_2': {mb for mb in MarkerBlock if 'critical' in mb.name},
    'block_victim_proximity': {mb for mb in MarkerBlock if 'critical' in mb.name},
    'block_victim_regular': {mb for mb in MarkerBlock if 'regular' in mb.name},
    'Regular Victim Here': {mb for mb in MarkerBlock if 'regular' in mb.name},
    'Regular Victim Detected': {mb for mb in MarkerBlock if 'regular' in mb.name},
    'Critical Victim Here': {mb for mb in MarkerBlock if 'critical' in mb.name},
    'Critical Victim Detected': {mb for mb in MarkerBlock if 'critical' in mb.name},
    'No Victim Detected': {mb for mb in MarkerBlock if 'novictim' in mb.name},
}



def to_2D(v):
    """
    Convert to (x, z) coordinates.
    """
    return (v[0], v[2]) if len(v) == 3 else v[:2]


def angle_between(v1, v2, degrees=True):
    """
    Returns the angle between two vectors in Euclidean space.
    """
    unit_vector = lambda v: np.array(v) / np.linalg.norm(v)

    # Check for zero vectors
    if not np.any(v1) or not np.any(v2):
        return None

    v1, v2 = to_2D(v1), to_2D(v2)
    u1, u2 = unit_vector(v1), unit_vector(v2)
    angle = np.arccos(np.clip(np.dot(u1, u2), -1.0, 1.0))

    return np.degrees(angle) if degrees else angle


def compass_direction(direction_vector):
    """
    Return the ordinal compass direction corresponding to the given
    (x, z) direction vector.
    """
    x, z = direction_vector
    angle = np.degrees(np.arctan2(z, x))
    rounded_angle = 45 * round(angle / 45) % 360
    return COMPASS_ROSE[rounded_angle]


def distance(v1, v2):
    """
    Calculate the Euclidean distance between two locations.
    """
    v1, v2 = to_2D(v1), to_2D(v2)
    return math.sqrt(sum([(a - b)**2 for a, b in zip(v1, v2)]))


def neighbors(loc, include_diagonals=False):
    """
    Return the set of neighbors locations to a given location.
    """
    x, z = to_2D(loc)
    deltas = {(-1, 0), (1, 0), (0, -1), (0, 1)}
    if include_diagonals:
        deltas |= {(-1, -1), (-1, 1), (1, -1), (1, 1)}

    return {(x + dx, z + dz) for dx, dz in deltas}



class VictimMonitor(Loggable):
    """
    Object that monitors triaged / evacuated victims.
    """

    def __init__(self, component, **kwargs):
        """
        Arguments
        ---------
        component : BaseComponent
            The Intervention manager component to connect this monitor to

        Keyword Arguments
        -----------------
        on_evacuation : callable(msg), default=(lambda msg: None)
            Callback for when a victim is evacuated
        on_triage : callable(msg), default=(lambda msg: None)
            Callback for when a victim is triaged
        """

        # Set attributes
        self.evacuated_victim_ids = set()
        self.triaged_victim_ids = set()
        self.victim_locations = {} # indexed by victim ID
        self.victim_types = {} # indexed by victim ID

        # Set victim callbacks
        self._on_evacuation = kwargs.get('on_evacuation', lambda msg: None)
        self._on_triage = kwargs.get('on_triage', lambda msg: None)

        # Add Minecraft callbacks through component
        component.add_minecraft_callback(TriageEvent, self.__onTriageEvent)
        component.add_minecraft_callback(VictimEvacuated, self.__onVictimEvacuated)
        component.add_minecraft_callback(VictimPickedUp, self.__onVictimPickedUp)
        component.add_minecraft_callback(VictimPlaced, self.__onVictimPlaced)


    def copy(self, component, **kwargs):
        """
        Create a copy of this VictimMonitor connected to an arbitrary BaseComponent.
        """
        vm = VictimMonitor(component, **kwargs)
        vm.evacuated_victim_ids = self.evacuated_victim_ids
        vm.triaged_victim_ids = self.triaged_victim_ids
        vm.victim_locations = self.victim_locations
        vm.victim_types = self.victim_types
        return vm


    def triage_excess(self):
        """
        Return the number of triaged, unevacuated victims.
        """
        return len(self.triaged_victim_ids - self.evacuated_victim_ids)


    def evacuated_ratio(self):
        """
        Return the regular-to-critical ratio of evacuated victims.
        """
        R = len(set(filter(self._is_regular, self.evacuated_victim_ids)))
        C = len(set(filter(self._is_critical, self.evacuated_victim_ids)))
        return float('inf') if C == 0 else R / C


    def triaged_ratio(self):
        """
        Return the regular-to-critical ratio of triaged victims.
        """
        R = len(set(filter(self._is_regular, self.triaged_victim_ids)))
        C = len(set(filter(self._is_critical, self.triaged_victim_ids)))
        return float('inf') if C == 0 else R / C


    def _is_regular(self, victim_id):
        victim_type = self.victim_types[victim_id]
        return victim_type in {'victim_a', 'victim_b', 'victim_saved_a', 'victim_saved_b'}


    def _is_critical(self, victim_id):
        victim_type = self.victim_types[victim_id]
        return victim_type in {'victim_c', 'victim_saved_c'}


    def __onTriageEvent(self, message):
        if message.triage_state == TriageEvent.TriageState.SUCCESSFUL:
            self.victim_locations[message.victim_id] = message.victim_location 
            self.victim_types[message.victim_id] = message.type
            self.triaged_victim_ids.add(message.victim_id)
            self._on_triage(message)


    def __onVictimEvacuated(self, message):
        self.victim_locations[message.victim_id] = message.victim_location
        self.victim_types[message.victim_id] = message.type
        self.evacuated_victim_ids.add(message.victim_id)
        self._on_evacuation(message)


    def __onVictimPickedUp(self, message):
        self.victim_locations[message.victim_id] = None


    def __onVictimPlaced(self, message):
        self.victim_locations[message.victim_id] = message.location
