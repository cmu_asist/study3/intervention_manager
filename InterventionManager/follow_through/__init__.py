# -*- coding: utf-8 -*-
"""
.. module:: InterventionManager.follow_through
    :platform: Linux, Windows, OSX
    :synopsis: Definition of abstract follow through classes
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

"""

from .prediction_follow_through import PredictionFollowThrough