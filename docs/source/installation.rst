Installation
============

Requirements
------------

This package may be used with Python version 3.5 and later.  Installation is best performed using `pip`_.

This package requires version 1.0 or later of the `MinecraftBridge`_ package to be installed.


Installation using pip
----------------------

This module can be installed as a package using `pip`.  Assuming `pip` is installed, the package can be installed (for users) from the root folder by::

    pip install --user -e .

Once installed, the module can be imported from arbitrary locations.


Documentation
-------------
Documentation for this module can be built using `Sphinx`_, which is also installed with `pip`_.  The source and build files for the documentation is in the `docs` folder.


Requirements
~~~~~~~~~~~~
Note that build targets may require additional dependencies to be installed.  For building a PDF of the documentation using Latex, basic Latex dependencies need to be installed.  On Ubuntu-based systems, this can be done by installing the following packages using `aptitude`::

    apt-get install texlive-latex-recommended texlive-latex-extra texlive-fonts-recommended


Building Documentation
~~~~~~~~~~~~~~~~~~~~~~
To build a PDF of the *Intervention Manager Manual*, from the `docs` folder, run the build command with the latexpdf target::

    cd docs
    make latexpdf

This will create a `build` subfolder, and `latex` subfolder within build.  The final pdf file, `MinecraftBridge_Manual.pdf` will be written to the `latex` subfolder, which can be moved to the root directory easily.  In Latex or OSX, this is done with::

    mv build/latex/InterventionManager_Manual.pdf ..

While in Windows, this would be done by::

    move build\latex\InterventionManager_Manual.pdf ..



.. _pip: https://pip.pypa.io/en/stable/

.. _Sphinx: https://www.sphinx-doc.org/en/master/

.. _MinecraftBridge: https://gitlab.com/cmu_asist/MinecraftBridge