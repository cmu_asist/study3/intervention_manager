import setuptools
from setup_utils import filter_deps

with open('README.md', 'r') as readme_file:
    long_description = readme_file.read()

extras = {
    'asist_nlp': filter_deps([
        'asist_nlp @ git+https://gitlab.com/cmu_asist/nlp@v0.2.0',
    ]),
    'BaseAgent': filter_deps([
        'BaseAgent @ git+https://gitlab.com/cmu_asist/BaseAgent@develop',
        'MinecraftBridge @ git+https://gitlab.com/cmu_asist/MinecraftBridge@v1.3.3',
        'MinecraftElements @ git+https://gitlab.com/cmu_asist/MinecraftElements@v0.4.5',
        'RedisBridge @ git+https://gitlab.com/cmu_asist/RedisBridge@v1.1.3',
    ]),
    'MinecraftBridge': filter_deps([
        'MinecraftBridge @ git+https://gitlab.com/cmu_asist/MinecraftBridge@v1.3.3',
        'MinecraftElements @ git+https://gitlab.com/cmu_asist/MinecraftElements@v0.4.5',
    ]),
    'MinecraftElements': filter_deps([
        'MinecraftElements @ git+https://gitlab.com/cmu_asist/MinecraftElements@v0.4.5',
    ]),
    'tom': filter_deps([
        'tom @ git+https://gitlab.com/cmu_asist/study3/tom@v3.4.2',
    ]),
}

setuptools.setup(
	name="InterventionManager",
	version="0.3.7",
	author="CMU-TA1",
	author_email="danahugh@andrew.cmu.edu",
	description="Module for managing and generating interventions in the DARPA ASIST USAR Scenario",
	long_description=long_description,
	long_description_content_type="text/markdown",
	url="https://gitlab.com/cmu_asist/study3/intervention_manager",
	packages=setuptools.find_packages(include=["InterventionManager"]),
	classifiers=[
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: MIT License",
		"operating System :: OS Independent",
	],
    python_requires='>=3.6',
    install_requires=[
        'numpy',
    ],
    extras_require={
        **extras,
        'all': list(set(sum(extras.values(), [])))
    }
)
