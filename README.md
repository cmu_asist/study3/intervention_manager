# Intervention Manager

Repository containing classes for managing the lifecycle of interventions, include definition of a Base Intervention class, trigger management, and presentation management.


## Installation

    git clone https://gitlab.com/cmu_asist/study3/intervention_manager
    cd intervention_manager

To do a basic install:

    pip install --user -e .

To install with all extras:

    pip install --user -e .[all]


## Dependencies

The intervention manager is dependent on the following repositories:
- [`MinecraftElements`](https://gitlab.com/cmu_asist/MinecraftElements) (version >= 0.4.5)
- [`MinecraftBridge`](https://gitlab.com/cmu_asist/MinecraftBridge) (version >= 1.3.3)

The test module may also use any of the following:
- [`BaseAgent`](https://gitlab.com/cmu_asist/BaseAgent) (develop branch)
- [`tom`](https://gitlab.com/cmu_asist/study3/tom) (version >= 3.4.2)
- [`asist_nlp`](https://gitlab.com/cmu_asist/NLP) (version >= 0.2.0)

These can all be pip-installed as extras. From the root intervention_manager folder, run:

    pip install --user -e .[<package1>,<package2>,...]

or:

    pip install --user -e all

If you already have a package installed locally and want to avoid overriding it with remote versions, simply omit it from the extras on installation.


## Testing Interventions

There are testing resources in the `test` folder.  To run a test agent for an intervention module from the InterventionManager library (e.g. "time_elapsed"), issue the following command from the `test` folder:

    python test.py <intervention>

To run with a specific metadata file:

    python test.py <intervention> -i <input_path>

For more details, [see the `test` module](./test/).


## Creating Interventions

Creating new interventions involves the following steps:

1.  Defining the `InterventionTriggger` subclass for the specific intervention
2.  Defining the `Intervention` subclass for the specific intervention
3.  Optionally defining a  `Followup` subclass for the specific intervention

### Creating an InterventionTrigger

An `InterventionTrigger` class is a class the `InterventionManager` instance uses to initiate new interventions.  The trigger needs to subclass `InterventionTrigger`.  The `__init__` class must accept an InterventionManager instances as a single argument (`manager`), and an arbitrary number of keyword arguments.  The trigger should call the parent class (`InterventionTrigger`), and add any Minecraft or Redis callback methods using the `self.add_minecraft_callback()` or `self.add_redis_callback()` methods.  A minimal working example is given here.

    class MyInterventionTrigger(InterventionTrigger):

        def __init__(self, manager, **kwargs):

            InterventionTrigger.__init__(self, manager, **kwargs)
            self.add_minecraft_callback(FoVSummary, self.__onFoVMessage)
            self.add_redis_callback(self.__onNLPMessage, 'nlp')

It is assumed that the actual intervention will be started in one of the callback methods.  Interventions can be passed to the manager through the `spawn` method:

    def __onSomeCallback(self, message):
        ...
     
        intervention = MyIntervention(self.manager, ...)
        self.manager.spawn(intervention)

Once spawned, the intervention maintains its own lifecycle by calling a set of pre-defined methods on itself.


### Creating an Intervention

An `Intervention` class is the class that manages the state and history of an intervention, and should decide if the intervention should be _resolved_ or _discarded_.  Similar to the `InterventionTrigger`, the intervention manager needs to call the parent `__init__` method, and add callbacks from the message bus using the `add_minecraft_callback` or `add_redis_callback` methods.  A minimal working example is given here:

    class MyIntervention(Intervention):

        def __init__(self, manager, ...., **kwargs):

            Intervention.__init__(self, manager, **kwargs)
            self.register_callback(FoVSummary, self.__onFoVMessage)
              

During handling of message callbacks, the intervention can _discard_ itself by calling `self.disard()`, or can queue itself for resolution by calling `self.queueForResolution()`.  In both cases, the Intervention is disconnected from the message bus and considered _done_.  Finally, `Intervention` classes need to implement a `self.getInterventionInformation()` method.  A minimal working example is shown here:

    def getInterventionInformation(self):
        return {
            'default_recipients': [self.participant_id],
            'default_message': "Some Message to Display"
        }


### Creating a Followup

A `Followup` class is a class for logic about interventions that have been already been resolved. A followup often monitors to determine whether the intervention was complied with, and can potentially spawn new interventions. The `__init__` class must accept an Intervention instance and an InterventionManager instance as positional arguments. Similar to the `InterventionTrigger`, the followup should add callbacks from the message bus using the `add_minecraft_callback` or `add_redis_callback` methods.  A minimal working example is given here:

    class MyFollowup(Followup):

        def __init__(self, intervention, manager):

            Followup.__init__(self, intervention, manager)
            self.add_minecraft_callback(FoVSummary, self.__onFoVMessage)

During handling of message callbacks, the followup can _complete_ itself by calling `self.complete()`, on which the followup is disconnected from the message bus and considered _done_.

    def __onSomeCallback(self, message):
        ...

        new_intervention = MyIntervention(self.manager, ...)
        self.manager.spawn(new_intervention)
        self.complete()
